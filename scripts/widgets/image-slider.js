define([
    'modules/jquery-mozu',
    'underscore',
    'modules/api',
    'modules/backbone-mozu',
    'slick'       
], function ($, _, api, Backbone) {

	var HomeSliderView = Backbone.View.extend({
        initialize: function() {
            this.render();  
        },
        sliderFunction: function() {
            var divLength = $('.slider-first-item').size();  
            var sildeLoop = null;
            if(divLength > 1) {
                sildeLoop =  true;
            }
            else {
                sildeLoop = false;
            }
            $(".regular").removeClass('hidden');
            $(".regular").slick({
                dots: true,
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1
            });
            $(".productTile-title-link").addClass('add-ellipsis');
        },  
        render: function() {
            this.sliderFunction();
        } 
    });
        
        var homeSliderView = new HomeSliderView();
        window.homeSliderView = homeSliderView;
    
    
    
    
});
