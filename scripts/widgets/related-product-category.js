define(['modules/jquery-mozu',
	"modules/backbone-mozu",
	'underscore',
	"modules/api",
	'hyprlive'], function($, Backbone, _, api, Hypr) {
	var RelatedProductCategoryView= Backbone.MozuView.extend({
        templateName: 'modules/articles/related-products-category'
	});
	
	$(document).ready(function() {
		
		var productStr = $('#related-product-category').data('product-codes');
		var productArray = productStr.split(',');
		
		if(productArray.length > 0) {
			var filterString = "ProductCode eq ",
				lastIndex = productArray.length;
			_.map(productArray, function(item, i) {
				item = item.replace('-','');
				item = item.replace(' ','');
				if (item) {
					if(i === 0 ) {
						filterString = filterString + item;
					} else {
						filterString = filterString + ' or ProductCode eq ' + item;
					}
				}
				
			});
			api.get('products', {filter:filterString} ).then(function(response) {
				var newProductArray = [];
				_.each(productArray, function(productCode){
					var item = _.findWhere(response.data.items, {productCode: productCode.trim()});
				    if(item){
				    	newProductArray.push(item);
				    }
				}); 
				response.data.items = newProductArray;
				var commonCategoryCode = response.data.items[0].categories[0].categoryCode;
				var commonCategoryName = response.data.items[0].categories[0].content.name;
				var productmodel = new Backbone.Model(response.data);
				productmodel.set('commonCategoryCode', commonCategoryCode);
				productmodel.set('commonCategoryName', commonCategoryName);
				var relatedProductCategoryView = new RelatedProductCategoryView({
			       el: $('#related-product-category'),
			       model: productmodel
				});
				relatedProductCategoryView.render();
			});
		}
		
	});
});