define(['modules/jquery-mozu',
	"modules/backbone-mozu",
	'underscore',
	"modules/api"], function($, Backbone, _, api) {
	var PopularCategoriesView = Backbone.MozuView.extend({
        templateName: 'widgets/popular-categories/popular-categories-view'
	});
	
	$(document).ready(function() {	
		if($('#popular-categories-view').data('categorylist')){
			var categoryList = $('#popular-categories-view').data('categorylist').popularCategories;
			var filterString = "categoryId eq ",
			lastIndex = 5;
			if(categoryList.length >= 6){			
				_.map(categoryList, function(item, i) {
					filterString = filterString + item;
					if(i != lastIndex){
						filterString = filterString + ' or categoryId eq ';
					}
				});
			}
			var language = require.mozuData('apicontext').headers["x-vol-locale"];
			if(language === 'en-US') {
				language = 'English';
			} else if(language === 'fr-CA'){
				language = 'French';
			}
			
			api.get('categories', {filter: filterString} ).then(function(response) {
				var popularCategoriesView = new PopularCategoriesView({
			       el: $('#popular-categories-view'),
			       model:  new Backbone.Model(response.data)
				});
				popularCategoriesView.render();
			});
		}
	});
});