define(['modules/jquery-mozu',
	"modules/backbone-mozu",
	"modules/api",
	'hyprlive'], function($, Backbone, api, Hypr) {
	
	var RelatedArticlesView = Backbone.MozuView.extend({
        templateName: 'modules/articles/related-articles'
	});
	
	$(document).ready(function() {
		var articleProductTypeId = Hypr.getThemeSetting('articleProductTypeId');
		var searchArticleQueryString = 'productTypeId eq ' + articleProductTypeId + ' and CategoryId eq ' + require.mozuData('pagecontext').categoryId;
		
		api.get('products', {filter:searchArticleQueryString, pageSize: 3} ).then(function(response) {
			var articleModel = new Backbone.Model(response.data);
			var relatedArticlesView = new RelatedArticlesView({
		       el: $('#related-articles'),
		       model:  articleModel
			});
			relatedArticlesView.render();
			
			if($(document).find('#mz-drop-zone-related-article-widget ').html() && $(document).find('#mz-drop-zone-related-article-widget').html().length > 150) {
				$(document).find('#related-articles').closest('.related-articles').addClass('hidden');
			} else {
				$(document).find('#mz-drop-zone-related-article-widget').closest('.related-articles').addClass('hidden');
			}
		});
	});
	
});