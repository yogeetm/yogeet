require([
    'modules/jquery-mozu', 
    'hyprlive', 
    'underscore', 
    'modules/api', 
    'modules/backbone-mozu'
], function ($, Hypr, _, api, Backbone) {
    $(document).ready(function() {
        $('.dropdown-open').on('click', function(e) {
            e.preventDefault();
            $('.primary-menu-nav').toggle();
            $(this).attr('aria-expanded', function (i, attr) {
                return attr == 'true' ? 'false' : 'true';
            });
            closeSearchBox();
        });
        var closeSearchBox = function() {
        	$('.mz-searchbox-input, .glyphicon-search, .search-close').css({'display': 'none'}); 
        	$('#searchbox').removeClass('active');
        	$('#search_submit').css({'display': 'block'});
    	};
        var lockMobileBodyOnActiveMenu = function(){
        	if(!$('body').hasClass('is-locked')){
        		$('body').addClass('is-locked');
        	}else {
        		$('body').removeClass('is-locked');
        	}
        	if(!$('html').hasClass('is-locked')){
        		$('html').addClass('is-locked');
        	}else {
        		$('html').removeClass('is-locked');
        	}
        };
        $('.mobilemenutoggle').on('click', function() {
        	lockMobileBodyOnActiveMenu();
            $(this).addClass('mobilemenuclose');
            $(this).removeClass('mobilemenutoggle');
            $('.mz-pageheader').removeClass('is-search-open');
            $('.mystore-details').removeClass('in');
            $('.mz-sitenav').removeClass('fixed-on-search').addClass('fixed-on-mobile-search'); 
            closeSearchBox();
        }); 
        
        $('.mobilemenuclose').on('click', function() {
        	//lockMobileBodyOnActiveMenu();
            $(this).addClass('mobilemenutoggle');
            $(this).removeClass('mobilemenuclose');
            $('.mz-pageheader').removeClass('is-search-open');
            $('.mystore-details').removeClass('in');
            $('.mz-sitenav').removeClass('fixed-on-mobile-search');
            closeSearchBox();
        });

    	$('ul.menu-container .mz-sitenav-item:first-child .mz-sitenav-sublink').addClass("selected"); //show first category selected on home page and non-category pages 
    	$('.primary-menu-pannel .mz-sitenav-sub-sub:first-child').addClass("active");
        $('.mz-sitenav-sublink').on('click', function(e) {
            e.preventDefault();
            $('.mz-sitenav-sublink').removeClass("selected");
            $('.primary-menu-pannel').css({'display': 'block'}); 
            $('.primary-menu').addClass('is-primary-selected');
            $('.primary-menu-nav').addClass('top-0');
            $(this).addClass("selected");
            var category = $(this).attr('name');
            $("div.mz-sitenav-sub-sub").removeClass("active"); 
            $("[name='" + category + "']").addClass('active');
            $("div.mz-sitenav-sub-sub").removeClass("slide-submenu"); //for mobile view
            $("[name='" + category + "']").addClass( "slide-submenu" ); //for mobile view
            $(".navbar-collapse").addClass("is-category-selected");
            $(".feturedCategoriesOfMobile").css({'display': 'none'});
        });
        $('.back-link').on('click', function(e) { //for mobile view
            e.preventDefault();
            $('.primary-menu').removeClass('is-primary-selected');
            $(".mz-sitenav-sub-sub").removeClass( "slide-submenu" );
            $(".navbar-collapse").removeClass("is-category-selected");
            $('.primary-menu-nav').removeClass('top-0');
            $(".feturedCategoriesOfMobile").css({'display': 'block'});
        });
        
        $("#search-button, #search-icon").click(function(e){
            e.preventDefault();
            $("#search-button, #search-form").show();
        });
    });
});