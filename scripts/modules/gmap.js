// convert Google Maps into an AMD module
define(['shim!https://maps.googleapis.com/maps/api/js?v=3'],
function(){
    // return the gmaps
    console.log("Initializing map" + window.google.maps);
    return window.google.maps;
});