define(['modules/jquery-mozu',
	"modules/backbone-mozu",
	'underscore',
	"hyprlivecontext",
	"modules/api",
	'hyprlive'], function($, Backbone, _, HyprLiveContext, api, Hypr) {
	var locale = require.mozuData('apicontext').headers['x-vol-locale'];
	var HearsHowHubView = Backbone.MozuView.extend({
        templateName: 'modules/articles/hhh',
        events: {
            "change #pagemenu": "gotoArticleCategory"
        },
        gotoArticleCategory: function(e){
        	console.log($("#pagemenu option:selected")[0].value);
        	window.location.href = $("#pagemenu option:selected")[0].value;
        	
        }
	});

	
	$(document).ready(function() {    	
		var articleProductTypeId = Hypr.getThemeSetting('articleProductTypeId');
		var tempSearchArticleQueryString = 'productTypeId eq ' + articleProductTypeId;
		api.get('search', {query:'', filter:tempSearchArticleQueryString, facet: 'categoryId', pageSize: 0, startIndex: 0} ).then(function(response) {			
			$('.content-loading').hide();
			var parentCategories = response.data.facets[0].values[0].childrenFacetValues;			
			var hearsHowHubModel = {};
			hearsHowHubModel.parentCategories = parentCategories;
			hearsHowHubModel.locale = locale;
			var hearsHowHubView = new HearsHowHubView({
		       el: $('#hears-how'),
		       model: new Backbone.Model(hearsHowHubModel)
			});
			hearsHowHubView.render(); 
			
		});
	});
});
