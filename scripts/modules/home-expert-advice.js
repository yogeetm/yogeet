define(['modules/jquery-mozu',
	"modules/backbone-mozu",
	"modules/api",
	'hyprlive',
	'modules/models-product'], function($, Backbone, api, Hypr, ProductModels) {
	
	var HomeExpertAdviceView = Backbone.MozuView.extend({
        templateName: 'modules/homepage/home-expert-advice-live'
	});
	
	$(document).ready(function() {
		var articleProductTypeId = Hypr.getThemeSetting('articleProductTypeId');
		var searchExpertAdviceQueryString = 'productTypeId eq ' + articleProductTypeId;
		api.get('products', {filter:searchExpertAdviceQueryString, pageSize: 3, sortBy: 'createDate desc'} ).then(function(response) {
			var homeExpertAdviceView = new HomeExpertAdviceView({
		       el: $('#home-expert-advice'),
		       model:  new Backbone.Model(response.data)
			});
			homeExpertAdviceView.render();
			
			if($(document).find('#mz-drop-zone-expert-advice-widget').html() && $(document).find('#mz-drop-zone-expert-advice-widget').html().length > 150) {
				$(document).find('#home-expert-advice').closest('.home-expert-advice-sect').addClass('hidden');
			} else {
				$(document).find('#mz-drop-zone-expert-advice-widget').closest('.home-expert-advice-sect').addClass('hidden');
			}
		});
	});
	
});