﻿define(['shim!vendor/typeahead.js/typeahead.bundle[modules/jquery-mozu=jQuery]>jQuery', 'hyprlive', 'underscore', 'modules/api',
      'hyprlivecontext'], function($, Hypr, _, api,
        HyprLiveContext) {
	var searchQuery = HyprLiveContext.locals.pageContext.search.query;
    // bundled typeahead saves a lot of space but exports bloodhound to the root object, let's lose it
    var Bloodhound = window.Bloodhound.noConflict();

    // bloodhound wants to make its own AJAX requests, and since it's got such good caching and tokenizing algorithms, i'm happy to help it
    // so instead of using the SDK to place the request, we just use it to get the URL configs and the required API headers
    var qs = '%QUERY',
        eqs = encodeURIComponent(qs),
        suggestPriorSearchTerms = Hypr.getThemeSetting('suggestPriorSearchTerms'),
        getApiUrl = function(groups) {
            return api.getActionConfig('suggest', 'get', { query: qs, groups: groups }).url;
        },
        termsUrl = getApiUrl('terms'),
        productsUrl = getApiUrl('pages'),
        ajaxConfig = {
            headers: api.getRequestHeaders()
        },
        i,
        nonWordRe = /\W+/,
        makeSuggestionGroupFilter = function(name) {
            return function(res) {
                var suggestionGroups = res.suggestionGroups,
                    thisGroup;
                for (i = suggestionGroups.length - 1; i >= 0; i--) {
                    if (suggestionGroups[i].name === name) {
                        thisGroup = suggestionGroups[i];
                        break;
                    }
                }
                if(name == 'Pages' && thisGroup.suggestions.length === 0) {
                    $('.tt-dataset-cat').remove();
                }
                if(name == 'Terms') {
                	if(suggestionGroups[0].suggestions.length > 0) {
                		window.autocomplete = suggestionGroups[0].suggestions[0].suggestion.term;
                	} else {
                    	window.autocomplete = $('.tt-input').val();
                    }                	
                }
                return thisGroup.suggestions;
            };
        },

        makeTemplateFn = function(name) {
            var tpt = Hypr.getTemplate(name);
            return function(obj) {
                return tpt.render(obj);
            };
        },

    // create bloodhound instances for each type of suggestion

    AutocompleteManager = {
        datasets: {
            pages: new Bloodhound({
                datumTokenizer: function(datum) {
                    return datum.suggestion.term.split(nonWordRe);
                },
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote: {
                    url: productsUrl,
                    wildcard: eqs,
                    filter: makeSuggestionGroupFilter("Pages"),
                    rateLimitWait: 400,
                    ajax: ajaxConfig
                }
            })
        }
    };

    $.each(AutocompleteManager.datasets, function(name, set) {
        set.initialize();
    });
    
    var catarray = [];
    var count = 0;
    var deferCount = 0;
    var showProducts = function(categories) {
    	_.each(categories, function(category) {
            catarray.push(category);
        });
    	_.defer(function() {
    		deferCount++;
    		if(deferCount == count) {
    			deferCount = 0;
    			count = 0;
    			$('.tt-dataset-cat').remove();
    			var categoryArray = [];
    		    for(var i = 0;i < catarray.length; i++){
    		    	var isMatch = _.findWhere(categoryArray, {categoryId: catarray[i].categoryId});
    		        if(!isMatch){
    		        	categoryArray.push(catarray[i]);
    		        }
    		    }
    		    var counter = 0;
    		    var element = '<div class="tt-dataset-cat"><p>' + window.autocomplete + '</p><ul>';
    		    _.each(categoryArray, function(category) {    		    	
    		    	if(counter <= 3){
    		    		element += '<li class="search-category" data-category-id=' + category.categoryId + '><a href="/c/' + category.categoryId + '">'+ Hypr.getLabel('inText') + category.content.name + '</a></li>';
    		    	}
    		    });
    		    
    		    element += '</ul></div>';
                $('.tt-dataset-pages').before(element);
                catarray = [];
    		    
    		}
            
         });
    };
    
    var dataSetConfigs = [
        {
            name: 'pages',
            displayKey: function(datum) {
            	count ++;
            	showProducts(datum.suggestion.categories);
            },
            templates: {
                suggestion: makeTemplateFn('modules/search/autocomplete-page-result'),
                empty: ''
            },
            source: AutocompleteManager.datasets.pages.ttAdapter()
        }
    ];

    if (suggestPriorSearchTerms) {
        AutocompleteManager.datasets.terms = new Bloodhound({
            datumTokenizer: function(datum) {
                return datum.suggestion.term.split(nonWordRe);
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: termsUrl,
                wildcard: eqs,
                filter: makeSuggestionGroupFilter("Terms"),
                rateLimitWait: 100,
                ajax: ajaxConfig
            }
        });
        AutocompleteManager.datasets.terms.initialize();
        dataSetConfigs.push({
            name: 'terms',
            displayKey: function(datum) {
                return datum.suggestion.term;
            },
            source: AutocompleteManager.datasets.terms.ttAdapter()
        });
    }

    $(document).ready(function() {
        var $field = AutocompleteManager.$typeaheadField = $('[data-mz-role="searchquery"]');
        AutocompleteManager.typeaheadInstance = $field.typeahead({
            minLength: 3
        }, dataSetConfigs).data('ttTypeahead');
        // user hits enter key while menu item is selected;
        $field.on('typeahead:selected', function (e, data, set) {
        	if (data.suggestion.productCode) {
        		window.location.href = '/p/' + data.suggestion.productCode;
        	}
        });
        
        $('.tt-input').on('blur', function(e){
        	e.stopPropagation();
        	if(e.relatedTarget === null || e.relatedTarget.className != 'search-close') {
        		if($('.tt-input').val().length === 0){
        			$('.tt-input').val(searchQuery);
        		}        		
        	}
        });
        
        
    });

    return AutocompleteManager;
});