/**
 * Adds a login popover to all login links on a page.
 */
define([
        'shim!vendor/bootstrap/js/popover[shim!vendor/bootstrap/js/tooltip[modules/jquery-mozu=jQuery]>jQuery=jQuery]>jQuery', 
        'modules/api', 
        'hyprlive', 
        "modules/backbone-mozu",
        "modules/views-messages",
        'underscore', 
        'hyprlivecontext',
        "widgets/bazaarvoice",
        'modules/models-product',
        "pages/preferred-store",
        'vendor/jquery-placeholder/jquery.placeholder',
        'shim!bootstrap[modules/jquery-mozu=jQuery]>jQuery=jQuery>jQuery'
    ],
function ($, api, Hypr, Backbone, messageViewFactory, _, HyprLiveContext, ProductModels) {
	$.urlParam = function(name){
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results===null){
           return null;
        }
        else{
           return decodeURI(results[1]) || 0;
        }
    };
    
    var user = require.mozuData('user'),
        apiContext = require.mozuData('apicontext'),
        contextSiteId = apiContext.headers["x-vol-site"],
        homeFurnitureSiteId = Hypr.getThemeSetting("homeFurnitureSiteId"),
        isHomeFurnitureSite = false;

	var relation1InsertRecord, relation1Link, goto2020Link, b_token;
	var locale = require.mozuData('apicontext').headers['x-vol-locale'], siteId = apiContext.headers["x-vol-site"];
	locale = locale.split('-')[0];
	if(locale === "fr"){
		locale = "French";
	} else {
		locale = "English";
	}  
    
    if (!require.mozuData('pagecontext').isEditMode) { 
        //set preferred language
        var expiryDate = new Date();
        expiryDate.setYear(expiryDate.getFullYear() + 1);
        var tenantId = apiContext.headers["x-vol-tenant"],
        locationPath = window.location.pathname, landingSiteId = '', search = '';
        if($.cookie("hhLanguage")) {
            if(window.location.search) {
                search = window.location.search.indexOf('returnurl') === -1 && window.location.search.indexOf('returnUrl') === -1 ? window.location.search : '';
            }
            if(siteId === Hypr.getThemeSetting('enSiteId') && $.cookie("hhLanguage") === 'fr') { //current site is english
                landingSiteId = Hypr.getThemeSetting('frSiteId');
                if(locationPath.indexOf('/en') !== -1){
                    locationPath = locationPath.replace('/en', '/fr');
                }else{
                    locationPath = '/fr' + locationPath;
                }
                if(Hypr.getThemeSetting('selectedEnvironment') === 'DEV') { 
                    window.location.href = Hypr.getThemeSetting('hhWebsiteLink') + locationPath + search;
                }else if(Hypr.getThemeSetting('selectedEnvironment') === 'PROD') {
                    window.location.href = Hypr.getThemeSetting('hhWebsiteLink') + locationPath + search;
                }
            }else if(siteId === Hypr.getThemeSetting('frSiteId') && $.cookie("hhLanguage") === 'en'){
                landingSiteId = Hypr.getThemeSetting('enSiteId');
                if(locationPath.indexOf('/fr') !== -1){
                    locationPath = locationPath.replace('/fr', '/en');
                }else{
                    locationPath = '/en' + locationPath;
                }
                if(Hypr.getThemeSetting('selectedEnvironment') === 'DEV') { 
                    window.location.href = Hypr.getThemeSetting('hhWebsiteLink') + locationPath + search;
                }else if(Hypr.getThemeSetting('selectedEnvironment') === 'PROD') {
                    window.location.href = Hypr.getThemeSetting('hhWebsiteLink') + locationPath + search;
                }
            }
        }else if(siteId !== Hypr.getThemeSetting('homeFurnitureSiteId')){
            if(locale === "French"){
                $.cookie("hhLanguage", 'fr', {path:'/', expires: expiryDate});
            }else {
                $.cookie("hhLanguage", 'en', {path:'/', expires: expiryDate});
            }
        }
    }

	if(Hypr.getThemeSetting('selectedEnvironment') === 'PROD') {
		relation1InsertRecord = Hypr.getThemeSetting('relation1InsertRecordPROD');
		b_token =  Hypr.getThemeSetting('bTokenRelation1Prod');
		
		if(locale === "English") {
			goto2020Link = Hypr.getThemeSetting('enGoto2020LinkPROD');
			relation1Link = Hypr.getThemeSetting('relation1LinkPRODEN');
		} else if (locale === "French"){
			goto2020Link = Hypr.getThemeSetting('frGoto2020LinkPROD');
			relation1Link = Hypr.getThemeSetting('relation1LinkPRODFR');
		}		
	} else {
		relation1InsertRecord = Hypr.getThemeSetting('relation1InsertRecordDEV');
		relation1Link = Hypr.getThemeSetting('relation1LinkDEV');
		b_token =  Hypr.getThemeSetting('bTokenRelation1Dev');
		if(locale === "French") {
			goto2020Link = Hypr.getThemeSetting('frGoto2020LinkDEVLogin');
		} else {
			goto2020Link = Hypr.getThemeSetting('enGoto2020LinkDEVLogin');
		}
	}
	
    
    
    /* check current site is home furniture site or not */          
        if(homeFurnitureSiteId === contextSiteId){
            isHomeFurnitureSite = true;
        }

    if (!(user.isAnonymous)) {
        $('.mz-user-firstname').html(' ');
        var cust = api.get('customer', {
            id: user.accountId
        }).then(function(response) {
            $('.mz-user-firstname').html(response.data.firstName);
        });
    }
        
    var usePopovers = function() {
        return !Modernizr.mq('(max-width: 480px)');
    },
    isTemplate = function(path) {
        return require.mozuData('pagecontext').cmsContext.template.path === path;
    },
    returnFalse = function () {
        return false;
    },
    returnUrl = function() {
        var returnURL = $('input[name=returnUrl]').val();
        if(!returnURL) {
            returnURL = '/';
        }
        return returnURL;
    },
    $docBody,

    polyfillPlaceholders = !('placeholder' in $('<input>')[0]);

    var DismissablePopover = function () { };

    $.extend(DismissablePopover.prototype, {
        boundMethods: [],
        setMethodContext: function () {
            for (var i = this.boundMethods.length - 1; i >= 0; i--) {
                this[this.boundMethods[i]] = $.proxy(this[this.boundMethods[i]], this);
            }
        },
        dismisser: function (e) {
            if (!$.contains(this.popoverInstance.$tip[0], e.target) && !this.loading) {
                // clicking away from a popped popover should dismiss it
                this.$el.popover('destroy');
                this.$el.on('click', this.createPopover);
                this.$el.off('click', returnFalse);
                this.bindListeners(false);
                $docBody.off('click', this.dismisser);
            }
        },
        setLoading: function (yes) {
            this.loading = yes;
            this.$parent[yes ? 'addClass' : 'removeClass']('is-loading');
        },
        onPopoverShow: function () {
            var self = this;
            _.defer(function () {
                $docBody.on('click', self.dismisser);
                self.$el.on('click', returnFalse);
            });
            this.popoverInstance = this.$el.data('bs.popover');
            this.$parent = this.popoverInstance.tip();
            this.bindListeners(true);
            this.$el.off('click', this.createPopover);
            if (polyfillPlaceholders) {
                this.$parent.find('[placeholder]').placeholder({ customClass: 'mz-placeholder' });
            }
        },
        createPopover: function (e) {
            // in the absence of JS or in a small viewport, these links go to the login page.
            // Prevent them from going there!
            var self = this;
            if (usePopovers()) {
                e.preventDefault();
                // If the parent element's not positioned at least relative,
                // the popover won't move with a window resize
                //var pos = $parent.css('position');
                //if (!pos || pos === "static") $parent.css('position', 'relative');
                this.$el.popover({
                    //placement: "auto right",
                    animation: true,
                    html: true,
                    trigger: 'manual',
                    content: this.template,
                    container: 'body'
                }).on('shown.bs.popover', this.onPopoverShow)
                .popover('show');

            }
        },
        retrieveErrorLabel: function (xhr) {
            var message = "";
            if (xhr.message) {
                message = Hypr.getLabel(xhr.message);
            } else if ((xhr && xhr.responseJSON && xhr.responseJSON.message)) {
                message = Hypr.getLabel(xhr.responseJSON.message);
            }

            if (!message || message.length === 0) {
                this.displayApiMessage(xhr);
            } else {
                var msgCont = {};
                msgCont.message = message;
                this.displayApiMessage(msgCont);
            }
        },
        displayApiMessage: function (xhr) {
            this.displayMessage(xhr.message ||
                (xhr && xhr.responseJSON && xhr.responseJSON.message) ||
                Hypr.getLabel('unexpectedError'));
        },
        displayApiCustomMessage: function(xhr, isSignupForm) {
            this.displayMessage(xhr.message ||
                (xhr && xhr.responseJSON && xhr.responseJSON.message) ||
                Hypr.getLabel('unexpectedError'), isSignupForm);
        },
        displayMessage: function (msg) {
            this.setLoading(false);

            var MessageModel = Backbone.MozuModel.extend({});
                var MessageCollection = new Backbone.Collection();

                var messageView = messageViewFactory({
                    el: $('[data-ign-message-bar]'),
                    model: MessageCollection
                });

                var messageModel = null, message = null;

                if (msg === "Missing or invalid parameter: resetPasswordInfo UserName or EmailAddress must be provided" || msg.search("Item not found:") === 0 || msg === "You should receive an email with instructions to reset your password shortly.") {
                    if (msg === "You should receive an email with instructions to reset your password shortly.") {
                        message = Hypr.getLabel('successEmail');
                    } else if (msg === "Missing or invalid parameter: resetPasswordInfo UserName or EmailAddress must be provided") {
                        message = Hypr.getLabel('requireUsername');
                    } else if ($('[data-ign-message-bar]:contains("Item not found:")')) {
                        message = Hypr.getLabel('itemNotFound');
                    } else {
                        message = msg;
                    }
                } 
                else if (msg === "Login failed. Please specify a user.") {
                    message = msg;
                }
                else {
                    if (msg === "Missing or invalid parameter: username ") {
                        message = Hypr.getLabel('requireEmail');
                    } else if (msg === "Missing or invalid parameter: emailAddress Email address is not valid") {
                        message = Hypr.getLabel('invalidEmail');
                    } else if (msg === "One or more errors occurred.") {
                        message = Hypr.getLabel('invalidEmail');
                    } else if (msg === "Missing or invalid parameter: password Password must be a minimum of 6 characters with at least 1 number and 1 alphabetic character") { 
                        message = Hypr.getLabel('invalidPassword');
                    } else if (msg === "Missing or invalid parameter: password Password cannot be empty") {
                        message = Hypr.getLabel('emptyPassword');
                    } else if (msg === "Missing or invalid parameter: EmailAddress EmailAddress already associated with a login") {
                        message = Hypr.getLabel('associateEmail');
                    } else if (msg === "Validation Error: update exception") {
                        message = Hypr.getLabel('associateEmail');
                    } else if (msg === "One or more errors occurred.") {
                        message = Hypr.getLabel('invalidEmail');
                    } else if ("[data-ign-message-bar] li:contains('Login as')") {
                    	 message = Hypr.getLabel('loginFailed');
                    } else {
                        message = msg;
                        window.validated = true;
                    }
                }

                messageModel = new MessageModel();
                messageModel.set({message: message});
                MessageCollection.add(messageModel);
                messageView.render();

           // this.$parent.find('[data-mz-role="popover-message"]').html('<span class="mz-validationmessage">' + msg + '</span>');
        },
        displayCustomMessage: function(msg, isSignupForm, validationFor) {
                this.setLoading(false);
                if (isSignupForm) {
                    this.$parent.find('[data-mz-validationmessage-for="' + validationFor + '"]').html(msg);
                    this.$parent.find('[data-mz-signup-' + validationFor.toLowerCase() + ']').addClass('is-invalid');
                    this.$parent.find('[data-mz-role="popover-message"]').html('<span class="mz-validationmessage"></span>');
                } else {
                    this.$parent.find('[data-mz-validationmessage-for="' + validationFor + '"]').html(msg);
                    this.$parent.find('[data-mz-login-' + validationFor.toLowerCase() + ']').addClass('is-invalid');
                    this.$parent.find('[data-mz-role="popover-message"]').html('<span class="mz-validationmessage"></span>');
                }
            },
        init: function (el) {
            this.$el = $(el);
            this.loading = false;
            this.setMethodContext();
            if (!this.pageType){
                this.$el.on('click', this.createPopover);
            }
            else {
               this.$el.on('click', _.bind(this.doFormSubmit, this));
            }
        },
        doFormSubmit: function(e){
            e.preventDefault();
            this.$parent = this.$el.closest(this.formSelector);
            this[this.pageType]();
        }
    });

    var LoginPopover = function() {
        DismissablePopover.apply(this, arguments);
        this.login = _.debounce(this.login, 150);
        this.retrievePassword = _.debounce(this.retrievePassword, 150);
    };
    LoginPopover.prototype = new DismissablePopover();
    $.extend(LoginPopover.prototype, {
        boundMethods: ['handleEnterKey', 'handleLoginComplete', 'displayResetPasswordMessage', 'dismisser', 'displayMessage', 'displayApiMessage', 'createPopover', 'slideRight', 'slideLeft', 'login', 'retrievePassword', 'onPopoverShow'],
        template: Hypr.getTemplate('modules/common/login-popover').render(),
        bindListeners: function (on) {
            var onOrOff = on ? "on" : "off";
            this.$parent[onOrOff]('click', '[data-mz-action="forgotpasswordform"]', this.slideRight);
            this.$parent[onOrOff]('click', '[data-mz-action="loginform"]', this.slideLeft);
            this.$parent[onOrOff]('click', '[data-mz-action="submitlogin"]', this.login);
            this.$parent[onOrOff]('click', '[data-mz-action="submitforgotpassword"]', this.retrievePassword);
            this.$parent[onOrOff]('keypress', 'input', this.handleEnterKey);
        },
        onPopoverShow: function () {
            DismissablePopover.prototype.onPopoverShow.apply(this, arguments);
            this.panelWidth = this.$parent.find('.mz-l-slidebox-panel').first().outerWidth();
            this.$slideboxOuter = this.$parent.find('.mz-l-slidebox-outer');

            if (this.$el.hasClass('mz-forgot')){
                this.slideRight();
            }
        },
        handleEnterKey: function (e) {
            if (e.which === 13) {
                var $parentForm = $(e.currentTarget).parents('[data-mz-role]');
                switch ($parentForm.data('mz-role')) {
                    case "login-form":
                        this.login();
                        break;
                    case "forgotpassword-form":
                        this.retrievePassword();
                        break;
                }
                return false;
            }
        },
        slideRight: function (e) {
            if (e) e.preventDefault();
            this.$slideboxOuter.css('left', -this.panelWidth);
        },
        slideLeft: function (e) {
            if (e) e.preventDefault();
            this.$slideboxOuter.css('left', 0);
        },
        validatelogin: function(emailAddr, pass) {
            var emailReg = Backbone.Validation.patterns.email;
            var validation = false, msg1, msg2; 

            var MessageModel = Backbone.MozuModel.extend({});
            var MessageCollection = new Backbone.Collection();

            var messageView = messageViewFactory({
                el: $('[data-ign-message-bar]'),
                model: MessageCollection
            });

            var messageModel = null;

            if (!emailAddr || !emailReg.test( emailAddr )) {
                validation = false;
                msg1 = this.displayCustomMessage(Hypr.getLabel('emailMissing'), false, "email");
                messageModel = new MessageModel();
                messageModel.set({message: Hypr.getLabel('emailMissing')});
                MessageCollection.add(messageModel);
            } else {
                validation = true;
                this.displayCustomMessage("", false, 'email');
                this.$parent.find('[data-mz-login-email]').removeClass('is-invalid');
            }
            if (!pass) { 
                validation = false;
                msg2 = this.displayCustomMessage(Hypr.getLabel("passwordMissing"), false, "password");
                messageModel = new MessageModel();
                messageModel.set({message: Hypr.getLabel('passwordMissing')});
                MessageCollection.add(messageModel);
            }
            else{
                validation = true;
                this.displayCustomMessage("", false, 'password');
                this.$parent.find('[data-mz-login-password]').removeClass('is-invalid');
            }
            messageView.render();

            return validation ? true : false;
        },
        login: function () {

        	var self = this,
            emailAddr = this.$parent.find('[data-mz-login-email]').val(),
            pass = this.$parent.find('[data-mz-login-password]').val();
            
          //NGCOM-623
            //If a returnUrl has been specified in the url query and there
            //is no returnUrl value provided by the server,
            //we'll use the one specified in the url query. If a returnURl has been
            //provided by the server, it will live in an invisible input in the
            //login links box.
            
			var returnUrl, returnUrlParam = "";
			if (window.location.search.indexOf('returnurl') > 0) {
				returnUrlParam = (window.location.search.split('returnurl=')[1] || '');
            }
			if (returnUrlParam && !this.$parent.find('input[name=returnUrl]').val()){
			  returnUrl = returnUrlParam;
			} else {
			  returnUrl = this.$parent.find('input[name=returnUrl]').val();
			}
            if (this.validatelogin(emailAddr, pass)) {
                this.setLoading(true);
                api.action('customer', 'loginStorefront', {
                    email: this.$parent.find('[data-mz-login-email]').val(),
                    password: this.$parent.find('[data-mz-login-password]').val()
                }).then(this.handleLoginComplete.bind(this, returnUrl,emailAddr,pass), this.displayApiMessage);
            }
        },
        anonymousorder: function() {
            var email = "";
            var billingZipCode = "";
            var billingPhoneNumber = "";

            switch (this.$parent.find('[data-mz-verify-with]').val()) {
                case "zipCode":
                    {
                        billingZipCode = this.$parent.find('[data-mz-verification]').val();
                        email = null;
                        billingPhoneNumber = null;
                        break;
                    }
                case "phoneNumber":
                    {
                        billingZipCode = null;
                        email = null;
                        billingPhoneNumber = this.$parent.find('[data-mz-verification]').val();
                        break;
                    }
                case "email":
                    {
                        billingZipCode = null;
                        email = this.$parent.find('[data-mz-verification]').val();
                        billingPhoneNumber = null;
                        break;
                    }
                default:
                    {
                        billingZipCode = null;
                        email = null;
                        billingPhoneNumber = null;
                        break;
                    }

            }

            this.setLoading(true);
            // the new handle message needs to take the redirect.
            api.action('customer', 'orderStatusLogin', {
                ordernumber: this.$parent.find('[data-mz-order-number]').val(),
                email: email,
                billingZipCode: billingZipCode,
                billingPhoneNumber: billingPhoneNumber
            }).then(function () { window.location.href = (HyprLiveContext.locals.siteContext.siteSubdirectory||'') +  "/my-anonymous-account?returnUrl="+(HyprLiveContext.locals.siteContext.siteSubdirectory||'')+"/myaccount"; }, _.bind(this.retrieveErrorLabel, this));
        },
        retrievePassword: function () {
            this.setLoading(true);
            api.action('customer', 'resetPasswordStorefront', {
                EmailAddress: this.$parent.find('[data-mz-forgotpassword-email]').val()
            }).then(_.bind(this.displayResetPasswordMessage,this), this.displayApiMessage);
        },
        handleLoginComplete: function (returnUrl, email, pass) {
            var currentLocale = require.mozuData('apicontext').headers['x-vol-locale'];
            var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
            currentLocale = '/'+currentLocale.split('-')[0];
            if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite){
                currentLocale = currentLocale === 'fr' ? '/fr' : '/en';
            }
            api.request('POST', '/api/commerce/customer/authtickets/', {
                password: pass,
                username: email
            }).then(function (response) {
                var accountId = response.customerAccount.id;
                var productCode  = $.urlParam('productCode');
                var userStore = isHomeFurnitureSite ? Hypr.getThemeSetting('hfPreferredStore') : Hypr.getThemeSetting('preferredStore');                
                api.request('GET', '/api/commerce/customer/accounts/'+accountId+'/attributes/'+userStore).then(function(data){
                    var customerStore = data.values[0]; 
                    if($.cookie("preferredStore")){
                        var currentStore = $.parseJSON($.cookie("preferredStore"));
                        if(customerStore != 'N/A' && customerStore != currentStore.code){
                            $.ajax({
                                url: "/set-purchase-location", 
                                data :{ "purchaseLocation": customerStore}, 
                                type:"GET",
                                success:function(response){
                                    api.get("location",{code:customerStore}).then(function(response){
                                        var selectedStore = response.data;  
                                        var expiryDate = new Date();
                                        expiryDate.setYear(expiryDate.getFullYear() + 1);
                                        $.cookie("preferredStore",JSON.stringify(selectedStore),{path:'/',expires: expiryDate});
                                        if ( returnUrl ){
                                            window.location.href= returnUrl;
                                        }else if (productCode){                                        	
                                        	window.location.href = currentLocale+"/p/" + productCode + "#addToWishlist";
                                        }else {
                                            window.location.href = "/myaccount";
                                        }
                                    });
                                },
                                error: function (error) {
                                    console.log(error);
                                }
                            });
                        }else{
                            if ( returnUrl ){
                                window.location.href= returnUrl;
                            }else if (productCode){                            	
                            	window.location.href = currentLocale+"/p/" + productCode + "#addToWishlist";                            	
                            }else{
                                window.location.href = "/myaccount";
                            }
                        }
                    }else{
                    	if(customerStore != 'N/A'){
                    		$.ajax({
                                url: "/set-purchase-location", 
                                data :{ "purchaseLocation": customerStore}, 
                                type:"GET",
                                success:function(response){
                                    api.get("location",{code:customerStore}).then(function(response){
                                        var selectedStore = response.data;  
                                        var expiryDate = new Date();
                                        expiryDate.setYear(expiryDate.getFullYear() + 1);
                                        $.cookie("preferredStore",JSON.stringify(selectedStore),{path:'/',expires: expiryDate});
                                        if ( returnUrl ){
                                            window.location.href= returnUrl;
                                        }else if (productCode){                                        	
                                        	window.location.href = currentLocale+"/p/" + productCode + "#addToWishlist";
                                        }else {
                                            window.location.href = "/myaccount";
                                        }
                                    });
                                },
                                error: function (error) {
                                    console.log(error);
                                }
                            });
                    	}else{
                            if ( returnUrl ){
                                window.location.href= returnUrl;
                            }else if (productCode){                            	
                            	window.location.href = currentLocale+"/p/" + productCode + "#addToWishlist";                            	
                            }else{
                                window.location.href = "/myaccount";
                            }
                        }
                    }
                });
           });
            
            //delete instorePickUpStep from cookie
            if($.cookie('inStorePickUpStep')){
            	$.removeCookie('inStorePickUpStep', { path: '/' });
            }
            if($.cookie('recentlyViewd')){
            	$.removeCookie('recentlyViewd', { path: '/' });
            }
        },
        displayResetPasswordMessage: function () {
        	$('.forgot-password-container').hide();
            $('.forgot-success-message').removeClass('hidden').html(Hypr.getLabel('resetEmailSentMessage'));
        }
    });
    
    
    var SignupPopover = function() {
        DismissablePopover.apply(this, arguments);
        this.signup = _.debounce(this.signup, 150);
    };
    SignupPopover.prototype = new DismissablePopover();
    $.extend(SignupPopover.prototype, LoginPopover.prototype, {
        boundMethods: ['handleEnterKey', 'dismisser', 'displayMessage', 'displayApiMessage', 'createPopover', 'signup', 'onPopoverShow'],
        template: Hypr.getTemplate('modules/common/signup-popover').render(),
        bindListeners: function (on) {
            var onOrOff = on ? "on" : "off";
            this.$parent[onOrOff]('click', '[data-mz-action="signup"]', this.signup);
            this.$parent[onOrOff]('keypress', 'input', this.handleEnterKey);
        },
        handleEnterKey: function (e) {
            if (e.which === 13) { this.signup(); }
        },
        
       
        validate: function (payload, firstName, lastName, emailAddr, pass, aeroplanNumber, aeroplanLastName) {
        	
            var emailReg = Backbone.Validation.patterns.email;
            var nameReg = /\s/g;
            
            var validation = false, msg1, msg2, msg3, msg4, msg5, msg6, msg7;
            var isEmailValid = false;
            var isFnameValid = false;
            var isLnameValid = false;
            var isAeroplanNumber = false;
            var isAeroplanLastName = false;
            var aeroplanAvailableClass = $('#signupAeroplanSection').hasClass('aeroplanAvailable');

            var MessageModel = Backbone.MozuModel.extend({});
            var MessageCollection = new Backbone.Collection();

            var messageView = messageViewFactory({
                el: $('[data-mz-message-bar]'),
                model: MessageCollection
            });

            var messageModel = null;

            if (!firstName || nameReg.test(firstName) ) {
                isFnameValid = false;
                msg4 = this.displayCustomMessage(Hypr.getLabel('fnameMissing'), false, "firstName");
                messageModel = new MessageModel();
                messageModel.set({message: Hypr.getLabel('fnameMissing')});
                this.$parent.find('[data-mz-signup-firstname]').addClass('is-invalid');
                MessageCollection.add(messageModel);
            } else {
            	isFnameValid = true;
                this.displayCustomMessage("", false, 'firstName');
                this.$parent.find('[data-mz-signup-firstname]').removeClass('is-invalid');
            }
            if (!lastName || nameReg.test(lastName)) {
                isLnameValid = false;
                msg5 = this.displayCustomMessage(Hypr.getLabel('lnameMissing'), false, "lastName");
                messageModel = new MessageModel();
                messageModel.set({message: Hypr.getLabel('lnameMissing')});
                this.$parent.find('[data-mz-signup-lastname]').addClass('is-invalid');
                MessageCollection.add(messageModel);
            } else {
            	isLnameValid = true;
                this.displayCustomMessage("", false, 'lastName');
                this.$parent.find('[data-mz-signup-lastname]').removeClass('is-invalid');
            }
            if (!emailAddr.val() || !emailReg.test(emailAddr.val() )) {
            	isEmailValid = false;
                msg1 = this.displayCustomMessage(Hypr.getLabel('emailMissing'), true, 'emailAddress');
                messageModel = new MessageModel();
                messageModel.set({message: Hypr.getLabel('emailMissing')});
                MessageCollection.add(messageModel);
            } else {
            	isEmailValid = true;
                this.displayCustomMessage("", true, 'emailAddress');
                this.$parent.find('[data-mz-signup-emailaddress]').removeClass('is-invalid');
            }

            if (!payload.password) {
                validation = false;
                msg2 = this.displayCustomMessage(Hypr.getLabel('passwordMissing'), true, 'password');
                messageModel = new MessageModel();
                messageModel.set({message: Hypr.getLabel('passwordMissing')});
                MessageCollection.add(messageModel);
            } else {
                validation = true;
                this.displayCustomMessage("", true, 'password');
                this.$parent.find('[data-mz-signup-password]').removeClass('is-invalid');
            }

            if (payload.password !== this.$parent.find('[data-mz-signup-confirmpassword]').val()) {
                validation = false;
                msg3 = this.displayCustomMessage(Hypr.getLabel('passwordsDoNotMatch'), true, 'confirmPassword');
                messageModel = new MessageModel();
                messageModel.set({message: Hypr.getLabel('passwordsDoNotMatch')});
                MessageCollection.add(messageModel);
            } else {
                validation = true;
                this.displayCustomMessage("", true, 'confirmPassword');
                this.$parent.find('[data-mz-signupaeroplanAvailableClass-confirmpassword]').removeClass('is-invalid');
                this.$parent.find('[data-mz-signup-confirmpassword]').removeClass('is-invalid');
            }
            if(aeroplanAvailableClass){
            	if (!aeroplanNumber || aeroplanNumber.length != 9) {
                    isAeroplanNumber = false;
                    msg6 = this.displayCustomMessage(Hypr.getLabel('aeroplanNumberMissingInvalid'), false, 'aeroplanNumber');
                    messageModel = new MessageModel();
                    messageModel.set({message: Hypr.getLabel('aeroplanNumberMissingInvalid')});
                    $('#signupAeroplanNumberInput').addClass('is-invalid');
                    $('.digit-length').addClass('is-invalid');
                    MessageCollection.add(messageModel);
                } else {
                	isAeroplanNumber = true;
                    this.displayCustomMessage("", false, 'aeroplanNumber');
                    $('#signupAeroplanNumberInput').removeClass('is-invalid');
                    $('.digit-length').removeClass('is-invalid');
                }
            	
            	if (!aeroplanLastName) {
            		isAeroplanLastName = false;
	                msg7 = this.displayCustomMessage(Hypr.getLabel('lnameMissing'), false, "aeroplanLastName");
	                messageModel = new MessageModel();
	                messageModel.set({message: Hypr.getLabel('lnameMissing')});
	                $('#signupAeroplanLastName').addClass('is-invalid');
	                MessageCollection.add(messageModel);
	            } else {
	            	isAeroplanLastName = true;
	                this.displayCustomMessage("", false, 'aeroplanLastName');
	                $('#signupAeroplanLastName').removeClass('is-invalid');
	            }
            }

            //messageView.render();
            if(aeroplanAvailableClass){
                if(!(isFnameValid && isLnameValid && validation && isEmailValid && isAeroplanNumber && isAeroplanLastName)){
                    $("html, body").animate({ scrollTop: 400 }, "slow");
                }
                return isFnameValid && isLnameValid && validation && isEmailValid && isAeroplanNumber && isAeroplanLastName ? true : false;
            }
            else{
                
                if(!(isFnameValid && isLnameValid && validation && isEmailValid)){
                    $("html, body").animate({ scrollTop: 400 }, "slow");
                }
            	return isFnameValid && isLnameValid && validation && isEmailValid ? true : false;
            }
        },
        signup: function () {
        	var aeroplanMemberNumberValue = $('#signupAeroplanNumberInput').val();
            var currentStore;
            if($.cookie("preferredStore")){
               currentStore = $.parseJSON($.cookie("preferredStore"));
           
                var contractor = this.$parent.find('input[name=contractormember]:checked').val();
                var aeroplan = this.$parent.find('input[name=aeroplanmember]:checked').val();
                var marketingPreference = this.$parent.find('input[name=subscribenewsletter]:checked').val();
                var aeroplanNumber;
                if(aeroplan) {
                    aeroplanNumber = aeroplanMemberNumberValue ? aeroplanMemberNumberValue : "N/A";
                }else {
                    aeroplanNumber = 'N/A';
                }
                var self = this,
                    emailAddr = this.$parent.find('[data-mz-signup-emailaddress]'),
                    pass = this.$parent.find('[data-mz-signup-password]'),
                    email = this.$parent.find('[data-mz-signup-emailaddress]').val(),
                    firstName = this.$parent.find('[data-mz-signup-firstname]').val(),
                    lastName = this.$parent.find('[data-mz-signup-lastname]').val(),
                    password = this.$parent.find('[data-mz-signup-password]').val(),
                    aeroplanLastName = aeroplan ? this.$parent.find('[data-mz-aeroplan-lastname]').val() : '',
                    acceptsMarketing = marketingPreference ? marketingPreference : false,
                    isContractor = contractor ? contractor : false,
                    isAeroplanMember = aeroplan ? aeroplan : false,
                    defaultStore = isHomeFurnitureSite ? Hypr.getThemeSetting('hhDefaultStore') : Hypr.getThemeSetting('hfDefaultStore'),
                    payload = {
                        account: {
                            acceptsMarketing: acceptsMarketing,
                            emailAddress: email,
                            userName: email,
                            firstName: firstName,
                            lastName: lastName,
                            aeroplanNumber: aeroplanNumber,
                            aeroplanLastName: aeroplanLastName,
                            contacts: [{
                                email: email,
                                firstName: firstName,
                                lastNameOrSurname: lastName
                            }],
                            attributes:[
                                {
                                fullyQualifiedName: isHomeFurnitureSite ? Hypr.getThemeSetting('hfPreferredStore') : Hypr.getThemeSetting('preferredStore'),
                                values: [currentStore.code]
                                },
                                {
                                    fullyQualifiedName: Hypr.getThemeSetting('isAeroplanMember'), 
                                    values: [isAeroplanMember]
                                },
                                {
                                    fullyQualifiedName: Hypr.getThemeSetting('aeroplanMemberNumber'), 
                                    values: [aeroplanNumber]
                                },
                                {
                                    fullyQualifiedName: Hypr.getThemeSetting('isContractor'),
                                    values: [isContractor]
                                },
                                {
                                    fullyQualifiedName: Hypr.getThemeSetting('recentlyViewed'),
                                    values: ['N/A']
                                },
                                {
                                fullyQualifiedName: isHomeFurnitureSite ? Hypr.getThemeSetting('preferredStore') : Hypr.getThemeSetting('hfPreferredStore'),
                                values: [defaultStore]
                                }
                            ]
                        },
                        password: password
                    };
                var MessageCollection = new Backbone.Collection();
                var messageView = messageViewFactory({
                    el: $('[data-ign-message-bar]'),
                    model: MessageCollection
                });
                messageView.render();    
                if (this.validate(payload, firstName, lastName, emailAddr, pass, aeroplanNumber, aeroplanLastName)) {   
                    //var user = api.createSync('user', payload);
                    var acceptsMarketingStatus = payload.account.acceptsMarketing;
                    var isContractorStatus = _.findWhere(payload.account.attributes, {fullyQualifiedName: Hypr.getThemeSetting('isContractor')}).values[0];
                    if(isContractorStatus){
                        isContractorStatus = "Yes";
                    } else {
                        isContractorStatus = "No";
                    }
                    this.setLoading(true);
                    return api.action('customer', 'createStorefront', payload).then(function () {
                        var returnUrl = "";
                        if (window.location.search.indexOf('returnurl') > 0) {
                            returnUrl = (window.location.search.split('returnurl=')[1] || '');
                        }
                        if (returnUrl) {
                            window.location.href = returnUrl;
                        } else {                		
                            /*Code for Relation1*/
                            
                            if(locale === "fr"){
                                locale = "French";
                            } else {
                                locale = "English";
                            }
                            var headers = {};
                            headers.Authorization = b_token;
                            var postalCode = $.parseJSON($.cookie('preferredStore')).address.postalOrZipCode;                    	              		
                            var now = new Date();
                            var timeStamp = now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + (now.getDate()) + "-" + now.getHours() + ':' + ((now.getMinutes() < 10) ? ("0" + now.getMinutes()) : (now.getMinutes())) + ':' + ((now.getSeconds() < 10) ? ("0" + now.getSeconds()) : (now.getSeconds()));
                            var data = {
                                EmailAddress: emailAddr.val(),
                                FirstName: firstName,
                                PostalCode: postalCode,
                                Preferred_Language: locale,
                                Is_Contractor: isContractorStatus,
                                Source: "HH Ecommerce Website"
                            };
                            var dataForRelation1 = "emailaddress="+emailAddr.val()+"&timeStamp="+timeStamp;
                            if(acceptsMarketingStatus) {
                                $.ajax({
                                    method: 'POST',
                                    headers: headers,  
                                    contentType: 'application/json; charset=utf-8',
                                    url: relation1InsertRecord,
                                    data: JSON.stringify(data),
                                    success:function(response){
                                        $.ajax({
                                            method: 'GET',
                                            url: Hypr.getThemeSetting('linkRelation1GetToken'),
                                            data: dataForRelation1,
                                            success:function(response){
                                                window.location.href = relation1Link + response;
                                            },
                                            error:function(error){
                                                console.log("error: " + JSON.stringify(error));
                                            }
                                        });
                                    },
                                    error: function (error) {
                                        console.log(error);
                                    }
                                });
                            }
                            $('.signup-container').hide();
                            $('.signup-success-message').removeClass('hidden');
                            $('.recently-viewed-on-signup').removeClass('hidden');
                            $('.signup-header').addClass('hidden');
                            $('.singin-link').addClass('hidden');
                            $('body').animate({scrollTop: 100});
                        }
                    }, self.displayApiMessage);
                }
            }else{
                $('#goToStoreLocator').modal().show();
            }    
        } 
    });
    $(document).on("pageload",function() {
        $('.mz-user-firstname').html(' ');
    });
    
    //To set preferred store in purchase location and cookie
    function setMyStore(myStore) {
    	$.ajax({
            url: "/set-purchase-location", 
            data :{ "purchaseLocation": myStore}, 
            type:"GET",
            success:function(response){
                api.get("location",{code:myStore}).then(function(response){
                    var selectedStore = response.data;  
                    var expiryDate = new Date();
                    expiryDate.setYear(expiryDate.getFullYear() + 1);
                    $.cookie("preferredStore",JSON.stringify(selectedStore),{path:'/',expires: expiryDate});
                    window.location.reload();
                });
            },
            error: function (error) {
                console.log(error);
            }
        });
    }
    $(document).ready(function() {
    	var locale = require.mozuData('apicontext').headers['x-vol-locale'], apiContext = require.mozuData('apicontext');
    	locale = locale.split('-')[0];
    	if(locale === 'en' && siteId !== Hypr.getThemeSetting('homeFurnitureSiteId')){ //execute this code only for English site
    		// Set searchField to the search input field.
    		// Set timeout to the time you want to wait after the last character in milliseconds.
    		// Set minLength to the minimum number of characters that constitutes a valid search.
    			var searchField = $('#search-input'),
    			autocompleteCategory = $('.search-category'),
    			autocompleteProduct = $('.search-product'),
    			timeout = 2000,
    			minLength = 2;
    			
    			var textEntered = false;
    			
    			var timer, searchText;
    			
    			$('.tt-dropdown-menu').on('click', '.search-category', function(event){
    				window.clearTimeout(timer);
    				var categoryId = $(event.currentTarget).data('category-id');
    				searchText = searchField.val();
    				handleInput("Category Autocompelete Click", searchText+"#"+categoryId);
    			});
    			
    			$('.tt-dropdown-menu').on('click', '.search-product', function(event){
    				window.clearTimeout(timer);
    				var productCode = $(event.currentTarget).data('product-code');
    				searchText = searchField.val();
    				handleInput("Product Autocompelete Click",searchText+"#"+productCode);
    			});
    			
    		var handleInput = function(eventType, eventLabel) {
    			searchText = searchField ? searchField.val() : '';
    			if (searchText!== null && searchText.length >= minLength) {
    				return false;
    			}
                ga('send', 'event', 'Autocomplete', eventType, eventLabel);
    			/*window.dataLayer.push({
    				event: 'typeahead',
    				eventCategory: 'Search Autocomplete',
    				eventAction: eventType,
    				eventLabel: eventLabel,
    				eventValue : 1,
    				nonInteraction : false
    			});*/
    			textEntered = false;
    		};
    		
    		var startTimer = function(e) {
    			textEntered = true;
    			window.clearTimeout(timer);
    			searchText = searchField.val();
    			if (e.keyCode === 13) {
    		        handleInput("Autocomplete Typeahead", searchText);
    		        return;
    			}
    			timer = setTimeout(handleInput("Autocomplete Typeahead", "Search Term"), timeout);
    		};
    		
    		if (searchField) {
    			searchField.keyup(startTimer);
    		}
    	}
		
    	//set preferred store from 'myStore' parameter of the URL based on store in link from email (currently implemented for anonymous user)
    	var user = require.mozuData('user');
    	if(user.isAnonymous){
	    	if(window.location.search.indexOf('myStore') > 0) {
	    		var myStore = $.urlParam('myStore');
	    		if($.cookie("preferredStore")){
	    			var currentStore = $.parseJSON($.cookie("preferredStore"));
	                if(myStore != currentStore.code){
	                	setMyStore(myStore);
	                }
	            }else{
	            	setMyStore(myStore);
	            }
	    	}
    	}
    	
    	setTimeout(function() {
	    	if($('#searchbox').hasClass('active')){
	    		$('#page-content').addClass('addTopOnMobile');
	    	}
    	}, 1000);
    	$('.search-close').click(function() {
    		$('#page-content').removeClass('addTopOnMobile addTopOnMobileAfterSearch');
    	});
    	$('#search_submit').click(function() {
    		setTimeout(function() {
    			$('#page-content').addClass('addTopOnMobileAfterSearch');
    		}, 500);
    	});
    	setTimeout(function() {
    		$('linearGradient').find('stop[offset="1%"]').css('stop-color', '#c7c7c7');
    	}, 5000);

    	//Popover for longin keep me sign in
    	$("[data-toggle=popover]").popover({
            html : true,
            content: function() {
              var content = $(this).attr("data-popover-content");
              return $(content).children(".popover-body").html();
            },
            title: function() {
              var title = $(this).attr("data-popover-content");
              return $(title).children(".popover-heading").html();
            }
        });
    	
        // Removing is-loading class on load forgot password page
        $('.forgot-password-cont > .btn-block').removeClass('is-loading');
    	
        $(document).on("changeUserName", function() {
            var cust = api.get('customer', {
                id: user.accountId
            }).then(function(response) {
                $('.mz-user-firstname').html(response.data.firstName);
            });
        });
        
        var fName = document.getElementById('firstname');
        if (fName) {
            fName.addEventListener('keydown', function (event) {
                if (event.which === 32) {
                    fName.value = fName.value.replace(/^\s+/, '');
                    if (fName.value.length === 0) {
                        event.preventDefault();
                    }
                }
            });
        }

        var lName = document.getElementById('lastname');
        if (lName) {
            lName.addEventListener('keydown', function (event) {
                if (event.which === 32) {
                    lName.value = lName.value.replace(/^\s+/, '');
                    if (lName.value.length === 0) {
                        event.preventDefault();
                    }
                }
            });
        }
        var lAeroplanName = document.getElementById('signupAeroplanLastNameInput');
        if (lAeroplanName) {
        	lAeroplanName.addEventListener('keydown', function (event) {
                if (event.which === 32) {
                	lAeroplanName.value = lAeroplanName.value.replace(/^\s+/, '');
                    if (lAeroplanName.value.length === 0) {
                        event.preventDefault();
                    }
                }
            });
        }

        
        //preferred store view
        var PreferredStoreView = Backbone.MozuView.extend({
            templateName: 'modules/location/preferred-store',
            additionalEvents: {
            "click .changePreferredStore": "changePreferredStore"
            },
            changePreferredStore: function(e){
                e.preventDefault();
                var locale = require.mozuData('apicontext').headers['x-vol-locale'];
                locale = locale.split('-')[0];
                var currentLocale = locale === 'fr' ? '/fr' : '/en';
                window.location.href=currentLocale + "/store-locator?returnUrl="+window.location.pathname;
            }
        });

        var preferredStore;    
        if($.cookie("preferredStore")){    
            preferredStore = $.parseJSON($.cookie("preferredStore"));     
        }else{
            preferredStore = {
                noPreferredStore:true
             };
        }
        if(preferredStore){
            var preferredStoreView = new PreferredStoreView({
                                        el: $("#preferred-store"),
                                        model: new Backbone.Model(preferredStore)
                                    });
            preferredStoreView.render();
        }
        $("#selectPreferStoreBtn").click(function(e){
            e.preventDefault();
            var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            locale = locale.split('-')[0];
            var currentLocale = locale === 'fr' ? '/fr' : '/en';
            window.location.href=currentLocale + "/store-locator?returnUrl="+window.location.pathname;
        });
        var SessionSharingView = Backbone.MozuView.extend({
        	templateName: 'modules/session-sharing-links/session-sharing-links',
        	additionalEvents: {
        		"click [data-mz-lang]": "changeSite"
        	},
        	initialize: function() {
        		var urlData = this.makeUrlToLand();
        		var apiContext = require.mozuData('apicontext'), tenantId = apiContext.headers["x-vol-tenant"], siteId = apiContext.headers["x-vol-site"];
        		this.model.set('tenantId', urlData.tenantId);
        		this.model.set('siteId', urlData.siteId);
        		this.model.set('locationPath', urlData.locationPath);
        		this.model.set('search', urlData.search);
        	},
        	changeSite: function(e) {
        		e.preventDefault();
        		var urlData = this.makeUrlToLand();
        		
        		if(!require.mozuData('pagecontext').isEditMode && Hypr.getThemeSetting('setPreferredLanguage')) {
        			var siteId = apiContext.headers["x-vol-site"];
	        		var expiryDate = new Date();
	            	expiryDate.setYear(expiryDate.getFullYear() + 1);
	            	
	        		if(siteId === Hypr.getThemeSetting('enSiteId')) { //current site is english
	        			$.cookie("hhLanguage", 'fr', {path:'/', expires: expiryDate});
	        		}else if(siteId === Hypr.getThemeSetting('frSiteId')) {
	        			$.cookie("hhLanguage", 'en', {path:'/', expires: expiryDate});
	        		}
        		}
        		
        		if(Hypr.getThemeSetting('selectedEnvironment') === 'DEV') { 
        			window.location.href = Hypr.getThemeSetting('hhWebsiteLink') + urlData.locationPath + urlData.search;
        		}else if(Hypr.getThemeSetting('selectedEnvironment') === 'PROD') {
        			window.location.href = Hypr.getThemeSetting('hhWebsiteLink') + urlData.locationPath + urlData.search;
        		}
        	},
        	makeUrlToLand: function() {
        		var apiContext = require.mozuData('apicontext'), tenantId = apiContext.headers["x-vol-tenant"],
        		siteId = apiContext.headers["x-vol-site"], locationPath = window.location.pathname, landingSiteId = '', search = '', pageType = require.mozuData('pagecontext').pageType,
        		tempLoc = '', newTempLoc = '';
        		if(window.location.search) {
        			var paramsArray = window.location.search.split('&');
        			_.each(paramsArray, function(param, index) {
        				if(param.indexOf('categoryId') !== -1) {
        					paramsArray.splice(index, 1);
        				}
        			});
        			var newSearch = paramsArray.join('&');
        			search = newSearch.indexOf('returnurl') == -1 ? newSearch : '';
        		}
        		if(siteId === Hypr.getThemeSetting('enSiteId')) { //current site is english
        			landingSiteId = Hypr.getThemeSetting('frSiteId');
        			if(locationPath.indexOf('/en') != -1){
        				locationPath = locationPath.replace('/en', '/fr');
        				if(pageType === 'category') {
        					tempLoc = locationPath.split('/fr/');
        					newTempLoc = tempLoc[1].split('/c/');
        					locationPath = '/fr/c/' + newTempLoc[1];
        				}
        			}else{
        				locationPath = '/fr' + locationPath;
        			}
        		}else if(siteId === Hypr.getThemeSetting('frSiteId')){
        			landingSiteId = Hypr.getThemeSetting('enSiteId');
        			if(locationPath.indexOf('/fr') != -1){
        				locationPath = locationPath.replace('/fr', '/en');
        				if(pageType === 'category') {
        					tempLoc = locationPath.split('/en/');
        					newTempLoc = tempLoc[1].split('/c/');
        					locationPath = '/en/c/' + newTempLoc[1];
        				}
        			}else{
        				locationPath = '/en' + locationPath;
        			}
        		}
        		var urlData = {
        				'tenantId': tenantId,
        				'siteId': siteId,
        				'landingSiteId': landingSiteId,
        				'locationPath': locationPath,
        				'search': search
        		};
        		return urlData;
        	}
        	
        });
        var sessionSharingView = new SessionSharingView({
            el: $(".frLink"),
            model: new Backbone.Model()
        });
        sessionSharingView.render();
        
        $docBody = $(document.body);

        $('[data-mz-action="login"]').each(function() {
            var popover = new LoginPopover();
            popover.init(this);
            $(this).data('mz.popover', popover);
        });
        $('[data-mz-action="signup"]').each(function() {
            var popover = new SignupPopover();
            popover.init(this);
            $(this).data('mz.popover', popover);
        });
        $('[data-mz-action="continueAsGuest"]').on('click', function(e) {
            e.preventDefault();
            var returnURL = returnUrl();
            if(returnURL .indexOf('checkout') === -1) {
                returnURL = '';
            }

            //saveUserId=true Will logut the current user while persisting the state of the current shopping cart
            $.ajax({
                    method: 'GET',
                    url: '/logout?saveUserId=true&returnUrl=' + returnURL,
                    complete: function(data) {
                        location.href = require.mozuData('pagecontext').secureHost + '/' + returnURL;
                        if($.cookie('inStorePickUpStep')){
                        	$.removeCookie('inStorePickUpStep', { path: '/' });
                        }
                    }
            });

        });
        $('[data-mz-action="launchforgotpassword"]').each(function() {
            var popover = new LoginPopover();
            popover.init(this);
            $(this).data('mz.popover', popover);
        });
        $('[data-mz-action="signuppage-submit"]').each(function(){
            var signupPage = new SignupPopover();
            signupPage.formSelector = 'form[name="mz-signupform"]';
            signupPage.pageType = 'signup';
            //signupPage.redirectTemplate = 'myaccount';
            signupPage.init(this);
        });
        $('[data-mz-action="loginpage-submit"]').each(function(){
            var loginPage = new SignupPopover();
            loginPage.formSelector = 'form[name="mz-loginform"]';
            loginPage.pageType = 'login';
            loginPage.init(this);
        });
        $('[data-mz-action="anonymousorder-submit"]').each(function () {
            var loginPage = new SignupPopover();
            loginPage.formSelector = 'form[name="mz-anonymousorder"]';
            loginPage.pageType = 'anonymousorder';
            loginPage.init(this);
        });
        $('[data-mz-action="forgotpasswordpage-submit"]').each(function(){
            var loginPage = new SignupPopover();
            loginPage.formSelector = 'form[name="mz-forgotpasswordform"]';
            loginPage.pageType = 'retrievePassword';
            loginPage.init(this);
        });
        $('[data-mz-action="resetpassword-submit"]').each(function() {
            $('[data-mz-action="resetpassword-submit"]').attr('disabled',true);
            $('[data-mz-new-password]').keyup(function(){
                if($(this).val().length !== 0){
                    $('[data-mz-action="resetpassword-submit"]').attr('disabled', false);
                }
                else
                {
                    $('[data-mz-action="resetpassword-submit"]').attr('disabled', true);        
                }
            });
        });
        var closeSearchBox = function() {
        	$('.mz-searchbox-input, .glyphicon-search, .search-close').css({'display': 'none'}); 
        	$('#searchbox').removeClass('active');
        	$('#search_submit').css({'display': 'block'});
    	};
        $('#storecollapse').on('click', function() {
            if ($(this).find('#store-section').hasClass('fa-angle-down')) {
                $(this).find('#store-section').removeClass('fa-angle-down').addClass('fa-angle-up');
            } else {
                $(this).find('#store-section').addClass('fa-angle-down').removeClass('fa-angle-up');
            }
        });

        var id = '#dialog';

        //transition effect
        if($.cookie("showDialog")){
            if ($.cookie("preferredStore")){
                $("#no-store-dialog").hide();
                $(id).show();
            }else{
                $(id).hide();
                if(require.mozuData('pagecontext').cmsContext.page.path !== "store-locator")
                    $("#no-store-dialog").show();
            }         
        }else{
            if ($.cookie("preferredStore")){
                $(id).hide();
            }else{  
                if(require.mozuData('pagecontext').cmsContext.page.path !== "store-locator")
                    $("#no-store-dialog").show();
            }    
        }
        //if close button is clicked
        $('.window .close').click(function (e) {
            //Cancel the link behavior
            e.preventDefault();
            $('.window').hide();
            $.removeCookie('showDialog', { path: '/' });
        });  
        // Close no-store popover
        $('.window .no-store-close').click(function (e) {
            //Cancel the link behavior
            e.preventDefault();
            $('.window').hide();
        });

        $(".popbadge-link").click(function(){
            $.removeCookie('showDialog', { path: '/' });
        });
        
        $('#aeroplan-member').on('click',function(){
        	var self = $('#signupAeroplanSection');
	        if(self.hasClass('hidden')){
		    	$(self).removeClass('hidden').addClass('aeroplanAvailable');
	        }else{
	        	$(self).addClass('hidden').removeClass('aeroplanAvailable');
	        }
	        if(self.hasClass('aeroplanAvailable')){
	        	$('#lastname').change(function() {
	        	    $('#signupAeroplanLastName').val($(this).val());
	        	});
	        }
	        
	      //starts aeroplan 3 fields value
	        var inputQuantity = [];
	        $(function() {
	          $(".digit-length").each(function(i) {
	            inputQuantity[i]=this.defaultValue;
	             $(this).data("idx",i);
	          });
	          $(".digit-length").on("keyup", function (e) {
	            var $field = $(this),
	                val=this.value,
	                $thisIndex=parseInt($field.data("idx"),10); 
	            if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid") ) {
	                this.value = inputQuantity[$thisIndex];
	                return;
	            } 
	            if (val.length > Number($field.attr("maxlength"))) {
	              val=val.slice(0, 5);
	              $field.val(val);
	            }
	            inputQuantity[$thisIndex]=val;
	            if($(this).val().length==$(this).attr("maxlength")){
	                $(this).next().focus();
	            }
	            var num1 = $('#number1').val();
	            var num2 = $('#number2').val();
	            var num3 = $('#number3').val();
	            var number = num1 + num2 + num3;
	            $('#signupAeroplanNumberInput').val(number);
	            
	          });      
	        });
        
        //end aeroplan 3 fields value
        });
        
        $(document).mouseup(function (e) {
            var dialog = $("#dialog");
            if (!$('.close').is(e.target) && !dialog.is(e.target) && dialog.has(e.target).length === 0) {
                dialog.hide();
                $.removeCookie('showDialog', { path: '/' });
            }
        });
        

        $(function(){

              //search form
              //close on escape key
              $(document).keyup(function(e) {
                  if(e.which === 27){
                      closeSearch();
                  }
              });

              function closeSearch() {
            	  if($('.tt-input').val()){
            		  $('.mz-searchbox-input').val('');
            	  } else {
            		  $('.mz-searchbox-input').val('');
                      $('.mz-searchbox-input').hide();
                      $('#searchbox').removeClass('active');
                      $('#searchbox button[type="reset"]').hide();
                      $('.mz-sitenav').removeClass('fixed-on-search'); 
            	  }
                  
              }

              function openSearch() {
                  $('#searchbox').addClass('active');
                  $('.mz-searchbox-input').show();
                  $('#searchbox button[type="reset"]').show();
                  $('.mz-sitenav').addClass('fixed-on-search'); 
                  $('.mz-sitenav').removeClass('fixed-on-mobile-search'); 
                  $('.tt-input').focus().val('');
              }
              openSearch();
             
              // Show Search if form is not active or input search empty
              $('#searchbox button[type="submit"]').click(function(event) {
                  if(!$( "#searchbox" ).hasClass( "active" )) {
                      event.preventDefault();
                      openSearch();
                  }
                  $(this).parents().eq(6).find('.mz-pageheader').addClass('is-search-open');
                  $('.mobilemenuclose').removeClass('mobilemenuclose').addClass('mobilemenutoggle');
                  $('#example-navbar-collapse, #collapseOne').removeClass('in');
                  
                  if($('#storecollapse').hasClass('collapsed')){
                	  $('#store-section').addClass('fa-angle-up').removeClass('fa-angle-down');
                  }
                  else{
                	  $('#store-section').addClass('fa-angle-down').removeClass('fa-angle-up');
                  }
              });

              //close form
              $('#searchbox button[type="reset"]').click(function(event) {
                  //event.preventDefault();
                  closeSearch();
                  $(this).parents().eq(6).find('.mz-pageheader').removeClass('is-search-open');
              });
        });

        $('[data-mz-action="logout"]').each(function(){
            var el = $(this);

            //if were in edit mode, we override the /logout GET, to preserve the correct referrer/page location | #64822
            if (require.mozuData('pagecontext').isEditMode) {

                 el.on('click', function(e) {
                    e.preventDefault();
                    $.ajax({
                        method: 'GET',
                        url: '/logout',
                        complete: function() { location.reload();}
                    });
                });
            }

        });
        $('[data-mz-action="logout"]').on("click",function(e) {
        	e.preventDefault();
        	var preferredStore = $.cookie("preferredStore");
        	$.ajax({
                method: 'GET',
                url: '/logout',
                success: function() {
                	if(preferredStore) {
                		var store = JSON.parse(preferredStore);
                		$.ajax({
                    		url: "/set-purchase-location", 
                    		data :{ "purchaseLocation": store.code}, 
                    		type:"GET",
                    		success:function(response){
                    			if($.cookie('inStorePickUpStep')){
                                	$.removeCookie('inStorePickUpStep', { path: '/' });
                                }
                    			if($.cookie('recentlyViewd')){
                                	$.removeCookie('recentlyViewd', { path: '/' });
                                }
                            	window.location.href = require.mozuData('pagecontext').secureHost + '/';
                            	
                			},
                			error: function (error) {
                				// if arc action failed
                			}
                    	});
                	}else {
                		window.location.href = require.mozuData('pagecontext').secureHost + '/';
                	}
                }
            });
        });
        
        /* password show-hide functionality */
        $(".show-hide").on("click",function(e){
        	if(e){
        		e.preventDefault();
        		e.stopPropagation();
        	}
            var inputElement = $(this).parent().find('input[name="password"]');
            if(inputElement.attr('type') == 'password'){
            	inputElement.attr('type', 'text');
        		$(this).text($(this).data('label-hide'));
            }else{
            	inputElement.attr('type', 'password');
    			$(this).text($(this).data('label-show'));
            }
        });
        $(".show-hide-pwd").on("click",function(e){
        	if(e){
        		e.preventDefault();
        		e.stopPropagation();
        	}
            var inputElement = $(this).parent().find('input[name="confirmPassword"]');
            if(inputElement.attr('type') == 'password'){
            	inputElement.attr('type', 'text');
        		$(this).text($(this).data('label-hide'));
            }else{
            	inputElement.attr('type', 'password');
    			$(this).text($(this).data('label-show'));
            }
        });
        $(".show-hide-current-pwd").on("click",function(e){
        	if(e){
        		e.preventDefault();
        		e.stopPropagation();
        	}
            var inputElement = $(this).parent().find('input[name="currentPassword"]');
            if(inputElement.attr('type') == 'password'){
            	inputElement.attr('type', 'text');
        		$(this).text($(this).data('label-hide'));
            }else{
            	inputElement.attr('type', 'password');
    			$(this).text($(this).data('label-show'));
            }
        });
        /* Popover functionality*/
        $('[data-toggle="popover"]').popover();
        
        $('body').on("click",function(e){
        	if (typeof $(e.target).data('original-title') == 'undefined') {
        	   $('[data-original-title]').popover('hide');
        	}
        });
        
        $('a[href="#collapse-"]').on("click",function(e){
        	if($('.accordion-collapse').hasClass('in')){
        		$(this).parents('.accordion-panel').addClass('open');
        	}
        });
        
        /* Checkout as guest */
        $('#cart-checkout').click(function() {
        	$('#cart-form').submit();
        	var locale = require.mozuData('apicontext').headers['x-vol-locale'];
        	locale = locale.split('-')[0];
        	var currentLocal = locale === 'fr' ? '/fr' : '/en';
        	window.location.href = currentLocal + "/cart/checkout";
        });
        
        /*Update Wishlist Count*/
        if (!(user.isAnonymous) && user.isAuthenticated) {
	        api.get('wishlist',{}).then(function(response){
	        	if(response && response.data && response.data.items.length > 0) {
	        		var items = response.data.items[0].items;
		            var totalQuantity = 0;
		            _.each(items, function(item, index) {
		            	totalQuantity += item.quantity;
		            });
		            $('#wishlist-count').html('('+totalQuantity+')');
	        	}
	        });
        }
        
        /*2020 login call*/                
        $('.goto2020').on('click', function(e) {
        	e.preventDefault();        	 
        	if (!(user.isAnonymous) && user.isAuthenticated) {
        		var requireUser = require.mozuData('pagecontext').user;
        		$.ajax({
            		type:"GET",
            		dataType: 'text',
            		url: Hypr.getThemeSetting('link2020'),
            		data: {
            			firstName: requireUser.firstName,
            			emailAddress: requireUser.email,
            			userId: requireUser.accountId
            		},
            		success:function(response){
            			window.location.href = goto2020Link + '?token=' + response;
        			},
        			error: function (error) {
        				console.log(error);
        			}
            	});
        	} else {
        		window.location.href = goto2020Link;
        	} 	      
        });   
        
        /*Relation1 for Newsletter from Footer*/
        $('.signup-btn').on('click', function(){
        	var email = $('.footer-newsletter-email').val();
        	var emailReg = Backbone.Validation.patterns.email;
        	var emailFormat = emailReg.test( email );
            
            if($.cookie('preferredStore')){
                if(emailFormat) {
                    $('.emailFormatError').hide();
                    var headers = {};
                    headers.Authorization = b_token;
                    
                    if(locale === "fr"){
                        locale = "French";
                    } else {
                        locale = "English";
                    } 
                    
                    var postalCode = $.parseJSON($.cookie('preferredStore')).address.postalOrZipCode;
                    var now = new Date();
                    var timeStamp = now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + (now.getDate()) + "-" + now.getHours() + ':' + ((now.getMinutes() < 10) ? ("0" + now.getMinutes()) : (now.getMinutes())) + ':' + ((now.getSeconds() < 10) ? ("0" + now.getSeconds()) : (now.getSeconds()));
                    var data = {
                                EmailAddress: email,
                                FirstName: " ",
                                PostalCode: postalCode,
                                Preferred_Language: locale,
                                Is_Contractor: "No",
                                Source: "HH Ecommerce Website"
                            };
                    var dataForRelation1 = "emailaddress="+email+"&timeStamp="+timeStamp;
                    if(email) {
                        $.ajax({
                            method: 'POST',
                            headers: headers,  
                            contentType: 'application/json; charset=utf-8',
                            url: relation1InsertRecord,
                            data: JSON.stringify(data),
                            success:function(response){
                                $.ajax(
                                        {
                                        method: 'GET',
                                        url: Hypr.getThemeSetting('linkRelation1GetToken'),
                                        data: dataForRelation1,
                                        success: function(response) {
                                            window.location.href = relation1Link + response;
                                        },
                                        error: function (error) {
                                            console.log("error: " + JSON.stringify(error));
                                        }
                                });
                            },
                            error:function(error){
                                console.log(error);
                            }
                            
                        });
                    }
                } else {
                    $('.emailFormatError').show();
                }
            }else{
                window.location.href = "https://pc.en.homehardware.ca/preferences.aspx";
            }
        	
        });   
        
        if($(window).width() > 767) {
        	$('#static-navigation-collapse').addClass('in');
        }
        
    });
    
    //Change icon of footer lists on collapse
	$('.footer-list').on('shown.bs.collapse', function(e) {
 		$(this).parent().find('.collapseBtn').text('-');
    });
    $('.footer-list').on('hidden.bs.collapse', function(e) {
		$(this).parent().find('.collapseBtn').text('+');
    });
    if( $('#mzWishlist').is(':empty') ) {
    	$(".empty-wishlist-container").show(); 
    }
    
    $(".accordion-panel").on("show.bs.collapse hide.bs.collapse", function(e) {
        if (e.type=='show'){
          $(this).addClass('active');
        }else{
          $(this).removeClass('active');
        }
      });

    $("#signInLink, #signInLinkMobile,.annonymous-signin-link ").click(function(e) {
        e.preventDefault();
        var locale = require.mozuData('apicontext').headers['x-vol-locale'];
        var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
        locale = locale.split('-')[0];
        var currentLocale = '';
        if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite){
            currentLocale = locale === 'fr' ? '/fr' : '/en';
        }
        if(window.location.pathname === (currentLocale+"/login-shopping-list")){
            window.location.href=currentLocale + "/user/login?returnurl=/shopping-list";
        }else{
            window.location.href=currentLocale + "/user/login?returnurl="+window.location.pathname;
        }
    });
});
