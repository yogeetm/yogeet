define(["modules/jquery-mozu", 
	"underscore", 
	"hyprlive", 
	"modules/backbone-mozu", 
	"modules/cart-monitor", 
	"modules/models-product",
	"modules/views-productimages",  
	"hyprlivecontext",
	'modules/api',
	'cloud',
	"widgets/bazaarvoice",
    'slick'], function ($, _, Hypr, Backbone, CartMonitor, ProductModels, ProductImageViews, HyprLiveContext, api) {
	var apiContext = require.mozuData('apicontext');
	var user = require.mozuData('user');
	var QuickProductModalView = Backbone.MozuView.extend({
        templateName: 'modules/product/product-quick-view', 
        additionalEvents: {
            "change [data-mz-product-option]": "onOptionChange",
            "change [data-mz-value='quantity']": "onQuantityChange",
            "click [data-mz-value='qty-increment']": "onQuantityChange",
            "click [data-mz-value='qty-decrement']": "onQuantityChange",
            "keyup input[data-mz-value='quantity']": "onQuantityChange"
        },
        render: function () { 
        	$('#add-to-cart').off();
        	$('#add-to-wishlist').off();
        	var me = this; 
	        
        	Backbone.MozuView.prototype.render.apply(this, arguments);            
            var variationProdCode = this.model.get('variationProductCode');
            if(variationProdCode){
            	var prodImg = this.model.get('content').get('productImages');
            	me.swatchProductImageView(prodImg, variationProdCode);
            }
            $('#add-to-cart').on('click', function() {
            	me.addToCartForPickup();
            });
            $('#add-to-wishlist').on('click', function(e) {
            	e.stopImmediatePropagation(); //To off previous modal's click events
            	me.addToWishlist();
            });
            me.sliderFunction();
        },
        onOptionChange: function (e) {
            return this.configure($(e.currentTarget));
        },
        onQuantityChange: _.debounce(function (e) {
        	var field = $(e.target).parent().attr('data-mz-value'); //this field is used to increment/decrement the quantity                
        	var quantity = $(e.target).parent().siblings('.mz-productdetail-qty').val();    
        	
        	if(field === 'qty-decrement'){    
        		$(e.target).parent().siblings('.mz-productdetail-qty').val(parseInt(quantity, 10) - 1); 
        	}
        	if(field === 'qty-increment'){
        		$(e.target).parent().siblings('.mz-productdetail-qty').val(parseInt(quantity, 10) + 1);
        	}    
        	var $qField = "";    
        	if(field != 'quantity'){    
        		$qField = $(e.target).parent().siblings('.mz-productdetail-qty');    
        	}else {    
        		$qField = $(e.target).parent();    
        	}
        	
            var newQuantity = parseInt($qField.val(), 10);
            
            if(newQuantity <= 1 || isNaN(newQuantity)){
            	$(".decrement").addClass("is-disabled");
            }else {
            	$(".decrement").removeClass("is-disabled");
            }
            
            if (!isNaN(newQuantity)) {
                this.model.updateQuantity(newQuantity);
            }
        },500),
        configure: function ($optionEl) {
        	var newValue = $optionEl.val(),
	            oldValue,
	            id = $optionEl.data('mz-product-option'),
	            optionEl = $optionEl[0],
	            isPicked = (optionEl.type !== "checkbox" && optionEl.type !== "radio") || optionEl.checked,
	            option = this.model.get('options').findWhere({'attributeFQN':id});
	        if (option) {
	            if (option.get('attributeDetail').inputType === "YesNo") {
	                option.set("value", isPicked);
	            } else if (isPicked) {
	                oldValue = option.get('value');
	                if (oldValue !== newValue && !(oldValue === undefined && newValue === '')) {
	                    option.set('value', newValue);
	                }
	            }
	        }
	        
        },
        swatchProductImageView: function(prodContent, variationProductCode) {
            var prodImg = _.findWhere(prodContent, {
                altText: variationProductCode
            });
            
            if (prodImg) {
                var prodImage = prodImg.imageUrl;
                var ImgAltText = prodImg.altText;
               $('.mz-productimages-main').find('img[data-mz-productimage-main]').attr({
                   "src": prodImage + "?max=768",
                   "alt": ImgAltText
               });
               
               $(".mz-productimages-thumbimage").removeClass("is-selected");
               $('.mz-productimages-thumbs').find('img[alt="' + variationProductCode + '"]:first').addClass("is-selected");
            } else {
            	var defaultProdImage = prodContent[0].imageUrl;
                var defaultImgAltText = prodContent[0].altText;
               $('.mz-productimages-main').find('img[data-mz-productimage-main]').attr({
                   "src": defaultProdImage + "?max=768",
                   "alt": defaultImgAltText
               });
               
               $(".mz-productimages-thumbimage").removeClass("is-selected");
               $('.mz-productimages-thumbs').find('img:first').addClass("is-selected");
            }
        },
        addToCart: function () {
            this.model.addToCart();
        },
        addToCartForPickup: function () {
        	var quantity = $('.mz-productdetail-qty').val();
            var preferredStore = $.parseJSON($.cookie("preferredStore"));
            this.model.addToCartForPickup(preferredStore.code, preferredStore.name, quantity);
        },
        addToWishlist: function () {
        	var me = this;
        	if (user.isAnonymous) {
        		var locale = require.mozuData('apicontext').headers['x-vol-locale'];
        		var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
                locale = locale.split('-')[0];
                var currentLocale = '';
                if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite){
                	currentLocale = locale === 'fr' ? '/fr' : '/en';
				}
				window.location.href=currentLocale + "/user/login?productCode="+me.model.get('productCode');
        	} else {
        		this.model.addToWishlist();
        	}
        },
        optionInitialize: function () {
        	// handle preset selects, etc
        	var me = this;
        	setTimeout(function(){
        		me.$('[data-mz-product-option]').each(function (index) {
                	if (index === 0) {
                		var $this = $(this), isChecked, wasChecked;
                        if ($this.val()) {
                            switch ($this.attr('type')) {
                                case "checkbox":
                                case "radio":
                                    isChecked = $this.prop('checked');
                                    wasChecked = !!$this.attr('checked');
                                    if ((isChecked && !wasChecked) || (wasChecked && !isChecked)) {
                                        me.configure($this);
                                    }
                                    break;
                                default:
                                    me.configure($this);
                            }
                        }            		
                	}
                    
                });
        	},500);
        },
        setAeroplanMiles: function(aeroplanMiles) {
        	var me = this;
        	var documentListForAeroplanPromo = Hypr.getThemeSetting('documentListForAeroplanPromo');
        	api.get('documentView', {listName: documentListForAeroplanPromo}).then(function(response) {
            	if(response.data.items.length > 0) {
            		var aeroplanMilesFactor = response.data.items[0].properties.markupFactor;
            		me.model.set('aeroplanMiles', aeroplanMilesFactor);
            		var promoCode = response.data.items[0].properties.promoCode;
            		me.model.set('aeroplanPromoCode', promoCode);
            		var startDate = new Date(response.data.items[0].activeDateRange.startDate);
            		var endDate = new Date(response.data.items[0].activeDateRange.endDate); 
            		me.model.set('aeroplanPromoDate', true);
            		me.model.set('aeroplanStartDate', startDate);
            		me.model.set('aeroplanEndDate', endDate);
            		me.render();
            	}else {
            		me.model.set('aeroplanMiles', aeroplanMiles);
            		me.render();
            	}
        	});
        },
        initialize: function () {
        	this.optionInitialize();
        	var me = this;
            var currentStore;
            
            var contextSiteId = apiContext.headers["x-vol-site"];
            var homeFurnitureSiteId = Hypr.getThemeSetting("homeFurnitureSiteId");
            var frSiteId = Hypr.getThemeSetting("frSiteId");
            if(homeFurnitureSiteId === contextSiteId){
                me.model.set("isHomeFurnitureSite", true);
            }
            if(frSiteId === contextSiteId){
                me.model.set("isFrenchSite", true);   
            }
            if($.cookie('preferredStore')){
                currentStore = JSON.parse($.cookie('preferredStore'));
                me.model.set({"preferredStore":currentStore});
            }
        	
            if(currentStore) {
            	 var attribute = _.findWhere(currentStore.attributes, {"fullyQualifiedName":Hypr.getThemeSetting("isEcomStore")});
                        
	            if(attribute){
	                var isEcomStore = attribute.values[0] === "Y" ? true : false; 
	                me.model.set("isEcomStore", isEcomStore);
	            }
	           
	            var aeroplanMiles;
	            me.model.on("productConfigured", function(response){
	            	if(response.get('price.salePrice')) {
	                	aeroplanMiles = Math.floor(response.get('price.salePrice')/2); 
	                }else {
	                	aeroplanMiles = Math.floor(response.get('price.price')/2); 
	                }
	                me.setAeroplanMiles(aeroplanMiles);
	            });
	            
	            if(!me.model.get('hasPriceRange')) {
	            	if(me.model.get('price.salePrice')) {
	                	aeroplanMiles = Math.floor(me.model.get('price.salePrice')/2); 
	                }else {
	                	aeroplanMiles = Math.floor(me.model.get('price.price')/2); 
	                }
	                me.setAeroplanMiles(aeroplanMiles);
	            }
	            
	        	//check EHF for current product
	            var entityListForEHF = Hypr.getThemeSetting('entityListForEHF');
	        	var productCode;
	        	if(me.model.get('variations') && me.model.get('variations').length > 0) {
	        		//from current variation find the variationProductCode
	        		var productOptionValue = $('.mz-productoptions-option').val();
	        		var variation = _.find(me.model.get('variations'), function(variation) {
	        			return _.findWhere(variation.options, {'value': productOptionValue});
	        		});
	        		productCode = me.model.get('variations')[0].productCode;
	        	}else {
	        		productCode = me.model.get('productCode');
	        	}
	        	var filterQuery = 'productCode eq ' + productCode + ' and province eq ' + currentStore.address.stateOrProvince;
	        	var ehfAttributeFQN = Hypr.getThemeSetting('ehfFees');
	        	api.get('entityList', { listName: entityListForEHF, filter: filterQuery }).then(function(response) {
	        		if(response.data.items.length > 0) { //if EHF is applied for current product
	        			var ehfOption = me.model.get('options').findWhere({'attributeFQN': ehfAttributeFQN});
	                    if(ehfOption) {
	                    	ehfOption.set('shopperEnteredValue', response.data.items[0].feeAmt);
	                    }
	        			me.render();
	        		}
	        	});
	        	
	        	//entityListForItemRestricted
	        	filterQuery = 'productCode eq ' + productCode + ' and (province eq ' + currentStore.address.stateOrProvince + ' or store eq ' + currentStore.code + ')';
	        	var entityListForItemRestricted = Hypr.getThemeSetting('entityListForItemRestricted');
	        	api.get('entityList', { listName: entityListForItemRestricted, filter: filterQuery}).then(function(response) {
	        		if(response.data.items.length > 0) {
	        			me.model.set('isItemRestricted', true);
	                    var websiteInd = response.data.items[0].website_ind;
	                    if(websiteInd === 'N'){
	                         me.model.set('isWebsiteInd', true);
	                    }
	        			me.render();
	        		}
	        	});
	        	
	        	var currentStoresWarehouse = _.find(currentStore.attributes, function(attribute){
	        		return attribute.fullyQualifiedName === Hypr.getThemeSetting('warehouseAttr');
	        	});
	        	if(currentStoresWarehouse) {
	        		var inventoryApi = '/api/commerce/catalog/storefront/products/' + productCode + '/locationinventory?locationCodes=' + currentStoresWarehouse.values[0];
	            	api.request("GET", inventoryApi).then(function(response){
	            		if(response.items.length > 0){
	            			if(response.items[0].stockAvailable > 0){
	            				me.model.set('isInventoryAvail', true);
	            				me.render();
	            			}
	            		}
	            	});
	        	}
            }
        },
        sliderFunction: function() {
            var divLength = $('.slider-first-item').size();  
            var sildeLoop = null;
            if(divLength > 1) {
                sildeLoop =  true;
            }
            else {
                sildeLoop = false;
            }
            $(".regular").removeClass('hidden');
            $(".regular").not('.slick-initialized').slick({
                dots: false,
                infinite: false,
                slidesToShow: 5,
                slidesToScroll: 5,
                variableWidth:true,
                responsive: [
                 {
                   breakpoint: 420,
                   settings: {
                     slidesToShow: 4,
                     slidesToScroll: 4
                   }
                 },
                 {
                   breakpoint: 320,
                   settings: {
                     slidesToShow: 2,
                     slidesToScroll: 2
                   }
                 }
               ]
            });
		},
		changePreferredStore: function(e){
			e.preventDefault();
			var locale = require.mozuData('apicontext').headers['x-vol-locale'];
			var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
			locale = locale.split('-')[0];
			var currentLocale = '';
			if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite){
				currentLocale = locale === 'fr' ? '/fr' : '/en';
			}
			window.location.href=currentLocale + "/store-locator?returnUrl="+window.location.pathname + window.location.search;
        }
    });
	
	var quickViewBind = function(){		
		
		var globalProductImagesView;
		$(document).on('click', 'a[data-mz-product-code]', function(e){
			e.preventDefault();
			var productCode = $(this).data('mz-product-code');
			var productToAdd = new ProductModels.Product({
	            productCode: productCode
	        });
			
			/* Info Popover */
			$('[data-toggle="popover"]').popover();
			
			productToAdd.fetch().then(function() {
				var productImagesView = new ProductImageViews.ProductPageImagesView({
		            el: $('[data-mz-productimages]'),
		            model: productToAdd
		        });
				globalProductImagesView = productImagesView;
				
				var quickProductModalView = new QuickProductModalView({
		            el: $('#QuickviewModal'),
		            model: productToAdd
		        });
				quickProductModalView.render();				
				$('#QuickviewModal').modal();
			});
			
			productToAdd.on('addedtocart', function (cartitem) {
	            if (cartitem && cartitem.prop('id')) {
	            	productToAdd.isLoading(true);
	                CartMonitor.addToCount(productToAdd.get('quantity'));
	                var locale = require.mozuData('apicontext').headers['x-vol-locale'];
	                var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
	                locale = locale.split('-')[0];
	                var currentLocale = '';
	                if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite){
	                	currentLocale = locale === 'fr' ? '/fr' : '/en';
	                }
	                window.location.href = currentLocale + "/cart";
	            } else {
	            	productToAdd.trigger("error", { message: Hypr.getLabel('unexpectedError') });
	            }
	        });
			
			productToAdd.on('addedtowishlist', function (cartitem) {
	            $('#add-to-wishlist').prop('disabled', 'disabled').text(Hypr.getLabel('savedToList'));
	            
	            /*Update Wishlist Count*/
	            api.get('wishlist',{}).then(function(response){        	
	            	var items = response.data.items[0].items;
	                var totalQuantity = 0;
	                _.each(items, function(item, index) {
	                	totalQuantity += item.quantity;
	                });
	                $('#wishlist-count').html('('+totalQuantity+')');
	            });
	            
	        });
		});
		
		//code to change product image on click of thumbnail		
		$(document).on('click', 'span[data-mz-productimage-thumb]', function (e) {
			var $thumb = $(e.currentTarget);
	        
            if($("#videoContainer").css("display") == "block"){
                $("#product-main-video")[0].pause();
                $("#videoContainer").toggleClass("show-video");
                $(".ign-product-videothumb").removeClass("is-selected");
                $("#imageContainer").show();
            }

            $(".mz-productimages-thumbimage").removeClass("is-selected");
	        $thumb.find(".mz-productimages-thumbimage").addClass("is-selected");
	        globalProductImagesView.selectedImageIx = $thumb.data('mz-productimage-thumb');
	        if (globalProductImagesView.imageCache[globalProductImagesView.selectedImageIx]) {
	        	$('[data-mz-productimage-main]')
	                .prop('src', globalProductImagesView.imageCache[globalProductImagesView.selectedImageIx].src)
	                .prop('title', globalProductImagesView.imageCache[globalProductImagesView.selectedImageIx].title)
	                .prop('alt', globalProductImagesView.imageCache[globalProductImagesView.selectedImageIx].alt);
	        }
	        
		});
        /*click listener on video thumbnail*/
        $(document).on("click","#main-video-thumb",function(e){
            if($("#videoContainer").css("display") == "none"){
                $("#videoContainer").toggleClass("show-video");
                $("#imageContainer").hide();
                $(".mz-productimages-thumbimage").removeClass("is-selected");
                $(".ign-product-videothumb").addClass("is-selected");
            }
        });
        /*Play/Pause main video */
        var videoPlaying=false;
        $(document).on("click",'#product-main-video',function() {
            if (videoPlaying) {
                this.pause();videoPlaying = false;
                $(".main-play-video").removeClass("video-pause");
            }
            else {
                this.play();videoPlaying = true;
                $(".main-play-video").addClass("video-pause");
            }
        });
	};
	$(document).ready(function() {
		$('.mz-productlisting .mz-productlisting-info .mz-productlisting-title').addClass('add-ellipsis');
		quickViewBind();
	});
	
	window.quickViewBind = quickViewBind;
	
	
});
