﻿/**
 * Unidirectional dispatch-driven collection views, for  your pleasure.
 */


define([
	'modules/jquery-mozu',
    'backbone',
    'underscore',
    'modules/url-dispatcher',
    'modules/intent-emitter',
    'modules/get-partial-view',
    'modules/product-quick-view',
    "modules/api",
    "modules/models-product",
    'modules/models-orders',
    'modules/recently-viewed',
    'hyprlive',
    "widgets/bazaarvoice"
], function($, Backbone, _, UrlDispatcher, IntentEmitter, getPartialView, quickViewBind, Api, ProductModels, OrderModels, recentlyViewed, Hypr) {

    function factory(conf) {

        var _$body = conf.$body;
        var _dispatcher = UrlDispatcher;
        var ROUTE_NOT_FOUND = 'ROUTE_NOT_FOUND';
    	function moreLessConfig(){
    		/*Code for moretag on facets - Start*/
            var facetHeight = Hypr.getThemeSetting('facetDefaultHeight');
            var facetHeightWithPX = Hypr.getThemeSetting('facetDefaultHeight')+'px';
            $('.mz-facetingform').find('.mz-l-sidebaritem .facet-details > ul').each(function(){
            	if($(this).height() > facetHeight) {
            		$(this).parent().find('.moretag').css('display', 'inline-block');
            		if($(this).closest('.mz-l-sidebaritem').hasClass('for-caregory')){
            			$(this).css({'height': '205px', 'overflow-y': 'hidden'});
            		}else {
            			$(this).css({'height': facetHeightWithPX, 'overflow-y': 'hidden'});
            		}
            		
            	}
            });
            $('.mz-facetingform').find('.mz-l-sidebaritem #Price > ul').css({'height': 'auto', 'overflow-y': 'hidden'});
            
            /*Code for moretag on facets - End*/
        }
        function updateUi(response) {
            var url = response.canonicalUrl;
            _$body.html(response.body);
            if (url) _dispatcher.replace(url);
            _$body.removeClass('mz-loading');
            
            $('.result-view-toggle li a').on('click', function (e){
            	e.preventDefault();
            	$(this).closest('.result-view-toggle').find('li').removeClass('is-current');
            	$(this).closest('li').addClass('is-current');
            	
            	if($(this).data('toggle-type') == 'list') {
            		window.gridStatus = "list";
            		$(document).find('.mz-productlist').addClass('list');
            	} else {
            		window.gridStatus = "";
            		$(document).find('.mz-productlist').removeClass('list');
            	}  
            	$(document).find('#expert-advice-tab .mz-productlist').removeClass('list');
           });
            
            if (window.gridStatus == "list") {
            	$('.result-view-toggle').find('li').removeClass('is-current');
            	$('.result-view-toggle').find('li:first-child').addClass('is-current');
            	$(document).find('.mz-productlist').addClass('list');
            } else {
            	$('.result-view-toggle').find('li').removeClass('is-current');
            	$('.result-view-toggle').find('li:nth-child(2)').addClass('is-current');
            	$(document).find('.mz-productlist').removeClass('list');
            }
            $(document).find('#expert-advice-tab .mz-productlist').removeClass('list');
            
            if ($("#mz-drop-zone-expert-advice-bropzone > .mz-cms-row > .mz-cms-col-12-12").html() && $("#mz-drop-zone-expert-advice-bropzone > .mz-cms-row > .mz-cms-col-12-12").html().length > 0) {
            	$("#mz-drop-zone-expert-advice-bropzone").addClass('isdropped');
            } else {
            	$("#mz-drop-zone-expert-advice-bropzone").removeClass('isdropped');
            }
            
            moreLessConfig();
            
            window.recentlyViewed();
            window.quickViewBind();
        }

        function showError(error) {
            // if (error.message === ROUTE_NOT_FOUND) {
            //     window.location.href = url;
            // }
            _$body.find('[data-mz-messages]').text(error.message);
        }
        
        var facetSearch = '';

        function intentToUrl(e) {
    		var elm = e.target;
            var url;
            if ($(elm).data('mz-action') == "sortAndFilter") {
            	e.preventDefault();
            	$(document).find('.filters').addClass('open');
            	moreLessConfig();
            }
            
            if ($(window).width() < 767) {
            	if ($(elm).data('mz-action') == "applyFacets") {
            		var desktopUrl = window.location.pathname + "?facetValueFilter=";
                    var selected = $(document).find('input[data-mz-facet-value]:checked');
                    
                    if ($("input[name='sortBy']:checked").length == 1) {
                    	var sortByValue = $("input[name='sortBy']:checked").data('mz-facet-value');
                    	sortByValue = sortByValue.replace(' ', '+');
                    	desktopUrl = window.location.pathname + '?sortBy=' + sortByValue + '&facetValueFilter=';
                    }
                    
                    selected.each(function(index) {   
                    	var facetName = $(selected[index]).data('mz-facet');
                    	var facetValue = $(selected[index]).data('mz-facet-value');
                    	if (facetName !=  'sortBy') {
                    		if(facetName == 'Price') {
                        		desktopUrl = desktopUrl + facetValue + ',';
                        	}else {
                        		desktopUrl = desktopUrl + facetName + '%3a' + facetValue + ',';
                        	}
                    	}                   	
                    	
                    });
                    
                    url = desktopUrl;
                    $('html, body').scrollTop(145);
                    return url;
            	} else if ($(elm).data('mz-action') == "clearFacets") {
            		$(document).find('.filters').removeClass('open');
            		 url = window.location.pathname;
            		 $('html, body').scrollTop(145);
                     return url;
            	} else {
            	    if (elm.tagName.toLowerCase() === "select") {
                        elm = elm.options[elm.selectedIndex];
                    }
                    url = elm.getAttribute('data-mz-url') || elm.getAttribute('href') || '';
                    if (url && url[0] != "/") {
                        var parser1 = document.createElement('a');
                        parser1.href = url;
                        url = window.location.pathname + parser1.search;
                    }
                    $('html, body').scrollTop(145);
                    return url;
            	}
            	
            } else {
            	
            	if (elm.tagName.toLowerCase() === "select") {
                    elm = elm.options[elm.selectedIndex];
                }
                url = elm.getAttribute('data-mz-url') || elm.getAttribute('href') || '';
                if (url && url[0] != "/") {
                    var parser = document.createElement('a');
                    parser.href = url;
                    url = window.location.pathname + parser.search;
                }
                $('html, body').scrollTop(145);
                return url;
        	}
            
           
            
            
            
        } 

        var navigationIntents = IntentEmitter(
            _$body,
            [
                'click [data-mz-pagingcontrols] a',
                'click [data-mz-pagenumbers] a',
                'click a[data-mz-facet-value]',
                'click [data-mz-action="clearFacets"]',
                'change input[data-mz-facet-value]',
                'change [data-mz-value="pageSize"]',
                'click [data-mz-value="sortBy"]',
                'click [data-mz-action="applyFacets"]',
                'click [data-mz-action="customPrice"]',
                'click [data-mz-action="sortAndFilter"]'
            ],
            intentToUrl
        );
        
        navigationIntents.on('data', function(url, e) {
            if (url && _dispatcher.send(url)) {
                _$body.addClass('mz-loading');
                e.preventDefault();
            }
        });

        _dispatcher.onChange(function(url) {
            getPartialView(url, conf.template).then(updateUi, showError);
        });

    }

    return {
        createFacetedCollectionViews: factory
    };

});