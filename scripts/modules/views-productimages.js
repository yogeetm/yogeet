﻿define(['modules/jquery-mozu', 'underscore', "modules/backbone-mozu", 'hyprlive'], function ($, _, Backbone, Hypr) {

    var ProductPageImagesView = Backbone.MozuView.extend({
        templateName: 'modules/product/product-images',
        events: {
            'click span[data-mz-productimage-thumb]': 'switchImage'
        },
        initialize: function () {
            // preload images
            var imageCache = this.imageCache = {},
                cacheKey = Hypr.engine.options.locals.siteContext.generalSettings.cdnCacheBustKey;
            _.each(this.model.get('content').get('productImages'), function (img) {
                var i = new Image();
                i.src = img.imageUrl + '?max=' + Hypr.getThemeSetting('productImagesContainerWidth') + '&_mzCb=' + cacheKey;
                if (img.altText) {
                    i.alt = img.altText;
                    i.title = img.altText;
                }
                imageCache[img.sequence.toString()] = i;
            });
            $('.cloud-zoom, .cloud-zoom-gallery').CloudZoom();
        },
        switchImage: function (e) {
        	// unbind and remove previous thumb zoom effect
            if($("#videoContainer").css("display") == "block"){
                $("#product-main-video")[0].pause();
                $("#videoContainer").toggleClass("show-video");
                $(".ign-product-videothumb").removeClass("is-selected");
                $("#imageContainer").show();
            }
        	$('.cloud-zoom, .cloud-zoom-gallery').unbind();
        	$('div.mousetrap, div#cloud-zoom-big').remove();
        	$('.cloud-zoom').removeData('zoom');
        	
            var $thumb = $(e.currentTarget);
            $(".mz-productimages-thumbimage").removeClass("is-selected");
            $thumb.find(".mz-productimages-thumbimage").addClass("is-selected");
            this.selectedImageIx = $thumb.data('mz-productimage-thumb');
            this.updateMainImage();
            
            return false;
        },
        updateMainImage: function () {
            if (this.imageCache[this.selectedImageIx]) {
                this.$('[data-mz-productimage-main]')
                    .prop('src', this.imageCache[this.selectedImageIx].src)
                    .prop('title', this.imageCache[this.selectedImageIx].title)
                    .prop('alt', this.imageCache[this.selectedImageIx].alt);
                
                // change zoom image to selected thumb
                this.$('.cloud-zoom')
                	.prop('href', this.imageCache[this.selectedImageIx].src)
                	.prop('title', this.imageCache[this.selectedImageIx].title);	
            }
            $('.cloud-zoom, .cloud-zoom-gallery').CloudZoom();
        },
        render: function () {
            Backbone.MozuView.prototype.render.apply(this, arguments);
            this.updateMainImage();
        }
    });


    return {
        ProductPageImagesView: ProductPageImagesView
    };

});