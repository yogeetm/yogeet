require(["modules/jquery-mozu", 
	"underscore", 
	"hyprlive", 
	"modules/backbone-mozu", 
	'modules/api',
	'hyprlivecontext'], 
	function ($, _, Hypr, Backbone, api, HyprLiveContext) {
	var clearFields = function(){
		$("#firstName, #lastName, #emailAddress, #number1, #number2, #number3, #message").val('');
	};
	$(document).ready(function() {	

		var emailReg = Backbone.Validation.patterns.email;
		
		var homeInstallStore = $.cookie('homeInstallStore');
		
		clearFields();
		
		var locale = require.mozuData('apicontext').headers['x-vol-locale'];
		
		var homeInstallView = Backbone.MozuView.extend({
			templateName: 'modules/common/home-install-view'	
		});
		
		if (homeInstallStore) {
			homeInstallStore = JSON.parse(homeInstallStore);
			homeInstallStore.locale = locale;
			var HomeInstallView = new homeInstallView({
			       el: $('#home-install-store'),
			       model: new Backbone.Model(homeInstallStore)
				});
			HomeInstallView.render();
		} else if (HyprLiveContext.locals.pageContext.purchaseLocation && HyprLiveContext.locals.pageContext.purchaseLocation.code){
			var storeCode = HyprLiveContext.locals.pageContext.purchaseLocation.code;
			var filtersString = "properties.storeServices in['Home Installs'] and properties.documentKey eq " + storeCode;
			var locationList = Hypr.getThemeSetting("storeDocumentListFQN");
			
			api.get('documentView', {
				listName: locationList,
				filter:filtersString
			}).then(function(response) {		
				if(response.data.items.length > 0) {
					
					var item = response.data.items[0];
					homeInstallStore = {
							code: item.name,
							homeInstallEmail: item.properties.homeInstallsEmail,
							name: item.properties.storeName,
							address2: item.properties.address2,
							city: item.properties.city,
							province: item.properties.province[0],
							postalCode:item.properties.postalCode,
							phone:item.properties.phone,
							locale: locale
					};
					
					var HomeInstallView = new homeInstallView({
					       el: $('#home-install-store'),
					       model: new Backbone.Model(homeInstallStore)
						});
					HomeInstallView.render();
				}
			});

		}
		
				
		$('#home-install-button').on('click',function(){
			var errors = false;
			if (!$("#firstName").val() || $("#firstName").val().trim() === ""){
				$("#firstName").addClass('is-invalid');
				$('[data-mz-validationmessage-for="firstName"]').text(Hypr.getLabel('fnameMissing'));
				errors = true;
			}else{
				$("#firstName").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="firstName"]').text('');
			}
			
			if (!$("#lastName").val() || $("#lastName").val().trim() === ""){
				$("#lastName").addClass('is-invalid');
				$('[data-mz-validationmessage-for="lastName"]').text(Hypr.getLabel('lnameMissing'));
				errors = true;
			}else{
				$("#lastName").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="lastName"]').text('');
			}
		
			if($('input[name="prefererd-method"]:checked').val() == 'emailAddress') {
				$('[data-mz-validationmessage-for="phoneNumber"]').text('');
				$(".phone-numbercontainer input").removeClass('is-invalid');
				if($("#emailAddress").val().trim() === ""){
					$("#emailAddress").addClass('is-invalid');
					$('[data-mz-validationmessage-for="emailAddress"]').text(Hypr.getLabel('emailMissing'));
					errors = true;                            
				}else if(!emailReg.test($("#emailAddress").val())){
					$("#emailAddress").addClass('is-invalid');
					$('[data-mz-validationmessage-for="emailAddress"]').text(Hypr.getLabel('emailMissing'));
					errors = true;
				}else{
					 $("#emailAddress").removeClass('is-invalid');
					 $('[data-mz-validationmessage-for="emailAddress"]').text('');
				}
			}
			
			 digitLength();
			 if($('input[name="prefererd-method"]:checked').val() == 'phoneNumber') {
				 $('[data-mz-validationmessage-for="emailAddress"]').text('');
				 $("#emailAddress").removeClass('is-invalid');
				 if (!$("#phoneNumber").val() || $("#phoneNumber").val() === ""){
					$(".phone-numbercontainer input").addClass('is-invalid');
					$('[data-mz-validationmessage-for="phoneNumber"]').text(Hypr.getLabel('phoneNumberMissing'));
					errors = true;
				}else{
					$(".phone-numbercontainer input").removeClass('is-invalid');
					$('[data-mz-validationmessage-for="phoneNumber"]').text('');
				}
			 }
						
			
			if(!$("#message").val() || $("#message").val().trim() === ""){
				$("#message").addClass('is-invalid');
				$('[data-mz-validationmessage-for="message"]').text(Hypr.getLabel('messageMissing'));
				errors = true;
			}else{
				$("#message").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="message"]').text('');
			}
			

			if(!$('#g-recaptcha-response').val()){
				$('[data-mz-validationmessage-for="captcha"]').text(Hypr.getLabel('missingCaptcha'));
				errors = true;
			}else{
				$('[data-mz-validationmessage-for="captcha"]').text('');
			}
			
			if(!homeInstallStore) {
				$('[data-mz-validationmessage-for="homeInstallStore"]').text(Hypr.getLabel('homeInstallStoreErorMessage'));
				errors = true;
			} else {
				$('[data-mz-validationmessage-for="homeInstallStore"]').text('');
			}
			
			 digitLength();
			 
			 if(!errors) {
				 $('#home-install-form').addClass('is-loading');
				 var data = {
						 firstName:$("#firstName").val().trim(),
						 lastName: $("#lastName").val().trim(),
						 email: $("#emailAddress").val().trim(),
						 phoneNumber: $('#phoneNumber').val(),
						 projectDescription: $("#message").val(),
						 preferredContactMethod: $('input[name="prefererd-method"]:checked').val(),
						 homeInstallStoreCode: homeInstallStore.code,
						 homeInstallStoreName: homeInstallStore.name,
						 homeInstallStoreEmail: homeInstallStore.homeInstallEmail,
						 locale: homeInstallStore.locale
						};
				 $.ajax({
					method: 'POST',
            		contentType: 'application/json; charset=utf-8',
            		url: Hypr.getThemeSetting('formPostingUrl') + 'home-install',
            		data: JSON.stringify(data),     
            		success:function(response){            			
            			$('#home-install-store').hide();
            			$('.home-install-form').hide();
            			$('#home-install-form').removeClass('is-loading');
            			$('.sucess-email-template').show();
            			$('html, body').scrollTop($('#goto-thankyou').offset().top);
            			var storeDetailStr = "<p class='agenda-bold store-name'>" + homeInstallStore.name +"</p>"+
							"<p class=''address'>" + homeInstallStore.address2 +"</p>"+
							"<p class='address'>" + homeInstallStore.city +", " + homeInstallStore.province +"</p>"+
							"<p class='address'>" + homeInstallStore.postalCode +"</p>"+
							"<p class='address'>" + homeInstallStore.phone +"</p>";
            				$(".store-info-details").append(storeDetailStr);
        			},
        			error: function (error) {
        				$('.error-email').show();
        			}
				 });
			 }
		});
		
		$(".digit-length").keypress(function(){
			  digitLength();
		  });
		  
		  
		  var digitLength = function(){ 
			//starts phonenumber 3 and 4 fields value
		        var inputQuantity = [];
		        $(function() {
		          $(".digit-length").each(function(i) {
		            inputQuantity[i]=this.defaultValue;
		             $(this).data("idx",i);
		          });
		          $(".digit-length").on("keyup", function (e) {
		            var $field = $(this),
		                val=this.value,
		                $thisIndex=parseInt($field.data("idx"),10); 
		            if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid") ) {
		            	$('[data-mz-validationmessage-for="phoneNumber"]').text(Hypr.getLabel('invalidPhoneNumber'));
		                this.value = inputQuantity[$thisIndex];
		                return;
		            } 
		            if (val.length > Number($field.attr("maxlength"))) {
		              val=val.slice(0, 5);
		              $field.val(val);
		            }
		            inputQuantity[$thisIndex]=val;
		            if($(this).val().length==$(this).attr("maxlength")){
		                $(this).next().next().focus();
		            }
		            
		          });    
		          	var num1 = $('#number1').val();
		            var num2 = $('#number2').val();
		            var num3 = $('#number3').val();
		            var number = num1 + num2 + num3;
		            if(num1 && num2 && num3) {
		            	$('#phoneNumber').val(number);
		            } else {
		            	$('#phoneNumber').val('');
		            }
		        });
			
		    };
		
		
		  	  
	});
});