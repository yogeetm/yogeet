define([
    'modules/backbone-mozu', 
    "modules/api", 
    'hyprlive', 
    'hyprlivecontext', 
    'modules/jquery-mozu', 
    'underscore', 
    'modules/models-customer', 
    'modules/views-paging', 
    'modules/editable-view',
    'modules/models-product',
    'creditcardvalidator',
    "modules/cart-monitor",
    'modules/models-orders', 
    'mappings/omsreturntoreturn',
    'mappings/omsordertoorder'
    ], 
function(Backbone, api, Hypr, HyprLiveContext, $, _, CustomerModels, PagingViews, EditableView, ProductModels, creditcardvalidator, CartMonitor, OrderModels, omsReturnToReturn, omsOrderToOrder) {
	var apiContext = require.mozuData('apicontext');
	var contextSiteId = apiContext.headers["x-vol-site"];
    var AccountSettingsView = EditableView.extend({
        templateName: 'modules/my-account/my-account-settings',
        render: function() {
        	var self = this;
            Backbone.MozuView.prototype.render.apply(this, arguments);
            var fName = document.getElementById('myaccntFname');
            if (fName) {
                fName.addEventListener('keyup', function (event) {
                    if (event.which === 32) {
                        fName.value = fName.value.replace(/^\s+/, '');
                        if (fName.value.length === 0) {
                            event.preventDefault();
                        }
                    }
                });
            }

            var lName = document.getElementById('myaccntLname');
            if (lName) {
                lName.addEventListener('keyup', function (event) {
                    if (event.which === 32) {
                        lName.value = lName.value.replace(/^\s+/, '');
                        if (lName.value.length === 0) {
                            event.preventDefault();
                        }
                    }
                    else if (fName.value == " ") {
                        event.preventDefault();
                    }
                });
            }
            
            var user = require.mozuData('user');
            if (!(user.isAnonymous)) {
                var cust = api.get('customer', {
                    id: user.accountId
                }).then(function(response) {
                    $('.display-FirstName').text(response.data.firstName);
                    $('.display-lastName').text(response.data.lastName);
                    $('.display-email').text(response.data.emailAddress);
                    $('.mz-accountsettings-firstname').val(response.data.firstName);
                    $('.mz-accountsettings-lastname').val(response.data.lastName);
                    $('.mz-accountsettings-email').val(response.data.emailAddress);
                });
            }
            //aeroplan member number
            
        },
        autoUpdate: [
            'firstName',
            'lastName',
            'emailAddress',
            'acceptsMarketing'
        ],
        constructor: function() {
            EditableView.apply(this, arguments);
            this.editing = false;
            this.invalidFields = {};
        },
        initialize: function() {
            return this.model.getAttributes().then(function(customer) {
                customer.get('attributes').each(function(attribute) {
                    attribute.set('attributeDefinitionId', attribute.get('id'));
                });
                return customer;
            });
        },
        updateAttribute: function(e) {
            var self = this;
            var attributeFQN = e.currentTarget.getAttribute('data-mz-attribute');
            var attribute = this.model.get('attributes').findWhere({
                attributeFQN: attributeFQN
            });
            var nextValue = attribute.get('inputType') === 'YesNo' ? $(e.currentTarget).prop('checked') : $(e.currentTarget).val();

            attribute.set('values', [nextValue]);
            attribute.validate('values', {
                valid: function(view, attr, error) {
                    self.$('[data-mz-attribute="' + attributeFQN + '"]').removeClass('is-invalid')
                        .next('[data-mz-validationmessage-for="' + attr + '"]').text('');
                },
                invalid: function(view, attr, error) {
                    self.$('[data-mz-attribute="' + attributeFQN + '"]').addClass('is-invalid')
                        .next('[data-mz-validationmessage-for="' + attr + '"]').text(error);
                }
            });
        },
        startEdit: function(event) {
            event.preventDefault();
            this.editing = true;
            this.render();
        },
        cancelEdit: function() {
            this.editing = false;
            this.afterEdit();
        },
        finishEdit: function() {
            var self = this;
            if ($('[data-mz-value="acceptsMarketing"]:checked').val() === 'on') {
                this.model.set('acceptsMarketing', true); 
            }
            self.changeValidationFlag(true);
            var emailReg = Backbone.Validation.patterns.email;
            var fnameVal = !self.model.validate("firstName") ? true : false,
            lnameVal = !self.model.validate("lastName") ? true : false,
            emailVal = !self.model.validate("emailAddress") ? true : false ;
            var emailExp = self.model.get("emailAddress");
            if (emailVal && fnameVal && lnameVal ) { 
                if(emailExp.match(emailReg)){
                    self.doModelAction('apiUpdate').then(function () {
                    self.editing = false;
                    self.changeValidationFlag(false);
                    $('#settings-success').html('<div class="success-msg success-msg-timeout agenda"><div class="alert alert-success">' + Hypr.getLabel('successProfile') + '</div></div>');
                    setTimeout(function() {$('.success-msg-timeout').fadeOut();}, 3000); 
                    $(document).trigger("changeUserName"); 
                }).otherwise(function () {
                    self.changeValidationFlag(false);
                    $('#settings-success').html('');
                    self.editing = true;
                }).ensure(function () {
                    self.changeValidationFlag(false);
                    setTimeout(function() {$('.msg-timeout').fadeOut();}, 5000);
                    if(self.messageView.model.models.length === 0) {
                        self.afterEdit();
                    }
                });
            }
                else{
                    $('#settings-success').html('<div class="success-msg success-msg-timeout agenda"><div class="alert alert-danger">' + Hypr.getLabel('emailMissing') + '</div></div>');
                    setTimeout(function() {$('.success-msg-timeout').fadeOut();}, 5000);
                }
            }
        },
        afterEdit: function() {
            var self = this;

            self.initialize().ensure(function() {
                self.render();
            });
        },
        changeValidationFlag: function(value) {
            this.model.validateFirstName = value;
            this.model.validateLastName = value;
            this.model.validateEmailAddress = value;
        }
    });

    var PasswordView = EditableView.extend({
        templateName: 'modules/my-account/my-account-password',
        autoUpdate: [
            'oldPassword',
            'password',
            'confirmPassword'
        ],
        additionalEvents: {
    		"click .show-hide-current" : 'showHideCurrent',
    		"click .show-hide-new" : 'showHideNewPwd',
    		"click .show-hide-confirm" : 'showHideConfirmPwd'
    	},
        render: function() {
            var self = this;
            Backbone.MozuView.prototype.render.apply(this, arguments);
        },
        showHideCurrent: function (e){
    		e.preventDefault();
    		e.stopPropagation();
            var inputElement = $("#account-oldpassword");
            if(inputElement.attr('type') == 'password'){
            	inputElement.attr('type', 'text');
        		$('.show-hide-current').text(Hypr.getLabel('hidePassword'));
            }else{
            	inputElement.attr('type', 'password');
            	$('.show-hide-current').text(Hypr.getLabel('show'));
            }
    	},
    	showHideNewPwd: function (e){
    		e.preventDefault();
    		e.stopPropagation();
            var inputElement = $("#account-password");
            if(inputElement.attr('type') == 'password'){
            	inputElement.attr('type', 'text');
        		$('.show-hide-new').text(Hypr.getLabel('hidePassword'));
            }else{
            	inputElement.attr('type', 'password');
    			$('.show-hide-new').text(Hypr.getLabel('show'));
            }
    	},
    	showHideConfirmPwd: function (e){
    		e.preventDefault();
    		e.stopPropagation();
            var inputElement = $("#account-confirmpassword");
            if(inputElement.attr('type') == 'password'){
            	inputElement.attr('type', 'text');
        		$('.show-hide-confirm').text(Hypr.getLabel('hidePassword'));
            }else{
            	inputElement.attr('type', 'password');
    			$('.show-hide-confirm').text(Hypr.getLabel('show'));
            }
    	},
        startEditPassword: function() {
            this.editing.password = true;
            this.render();
        },
        finishEditPassword: function() {
            var self = this;

            var isSuccess = false;
            var oldPassword = $('[data-mz-value="oldPassword"]').val();
            if( !oldPassword ) {
                $('[data-mz-validationmessage-for="oldPassword"]').html(Hypr.getLabel('oldPasswordMissing'));  //Validation message
                $('[data-mz-value="oldPassword"]').addClass('is-invalid');
                return false;
            } else {
                $('[data-mz-validationmessage-for="oldPassword"]').html('');  //Validation message
                $('[data-mz-value="oldPassword"]').removeClass('is-invalid');
            }
            this.doModelAction('changePassword').then(function() {
                _.delay(function() {
                    self.$('[data-mz-validationmessage-for="passwordChanged"]').show().text(Hypr.getLabel('passwordChanged')).fadeOut(3000);
                }, 250);
                $('#settings-success').html(Hypr.getLabel('successPassword'));
                isSuccess = true;
            }, function() {
            	$('#settings-success').html('');
                self.editing.password = true;
            });
            this.editing.password = false;
        },
        cancelEditPassword: function() {
            this.editing.password = false;
            this.render();
        }
    });
    
    var WishListView = EditableView.extend({
        templateName: 'modules/my-account/my-account-wishlist',
        addItemToCart: function(e) {
            var self = this,
                $target = $(e.currentTarget),
                id = $target.data('mzItemId');
            if (id) {
                this.editing.added = id;
                return this.doModelAction('addItemToCart', id);
            }
        },
        doNotRemove: function() {
            this.editing.added = false;
            this.editing.remove = false;
            this.render();
        },
        beginRemoveItem: function(e) {
            var self = this;
            var id = $(e.currentTarget).data('mzItemId');
            if (id) {
                this.editing.remove = id;
                this.render();
            }
        },
        finishRemoveItem: function(e) {
            var self = this;
            var id = $(e.currentTarget).data('mzItemId');
            if (id) {
                var removeWishId = id;
                return this.model.apiDeleteItem(id).then(function() {
                    self.editing.remove = false;
                    var itemToRemove = self.model.get('items').where({
                        id: removeWishId
                    });
                    if (itemToRemove) {
                        self.model.get('items').remove(itemToRemove);
                        self.render();
                    }
                });
            }
        }
    });
    var noOrderHistoryView = Backbone.MozuView.extend({
        templateName: "modules/my-account/no-order-history-view"
    });

    var OrderHistoryView = Backbone.MozuView.extend({
        templateName: "modules/my-account/order-history-list",
        initialize: function() {
            var me = this;
            var locationCodeOfOrder = [];
            var queryString = "";
            var accountModel = window.accountModel = CustomerModels.EditableCustomer.fromCurrent();
            me.model.isLoading(true);
            api.request("POST", "/oms/omsOrders",
                { page: 0, perPage: 5, customerId: accountModel.get('externalId'), sortBy: '-orderDate' })
                .then(function (data) {
                	if(data.collection && data.collection.length > 0) {
                		data.items = _.map(data.collection, function (item) {
                            return omsOrderToOrder(item);
                        });
                        
                        data.startIndex = (data.page - 1) * data.perPage;
                        data.pageSize = data.perPage;
                        data.pageCount = Math.ceil(data.totalCount / data.perPage);
                        me.model.set('startIndex', data.startIndex);
                        me.model.set('pageSize', data.pageSize);
                        me.model.set('pageCount', data.pageCount);
                        delete data.collection;
                        
                        data.items.forEach(function(order, index) {
                            var items = order.items; 
                            var totalQuantity = 0, EHFTotal = 0;
                            _.each(items, function(item, index) {
             	                if(item.product.productCode != 'EHF101') {
             	                	totalQuantity += item.quantity;
             	                }
                            });
                            data.items[index].totalItemsQuantity = totalQuantity;
//                            locationCodeOfOrder.push(items.models[0].get('fulfillmentLocationCode'));
                         });
                        me.model.set('items', data.items);
                        
                        locationCodeOfOrder = _.unique(locationCodeOfOrder);
                        locationCodeOfOrder.forEach(function(item,index){
                          if(index != locationCodeOfOrder.length-1){
                            queryString += "code eq "+item+" or ";
                          }else{
                            queryString += "code eq "+item;
                          }
                        });
                        
                        api.get('locations', {filter: queryString}).then(function(response){
                        	if(response && response.data.items.length > 0) {
                        		response.data.items.forEach(function(location) {
                        			data.items.forEach(function(order, index) {
//                              		 if(order.items.models[0].get('fulfillmentLocationCode') === location.code){
//                              			 me.model.set('currentStore', location);
//                              		   }
                              	   });
                        		});
                        		 //me.model.set(new Backbone.Model(data));
                                 me.model.set('showPaging', true);
                                 me.model.isLoading(false);
                                 me.render();
                        	}
                        });
                	} else {
                		 var NoOrderHistory = new noOrderHistoryView({
                             el: $('#account-orderhistory').find('[data-mz-orderlist]'),
                             model: new Backbone.Model()
                         });
                         NoOrderHistory.render();
                	}
                    
                    
            });
        },
        getRenderContext: function() {
            var context = Backbone.MozuView.prototype.getRenderContext.apply(this, arguments);
            context.returning = this.returning;
            if (!this.returning) {
                context.returning = [];
            }
            context.returningPackage = this.returningPackage;
            return context;
        },
        loadOMSReturnItems: function (orderNumber, target) {
            var order = this.model.get('items').findWhere({ id: orderNumber });

            return api.request("POST", "oms/omsReturns",
                { ids: order.get('omsOrder').orderID.toString() }
            ).then(function (resp) {
                if (resp.totalCount > 0) {
                    var returnOrderCollection = new OrderModels.OrderCollection({});

                    _.each(resp.collection, function (item) {
                        var ngOrder = new OrderModels.Order(omsReturnToReturn(item));
                        returnOrderCollection.add(ngOrder);
                    });

                    var returnHistoryView = new ReturnHistoryView({
                        el: $(target).find('.omsReturnHistoryItem'),
                        model: returnOrderCollection
                    });
                    returnHistoryView.render();
                }
            });
        },
        render: function() {
            var self = this;
            
            var frSiteId = Hypr.getThemeSetting("frSiteId");
	        if(frSiteId === contextSiteId){
	        	self.model.set("isFrenchSite", true);
	        }
            Backbone.MozuView.prototype.render.apply(this, arguments);

            var orderHistoryPagingControls = new PagingViews.PagingControls({
                templateName: 'modules/my-account/order-history-paging-controls',
                el: self.$el.find('[data-mz-pagingcontrols]'),
                model: self.model
            }),

            orderHistoryPageNumbers = new PagingViews.PageNumbers({
                el: self.$el.find('[data-mz-pagenumbers]'),
                model: self.model
            });

            $.each(this.$el.find('[data-mz-order-history-listing]'), function (index, val) {

                var url = window.location.href.indexOf('click=true');
                if (url != -1){
        	        if(getParameterByName('click') == 'true') {
        	            $('#orderhistory-tab').trigger('click');
        	        }
                }
                function getParameterByName(name) {
                	$('#order-history-tab-link').addClass('active'); 
                    $('#orderhistory').addClass('active');
                    $('#settings-tab-link').removeClass('active');
                    $('#settings').removeClass('active');
                    $(".mz-orderlist li[data-mz-order-history-listing]").addClass('hidden');
                    $(".mz-orderlist li:first").attr("id","current-order-detail").removeClass('hidden');
                    $('#current-order-detail').find('.mz-orderlisting').addClass('hidden');
                    $('#current-order-detail').find('.myaccount-vieworderdetail-container').removeClass('hidden');
                    $('.mz-l-stack-sectiontitle').addClass('hidden');
                    $('.view-all-orders').on('click', function(){
                	    $(".mz-orderlist li[data-mz-order-history-listing]").removeClass('hidden');
                    });
                }
                
            });

            _.invoke([orderHistoryPagingControls, orderHistoryPageNumbers], 'render');
        },
        selectReturnItems: function() {
            if (typeof this.returning == 'object') {
                $.each(this.returning, function(index, value) {
                    $('[data-mz-start-return="' + value + '"]').prop('checked', 'checked');
                });
            }
        },
        addReturnItem: function(itemId) {
            if (typeof this.returning == 'object') {
                this.returning.push(itemId);
                return;
            }
            this.returning = [itemId];
        },
        removeReturnItem: function(itemId) {
            if (typeof this.returning == 'object') {
                if (this.returning.length === 0) {
                    delete this.returning;
                } else {
                    var itemIdx = this.returning.indexOf(itemId);
                    if (itemIdx != -1) {
                        this.returning.splice(itemIdx, 1);
                    }
                }
            }
        }
    });

    var ReturnOrderListingView = Backbone.MozuView.extend({
        templateName: "modules/my-account/order-history-listing-return",
        getRenderContext: function() {
            var context = Backbone.MozuView.prototype.getRenderContext.apply(this, arguments);
            var order = this.model;
            if (order) {
                this.order = order;
                context.order = order.toJSON();
            }
            return context;
        },
        render: function() {
            var self = this;
            var returnItemViews = [];

            self.model.fetchReturnableItems().then(function(data) {
                var shippedItems = [];
                var quantityReturnable;
                var mainBundleDataItem;
                var shippedItem;
                var dataItem;
                var k, a, x;
                var numPackages = self.model.apiModel.data.packages.length;
                var numPickups = self.model.apiModel.data.pickups.length;
                var numDigital = self.model.apiModel.data.digitalPackages.length;

                var dataItem1;
                var dataItem2;
                for (var w = 0; w < data.totalCount; w++) {
                    dataItem1 = data.items[w];
                    dataItem1.quantityReturnable = 0;
                    for (x = 0; x < data.totalCount; x++) {
                        dataItem2 = data.items[x];
                        if (x !== w) {
                            if (dataItem1.orderLineId === dataItem2.orderLineId && dataItem1.productCode === dataItem2.productCode &&
                                dataItem1.orderItemOptionAttributeFQN === dataItem2.orderItemOptionAttributeFQN &&
                                dataItem1.parentProductCode === dataItem2.parentProductCode) {
                                if (dataItem2.excludeProductExtras === true) {
                                    data.totalCount -= 1;
                                    data.items.splice(x, 1);
                                }
                            }
                        }
                    }
                }

                for (var i = 0; i < numPackages; i++) {
                    var shippedPackage = self.model.apiModel.data.packages[i];
                    var numPackageItems = shippedPackage.items.length;
                    for (var m = 0; m < numPackageItems; m++) {
                        shippedItems.push(shippedPackage.items[m]);
                    }
                }

                for (var z = 0; z < numDigital; z++) {
                    var digital = self.apiModel.data.digitalPackages[z];
                    var numDigitalItems = digital.items.length;
                    for (var y = 0; y < numDigitalItems; y++) {
                        shippedItems.push(digital.items[y]);
                    }
                }

                for (var j = 0; j < numPickups; j++) {
                    var pickup = self.model.apiModel.data.pickups[j];
                    var numPickupItems = pickup.items.length;
                    for (var n = 0; n < numPickupItems; n++) {
                        shippedItems.push(pickup.items[n]);
                    }
                }

                var numShippedItems = shippedItems.length;
                for (var q = 0; q < numShippedItems; q++) {
                    shippedItem = shippedItems[q];
                    if (shippedItem.orderID + '' === self.model.omsOrderID + '') {
                        for (k = 0; k < data.totalCount; k++) {
                            dataItem = data.items[k];
                            if (dataItem.orderItemId === shippedItem.ngOrderItemID && dataItem.productCode === shippedItem.productCode) {
                                quantityReturnable = shippedItem.quantity - shippedItem.returnQuantity;
                                if (dataItem.orderItemOptionAttributeFQN) {
                                    dataItem.quantityReturnable = 0;
                                }

                                if (!dataItem.parentProductCode) {
                                    if (!dataItem.orderItemID) {
                                        dataItem.quantityReturnable = quantityReturnable;
                                        dataItem.quantity = quantityReturnable;
                                        dataItem.shipmentID = shippedItem.shipmentID;
                                        dataItem.orderID = shippedItem.orderID;
                                        dataItem.orderItemID = shippedItem.orderItemID;
                                    } else {
                                        dataItem.quantityReturnable += quantityReturnable;
                                        dataItem.quantity += ',' + quantityReturnable;
                                        dataItem.shipmentID += ',' + shippedItem.shipmentID;
                                        dataItem.orderID += ',' + shippedItem.orderID;
                                        dataItem.orderItemID += ',' + shippedItem.orderItemID;
                                    }
                                } else {
                                    for (a = 0; a < data.totalCount; a++) {
                                        mainBundleDataItem = data.items[a];
                                        if (dataItem.parentProductCode === mainBundleDataItem.productCode) {
                                            if (!mainBundleDataItem.components) {
                                                mainBundleDataItem.components = [];
                                            }
                                            mainBundleDataItem.components.push({
                                                unitQuantity: dataItem.unitQuantity,
                                                quantity: quantityReturnable,
                                                shipmentID: shippedItem.shipmentID,
                                                orderID: shippedItem.orderID,
                                                orderItemID: shippedItem.orderItemID,
                                                productCode: dataItem.productCode
                                            });
                                            break;
                                        }
                                    }
                                }
                                break;
                            }
                        }
                    }
                }

                var returnableItems = self.model.returnableItems(data.items);
                if (self.model.getReturnableItems().length < 1) {
                    self.trigger('renderMessage', {
                        messageType: 'noReturnableItems'
                    });
                    //self.$el.find('[data-mz-message-for="noReturnableItems"]').show().text(Hypr.getLabel('noReturnableItems')).fadeOut(6000);
                    return false;
                }

                self.setElement($(document).find('.listing[data-mz-oms-id=' + self.model.omsOrderID + ']')[0]);
                Backbone.MozuView.prototype.render.apply(self, arguments);

                $.each(self.$el.find('[data-mz-order-history-listing-return-item]'), function(index, val) {
                    var packageItem = _.find(returnableItems, function(model) {
                        if($(val).data('mzOrderLineId') === model.get('orderLineId')){
                            if ($(val).data('mzOptionAttributeFqn')) {
                                return (model.get('orderItemOptionAttributeFQN') === $(val).data('mzOptionAttributeFqn') && model.uniqueProductCode() === $(val).data('mzProductCode'));
                            }
                            return (model.uniqueProductCode() === $(val).data('mzProductCode') + "");
                        }
                        return false;
                    });

                    returnItemViews.push(new ReturnOrderItemView({
                        el: this,
                        model: packageItem
                    }));
                });

                _.invoke(returnItemViews, 'render');

                if (self.model.getReturnableItems().length === 1) {
                    var singleCheckbox = self.$el.find('[data-mz-value="isSelectedForReturn"]');
                    singleCheckbox.click();
                    singleCheckbox.css('visibility', 'hidden');
                }
            });

        },
        clearOrderReturn: function() {
            this.model.clearReturn();

        },
        cancelOrderReturn: function() {
            this.clearOrderReturn();
            this.trigger('returnCancel');
        },
        finishOrderReturn: function() {
            var self = this;

            var op = this.model.finishReturn();
            if (op) {
                if (op[0] !== null) {
                    var errors = op[0];
                    var returnItems = self.$('[data-mz-order-history-listing-return-item]');
                    var numReturnItems = returnItems.length;
                    for (var i = 0; i < numReturnItems; i++) {
                        var returnItem = returnItems[i];

                        $(returnItem).find('[data-mz-validationmessage-for="rmaReason"]').hide();
                        $(returnItem).find('[data-mz-validationmessage-for="rmaReason"]').hide();
                        $(returnItem).find('[data-mz-validationmessage-for="rmaQuantity"]').hide();

                        var numAttributes = returnItem.attributes.length;
                        for (var k = 0; k < numAttributes; k++) {
                            if (returnItem.attributes[k].name === 'data-mz-order-line-id') {
                                var itemErrors = errors[returnItem.attributes[k].value];
                                if (typeof(itemErrors) !== 'undefined') {
                                    var numItemErrors = itemErrors.length;
                                    for (var j = 0; j < numItemErrors; j++) {
                                        var itemError = itemErrors[j];
                                        if (itemError === 'rmaReason') {
                                            $(returnItem).find('[data-mz-validationmessage-for="rmaReason"]').show().text(Hypr.getLabel('enterReturnReason'));
                                            continue;
                                        }
                                        if (itemError === 'rmaComments') {
                                            $(returnItem).find('[data-mz-validationmessage-for="rmaReason"]').show().text(Hypr.getLabel('enterOtherComments'));
                                            continue;
                                        }
                                        if (itemError === 'rmaQuantity') {
                                            $(returnItem).find('[data-mz-validationmessage-for="rmaQuantity"]').show().text(Hypr.getLabel('enterReturnQuantity'));
                                        }
                                    }
                                }
                                break;
                            }
                        }
                    }

                    self.model.isLoading(false);
                    //this.trigger('returnFailure');
                } else {
                    return op[1].then(function (data) {
                        window.location.reload(true);
                    }, function () {
                        self.model.isLoading(false);
                    self.clearOrderReturn();
                        self.trigger('returnFailure');
                });
            }
            } else {
                self.model.isLoading(false);
            }
        }
    });

    var ReturnOrderItemView = Backbone.MozuView.extend({
        templateName: "modules/my-account/order-history-listing-return-item",
        autoUpdate: [
            'isSelectedForReturn',
            'rmaReturnType',
            'rmaReason',
            'rmaQuantity',
            'rmaComments'
        ],
        dataTypes: {
            'isSelectedForReturn': Backbone.MozuModel.DataTypes.Boolean
        },
        startReturnItem: function(e) {
            var $target = $(e.currentTarget);

            if (this.model.uniqueProductCode()) {
                if (!e.currentTarget.checked) {
                    this.model.set('isSelectedForReturn', false);
                    //var itemDetails = packageItem.getItemDetails();
                    this.model.cancelReturn();
                    this.render();

                    return;
                }

                this.model.set('isSelectedForReturn', true);
                this.model.startReturn();
                //this.render();
            }
        },
        render: function() {
            Backbone.MozuView.prototype.render.apply(this, arguments);
        }
    });

    var ReturnHistoryView = Backbone.MozuView.extend({
        templateName: "modules/my-account/return-history-list",
        initialize: function() {
            var self = this;
            this.listenTo(this.model, "change:pageSize", _.bind(this.model.changePageSize, this.model));
            this.listenTo(this.model, 'returndisplayed', function(id) {
                var $retView = self.$('[data-mz-id="' + id + '"]');
                if ($retView.length === 0) $retView = self.$el;
                $retView.ScrollTo({
                    axis: 'y'
                });
            });
        },
        printReturnLabel: function(e) {
            var self = this,
                $target = $(e.currentTarget);

            //Get Whatever Info we need to our shipping label
            var returnId = $target.data('mzReturnid'),
                returnObj = self.model.get('items').findWhere({
                    id: returnId
                });

            var printReturnLabelView = new PrintView({
                model: returnObj
            });

            var _totalRequestCompleted = 0;

            _.each(returnObj.get('packages'), function(value, key, list) {
                window.accountModel.apiGetReturnLabel({
                    'returnId': returnId,
                    'packageId': value.id,
                    'returnAsBase64Png': true
                }).then(function(data) {
                    value.labelImageSrc = 'data:image/png;base64,' + data;
                    _totalRequestCompleted++;
                    if (_totalRequestCompleted == list.length) {
                        printReturnLabelView.render();
                        printReturnLabelView.loadPrintWindow();
                    }
                });
            });

        }
    });

    var PrintView = Backbone.MozuView.extend({
        templateName: "modules/my-account/my-account-print-window",
        el: $('#mz-printReturnLabelView'),
        initialize: function() {},
        loadPrintWindow: function() {
            var host = HyprLiveContext.locals.siteContext.cdnPrefix,
                printScript = host + "/scripts/modules/print-window.js",
                printStyles = host + "/stylesheets/modules/my-account/print-window.css";

            var my_window,
                self = this,
                width = window.screen.width - (window.screen.width / 2),
                height = window.screen.height - (window.screen.height / 2),
                offsetTop = 200,
                offset = window.screen.width * 0.25;


            my_window = window.open("", 'mywindow' + Math.random() + ' ', 'width=' + width + ',height=' + height + ',top=' + offsetTop + ',left=' + offset + ',status=1');
            my_window.document.write('<html><head>');
            my_window.document.write('<link rel="stylesheet" href="' + printStyles + '" type="text/css">');
            my_window.document.write('</head>');

            my_window.document.write('<body>');
            my_window.document.write($('#mz-printReturnLabelView').html());

            my_window.document.write('<script src="' + printScript + '"></script>');

            my_window.document.write('</body></html>');
        }
    });

    var scrollBackUp = _.debounce(function () {
        $('#orderhistory').ScrollTo({ axis: 'y', offsetTop: Hypr.getThemeSetting('gutterWidth') });
    }, 100);
    var OrderHistoryPageNumbers = PagingViews.PageNumbers.extend({
        previous: function () {
            var op = PagingViews.PageNumbers.prototype.previous.apply(this, arguments);
            if (op) op.then(scrollBackUp);
        },
        next: function () {
            var op = PagingViews.PageNumbers.prototype.next.apply(this, arguments);
            if (op) op.then(scrollBackUp);
        },
        page: function () {
            var op = PagingViews.PageNumbers.prototype.page.apply(this, arguments);
            if (op) op.then(scrollBackUp);
        }
    });

    var scrollBackUpReturns = _.debounce(function () {
        $('#returnhistory').ScrollTo({ axis: 'y', offsetTop: Hypr.getThemeSetting('gutterWidth') });
    }, 100);
    var ReturnHistoryPageNumbers = PagingViews.PageNumbers.extend({
        previous: function () {
            var op = PagingViews.PageNumbers.prototype.previous.apply(this, arguments);
            if (op) op.then(scrollBackUpReturns);
        },
        next: function () {
            var op = PagingViews.PageNumbers.prototype.next.apply(this, arguments);
            if (op) op.then(scrollBackUpReturns);
        },
        page: function () {
            var op = PagingViews.PageNumbers.prototype.page.apply(this, arguments);
            if (op) op.then(scrollBackUpReturns);
        }
    });

    var PaymentMethodsView = EditableView.extend({
        templateName: "modules/my-account/my-account-paymentmethods",
        additionalEvents: {
        	"change [data-mz-value='editingContact.address.countryCode']" : "changeProvince",
        	"keyup [data-mz-value='editingCard.cardNumberPartOrMask']" : "detectCardType"
        },
        autoUpdate: [
            'editingCard.isDefaultPayMethod',
            'editingCard.paymentOrCardType',
            'editingCard.nameOnCard',
            'editingCard.cardNumberPartOrMask',
            'editingCard.expireMonth',
            'editingCard.expireYear',
            'editingCard.cvv',
            'editingCard.isCvvOptional',
            'editingCard.contactId',
            'editingContact.firstName',
            'editingContact.lastNameOrSurname',
            'editingContact.address.address1',
            'editingContact.address.address2',
            'editingContact.address.address3',
            'editingContact.address.cityOrTown',
            'editingContact.address.countryCode',
            'editingContact.address.stateOrProvince',
            'editingContact.address.postalOrZipCode',
            'editingContact.address.addressType',
            'editingContact.phoneNumbers.home',
            'editingContact.isBillingContact',
            'editingContact.isPrimaryBillingContact',
            'editingContact.isShippingContact',
            'editingContact.isPrimaryShippingContact'
        ],
        addNewAddress: function(e){
        	var id = e.currentTarget.getAttribute('value');
        	this.model.set('contactId', id);
        	if (id === 'new') {
        		$(document).find('.address-form').removeClass('hidden');
        	} else {
        		$(document).find('.address-form').addClass('hidden');
        	}
        },
        beginEditCard: function(e) {
            var id = this.editing.card = e.currentTarget.getAttribute('data-mz-card');
            window.id = id;
            this.model.beginEditCard(id);
            this.render(id);
            
        },
        finishEditCard: function() {
            var self = this;
            var operation = this.doModelAction('saveCard');
            $(window).scrollTop(0);
            if (operation) {
            	$('#account-messages').hide();
                operation.otherwise(function() {
                    self.editing.card = true;
                });
                
                this.editing.card = false;
                $('[id*="edit-card-modal-"]').modal('hide');
            }
            
        },
        changeProvince: function(e) {
        	$(e.currentTarget).closest('.address-form').find('div[class*="province-"]').addClass('hidden');
        	if($(e.currentTarget).val() === 'US') {
        		$(document).find('.province-us').removeClass('hidden');
        		$(document).find('.province-other').addClass('hidden');
        		$(document).find('.province-ca').addClass('hidden');
        	}else if($(e.currentTarget).val() === 'CA') {
        		$(document).find('.province-ca').removeClass('hidden');
        		$(document).find('.province-us').addClass('hidden');
        		$(document).find('.province-other').addClass('hidden');
        	}else {
        		$(document).find('.province-other').removeClass('hidden').find('input').val('');
        		$(document).find('.province-ca').addClass('hidden');
        		$(document).find('.province-us').addClass('hidden');
        	}
        	
        },
        cancelEditCard: function() {
            this.editing.card = false;
            this.model.endEditCard();
            $('[id*="edit-card-modal-"]').modal('hide');
            if (window.id === 'new') {
            	$(window).scrollTop(0);
                this.render();
            }
        },
        beginDeleteCard: function(e) {
        	var id = e.currentTarget.getAttribute('data-mz-card'),
            card = this.model.get('cards').get(id);
        	$('#delete-card-modal-'+id).modal();
        },
        removeDeleteCard:function(e){
        	var self = this,
            id = e.currentTarget.getAttribute('data-mz-card'),
            card = this.model.get('cards').get(id);
        	if (card) {
        		window.cardDeleted = true;
                this.doModelAction('deleteCard', id);                
            }
        },
        showBillingAddresses:function(e) {
        	$(e.currentTarget).closest('.billing-address-summary').addClass('hidden');
        	$(document).find('.mz-creditcard-billingaddresses').removeClass('hidden');
        },
        deleteAllCardClasses: function(e) {
        	var classNames =  $(e.currentTarget).siblings('.defaultCardImg').attr("class").split(" ");
    	    var newclasses =[];
    	    var classToRemove = '';
    	    for(var i=0; i<classNames.length; i++) {
    	    	classToRemove = classNames[i].search(/show+/);
    	        if(classToRemove) newclasses[newclasses.length] = classNames[i];
    	    }
    	    return newclasses;
        },
        detectCardType: function(e) {
        	var me = this;
        	if($.trim($(e.currentTarget).val()) === ''){
        		var newcl = me.deleteAllCardClasses(e);
        	    $(e.currentTarget).siblings('.defaultCardImg').removeClass().addClass(newcl.join(" ")).addClass('showDefaultCard'); 
        	}else {
        		$(e.currentTarget).validateCreditCard(function(result) {
	           		 if(result.card_type){
	           			var newcl = [];
	           			 switch (result.card_type.name) {
	           			 	case 'visa' :
	           			 		newcl = me.deleteAllCardClasses(e);
	           			 		$(e.currentTarget).siblings('.defaultCardImg').removeClass().addClass(newcl.join(" ")).addClass('showVisaImg');
	           			 		me.model.set('editingCard.paymentOrCardType', 'VISA');
	           			 		break;
	           			 	case 'mastercard' :
		           				newcl = me.deleteAllCardClasses(e);
	           			 		$(e.currentTarget).siblings('.defaultCardImg').removeClass().addClass(newcl.join(" ")).addClass('showMastercardImg');
	           			 		me.model.set('editingCard.paymentOrCardType', 'MC');
	           			 		break;
	           			 	case 'amex' :
		           				newcl = me.deleteAllCardClasses(e);
	           			 		$(e.currentTarget).siblings('.defaultCardImg').removeClass().addClass(newcl.join(" ")).addClass('showAmexImg');
	           			 		me.model.set('editingCard.paymentOrCardType', 'AMEX');
	           			 		break;
	           			 	case 'discover' :
		           				newcl = me.deleteAllCardClasses(e);
	           			 		$(e.currentTarget).siblings('.defaultCardImg').removeClass().addClass(newcl.join(" ")).addClass('showDiscoverImg');
	           			 		me.model.set('editingCard.paymentOrCardType', 'DISCOVER');
	           			 		break;
	           			 	default : 
		           			 	newcl = me.deleteAllCardClasses(e);
		                	    $(e.currentTarget).siblings('.defaultCardImg').removeClass().addClass(newcl.join(" ")).addClass('showDefaultCard');
		                	    break;
	           			 }
	           		 }
                });
        	}
        },
        
        render: function(id) {
            var me = this;    
        	Backbone.MozuView.prototype.render.apply(this, arguments);     
        	
        	if(window.cardDeleted) {
        		$(document).find('.card-deleted').removeClass('hidden');
        		window.cardDeleted = false;
        	} else {
                $(document).find('.card-deleted').addClass('hidden');
                $(".modal-backdrop").remove();
                $(".mz-myaccount").removeClass("modal-open");
                $(".mz-myaccount").css({"padding-right":"0px"});
        	}
        	
        	if(typeof id === 'string'){
        		if (id != "new" ) {
        			$('#edit-card-modal-'+id).modal();
        		}        		
        	}
        }
    });

    var AddressBookView = EditableView.extend({
        templateName: "modules/my-account/my-account-addressbook",
        autoUpdate: [
            'editingContact.firstName',
            'editingContact.lastNameOrSurname',
            'editingContact.address.address1',
            'editingContact.address.address2',
            'editingContact.address.address3',
            'editingContact.address.cityOrTown',
            'editingContact.address.countryCode',
            'editingContact.address.stateOrProvince',
            'editingContact.address.postalOrZipCode',
            'editingContact.address.addressType',
            'editingContact.phoneNumbers.home',
            'editingContact.isBillingContact',
            'editingContact.isPrimaryBillingContact',
            'editingContact.isShippingContact',
            'editingContact.isPrimaryShippingContact'
        ],
        renderOnChange: [
            'editingContact.address.countryCode',
            'editingContact.isBillingContact',
            'editingContact.isShippingContact'
        ],
        beginAddContact: function() {
            this.editing.contact = "new";
            this.render();
        },
        beginEditContact: function(e) {
            var id = this.editing.contact = e.currentTarget.getAttribute('data-mz-contact');
            this.model.beginEditContact(id);
            this.render();
        },
        finishEditContact: function() {
            var self = this,
                isAddressValidationEnabled = HyprLiveContext.locals.siteContext.generalSettings.isAddressValidationEnabled;
            var operation = this.doModelAction('saveContact', {
                forceIsValid: isAddressValidationEnabled
            }); // hack in advance of doing real validation in the myaccount page, tells the model to add isValidated: true
            if (operation) {
                operation.otherwise(function() {
                    self.editing.contact = true;
                });
                this.editing.contact = false;
            }
        },
        cancelEditContact: function() {
            this.editing.contact = false;
            this.model.endEditContact();
            this.render();
        },
        beginDeleteContact: function(e) {
            var self = this,
                contact = this.model.get('contacts').get(e.currentTarget.getAttribute('data-mz-contact')),
                associatedCards = this.model.get('cards').where({
                    contactId: contact.id
                }),
                windowMessage = Hypr.getLabel('confirmDeleteContact', contact.get('address').get('address1')),
                doDeleteContact = function() {
                    return self.doModelAction('deleteContact', contact.id);
                },
                go = doDeleteContact;


            if (associatedCards.length > 0) {
                windowMessage += ' ' + Hypr.getLabel('confirmDeleteContact2');
                go = function() {
                    return self.doModelAction('deleteMultipleCards', _.pluck(associatedCards, 'id')).then(doDeleteContact);
                };

            }

            if (window.confirm(windowMessage)) {
                return go();
            }
        }
    });

    var StoreCreditView = Backbone.MozuView.extend({
        templateName: 'modules/my-account/my-account-storecredit',
        addStoreCredit: function(e) {
            var self = this;
            var id = this.$('[data-mz-entering-credit]').val();
            if (id) return this.model.addStoreCredit(id).then(function() {
                return self.model.getStoreCredits();
            });
        }
    });

    // Preferred Store view
    var PreferredStoreView = Backbone.MozuView.extend({
        templateName: 'modules/location/preferred-store',
         additionalEvents: {
            "click .changePreferredStore": "changePreferredStore"
        },
        changePreferredStore: function(e){
            e.preventDefault();
            window.location.href="/store-locator?returnUrl="+window.location.pathname;
        },
        callStore: function(e){
            var screenWidth = window.matchMedia("(max-width: 767px)");
            if(!screenWidth.matches){
                e.preventDefault();
            }
        }
    });
    
    $(document).ready(function() {
    	CartMonitor.update();
        var accountModel = window.accountModel = CustomerModels.EditableCustomer.fromCurrent();

        $('#aeroplan-member-edit').on('click',function(){
        	var self = $('#signupAeroplanSection');
	        if(self.hasClass('hidden')){
		    	$(self).removeClass('hidden').addClass('aeroplanAvailable');
		    	$('#aeroplan-number-available').addClass('hidden');
	        }else{
	        	$(self).addClass('hidden').removeClass('aeroplanAvailable');
	        	$('#aeroplan-number-available').removeClass('hidden');
	        }
        });
        
        //Aeroplan Section Start
        $("#signupAeroplanLastName").attr("value", accountModel.get('lastName'));
        var aeroplanAvailable = $('#signupAeroplanSection').hasClass('aeroplanAvailable');
	        var aeroplanNumber = _.find(accountModel.get('attributes').toJSON(), function (attribute) { 
		        return attribute.fullyQualifiedName === Hypr.getThemeSetting('aeroplanMemberNumber');
		    });
	        if (aeroplanNumber &&  aeroplanNumber.values[0] != "N/A"){ 
	        	$('#no-aeroplan').addClass('hidden');
	        	$('#aeroplan-number-available').removeClass('hidden');
	        	var aeroplanValue = aeroplanNumber.values[0];
		        $("#signupAeroplanNumberInput").attr("value", aeroplanNumber.values[0]);
		        var displayNumber = aeroplanValue.replace(/(\d{1})(.*)(\d{3})/, '*** *** $3');
		        $('#aeroplan-beforedisplay').text(aeroplanValue);
		        $('#aeroplan-display').text(displayNumber);
		        $('#aeroplan-beforeLastName').text(accountModel.get('lastName'));
		        $('#aeroplan-LastName').text(accountModel.get('lastName'));
		        var aeroplanNum1 = aeroplanValue.substring(0, 3);
		        var aeroplanNum2 = aeroplanValue.substring(3, 6);
		        var aeroplanNum3 = aeroplanValue.substring(6, 9);
		        $('#number1').attr("value", aeroplanNum1);
		        $('#number2').attr("value", aeroplanNum2);
		        $('#number3').attr("value", aeroplanNum3);
	        }else{
	        	$("#signupAeroplanNumberInput").attr("value"," ");
	        	$('#aeroplan-number-available').addClass('hidden');
	        	$('#no-aeroplan').removeClass('hidden');
	        }
	      //starts aeroplan 3 fields value 
	        var inputQuantity = [];
	        $(function() {
	          $(".digit-length").each(function(i) {
	            inputQuantity[i]=this.defaultValue;
	             $(this).data("idx",i);
	          });
	          $(".digit-length").on("keyup", function (e) {
	            var $field = $(this),
	                val=this.value,
	                $thisIndex=parseInt($field.data("idx"),10); 
	            if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid") ) {
	                this.value = inputQuantity[$thisIndex];
	                return;
	            } 
	            if (val.length > Number($field.attr("maxlength"))) {
	              val=val.slice(0, 5);
	              $field.val(val);
	            }
	            inputQuantity[$thisIndex]=val;
	            if($(this).val().length==$(this).attr("maxlength")){
	                $(this).next().focus();
	            }
	            var num1 = $('#number1').val();
	            var num2 = $('#number2').val();
	            var num3 = $('#number3').val();
	            var number = num1 + num2 + num3;
	            $('#signupAeroplanNumberInput').val(number);
	            
	          });
	          
	          $("#mz-payment-credit-card-number").each(function(i) {
		            inputQuantity[i]=this.defaultValue;
		             $(this).data("idx",i);
		          });
		          $("#mz-payment-credit-card-number").on("keyup", function (e) {
		            var $field = $(this),
		                val=this.value,
		                $thisIndex=parseInt($field.data("idx"),10); 
		            if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid") ) {
		                this.value = inputQuantity[$thisIndex];
		                return;
		            } 
		            if (val.length > Number($field.attr("maxlength"))) {
		              val=val.slice(0, 5);
		              $field.val(val);
		            }
		            inputQuantity[$thisIndex]=val;
		            if($(this).val().length==$(this).attr("maxlength")){
		                $(this).next().focus();
		            }
		          });
	          
	          
	          
	        });
	    //end aeroplan 3 fields value
        
	    $('#aeroplan-member').on("click",function(e){
	    	$('#aeroplan-number-available').addClass('hidden');
	    });    
        $('[data-mz-action="cancelAeroplanEdit"]').on("click",function(e){
        		$('#signupAeroplanSection').removeClass('aeroplanAvailable').addClass('hidden');
            	$('#aeroplan-member').attr('checked', false);
            	if ($('#signupAeroplanNumberInput') != " "){ 
            		var aeroplanLastName = $('#aeroplan-beforeLastName').text();
            		var aeroplanSetValue = $('#aeroplan-beforedisplay').text();
            		var aeroplanNum1 = aeroplanSetValue.substring(0, 3);
    		        var aeroplanNum2 = aeroplanSetValue.substring(3, 6);
    		        var aeroplanNum3 = aeroplanSetValue.substring(6, 9);
            		$('#number1').val(aeroplanNum1);
    		        $('#number2').val(aeroplanNum2);
    		        $('#number3').val(aeroplanNum3);
    		        $('#signupAeroplanLastName').val(aeroplanLastName);
            		$('#aeroplan-number-available').removeClass('hidden');
            	}else{
            		$('#aeroplan-number-available').addClass('hidden');
            	}
        });
        $('[data-mz-action="finishAeroplanEdit"]').on("click",function(e){
        	var aeroplanValidNumber = $('[data-mz-validate-aeroplan-number]').val();
            var aeroplanLastName = $('[data-mz-aeroplan-lastname]').val();
            var finalValue = aeroplanValidNumber.replace(/(\d{1})(.*)(\d{3})/, '*** *** $3');
            $('#aeroplan-beforedisplay').text(aeroplanValidNumber);
            $('#aeroplan-display').text(finalValue);
            $('#aeroplan-beforeLastName').text(aeroplanLastName);
	        $('#aeroplan-LastName').text(aeroplanLastName);
             if(aeroplanValidNumber === '' || aeroplanValidNumber.length !== 9 ){
            	 $('#signupAeroplanNumberInput').addClass('is-invalid');
            	 $('.digit-length').addClass('is-invalid');
            	 $('[data-mz-validationmessage-for="aeroplanNumber"]').text(Hypr.getLabel('aeroplanNumberMissingInvalid'));
             } else if(aeroplanLastName === ''){
            	 $('#signupAeroplanLastName').addClass('is-invalid');
            	 $('[data-mz-validationmessage-for="aeroplanLastName"]').text(Hypr.getLabel('aeroplanNumberMissingInvalid'));
             }else{
            	 $('#signupAeroplanNumberInput').removeClass('is-invalid');
            	 $('.digit-length').removeClass('is-invalid');
            	 $('[data-mz-validationmessage-for="aeroplanNumber"]').text('');
            	 $('#signupAeroplanLastName').removeClass('is-invalid');
            	 $('[data-mz-validationmessage-for="aeroplanLastName"]').text('');
            	 $('#no-aeroplan').addClass('hidden');
		        	var self = $('.myaccount-shopping-information');
		        	self.addClass('is-loading');
		            window.setTimeout(function() {
		                self.removeClass('is-loading');
		                $('#signupAeroplanSection').removeClass('aeroplanAvailable').addClass('hidden');
		                $('#aeroplan-number-available').removeClass('hidden');
		                $('#aeroplan-member').attr('checked', false);
		            }, 5 * 1000);
		        	
		        	$('.myaccount-shopping-information').addClass('is-loading');
		        	var aeroplanNumSet = $('#signupAeroplanNumberInput').val();
		        	 var aeroplanNumberSet = _.find(accountModel.get('attributes').toJSON(), function (attribute) { 
		        		 return attribute.attributeFQN === Hypr.getThemeSetting('aeroplanMemberNumber');
		 		    });
		        	 accountModel.set('aeroplanMemberNumber', aeroplanNumSet);
		 	         accountModel.getAttributes().then(function(customer) {
		               customer.get('attributes').each(function(attribute) {
		                   attribute.set('attributeDefinitionId', attribute.get('id'));
		               });
		               customer.updateAttribute(aeroplanNumberSet.attributeFQN,aeroplanNumberSet.attributeDefinitionId,[aeroplanNumSet]);
		            });
		 	        
		 	        var lAeroplanName = document.getElementById('signupAeroplanLastNameInput');
			        if (lAeroplanName) {
			        	lAeroplanName.addEventListener('keydown', function (event) {
			                if (event.which === 32) {
			                	lAeroplanName.value = lAeroplanName.value.replace(/^\s+/, '');
			                    if (lAeroplanName.value.length === 0) {
			                        event.preventDefault();
			                    }
			                }
			            });
			        }
			        
		 	        
             }
 	        
        });
        
        //Aeroplan Section End
        
        var $accountSettingsEl = $('#account-settings'),
            $passwordEl = $('#password-section'),
            $orderHistoryEl = $('#account-orderhistory'),
            $paymentMethodsEl = $('#account-paymentmethods'),
            $returnHistoryEl = $('#account-returnhistory'),
            $addressBookEl = $('#account-addressbook'),
            $wishListEl = $('#account-wishlist'),
            $messagesEl = $('#account-messages'),
            $storeCreditEl = $('#account-storecredit'),
            $editModalEl = $('#edit-card-modal'),
            orderHistory = accountModel.get('orderHistory'),
            returnHistory = accountModel.get('returnHistory'),
            $myStore = $("#user-preferred-store");

        var myStore;    
        if($.cookie("preferredStore")){    
            myStore = $.parseJSON($.cookie("preferredStore"));     
        }
        orderHistory = new OrderModels.OrderCollection({});

        var accountViews = window.accountViews = {
            settings: new AccountSettingsView({
                el: $accountSettingsEl,
                model: accountModel,
                messagesEl: $messagesEl
            }),
            password: new PasswordView({
                el: $passwordEl,
                model: accountModel,
                messagesEl: $messagesEl
            }),
            orderHistory: new OrderHistoryView({
                el: $orderHistoryEl.find('[data-mz-orderlist]'),
                model: orderHistory
            }),
            orderHistoryPagingControls: new PagingViews.PagingControls({
            	 templateName: 'modules/my-account/order-history-paging-controls',
            	 el: $orderHistoryEl.find('[data-mz-pagingcontrols]'),
            	 model: orderHistory
            }),
            orderHistoryPageNumbers: new OrderHistoryPageNumbers({
            	 el: $orderHistoryEl.find('[data-mz-pagenumbers]'),
            	 model: orderHistory
            }),
            returnHistory: new ReturnHistoryView({
            	 el: $returnHistoryEl.find('[data-mz-orderlist]'),
            	 model: returnHistory
            }),
            returnHistoryPagingControls: new PagingViews.PagingControls({
            	 templateName: 'modules/my-account/order-history-paging-controls',
            	 el: $returnHistoryEl.find('[data-mz-pagingcontrols]'),
            	 model: returnHistory
            }),
            returnHistoryPageNumbers: new ReturnHistoryPageNumbers({
            	 el: $returnHistoryEl.find('[data-mz-pagenumbers]'),
            	 model: returnHistory
            }),
            paymentMethods: new PaymentMethodsView({
                el: $paymentMethodsEl,
                model: accountModel,
                messagesEl: $messagesEl
            }),
            addressBook: new AddressBookView({
                el: $addressBookEl,
                model: accountModel,
                messagesEl: $messagesEl
            }),
            storeCredit: new StoreCreditView({
                el: $storeCreditEl,
                model: accountModel,
                messagesEl: $messagesEl
            }),
            userStore: new PreferredStoreView({
                el: $myStore,
                model: new Backbone.Model(myStore)
            })
			
        };
        
        window.accountViews = accountViews;
        if (HyprLiveContext.locals.siteContext.generalSettings.isWishlistCreationEnabled) accountViews.wishList = new WishListView({
            el: $wishListEl,
            model: accountModel.get('wishlist'),
            messagesEl: $messagesEl
        });

        // TODO: upgrade server-side models enough that there's no delta between server output and this render,
        // thus making an up-front render unnecessary.
        _.invoke(window.accountViews, 'render');
        
        $("#settings-tab").on('click', function() {            
        	$(".render-text").empty().append(Hypr.getLabel('myAccount'));    
        	$('.mz-pagetitle').empty().append(Hypr.getLabel('myAccount'));   
        });
        $("#paymentmethods-tab").on('click', function() {   
        	$(".render-text").empty().append(Hypr.getLabel('paymentInformationText'));    
        	$(".mz-pagetitle").empty().append(Hypr.getLabel('paymentInformationText'));    
        });
        $("#orderhistory-tab").on('click', function() {   
        	$(".render-text").empty().append(Hypr.getLabel('orderHistory'));    
        	$(".mz-pagetitle").empty().append(Hypr.getLabel('orderHistory'));    
        }); 
        
        
        $(document).on('click', '.myaccount-view-detail-link', function(e) { 
            e.preventDefault();
            var currentTarget  = e.currentTarget;
            var currentParent = $(currentTarget).closest(".listing");
            $(currentParent).find(".myaccount-vieworderdetail-container").removeClass('hidden');
             var viewOrderValue = $(this).attr('href');
             $(viewOrderValue).removeClass('hidden');
             $('.mz-orderlisting').addClass('hidden');
             $('.mz-l-stack-sectiontitle').addClass('hidden');        
        });
        
        $(document).on('click', '.view-all-orders', function(e) { 
            e.preventDefault();
            $('.myaccount-vieworderdetail-container').addClass('hidden');
            var orderListing = $(this).attr('href');
            $(orderListing).addClass('hidden');
            $('.mz-orderlisting').removeClass('hidden');
            $('.mz-l-stack-sectiontitle').removeClass('hidden');
        });
              
    });
});
