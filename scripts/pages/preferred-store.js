define([
         'modules/jquery-mozu',
         'hyprlive',
         "underscore", 
         'modules/backbone-mozu',
         'modules/api',
         'pages/mystore-details',
         'modules/models-customer'],
         function($, Hypr, _ ,Backbone, api, StoreViews,CustomerModels){
	
	//Page Context
	var pageContext = require.mozuData('pagecontext');
	var user = require.mozuData('user');
	var apiContext = require.mozuData('apicontext');

	var contextSiteId = apiContext.headers["x-vol-site"],
		homeFurnitureSiteId = Hypr.getThemeSetting("homeFurnitureSiteId"),
		isHomeFurnitureSite = false;
	
	/* check current site is home furniture site or not */			
		if(homeFurnitureSiteId === contextSiteId){
			isHomeFurnitureSite = true;
		}

	/*
		This function initialize and render preferred store views  in header 
	*/
	function renderPreferredStoreView(){
		var storeDetails =new StoreViews.MyStoreDetails({
			el:$("#mystore-details"),
			model:new Backbone.MozuModel($.parseJSON($.cookie("preferredStore")))
    	});
		var myStore =new StoreViews.MyStore({
			el:$("#my-store"),
			model:new Backbone.MozuModel($.parseJSON($.cookie("preferredStore")))
		});
    	myStore.render();
		storeDetails.render();
	}
	/*
		This function accept input parameter as store code and call Arc action to set purchase location. 
		On success - api call to fetch location with purchase location. Save data on success in 
		cookie then reoad page.
 	*/
	function setPurchaseLocation(store){
		$.ajax({
    		url: "/set-purchase-location", 
    		data :{ "purchaseLocation": store}, 
    		type:"GET",
    		success:function(response){
    			api.get("location",{code:store}).then(function(response){
    	        	var selectedStore = response.data;
    	        	var expiryDate = new Date();
    				expiryDate.setYear(expiryDate.getFullYear() + 1);
					$.cookie("preferredStore",JSON.stringify(selectedStore),{path:'/',expires: expiryDate});
					$.cookie("showDialog",true, {path:'/'});
    				window.location.reload();
    	        });
			},
			error: function (error) {
				// if arc action failed
			}
    	});
	}
	/*
		This function compares currently set purchase location with preferred store in cookie
		If equal - render views else make api call to fetch location with current purchase location
		on success of api call save response in cookie render view.
		If user authenticated update customer attribute
		If purchase location is not set and cookie is present call arc to to purchase location.
	*/
	function compareStores(shopperStore, preferredStore){
		
		if(pageContext.purchaseLocation){
			
			if(shopperStore){
				if(shopperStore === preferredStore){
					renderPreferredStoreView();
				}else{
					setPurchaseLocation(shopperStore);
				}
			}else{
				var purchaseLocation = pageContext.purchaseLocation.code;
				if(preferredStore === purchaseLocation){
					renderPreferredStoreView();
				}else{
					//get purchase location and set cookie with updated store
					api.get("location",{code:purchaseLocation}).then(function(response){
						var selectedStore = response.data;	
						var expiryDate = new Date();
    					expiryDate.setYear(expiryDate.getFullYear() + 1);
    	        		$.cookie("preferredStore",JSON.stringify(selectedStore),{path:'/',expires: expiryDate});
						renderPreferredStoreView();
					});
				}
			}
		}else{
			setPurchaseLocation(preferredStore);
		}	
	}
	/*
		This function updates the authenticated user's preferred store. i.e customer attribute
	*/
	function updateCustomerAttribute(){
 		//update customer attribute here with preferred store
 		var preferredStore = $.parseJSON($.cookie("preferredStore"));
 		var customer = new CustomerModels.EditableCustomer(); 
 		customer.set("id",user.accountId);
		customer.fetch().then(function(response){
			var attribute;
			if(isHomeFurnitureSite){
				attribute = _.findWhere(response.apiModel.data.attributes, {fullyQualifiedName:Hypr.getThemeSetting('hfPreferredStore')});
			}else{
				attribute = _.findWhere(response.apiModel.data.attributes, {fullyQualifiedName:Hypr.getThemeSetting('preferredStore')});
			}
			var attributeData = attribute;
			response.updateAttribute(attributeData.fullyQualifiedName,attributeData.attributeDefinitionId,[preferredStore.code]);
		});
	}
	/*
		This is geo location success call back function. This function set user's current preferred store according to their 
		location i.e nearest location 
	*/
	function locationFound(position){
		var shopperPos = {
				   "lat": position.coords.latitude,
				   "lng":position.coords.longitude
			     };
				     
		var	storeDistance,filter;

		$.cookie("currentPosition",JSON.stringify(shopperPos),{path:'/'});
		
		if(isHomeFurnitureSite){
			storeDistance = Hypr.getThemeSetting("storeDistanceInMeter") * 4;	
			filter = 'geo near('+shopperPos.lat+","+shopperPos.lng+","+storeDistance+') and '+ Hypr.getThemeSetting("customStoreType")+ ' eq "Home Furniture" and '+Hypr.getThemeSetting("displayOnline")+' eq true and locationType.Code eq "PH"';
			api.get("locations",{filter: filter }).then(function(response){
				if(response && response.data.items.length > 0){
					setPurchaseLocation(response.data.items[0].code);
				}else{
					//setPurchaseLocation(Hypr.getThemeSetting('defaultStoreCode'));
				}
			});
		}else{
			storeDistance = Hypr.getThemeSetting("storeDistanceInMeter") * 4;
			filter = 'geo near('+shopperPos.lat+","+shopperPos.lng+","+storeDistance+') and '+ Hypr.getThemeSetting("customStoreType")+ ' ne "Home Furniture" and '+Hypr.getThemeSetting("displayOnline")+' eq true and locationType.Code eq "PH"';
			api.get("locations",{filter: filter }).then(function(response){
				if(response && response.data.items.length > 0){
					setPurchaseLocation(response.data.items[0].code);
				}else{
					//setPurchaseLocation(Hypr.getThemeSetting('defaultStoreCode'));
				}
			});
		}
	}
	/*
		This is geo location Failure call back function. This function set purchase location to default as user blocks
		their location or browser doest not allow to access location.
	*/
	function locationError(error){
		//user blocked their location
    	//$.cookie("showDialog",true, {path:'/'});
		switch(error.code){
			case 1: 
					//permission denied
			case 2: 
					//location unavailable
			case 3:
					// timeout error
					//setPurchaseLocation(Hypr.getThemeSetting('defaultStoreCode'));
					break;
			default:
					//setPurchaseLocation(Hypr.getThemeSetting('defaultStoreCode'));
		}
	}
	$(document).ready(function(){
		var preferredStore;	
		
		if(!pageContext.isEditMode){
			if($.cookie("preferredStore")){
				preferredStore = $.parseJSON($.cookie("preferredStore"));
			}
			if(user.isAuthenticated && !user.isAnonymous){
				var shopperStore, attribute;
				var customer = new CustomerModels.EditableCustomer();  
				customer.set("id",user.accountId);
				customer.fetch().then(function(response){ 
					if(isHomeFurnitureSite){
						attribute = _.findWhere(response.apiModel.data.attributes, {fullyQualifiedName:Hypr.getThemeSetting('hfPreferredStore')});
				    }else{
				    	attribute = _.findWhere(response.apiModel.data.attributes, {fullyQualifiedName:Hypr.getThemeSetting('preferredStore')});
				    }    
					if(attribute){
						shopperStore = _.first(attribute.values);
					}
					if(shopperStore){
						if($.cookie("preferredStore")){
							if(shopperStore === 'N/A') {
								shopperStore = $.parseJSON($.cookie("preferredStore"));
								updateCustomerAttribute();
								renderPreferredStoreView();
							}else{
								if(!$.cookie("sessionStore")){
									$.cookie("sessionStore",JSON.stringify(preferredStore.code),{path:'/'});
									setPurchaseLocation(preferredStore.code);
								} else {
									compareStores(shopperStore, preferredStore.code);
								}
							}
						}else{
							// set user purchase location 
							if(shopperStore != 'N/A') {
								setPurchaseLocation(shopperStore);
							}
						}
					}else{
						if($.cookie("preferredStore")){
							updateCustomerAttribute();
						}
					}
				});
				
			}else{
				if(!$.cookie("preferredStore")){
					// check navigation enabled or not
					if (navigator.geolocation) {
						/*
							setTimeout(function(){
								setPurchaseLocation(Hypr.getThemeSetting('defaultStoreCode'));
							},7000);
						*/
			            navigator.geolocation.getCurrentPosition(locationFound, locationError, {enableHighAccuracy:true,maximumAge:0,timeout:5000}); 
			        } else {
			        	// browser does not support geolocation
			        	//setPurchaseLocation(Hypr.getThemeSetting('defaultStoreCode'));
			        }
				}else{
					//check preferredStore and selected store and update view based on it
					if(!$.cookie("sessionStore")){
						$.cookie("sessionStore",JSON.stringify(preferredStore.code),{path:'/'});
						setPurchaseLocation(preferredStore.code);
					} else {
						compareStores(null,preferredStore.code);
					}
				}
			}
		}
	});
});
