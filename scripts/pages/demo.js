require([
'modules/jquery-mozu',
'modules/backbone-mozu', 
'underscore'], 
function($, Backbone, _) {
    var DemoView = Backbone.MozuView.extend({
    	templateName: "modules/common/firstdemo",
    	
    	deleteRow:function(){
    		console.log($('.demo').data('fname'));
    		
    		this.render();
    	}
    });

    var demModel = {
        baseDetails: [
            {
                id:'1',
                Firstname:'Prasad',
                lastName: 'Kadam'
            },{
                id:'2',
                Firstname:'Prasad',
                lastName: 'Kadam'
            },
            {
                id:'3',
                Firstname:'Prasad2',
                lastName: 'Kadam'
            },
            {
                id:'4',
                Firstname:'Prasad3',
                lastName: 'Kadam'
            },
            {
                id:'5',
                Firstname:'Prasad4',
                lastName: 'Kadam'
            }
        ]
    };


    var demoView = new DemoView({
        el: $('#view-area'),
        model: new Backbone.Model(demModel)
    });
    demoView.render();


});