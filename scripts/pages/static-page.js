require(["modules/jquery-mozu", 
	"underscore", 
	"hyprlive", 
	"modules/backbone-mozu", 
	'modules/api',
	'hyprlivecontext'], 
	function ($, _, Hypr, Backbone, api, HyprLiveContext) {
	
	$(document).ready(function() {	
		$("h1.main-page-title").hide();
		if($('body').find('h1.title-for-print').length !== 0){
        	$("h1.main-page-title").remove(); 
    	}else{
    		$("h1.main-page-title").show(); 
    	}

    	$("#beautitone").change(function(){
		    console.log($("#beautitone option:selected")[0].value);
        	window.location.href = $("#beautitone option:selected")[0].value;
		});
	});
});