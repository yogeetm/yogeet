require(["modules/jquery-mozu", 
	"underscore", 
	"hyprlive", 
	"modules/backbone-mozu", 
	'modules/api',
	"datatable",
	'hyprlivecontext'], 
	function ($, _, Hypr, Backbone, api, datatable, HyprLiveContext) {
	var CareersView = Backbone.MozuView.extend({
        templateName: 'modules/common/careers-list'
	});
	var locale = require.mozuData('apicontext').headers['x-vol-locale'];
	var language;
	if(locale == "en-US") {
		language = "English";
	} else {
		language = "French";
	}
	function getCareersList(careersType){
		var jobData;
		var queryString = "properties.jobType in['" + careersType + "'] and properties.site in['" + language + "']";
		api.get('documentView', {
			listName: HyprLiveContext.locals.themeSettings.careersListFQN, 
			pageSize: 10000,
			filter: queryString
		}).then(function(response) {		
			var provience = [];
			jobData = response.data;
			_.each(jobData.items, function(item){
				_.each(item.properties.province, function(province){
					if (provience.indexOf(province) < 0){
						provience.push(province);
					}
				});
			});
						
			jobData.provience = provience;
			jobData.locale = locale;
			jobData.careersType = careersType;
			var setCareersView = new CareersView({
		       el: $('#mzCareers'),
		       model:  new Backbone.Model(jobData)
			});
			setCareersView.render();
			
			var table = $('#job-data-table').DataTable({
				"oLanguage": {
			      "oPaginate": {
			        "sNext": "",
			        "sPrevious": ""
			      }
			    },
			    "aaSorting": [[ 4, "desc" ]],
			    initComplete: function(){
		    		$('#provinceCareers').on('change', function(){
		    			table.search(this.value).draw();   
	    			});
		    		$("#sortCareers").change(function() {
		    		    var selectedValue = $(this).val();
		    		    if (selectedValue == '0') {
		    		    	table.order([ 0, 'asc']).draw();
		    		    } else if (selectedValue == '1') {
		    		    	table.order([ 0, 'desc']).draw();
		    		    } else if (selectedValue == '2') {
		    		    	table.order([ 1, 'asc']).draw();
		    		    } else if (selectedValue == '3') {
		    		    	table.order([ 1, 'desc']).draw();
		    		    }    		    
		    		});
		    	},
			    "bPaginate" : $('#job-data-table tbody tr').length>10			    
			}); 
		});
	}
	
	$.urlParam = function(name){
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results===null){
           return null;
        }
        else{
           return decodeURI(results[1]) || 0;
        }
    };
    
    $(document).ready(function() {	
		var careersType = $.urlParam('careersType');
		if(careersType == 'retail'){
			getCareersList(careersType);
		} else if(careersType == 'corporate') {
			getCareersList(careersType);
		} else {
			$('.careers-container').removeClass('hidden');
		}
		
	});
});
