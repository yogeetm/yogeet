require(["modules/jquery-mozu", 
	"underscore", 
	"hyprlive", 
	"modules/backbone-mozu", 
	'modules/api',
	'hyprlivecontext'], 
	function ($, _, Hypr, Backbone, api, HyprLiveContext) {
	
	$(document).ready(function() {	
		var emailReg = Backbone.Validation.patterns.email;		
		$('#unsubscribe-button').on('click',function(){
			var errors = false;
			if (!$("#firstName").val() || $("#firstName").val() === ""){
				$("#firstName").addClass('is-invalid');
				$('[data-mz-validationmessage-for="firstName"]').text(Hypr.getLabel('fnameMissing'));
				errors = true;
			}else{
				$("#firstName").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="firstName"]').text('');
			}
			
			if (!$("#lastName").val() || $("#lastName").val() === ""){
				$("#lastName").addClass('is-invalid');
				$('[data-mz-validationmessage-for="lastName"]').text(Hypr.getLabel('lnameMissing'));
				errors = true;
			}else{
				$("#lastName").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="lastName"]').text('');
			}
			if (!$("#address").val() || $("#address").val() === ""){
				$("#address").addClass('is-invalid');
				$('[data-mz-validationmessage-for="address"]').text(Hypr.getLabel('addressMissing'));
				errors = true;
			}else{
				$("#address").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="address"]').text('');
			}

			if (!$("#city").val() || $("#city").val() === ""){
				$("#city").addClass('is-invalid');
				$('[data-mz-validationmessage-for="city"]').text(Hypr.getLabel('cityMissing'));
				errors = true;
			}else{
				$("#city").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="city"]').text('');
			}
			if (!$("#postalCode").val() || $("#postalCode").val() === ""){
				$("#postalCode").addClass('is-invalid');
				$('[data-mz-validationmessage-for="postalCode"]').text(Hypr.getLabel('postalCodeMissing'));
				errors = true;
			}else{
				$("#postalCode").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="postalCode"]').text('');
			}
			
			var province = $('#province option:selected').val();
			if(!province || province === ""){
				$("#province").addClass('is-invalid');
				$('[data-mz-validationmessage-for="province"]').text(Hypr.getLabel('provinceMissing'));
				errors = true;
			}else{
				$("#province").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="province"]').text('');
			}
			
			if($("#emailAddress").val() === ""){
				$("#emailAddress").addClass('is-invalid');
				$('[data-mz-validationmessage-for="emailAddress"]').text(Hypr.getLabel('emailMissing'));
				errors = true;                            
			}else if(!emailReg.test($("#emailAddress").val())){
				$("#emailAddress").addClass('is-invalid');
				$('[data-mz-validationmessage-for="emailAddress"]').text(Hypr.getLabel('emailMissing'));
				errors = true;
			}else{
				 $("#emailAddress").removeClass('is-invalid');
				 $('[data-mz-validationmessage-for="emailAddress"]').text('');
			}
			

			if(!$('#g-recaptcha-response').val()){
				$('[data-mz-validationmessage-for="captcha"]').text(Hypr.getLabel('missingCaptcha'));
				errors = true;
			}else{
				$('[data-mz-validationmessage-for="captcha"]').text('');
			}
			
			if(!errors) {
				var data = {
						id: $("#idCode").val(),
						address:{
							firstName: $("#firstName").val(),
							lastName: $("#lastName").val(),
							address: $("#address").val(),
							city: $("#city").val(),
							province: $('#province option:selected').val(),
							postalCode: $("#postalCode").val()
						},  
						email: $("#emailAddress").val()
					};
				 $.ajax({
					method: 'POST',
            		contentType: 'application/json; charset=utf-8',
            		url: Hypr.getThemeSetting('formPostingUrl') + 'unsubscribe',
            		data: JSON.stringify(data),     
            		success:function(response){
            			window.location.reload();
        			},
        			error: function (error) {
        				console.log(error);
        			}
				 });
			 }
			
		});
		
		  	  
	});
});