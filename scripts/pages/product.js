﻿require([
	"modules/jquery-mozu", 
	"underscore", 
	"hyprlive", 
	"modules/backbone-mozu", 
	"modules/cart-monitor",
	"modules/models-product", 
	"modules/views-productimages",  
	"hyprlivecontext",
	'modules/api',
	'slick',
	'cloud',
    'shim!vendor/jquery/rrssb.min[jQuery=jquery]'], 
	function ($, _, Hypr, Backbone, CartMonitor, ProductModels, ProductImageViews, HyprLiveContext, api) {
	var user = require.mozuData('user');
    var apiContext = require.mozuData('apicontext');
    var ProductView = Backbone.MozuView.extend({
        templateName: 'modules/product/product-detail',
        additionalEvents: {
            "change [data-mz-product-option]": "onOptionChange",
            "blur [data-mz-product-option]": "onOptionChange",
            "click [data-mz-product-option]": "onOptionChange",
            "change [data-mz-value='quantity']": "onQuantityChange",
            "click [data-mz-value='qty-increment']": "onQuantityChange",
            "click [data-mz-value='qty-decrement']": "onQuantityChange",
            "keyup input[data-mz-value='quantity']": "onQuantityChange",
            "click .changePreferredStore": "changePreferredStore"
        },
        render: function () {
            var me = this;
            
            //set EHF value in 
            var ehfAttributeFQN = Hypr.getThemeSetting('ehfFees');
            var ehfOption = me.model.get('options').findWhere({'attributeFQN': ehfAttributeFQN});
            if(ehfOption && me.model.get('ehfValue')) {
            	ehfOption.set('shopperEnteredValue', me.model.get('ehfValue'));
            	ehfOption.set('value', me.model.get('ehfValue'));
            }
			
            Backbone.MozuView.prototype.render.apply(this);
            this.$('[data-mz-is-datepicker]').each(function (ix, dp) {
                $(dp).dateinput().css('color', Hypr.getThemeSetting('textColor')).on('change  blur', _.bind(me.onOptionChange, me));
            });
            this.sliderFunction();
            var prodDesc  = $(".collapse-section").find("ul");
            _.each(prodDesc, function(e){
                var listElm = $(e).find("li");
                if(listElm.length == 1){
                    $(e).addClass("no-column-count");
                }
            });
        },
        onOptionChange: function (e) {
            return this.configure($(e.currentTarget));
        },
        onQuantityChange: _.debounce(function (e) {
        	if(isNaN($(e.currentTarget).val()) || $(e.currentTarget).val() < 0) {
        		e.preventDefault();
        		$(e.currentTarget).val(1);
        		$(".decrement").addClass("is-disabled");
        	}else {
	        	if($('#add-to-wishlist').prop('disabled')){
	        		$('#add-to-wishlist').text(Hypr.getLabel('saveToList'));	
	        		$('#add-to-wishlist').prop('disabled',false);
	        	}
	        	var field = $(e.currentTarget).attr('data-mz-value'); //this field is used to increment/decrement the quantity                
	        	var quantity = $(e.currentTarget).siblings('.mz-productdetail-qty').val();    
	        	
	        	if(field === 'qty-decrement'){    
	        		$(e.currentTarget).siblings('.mz-productdetail-qty').val(parseInt(quantity, 10) - 1); 
	        	}
	        	if(field === 'qty-increment'){
	        		$(e.currentTarget).siblings('.mz-productdetail-qty').val(parseInt(quantity, 10) + 1);
	        	}    
	        	var $qField = "";    
	        	if(field != 'quantity'){    
	        		$qField = $(e.currentTarget).siblings('.mz-productdetail-qty');    
	        	}else {    
	        		$qField = $(e.currentTarget);    
	        	}
	        	
	            var newQuantity = parseInt($qField.val(), 10);
	            
	            if(newQuantity <= 1 || isNaN(newQuantity)){
	            	$qField.val(1);
	            	newQuantity = 1;
	            	$(".decrement").addClass("is-disabled");
	            }else {
	            	$(".decrement").removeClass("is-disabled");
	            }
	            
	            if (!isNaN(newQuantity)) {
	                this.model.updateQuantity(newQuantity);
	                $qField.val(newQuantity); //fix to set new value excluding decimal point
	            }
        	}
        },500),
        configure: function ($optionEl) {
            var newValue = $optionEl.val(),
                oldValue,
                id = $optionEl.data('mz-product-option'),
                optionEl = $optionEl[0],
                isPicked = (optionEl.type !== "checkbox" && optionEl.type !== "radio") || optionEl.checked,
                option = this.model.get('options').findWhere({'attributeFQN':id});
            if (option) {
                if (option.get('attributeDetail').inputType === "YesNo") {
                    option.set("value", isPicked);
                } else if (isPicked) {
                    oldValue = option.get('value');
                    if (oldValue !== newValue && !(oldValue === undefined && newValue === '')) {
                        option.set('value', newValue);
                    }
                }
            }
            var productDetailView = new ProductDetailView({
                    el: $('#product-header-detail'),
                    model: this.model
                });
            productDetailView.render();
        },
        addToCart: function () {
        	
            this.model.addToCart();
        },
        //to be changed when we integrate Ship to store functionality
        addToCartForPickup: function () {
        	var quantity = this.model.get("quantity");
        	var preferredStore = $.parseJSON($.cookie("preferredStore"));
        	this.model.addToCartForPickup(preferredStore.code, preferredStore.name, quantity);
        },
        addToWishlist: function () {
        	var me = this;
        	if (user.isAnonymous) {
        		var locale = require.mozuData('apicontext').headers['x-vol-locale'];
        		var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
                locale = locale.split('-')[0];
                var currentLocale = '';
                if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite){
                	currentLocale = locale === 'fr' ? '/fr' : '/en';
                }
				window.location.href=currentLocale + "/user/login?productCode="+me.model.get('productCode');
        	} else {
        		this.model.addToWishlist();
        	}
            
        },
        checkLocalStores: function (e) {
            var me = this;
            e.preventDefault();
            this.model.whenReady(function () {
                var $localStoresForm = $(e.currentTarget).parents('[data-mz-localstoresform]'),
                    $input = $localStoresForm.find('[data-mz-localstoresform-input]');
                if ($input.length > 0) {
                    $input.val(JSON.stringify(me.model.toJSON()));
                    $localStoresForm[0].submit();
                }
            });
        },
        sliderFunction: function() {
            var divLength = $('.slider-first-item').size();  
            var sildeLoop = null;
            if(divLength > 1) {
                sildeLoop =  true;
            }
            else {
                sildeLoop = false;
            }
            $(".regular").removeClass('hidden');
            $(".regular").slick({
                dots: false,
                infinite: false,
                slidesToShow: 4,
                slidesToScroll: 4,
                variableWidth:true,
                responsive: [
                 {
                   breakpoint: 991,
                   settings: {
                     slidesToShow: 3,
                     slidesToScroll: 3
                   }
                 },
                 {
                   breakpoint: 520,
                   settings: {
                     slidesToShow: 2,
                     slidesToScroll: 2
                   }
                 }
               ]
            });
            $(".regular-related-products").slick({
                dots: false,
                infinite: false,
                slidesToShow: 4,
                slidesToScroll: 4,
                variableWidth:true,
                responsive: [
				{
				    breakpoint: 991,
				    settings: {
				      slidesToShow: 2,
				      slidesToScroll: 2
				    }
				 },
                 {
                   breakpoint: 520,
                   settings: {
                     slidesToShow: 1,
                     slidesToScroll: 1
                   }
                 }
               ]
            });
        },
        changePreferredStore: function(e){
            e.preventDefault();
            var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
            locale = locale.split('-')[0];
            var currentLocale = '';
            if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite){
            	currentLocale = locale === 'fr' ? '/fr' : '/en';
            }
            window.location.href=currentLocale + "/store-locator?returnUrl="+window.location.pathname;
        },
        setAeroplanMiles: function(aeroplanMiles) {
        	var me = this;
        	var documentListForAeroplanPromo = Hypr.getThemeSetting('documentListForAeroplanPromo');
        	api.get('documentView', {listName: documentListForAeroplanPromo}).then(function(response) {
            	if(response.data.items.length > 0) {
            		var aeroplanMilesFactor = response.data.items[0].properties.markupFactor;
            		me.model.set('aeroplanMiles', aeroplanMilesFactor);
            		var promoCode = response.data.items[0].properties.promoCode;
            		me.model.set('aeroplanPromoCode', promoCode);
            		var startDate = new Date(response.data.items[0].activeDateRange.startDate);
            		var endDate = new Date(response.data.items[0].activeDateRange.endDate);
            		me.model.set('aeroplanPromoDate', true);
            		me.model.set('aeroplanStartDate', startDate);
            		me.model.set('aeroplanEndDate', endDate);
            		me.render();
            	}else {
            		me.model.set('aeroplanMiles', aeroplanMiles);
            		me.render();
            	}
        	});
        },
        initialize: function () {
            // handle preset selects, etc
            var me = this;
            this.$('[data-mz-product-option]').each(function (index) {
            	if(index === 0){
            		var $this = $(this), isChecked, wasChecked;
	                if ($this.val()) {
	                    switch ($this.attr('type')) {
	                        case "checkbox":
	                        case "radio":
	                            isChecked = $this.prop('checked');
	                            wasChecked = !!$this.attr('checked');
	                            if ((isChecked && !wasChecked) || (wasChecked && !isChecked)) {
	                                me.configure($this);
	                            }
	                            break;
	                        default:
	                            me.configure($this);
	                    }
	                }
            	}
            });
            
            /* Hiding More/Less Toggle button based on container height*/
            if($(".collapse-section > .collapse-content").height() < 100){
                $(".toggle-container").hide();
            }else{
                $(".toggle-container").show();
            }
            if($.cookie("preferredStore")) {
        		me.model.set({"preferredStore":$.parseJSON($.cookie("preferredStore"))});
        	}
            var aeroplanMiles;
            me.model.on("productConfigured", function(response){
            	if(response.get('price.salePrice')) {
                	aeroplanMiles = Math.floor(response.get('price.salePrice')/2); 
                }else {
                	aeroplanMiles = Math.floor(response.get('price.price')/2); 
                }
                me.setAeroplanMiles(aeroplanMiles);
            });
            
            if(!me.model.get('hasPriceRange')) {
            	if(me.model.get('price.salePrice')) {
                	aeroplanMiles = Math.floor(me.model.get('price.salePrice')/2); 
                }else {
                	aeroplanMiles = Math.floor(me.model.get('price.price')/2); 
                }
                me.setAeroplanMiles(aeroplanMiles);
            }
            
            var recentlyViewedItems = $.cookie('recentlyViewd');
            if(recentlyViewedItems) {
            	if (me.model.get('productType') != 'Articles') {
            		recentlyViewedItems = recentlyViewedItems + ',' + me.model.get('productCode');
            	}
            	recentlyViewedItems = recentlyViewedItems.split(',');
            	if(recentlyViewedItems.length > 6){
            		recentlyViewedItems = recentlyViewedItems.slice(Math.max(recentlyViewedItems.length - 6, 1)); 
            	}
            	recentlyViewedItems = _.unique(recentlyViewedItems);
            	recentlyViewedItems = recentlyViewedItems.join(',');
            }else {
            	 if (me.model.get('productType') != 'Articles') {
            		 recentlyViewedItems = me.model.get('productCode');
            	 }
            }
        	$.cookie('recentlyViewd', recentlyViewedItems, {path: '/' });

            var contextSiteId = apiContext.headers["x-vol-site"];
            var homeFurnitureSiteId = Hypr.getThemeSetting("homeFurnitureSiteId");
            var frSiteId = Hypr.getThemeSetting("frSiteId");
            if(homeFurnitureSiteId === contextSiteId){
                me.model.set("isHomeFurnitureSite", true);
            }
            if(frSiteId === contextSiteId){
                me.model.set("isFrenchSite", true);   
            }

            if($.cookie("preferredStore")){
                var currentStore = $.parseJSON($.cookie("preferredStore")); 
                var attribute = _.findWhere(currentStore.attributes, {"fullyQualifiedName":Hypr.getThemeSetting("isEcomStore")});   
            
                if(attribute){
                    var isEcomStore = attribute.values[0] === "Y" ? true : false; 
                    me.model.set("isEcomStore", isEcomStore);
                }
                
                //check EHF for current product
            	var entityListForEHF = Hypr.getThemeSetting('entityListForEHF');
            	var productCode;
            	if(me.model.get('variations') && me.model.get('variations').length > 0) {
            		//from current variation find the variationProductCode
            		var productOptionValue = $('.mz-productoptions-option').val();
            		var variation = _.find(me.model.get('variations'), function(variation) {
            			return _.findWhere(variation.options, {'value': productOptionValue});
            		});
            		productCode = variation.productCode;
            	}else {
            		productCode = me.model.get('productCode');
            	}
            	var filterQuery = 'productCode eq ' + productCode + ' and province eq ' + currentStore.address.stateOrProvince;
            	var ehfAttributeFQN = Hypr.getThemeSetting('ehfFees');
            	api.get('entityList', { listName: entityListForEHF, filter: filterQuery }).then(function(response) {
            		if(response.data.items.length > 0) { //if EHF is applied for current product
            			me.model.set('ehfValue', response.data.items[0].feeAmt);
            			me.render();
            		}
            	});
            	
            	//entityListForItemRestricted
            	filterQuery = 'productCode eq ' + productCode + ' and (province eq ' + currentStore.address.stateOrProvince + ' or store eq ' + currentStore.code + ')';
            	var entityListForItemRestricted = Hypr.getThemeSetting('entityListForItemRestricted');
            	api.get('entityList', { listName: entityListForItemRestricted, filter: filterQuery}).then(function(response) {
            		if(response.data.items.length > 0) {
            			me.model.set('isItemRestricted', true);
                        var websiteInd = response.data.items[0].website_ind;
                        if(websiteInd === 'N'){
            			     me.model.set('isWebsiteInd', true);
                        }
                        me.render();
            		}
            	});
            	
            	var currentStoresWarehouse = _.find(currentStore.attributes, function(attribute){
            		return attribute.fullyQualifiedName === Hypr.getThemeSetting('warehouseAttr');
            	});
            	if(currentStoresWarehouse) {
            		var inventoryApi = '/api/commerce/catalog/storefront/products/' + productCode + '/locationinventory?locationCodes=' + currentStoresWarehouse.values[0];
                	api.request("GET", inventoryApi).then(function(response){
                		if(response && response.items.length > 0){
                			if(response.items[0].stockAvailable > 0){
                				me.model.set('isInventoryAvail', true);
                				me.render();
                			}
                		}
                	});
            	}
            }
        },
        callStore: function(e){
            var screenWidth = window.matchMedia("(max-width: 767px)");
            if(!screenWidth.matches){
                e.preventDefault();
            }
        }
    });
    
    var ProductModalView = Backbone.MozuView.extend({
        templateName: 'modules/product/product-modal',
        render: function () {
        	var me = this;
            var contextSiteId = apiContext.headers["x-vol-site"];
            var frSiteId = Hypr.getThemeSetting("frSiteId");
            if(frSiteId === contextSiteId){
                me.model.set("isFrenchSite", true);   
            }
            Backbone.MozuView.prototype.render.apply(this);
        }
    });
    
    var ProductDetailView = Backbone.MozuView.extend({
        templateName: 'modules/product/product-header-detail'
    });
    
    var configureCart = function(product, cartitem) {
    	api.get('cart',{}).then(function(response){
        	var cartTotalAmount = response.data.total;
        	var cartTotalCount = 0;
        	_.each(response.data.items, function(product){
        		if(product.product.productCode != 'EHF101') {
        			cartTotalCount = cartTotalCount + product.quantity;
        		}
        	});
        	cartitem = new Backbone.Model(cartitem);
        	cartitem.set('cartTotalAmount', cartTotalAmount);
        	cartitem.set('cartTotalCount', cartTotalCount);
        	cartitem.set('preferredStore', product.get('preferredStore'));
        	cartitem.set('quantity',product.get('quantity'));
        	var productModalView = new ProductModalView({
                el: $('#product-modal-container'),
                model: cartitem
            });
            productModalView.render();
            $('.view-cart-link').parent().removeClass('is-loading');
        });
    };
    
    $(document).ready(function () {
        var product = ProductModels.Product.fromCurrent();

        product.on('addedtocart', function (cartitem) {
            if (cartitem && cartitem.prop('id')) {
                product.isLoading(false);
                CartMonitor.addToCount(product.get('quantity'));
                $('#product-modal-container').modal().show();
                configureCart(product, cartitem); 
                ga('ec:addProduct', {                 							           // Provide product details in an productFieldObject.
                	  'id': product.get('productCode'),                     		  // Product ID (string).
                	  'name': product.get('content').get('productName'),		 // Product name (string).
                	  'category': product.get('categories')[0].categoryId,     						// Product category (string).
                	  'brand': currAttribute ? currAttribute.values[0].stringValue : '',  // Product brand (string).
                	  'price': product.get('price').get('salePrice') > 0 ? parseFloat(product.get('price').get('salePrice')).toFixed(2) : parseFloat(product.get('price').get('price')).toFixed(2),
                	  'quantity': parseInt(product.get('quantity'), 10)
               	});
                ga('ec:setAction', 'add');
                ga('send', 'event', 'UX', 'Add To Cart Click', 'add to cart');     // Send data using an event.
            } else {
                product.trigger("error", { message: Hypr.getLabel('unexpectedError') });
            }
        });

        product.on('addedtowishlist', function (cartitem) {
            $('#add-to-wishlist').prop('disabled', 'disabled').text(Hypr.getLabel('savedToList'));
            
            /*Update Wishlist Count*/
            api.get('wishlist',{}).then(function(response){        	
            	var items = response.data.items[0].items;
                var totalQuantity = 0;
                _.each(items, function(item, index) {
                	totalQuantity += item.quantity;
                });
                $('#wishlist-count').html('('+totalQuantity+')');
            });
        });
        
        /*click listener on video thumbnail*/
        $(document).on("click touchstart","#main-video-thumb",function(e){
            if($("#videoContainer").css("display") == "none"){
                $("#videoContainer").toggleClass("show-video");
                $("#imageContainer").hide();
                $(".mz-productimages-thumbimage").removeClass("is-selected");
                $(".ign-product-videothumb").addClass("is-selected");    
            }
            var videoSrc = e.currentTarget.getAttribute("src");
            $("#product-main-video").attr("src", videoSrc);
            
        });
        /*Play/Pause main video */
        var videoPlaying=true;
        $(document).on("click",'#videoContainer',function() {
	        if (videoPlaying) {
		        document.getElementById('product-main-video').pause();videoPlaying = false;
		        $(".main-play-video").addClass("video-pause");
	        }
	        else {
		        document.getElementById('product-main-video').play();videoPlaying = true;
		        $(".main-play-video").removeClass("video-pause");
        	}
        });

        /*Specification toggle functionality */
        $(".toggle-btn").on("click",function(e){
        	e.preventDefault();
        	$(this).closest(".toggle-container").toggleClass("is-collapsed");
        	$(this).closest(".collapse-section").toggleClass("is-collapsed");
        	if($(this).closest(".toggle-container").hasClass("is-collapsed")){
        		$(this).text(Hypr.getLabel('less'));
        	}else{
        		$(this).text(Hypr.getLabel('more'));
        	}
        });
        
        /*View all related products functionality*/
        $('.related-list-toggle').on('click', function (e) {
            e.preventDefault();
            e.stopImmediatePropagation();

            var $toggle = $(e.target).closest('.related-list-toggle');
            var $parent = $toggle.closest('.related-products-link');
            var expanded = $parent.attr('aria-expanded') === 'true';
            expanded = ! expanded;
            
            $toggle.attr('aria-label', $toggle.attr(expanded ? 'data-label-less' : 'data-label-more'));
            $parent.attr('aria-expanded', expanded);
        });
        /* Info Popover */
        $('[data-toggle="popover"]').popover();
        
        var productImagesView = new ProductImageViews.ProductPageImagesView({
            el: $('[data-mz-productimages]'),
            model: product
        });

        var productView = new ProductView({
            el: $('#product-detail'),
            model: product,
            messagesEl: $('[data-mz-message-bar]')
        });
        
        window.productView = productView;        
        $.browser = {};
        (function () {
            $.browser.msie = false;
            $.browser.version = 0;
            if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
                $.browser.msie = true;
                $.browser.version = RegExp.$1;
            }
        })();
        productView.render();	
        
        var addToWishlist  = window.location.hash;
        if(addToWishlist == "#addToWishlist") {
        	productView.addToWishlist();
        }
        
        var currAttribute = _.findWhere(product.attributes.properties, {'attributeFQN':Hypr.getThemeSetting('brandDesc')});
        ga('ec:addProduct', {                 							           // Provide product details in an productFieldObject.
          'id': product.get('productCode'),                     		  // Product ID (string).
      	  'name': product.get('content').get('productName'),		 // Product name (string).
      	  'category': product.get('categories')[0].categoryId,     						// Product category (string).
      	  'brand': currAttribute ? currAttribute.values[0].stringValue : ''  // Product brand (string).
     	});
        
        ga('ec:setAction', 'detail');       // Detail action.
        ga('send', 'pageview'); 
    });
});
