require([
		'modules/jquery-mozu',
		'hyprlive',
		'modules/api',
		'modules/models-cart',
		'modules/models-customer',
		'underscore'
	],
	function ($, Hypr, api, CartModels, CustomerModels, _) {

		var user = require.mozuData('user'),
			contextSiteId = require.mozuData('apicontext').headers["x-vol-site"],
			homeFurnitureSiteId = Hypr.getThemeSetting("homeFurnitureSiteId"),
			locationList = Hypr.getThemeSetting("storeDocumentListFQN");
		var locationCode, isHomeFurnitureSite = false;

		var returnUrl ='/';
			var locale = require.mozuData('apicontext').headers['x-vol-locale'];
			locale = locale.split('-')[0];
			var currentLocale = '';
		if (Hypr.getThemeSetting('homeFurnitureSiteId') != contextSiteId){
			returnUrl = locale === 'fr' ? '/fr' : '/en';
		}
		if (homeFurnitureSiteId === contextSiteId) {
			isHomeFurnitureSite = true;
		}

		/*
			This function updates the authenticated user's preferred store. i.e customer attribute
		*/
		function updateCustomerAttribute() {
			//update customer attribute here with preferred store
			var preferredStore = $.parseJSON($.cookie("preferredStore"));
			var customer = new CustomerModels.EditableCustomer();
			customer.set("id", user.accountId);
			customer.fetch().then(function (response) {
				var attribute;
				if (isHomeFurnitureSite) {
					attribute = _.findWhere(response.apiModel.data.attributes, {
						fullyQualifiedName: Hypr.getThemeSetting('hfPreferredStore')
					});
				} else {
					attribute = _.findWhere(response.apiModel.data.attributes, {
						fullyQualifiedName: Hypr.getThemeSetting('preferredStore')
					});
				}
				var attributeData = {
					attributeFQN: attribute.fullyQualifiedName,
					attributeDefinitionId: attribute.attributeDefinitionId,
					values: [preferredStore.code]
				};
				response.apiUpdateAttribute(attributeData).then(function () {
					window.location.href = returnUrl;
				});
			});
		}

		function continueWithNewStore() {
			$.ajax({
				url: "/set-purchase-location",
				data: {
					"purchaseLocation": locationCode
				},
				type: "GET",
				success: function (response) {
						api.get("location", {
							code: locationCode
						}).then(function (response) {
							var selectedStore = response.data;
							var expiryDate = new Date();
							expiryDate.setYear(expiryDate.getFullYear() + 1);
							$.cookie("preferredStore", JSON.stringify(selectedStore), {
								path: '/',
								expires: expiryDate
							});
							$.cookie("sessionStore",JSON.stringify(selectedStore.code),{path:'/'});
							if (user.isAuthenticated && !user.isAnonymous) {
								updateCustomerAttribute();
							} else {
								window.location.href = returnUrl;
							}
						});
				},
				error: function (error) {
					console.log(error);
				}
			});
		}
		$(document).ready(function () {

			var map, bounds, infoWindow, pos, marker, icon;
			var markerIcons = {
				// "my store"
				selection: {
					url: '../resources/images/marker-selection.png',
					scaledSize: new google.maps.Size(33, 40),
					origin: new google.maps.Point(0, 0),
					anchor: new google.maps.Point(16, 35)
				},
				store: {
					url: '../resources/images/marker-' + (isHomeFurnitureSite ? 'hf' : 'hh') + '.png',
					scaledSize: new google.maps.Size(isHomeFurnitureSite ? 27 : 33, isHomeFurnitureSite ? 28 : 40),
					origin: new google.maps.Point(0, 0),
					anchor: new google.maps.Point(16, 35)
				}
			};

			pos = {
				"lat": parseFloat($(".store-details-top-section").data("mz-latitude")),
				"lng": parseFloat($(".store-details-top-section").data("mz-longitude"))
			};

			var selectedStore = $(".store-details-top-section").data("mz-store");
			// initialize the map
			map = new google.maps.Map(document.getElementById('googleMap'), {
				center: pos,
				zoom: Hypr.getThemeSetting('storeDetailsMapZoomLevel'),
				disableDefaultUI: true,
				zoomControl: true
			});

			//marker selction
			if ($.cookie("preferredStore")) {
				var preferredStore = $.parseJSON($.cookie("preferredStore"));
				if (parseInt(preferredStore.code, 10) === selectedStore) {
					icon = markerIcons.selection;
				} else {
					icon = markerIcons.store;
				}
			}

			marker = new google.maps.Marker({
				position: pos,
				icon: icon,
				map: map
			});

			/* make this my store functionality */
			$("#makeThisMyStoreBtn").on("click", function (e) {
				var currentTarget = e.currentTarget;
				locationCode = $(currentTarget).data("mzStore");
				var cartModelData = new CartModels.Cart();
				var cartCount = null;
				if (window.location.search.indexOf('storeService') > 0) {
					var returnParam = (window.location.search.split('storeService=')[1] || '');
					var queryString = "properties.documentKey eq " + locationCode;
					$("#content-loading").show();
					if (decodeURIComponent(returnParam) === "Home Installs") {
						api.get('documentView', {
							listName: locationList,
							filter: queryString
						}).then(function (response) {
							if (response && response.data.items.length > 0) {
								var storeDetails = response.data.items[0];
								var homeInstallStoreInfo = {
									"code": storeDetails.name,
									"name": storeDetails.properties.storeName,
									"homeInstallEmail": storeDetails.properties.homeInstallsEmail,
									"address1": storeDetails.properties.address1,
									"address2": storeDetails.properties.address2,
									"city": storeDetails.properties.city,
									"province": storeDetails.properties.province[0],
									"postalCode": storeDetails.properties.postalCode,
									"phone": storeDetails.properties.phone
								};
								$.cookie("homeInstallStore", JSON.stringify(homeInstallStoreInfo), {
									path: '/'
								});
								$("#content-loading").show();
								window.location.href = "/home-install";
							}
						});
					} else {
						api.get('documentView', {
							listName: locationList,
							filter: queryString
						}).then(function (response) {
							if (response && response.data.items.length > 0) {
								var storeDetails = response.data.items[0];
								var commercialMaintenanceStore = {
									"code": storeDetails.name,
									"commercialMaintenanceEmail": storeDetails.properties.commercialMaintenanceEmail,
									"name": storeDetails.properties.storeName,
									"address1": storeDetails.properties.address1,
									"address2": storeDetails.properties.address2,
									"city": storeDetails.properties.city,
									"province": storeDetails.properties.province[0],
									"postalCode": storeDetails.properties.postalCode,
									"phone": storeDetails.properties.phone
								};
								$.cookie("commercialMaintenanceStore", JSON.stringify(commercialMaintenanceStore), {
									path: '/'
								});
								$("#content-loading").show();
								window.location.href = "/commercial-maintenance";
							}
						});
					}
				} else {
					cartModelData.on('sync', function () {
						cartCount = cartModelData.count();
					});
					cartModelData.apiGet().then(function (response) {
						if (cartCount > 0) {
							api.get("location", {
								code: locationCode
							}).then(function (response) {
								var selectedStore = response.data;
								var isEcomStore = _.findWhere(selectedStore.attributes, {
									"fullyQualifiedName": Hypr.getThemeSetting("isEcomStore")
								});
								if (isEcomStore && isEcomStore.values[0] === 'Y') {
									$('#store-change-confirmation-modal').modal().show();
								} else {
									$('#non-ecom-store-confirmation-modal').modal().show();
								}
							});
						} else {
							continueWithNewStore();
						}
					});
				}
			});
			/*continue with new store if items in cart*/
			$("#continueWithNewStoreBtn").on("click", function () {
				continueWithNewStore();
			});
			$("#switchToNewStore").on("click", function () {
				continueWithNewStore();
			});
			$(".store-tel").on("click", function (e) {
				var screenWidth = window.matchMedia("(max-width: 767px)");
				if (!screenWidth.matches) {
					e.preventDefault();
				}
			});
		});
	});