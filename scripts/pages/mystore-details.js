define([
         'modules/jquery-mozu',
         'hyprlive',
         'modules/backbone-mozu',
         'hyprlivecontext'],
         function($, Hypr, Backbone,HyprLiveContext){
	
	var MyStore = Backbone.MozuView.extend({
		templateName: "modules/location/mystore",
		initialize: function(){
			var locale = require.mozuData('apicontext').headers['x-vol-locale'];
			var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
            locale = locale.split('-')[0];
            var currentLocale = locale === 'fr' ? '/fr' : '/en';
            this.model.set("currentLocale",currentLocale);
            this.model.set("currentSite",currentSite);
		}
	});
	
	var MyStoreDetails = Backbone.MozuView.extend({
		templateName: "modules/location/mystore-detail",
		 additionalEvents: {
		 	"click #chooseAnotherStore": "changeStore",
		 	"click #storeLocator": "changeStore"
		 },
		 initialize: function(){
		 	var weekDays = ["sunday", "monday", "tuesday",  "wednesday", "thursday", "friday", "saturday"],
		 		today = new Date(),
		 		day = weekDays[today.getDay()],
		 		hours=this.model.attributes.regularHours[day];
		 		this.model.set("todayHours",hours);
		 		
		 		var locale = require.mozuData('apicontext').headers['x-vol-locale'];
		 		var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
                locale = locale.split('-')[0];
                var currentLocale = locale === 'fr' ? '/fr' : '/en';
                this.model.set("currentLocale",currentLocale);
                this.model.set("currentSite",currentSite);
		 },
		 changeStore: function(e){
		 	e.preventDefault();
		 	if(window.location.pathname.length > 1){
		 	 window.location.href="/store-locator?returnUrl="+window.location.pathname;
		 	}else{
		 		window.location.href="/store-locator";
		 	}
		 },
		 headerCallStore: function(e){
		 	var screenWidth = window.matchMedia("(max-width: 767px)");
            if(!screenWidth.matches){
                e.preventDefault();
            }
		 }
	});
	
	return {
		MyStore : MyStore,
		MyStoreDetails : MyStoreDetails
	};
});