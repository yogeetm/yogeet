define(['modules/jquery-mozu', "modules/views-collections", 'hyprlive'], function($, CollectionViewFactory, Hypr) {
    $(document).ready(function() {
        window.facetingViews = CollectionViewFactory.createFacetedCollectionViews({
            $body: $('[data-mz-category]'),
            template: "category-interior"
        });
        
        $('.result-view-toggle li a').on('click', function (e){
        	e.preventDefault();
        	$(this).closest('.result-view-toggle').find('li').removeClass('is-current');
        	$(this).closest('li').addClass('is-current');
        	
        	if($(this).data('toggle-type') == 'list') {
        		window.gridStatus = "list";
        		$(document).find('.mz-productlist').addClass('list');
        	} else {
        		window.gridStatus = "";
        		$(document).find('.mz-productlist').removeClass('list');
        	}
        });
        
        if ($("#mz-drop-zone-expert-advice-bropzone > .mz-cms-row > .mz-cms-col-12-12").html() && $("#mz-drop-zone-expert-advice-bropzone > .mz-cms-row > .mz-cms-col-12-12").html().length > 0) {
        	$("#mz-drop-zone-expert-advice-bropzone").addClass('isdropped');
        } else {
        	$("#mz-drop-zone-expert-advice-bropzone").removeClass('isdropped');
          }   

       
        $(document).find('.filter-button').on('click', function(e) {
        	$(document).find('.filters').removeClass('open');
        });
       
        
        /*Code for moretag on facets - Start*/
        var facetHeight = Hypr.getThemeSetting('facetDefaultHeight');
        var facetHeightWithPX = Hypr.getThemeSetting('facetDefaultHeight')+'px';
        $('.mz-facetingform').find('.mz-l-sidebaritem .facet-details > ul').each(function(){        	
        	if($(this).height() > facetHeight) {
        		$(this).parent().find('.moretag').css('display', 'inline-block');
        		if($(this).closest('.mz-l-sidebaritem').hasClass('for-caregory')){
        			$(this).css({'height': '205px', 'overflow-y': 'hidden'});
        		}else {
        			$(this).css({'height': facetHeightWithPX, 'overflow-y': 'hidden'});
        		}
        		
        	}
        });
        
        
        $('.mz-facetingform').find('.mz-l-sidebaritem #Price > ul').css({'height': 'auto', 'overflow-y': 'hidden'});
        
       
        
        
        $(document).on('click', '.moretag', function(event){
        	event.preventDefault();
        	var viewLess = Hypr.getLabel('viewLess');
        	var viewMore = Hypr.getLabel('viewMore');
        	if($(this).find('span').text() === viewMore) {
        		$(this).parent().find('.mz-facetingform-facet').css('height', 'auto');
            	$(this).find('span').text(viewLess);
        	} else {
        		if($(this).closest('.mz-l-sidebaritem').hasClass('for-caregory')){
        			$(this).parent().find('.mz-facetingform-facet').css('height', '205px');
        		}else {
        			$(this).parent().find('.mz-facetingform-facet').css('height', facetHeightWithPX);
        		}
            	$(this).find('span').text(viewMore);
        	}
        	
        });
        /*Code for moretag on facets - End*/
        
       
        
    });
});