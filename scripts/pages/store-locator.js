require([
		'modules/jquery-mozu',
		'hyprlive',
		"underscore",
		'modules/backbone-mozu',
		'hyprlivecontext',
		'modules/api',
		'modules/models-cart',
		'modules/models-customer',
		'moment'
	],
	function ($, Hypr, _, Backbone, HyprLiveContext, api, CartModels, CustomerModels, moment) {

		/*Page Context*/
		var pageContext = require.mozuData('pagecontext'),
			apiContext = require.mozuData('apicontext'),
			user = require.mozuData('user'),
			contextSiteId = apiContext.headers["x-vol-site"],
			homeFurnitureSiteId = Hypr.getThemeSetting("homeFurnitureSiteId");

		var map, infoWindow, storeToChange, markers = [],
			search = null,
			today = new Date(),
			statusFilter = false,
			checkedFilters = [],
			locationList = Hypr.getThemeSetting("storeDocumentListFQN"),
			distanceFilters = Hypr.getThemeSetting("distanceFilters"),
			days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
			isHomeFurnitureSite = false,
			searchStoreFlag = false,
			geocoder = new google.maps.Geocoder(),
			searchBasedLatLong,
			cityChanges = false;

		var filtersString = "",
			returnParam = "",
			storeView;

		/* Filter for home furniture site */
		if (homeFurnitureSiteId === contextSiteId) {
			filtersString = "properties.storeType in['Home Furniture']";
			isHomeFurnitureSite = true;
		} else {
			if (window.location.search.indexOf('storeFilter') < 0)
				filtersString = "properties.storeType in['Home Hardware','Home Building Centre', 'Home Hardware Building Centre'] ";
		}

		/* store filter from static page i.e home install and commercial maintenance */
		if (window.location.search.indexOf('storeService') > 0) {
			returnParam = (window.location.search.split('storeService=')[1] || '');
			var serviceFilterString = decodeURIComponent(returnParam) === "Home Installs" ? "properties.storeServices in['Home Installs']" : "properties.commercialMaintenance eq true ";
			if (!filtersString) {
				filtersString = serviceFilterString;
			} else {
				filtersString += " and " + serviceFilterString;
			}
		}

		/* store filters for HBC and HF from PDP */
		if (window.location.search.indexOf('storeFilter') > 0) {
			returnParam = (window.location.search.split('storeFilter=')[1] || '');
			if (returnParam == "HBC") {
				if (!filtersString) {
					filtersString = "properties.storeType in['Home Building Centre']";
				} else {
					filtersString += " and " + "properties.storeType in['Home Building Centre']";
				}
			}
		}

		/* Adding display online filter */
		if (!filtersString) {
			filtersString = "properties.displayOnline eq true";
		} else {
			filtersString += "and properties.displayOnline eq true";
		}
		// default icon for stores
		var markerIcons = {
			store: {
				url: '../resources/images/marker-' + (isHomeFurnitureSite ? 'hf' : 'hh') + '.png',
				scaledSize: new google.maps.Size(isHomeFurnitureSite ? 27 : 33, isHomeFurnitureSite ? 28 : 40),
				origin: new google.maps.Point(0, 0),
				anchor: new google.maps.Point(16, 35)
			},
			// "my store"
			selection: {
				url: '../resources/images/marker-selection.png',
				scaledSize: new google.maps.Size(33, 40),
				origin: new google.maps.Point(0, 0),
				anchor: new google.maps.Point(16, 35)
			},
			// user's location
			center: {
				url: '../resources/images/marker.png',
				scaledSize: new google.maps.Size(35, 44),
				origin: new google.maps.Point(0, 0),
				anchor: new google.maps.Point(18, 38)
			}
		};

		/*store view starts here*/
		var StoreView = Backbone.MozuView.extend({
			templateName: "modules/location/store-locations",
			additionalEvents: {
				"focus input[id='postalCode']": "onSearchFocusBlur",
				"blur input[id='postalCode']": "onSearchFocusBlur",
				"mousedown #useMyLocation": "searchOnCurrentLocation",
				"click [id='applyFilters']": "applyFilters",
				"keypress input[id='postalCode']": "findStores",
				"keypress input[id='city']": "findStores",
				"focus select[id='province']": "onSearchFocusBlur",
				"focus input[id='city']": "onSearchFocusBlur"
			},
			render: function () {
				var me = this;
				var stores = me.model.get("stores");
				if ($.cookie("preferredStore")) {
					var preferredStore = $.parseJSON($.cookie("preferredStore"));
					var currentStore = _.findWhere(stores, {
						name: preferredStore.code
					});
					if (currentStore) {
						currentStore.isPreferresStore = true;
						me.model.set("isPreferresStore", true);
					}
				}
				if (search) {
					var searchObject = {};
					if (search.indexOf(",") > -1) {
						var searchString = search.split(",");
						searchObject = {
							"city": searchString[0].trim(),
							"province": searchString[1].trim()
						};
						me.model.set("search", searchObject);
					} else {
						searchObject = {
							"postalCode": search
						};
						me.model.set("search", searchObject);
					}
				}
				if (window.location.search.indexOf('storeService') > 0) {
					returnParam = (window.location.search.split('storeService=')[1] || '');
					var pageUrl = decodeURIComponent(returnParam) === "Home Installs" ? "Home Installs" : "commercial maintenance";
					me.model.set("pageUrl", pageUrl);
					me.model.set("isHomeInstComMain", true);
				}
				me.setStoreFilters();
				Backbone.MozuView.prototype.render.apply(this, arguments);
				var input = document.getElementById('city');
                var autocomplete = new google.maps.places.Autocomplete(input, { types: ['(cities)'], componentRestrictions: {
                    country: 'ca'} });
                google.maps.event.addListener(autocomplete, 'place_changed', function() {
					var place = autocomplete.getPlace();
                    if (!place.geometry) {
						// no location found
                        return;
                    }else{
						cityChanges = true;
					}
                }); 
				me.loadMap();
			},
			setStoreFilters: function () {
				var me = this;
				var stores = me.model.get("stores");
				var storeServices = [],
					storeTypes = [],
					hfServices = [],
					storeFilters,
					contextSiteId = apiContext.headers["x-vol-site"],
					homeFurnitureSiteId = Hypr.getThemeSetting("homeFurnitureSiteId");
				/*creating service filters array*/

				if (Hypr.getThemeSetting('frSiteId') === contextSiteId) {
					_.each(stores, function (store) {
						_.each(store.properties.storeServices, function (service) {
							if (service) {
								var frenchValue = me.getFrenchTranslation(service);
								var exist = _.findWhere(storeServices, {
									"serviceName": service,
									"serviceValue": frenchValue
								});
								if (!exist) {
									storeServices.push({
										"serviceName": service,
										"serviceValue": frenchValue
									});
								}
							}
						});
					});
				} else {
					_.each(stores, function (store) {
						_.each(store.properties.storeServices, function (service) {
							if (service) {
								var exist = _.findWhere(storeServices, {
									"serviceName": service,
									"serviceValue": service
								});
								if (!exist) {
									storeServices.push({
										"serviceName": service,
										"serviceValue": service
									});
								}
							}
						});
					});
				}

				storeServices = _.sortBy(storeServices, "serviceName");

				if (homeFurnitureSiteId === contextSiteId) {
					/*creating hf services filters array*/
					_.each(stores, function (store) {
						_.each(store.properties.homeFurnitureServices, function (service) {
							if (service) {
								var exist = _.contains(hfServices, service);
								if (!exist) {
									hfServices.push(service);
								}
							}
						});
					});
					hfServices = _.sortBy(hfServices);
					storeFilters = {
						distanceFilters: distanceFilters,
						hfServices: hfServices,
						storeServices: storeServices
					};
					me.model.set("isHomeFurnitureSite", true);
				} else {

					/*creating type filters array*/

					if (Hypr.getThemeSetting('frSiteId') === contextSiteId) {
						_.each(stores, function (store) {
							_.each(store.properties.storeType, function (type) {
								if (type) {
									var frenchType = me.getFrenchTypeTranslation(type);
									var exist = _.findWhere(storeTypes, {
										"storeType": type,
										"storeTypeValue": frenchType
									});
									if (!exist) {
										storeTypes.push({
											"storeType": type,
											"storeTypeValue": frenchType
										});
									}
								}
							});
						});
					} else {
						_.each(stores, function (store) {
							_.each(store.properties.storeType, function (type) {
								if (type) {
									var exist = _.findWhere(storeTypes, {
										"storeType": type,
										"storeTypeValue": type
									});
									if (!exist) {
										storeTypes.push({
											"storeType": type,
											"storeTypeValue": type
										});
									}
								}
							});
						});
					}
					storeTypes = _.sortBy(storeTypes, "storeType");

					storeFilters = {
						storeServices: storeServices,
						storeTypes: storeTypes,
						distanceFilters: distanceFilters
					};
					me.model.set("isHomeFurnitureSite", false);
				}

				me.model.set("storeFilters", storeFilters);
				if (checkedFilters && checkedFilters.length > 0) {
					var length = '(' + checkedFilters.length + ')';
					me.model.set("numOFFilters", length);
				} else {
					me.model.set("numOFFilters", "");
				}
			},
			makeMyStore: function (e) {
				e.stopPropagation();
				var me = this;
				var currentTarget = e.currentTarget;
				storeToChange = $(currentTarget).data("mzStore");
				var cartModelData = new CartModels.Cart();
				var cartCount = null;
				if (window.location.search.indexOf('storeService') > 0) {
					returnParam = (window.location.search.split('storeService=')[1] || '');
					var queryString = "";
					if (decodeURIComponent(returnParam) === "Home Installs") {
						queryString = "properties.documentKey eq " + storeToChange;
						$("#content-loading").show();
						api.request('GET', '/api/content/documentlists/' + locationList + '/views/default/documents?filter=' + queryString + '&pageSize=1100', {
							cache: false
						}).then(function (response) {
							if (response && response.items.length > 0) {
								var storeDetails = response.items[0];
								var homeInstallStoreInfo = {
									"code": storeDetails.name,
									"name": storeDetails.properties.storeName,
									"homeInstallEmail": storeDetails.properties.homeInstallsEmail,
									"address1": storeDetails.properties.address1,
									"address2": storeDetails.properties.address2,
									"city": storeDetails.properties.city,
									"province": storeDetails.properties.province[0],
									"postalCode": storeDetails.properties.postalCode,
									"phone": storeDetails.properties.phone
								};
								$.cookie("homeInstallStore", JSON.stringify(homeInstallStoreInfo), {
									path: '/'
								});
								$("#content-loading").show();
								window.location.href = "/home-install";
							}
						});
					} else {
						queryString = "properties.documentKey eq " + storeToChange;
						$("#content-loading").show();
						api.request('GET', '/api/content/documentlists/' + locationList + '/views/default/documents?filter=' + queryString + '&pageSize=1100', {
							cache: false
						}).then(function (response) {
							if (response && response.items.length > 0) {
								var storeDetails = response.items[0];
								var commercialMaintenanceStore = {
									"code": storeDetails.name,
									"commercialMaintenanceEmail": storeDetails.properties.commercialMaintenanceEmail,
									"name": storeDetails.properties.storeName,
									"address1": storeDetails.properties.address1,
									"address2": storeDetails.properties.address2,
									"city": storeDetails.properties.city,
									"province": storeDetails.properties.province[0],
									"postalCode": storeDetails.properties.postalCode,
									"phone": storeDetails.properties.phone
								};
								$.cookie("commercialMaintenanceStore", JSON.stringify(commercialMaintenanceStore), {
									path: '/'
								});
								$("#content-loading").show();
								window.location.href = "/commercial-maintenance";
							}
						});
					}
				} else {
					cartModelData.on('sync', function () {
						cartCount = cartModelData.count();
					});
					cartModelData.apiGet().then(function (response) {
						if (cartCount > 0) {
							api.get("location", {
								code: storeToChange
							}).then(function (response) {
								var selectedStore = response.data;
								var isEcomStore = _.findWhere(selectedStore.attributes, {
									"fullyQualifiedName": Hypr.getThemeSetting("isEcomStore")
								});
								if (isEcomStore && isEcomStore.values[0] === 'Y') {
									$('#store-change-confirmation-modal').modal().show();
								} else {
									$('#non-ecom-store-confirmation-modal').modal().show();
								}
							});
						} else {
							me.continueWithNewStore();
						}
					});
				}
			},
			updateCustomerAttribute: function (returnUrl) {
				var preferredStore = null;
				if ($.cookie("preferredStore")) {
					preferredStore = $.parseJSON($.cookie("preferredStore"));
				}
				var customer = new CustomerModels.EditableCustomer();
				customer.set("id", user.accountId);
				customer.fetch().then(function (response) {
					var attribute;
					if (isHomeFurnitureSite) {
						attribute = _.findWhere(response.apiModel.data.attributes, {
							fullyQualifiedName: Hypr.getThemeSetting('hfPreferredStore')
						});
					} else {
						attribute = _.findWhere(response.apiModel.data.attributes, {
							fullyQualifiedName: Hypr.getThemeSetting('preferredStore')
						});
					}
					var attributeData = {
						attributeFQN: attribute.fullyQualifiedName,
						attributeDefinitionId: attribute.attributeDefinitionId,
						values: [preferredStore.code]
					};
					response.apiUpdateAttribute(attributeData).then(function () {
						if (returnUrl.includes("/store/")) {
							window.location.href = "/";
						} else {
							window.location.href = returnUrl;
						}
					});
				});
			},
			onSearchFocusBlur: function (e) {
				var inputElement = e.currentTarget;
				if (inputElement.name === "postalCode") {
					if (e.type === "focusin") {
						$('.store-locator-search-loc').show();
					} else {
						$('.store-locator-search-loc').hide();
					}
					$("#city").val("");
					$("#province").val("");
				} else {
					$("#postalCode").val("");
				}
			},
			blurSearchField: function () {
				if ($("#postalCode").is(":focus")) {
					$("#postalCode").blur();
				}
				if ($("#city").is(":focus")) {
					$("#city").blur();
				}
			},
			findStores: function (e) {
				var me = this;
				var postalCode = "",
					city = "",
					searchValue = null;
				// validate form based on user input for postal code or city and province 

				if ($('#postalCode').val()) {
					postalCode = $('#postalCode').val().trim();
				}
				if ($("#city").val()) {
					city = $("#city").val().trim();
				}
				if (e.which === 13 || e.which === 1) {
					$("#content-loading").show();
					if (postalCode) {
						if (me.validatePostalCode(postalCode.trim())) {
							if (city === "") {
								searchValue = postalCode.trim();
								checkedFilters.splice(0, checkedFilters.length);
								me.model.unset("searchErrorMsg");
								geocoder.geocode({
									'address': searchValue
								}, function (results, status) {
									if (status == 'OK') {
										searchBasedLatLong = {
											"lat": results[0].geometry.location.lat(),
											"lng": results[0].geometry.location.lng()
										};
										me.getSearchStores(searchValue, searchBasedLatLong);
										me.blurSearchField();
									} else {
										search = searchValue;
										me.reRenderView();
									}
								});
							}
						} else {
							me.model.set("searchErrorMsg", Hypr.getLabel("notValidPostalCode"));
							setTimeout(function () {
								$("#content-loading").hide();
								search = postalCode.trim();
								me.model.unset("stores", []);
								me.render();
								me.blurSearchField();
							}, 500);
						}
					} else if (city !== "") {
						if(cityChanges){
							if (postalCode === "") {
								searchValue = city.trim();
								me.model.unset("searchErrorMsg");
								checkedFilters.splice(0, checkedFilters.length);
								geocoder.geocode({
									'address': searchValue
								}, function (results, status) {
									if (status == 'OK') {
										searchBasedLatLong = {
											"lat": results[0].geometry.location.lat(),
											"lng": results[0].geometry.location.lng()
										};
										me.getSearchStores(searchValue, searchBasedLatLong);
										me.blurSearchField();
									} else {
										search = searchValue;
										me.reRenderView();
									}
								});
							}
						}else{
							me.model.set("searchErrorMsg", Hypr.getLabel("selectCityFromDropdown"));
							setTimeout(function () {
								$("#content-loading").hide();
								search = city.trim() + ",";
								me.model.unset("stores", []);
								me.render();
								me.blurSearchField();
							}, 500);
						}
					} else {
						me.model.set("searchErrorMsg", Hypr.getLabel("enterValidSearchValue"));
						search = "";
						me.model.set("search", "empty");
						setTimeout(function () {
							$("#content-loading").hide();
							me.model.unset("stores", []);
							me.render();
						}, 500);
					}
				}
			},
			validatePostalCode: function (postal) {
				var regex = /^[ABCEGHJKLMNPRSTVXY]\d[ABCEGHJKLMNPRSTVWXYZ]( )?\d[ABCEGHJKLMNPRSTVWXYZ]\d$/i;
				return regex.test(postal) ? true : false;

			},
			getSearchStores: function (searchValue, searchBasedLatLong) {
				var self = this,
					queryString = "";
				if (isHomeFurnitureSite) {
					queryString = "properties.displayOnline eq true and properties.storeType in['Home Furniture']";
				} else {
					if (window.location.search.indexOf('storeService') > 0) {
						returnParam = (window.location.search.split('storeService=')[1] || '');
						var serviceFilterString = decodeURIComponent(returnParam) === "Home Installs" ? "properties.storeServices in['Home Installs']" : "properties.commercialMaintenance eq true ";
						queryString = "properties.displayOnline eq true and properties.storeType in['Home Hardware','Home Building Centre','Home Hardware Building Centre'] and " + serviceFilterString;
					} else {
						queryString = "properties.displayOnline eq true and properties.storeType in['Home Hardware','Home Building Centre','Home Hardware Building Centre']";
					}
				}
				api.request('GET', '/api/content/documentlists/' + locationList + '/views/default/documents?filter=' + queryString + '&pageSize=1100', {
					cache: false
				}).then(function (response) {
					search = searchValue;
					var nearStoresList = findNearStores(response.items, searchBasedLatLong, Hypr.getThemeSetting("storeDistanceInkm"));
					self.reRenderView(nearStoresList);
				});
			},
			openFilter: function (e) {
				if (e) {
					e.preventDefault();
					e.stopPropagation();
				}
				$(".store-locator-filters").addClass('store-locator-filters-open');
				var screenWidth = window.matchMedia("(max-width: 767px)");
				if (screenWidth.matches) {
					$("body,html").addClass("no-scroll");
				}
			},
			closeFilter: function (e) {
				if (e) {
					e.preventDefault();
					e.stopPropagation();
				}
				$(".store-locator-filters").removeClass('store-locator-filters-open');
				var screenWidth = window.matchMedia("(max-width: 767px)");
				if (screenWidth.matches) {
					$("body,html").removeClass("no-scroll");
				}
			},
			clearFilters: function (e) {
				var me = this;
				e.preventDefault();
				if (checkedFilters && checkedFilters.length > 0) {
					var screenWidth = window.matchMedia("(max-width: 767px)");
					if (screenWidth.matches) {
						$("body,html").removeClass("no-scroll");
					}
					_.each(checkedFilters, function (filter) {
						var id = $(".store-locator-filters").find("#" + filter.id);
						$(id).prop("checked", false);
					});
					checkedFilters.splice(0, checkedFilters.length);

					api.request('GET', '/api/content/documentlists/' + locationList + '/views/default/documents?filter=' + filtersString + '&pageSize=1100', {
						cache: false
					}).then(function (response) {
						var preferredStore, latLong;
						if ($.cookie("preferredStore")) {
							preferredStore = $.parseJSON($.cookie("preferredStore"));
							latLong = search ? searchBasedLatLong : preferredStore.geo;
						} else {
							latLong = searchBasedLatLong;
						}
						var nearStoresList = findNearStores(response.items, latLong, Hypr.getThemeSetting("storeDistanceInkm"));
						me.reRenderView(nearStoresList);
					});
				} else {
					checkedFilters = $(".store-locator-filters").find("input:checked");
					_.each(checkedFilters, function (filter) {
						var id = $(".store-locator-filters").find("#" + filter.id);
						$(id).prop("checked", false);
					});
					checkedFilters.splice(0, checkedFilters.length);
				}
			},
			toggleMapListView: function (e) {
				if (e) {
					e.preventDefault();
					e.stopPropagation();
				}
				$(".store-locator").toggleClass('store-loc-map-toggle');
				$(".store-locator-search-mobile").toggleClass('store-loc-mobile-toggled');
				//resize map here
				google.maps.event.trigger(map, 'resize');
			},
			selectStore: function (e) {
				var self = this;
				var currentTarget = e.currentTarget;
				$(".store-locator-result").removeClass("is-highlighted");
				$(currentTarget).addClass("is-highlighted");
				var storeIndex = parseInt(currentTarget.getAttribute("data-mz-index"), 10) - 1;
				var data = self.model.attributes.stores[storeIndex];
				var geo = {
					"lat": parseFloat(data.properties.latitude),
					"lng": parseFloat(data.properties.longitude)
				};

				self.clearInfoWindow();

				//setZoom and panTo to current selected store
				map.setZoom(Hypr.getThemeSetting('mapZoomLevel'));
				map.panTo(geo);

				//get store details in HTML for infowindow
				var content = self.infoWindowContent(data, (storeIndex === 0 && data.isPreferresStore));
				infoWindow = new google.maps.InfoWindow({
					content: content,
					position: geo
				});
				infoWindow.open(map);
			},
			loadMap: function () {
				var self = this;
				var storeList = self.model.get("stores");
				var index;

				// initialize the map
				map = new google.maps.Map(document.getElementById('googleMap'), {
					disableDefaultUI: true,
					zoomControl: true,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				});
				//plot markers on map
				if (storeList && storeList.length > 0) {
					_.each(storeList, function (store, index) {
						var location, marker, icon;
						if (store.properties.displayOnline) {
							location = new google.maps.LatLng(parseFloat(store.properties.latitude), parseFloat(store.properties.longitude));

							icon = (index === 0 && store.isPreferresStore) ? markerIcons.selection : markerIcons.store;

							marker = new google.maps.Marker({
								position: location,
								icon: icon,
								map: map
							});
							self.addMarkerEvent(marker, store, (index === 0 && store.isPreferresStore));
							markers.push(marker);
						}
					});
				} else {
					self.resetMap();
				}

				//plot current position on map
				if ($.cookie("currentPosition")) {
					self.currentPosition(map, markerIcons);
				}
				//fit map bounds
				self.zoomExtends();

				//additional map events
				self.addMapEvents();
			},
			resetMap: function () {
				this.clearMarkers();
				var center = new google.maps.LatLng(53.631611, -113.323975);
				map.setCenter(center);
				map.setZoom(4);
			},
			clearMarkers: function () {
				_.each(markers, function (marker) {
					marker.setMap(null);
				});
			},
			clearInfoWindow: function () {
				// close any existing InfoWindows	
				if (infoWindow) {
					infoWindow.close();
					infoWindow = null;
				}
			},
			zoomExtends: function () {
				var bounds = new google.maps.LatLngBounds();
				if (markers.length > 0) {
					_.each(markers, function (marker) {
						bounds.extend(marker.getPosition());
					});
					map.fitBounds(bounds);
					map.panToBounds(bounds);
				}
				// fit bounds as screen size changes
				map.addListener('resize', function () {
					map.fitBounds(bounds);
					map.panToBounds(bounds);
				});
			},
			currentPosition: function (map, markerIcons) {
				var self = this;
				var currentMarker, shopperPos;
				// check current position set in cookie or not if -> yes plot on map
				shopperPos = $.parseJSON($.cookie("currentPosition"));
				currentMarker = new google.maps.Marker({
					position: shopperPos,
					icon: markerIcons.center,
					map: map,
					clickable: false
				});
				markers.push(currentMarker);
				self.zoomExtends();
			},
			addMapEvents: function () {
				var self = this;
				// close any existing InfoWindows on click outside window 
				map.addListener('click', function () {
					self.clearInfoWindow();
				});
			},
			addMarkerEvent: function (marker, data, selection) {
				var self = this;
				marker.addListener("click", function () {
					// close any existing InfoWindows
					self.clearInfoWindow();

					var content = self.infoWindowContent(data, selection);
					var geo = {
						"lat": parseFloat(data.properties.latitude),
						"lng": parseFloat(data.properties.longitude)
					};
					infoWindow = new google.maps.InfoWindow({
						content: content,
						position: geo
					});
					infoWindow.open(map);
					$('.store-locator-results').animate({
						scrollTop: $("#" + data.name).parent().scrollTop() + $("#" + data.name).offset().top - $("#" + data.name).parent().offset().top
					}, 1000);
					$(".store-locator-result").removeClass("is-highlighted");
					$("#" + data.name).addClass("is-highlighted");
				});
			},
			infoWindowContent: function (data, selection) {
				var storeLink = "store/" + data.name;
				var status, address1 = "",
					address2 = "",
					phone = "";
				if (data.storeStatus.status === "open") {
					status = true;
				}
				if (data.properties.address1 !== "NA" && data.properties.address1 !== "N/A") {
					address1 = data.properties.address1;
				}
				if (data.properties.address2 !== "NA" && data.properties.address2 !== "N/A") {
					address2 = data.properties.address2;
				}
				if(data.properties.phone){
					var phoneNumList = data.properties.phone.split("#");
					phone = phoneNumList[0];
				}
				var content = '<div class="store-info-window">' +
					'<div class="info-container">' +
					'<a href=' + storeLink + ' ' + 'class="link"><span class="material-icons">keyboard_arrow_right</span></a>' +
					'<div class="info-content">' +
					'<h4 class="store-title">' + data.properties.storeName + '</h4>' +
					'<p class="store-address-details">' +
					(
						address1 ? '<span class="addr-street">' + address1 + '</span>' :
						""
					) +
					(
						address2 ? '<span class="addr-street">' + address2 + '</span>' :
						""
					) +
					'<span class="addr-loc">' + data.properties.city + '</span>' +
					'<span class="addr-tele">' + phone + '</span>' +
					'</p>' +
					'<p class="store-status">' +
					(
						status ? '<span><i class="material-icons">done</i>' + Hypr.getLabel("openNow") + '</span>' :
						'<span>' + Hypr.getLabel("closedNow") + '</span>'
					) +
					(
						status ? data.storeStatus.hours : ""
					) +
					'</p>' +
					'</div>' +
					'</div>' +
					(
						selection ? '<div class="home-store"><span class="material-icons">place</span>' + Hypr.getLabel('myStore') + '</div>' :
						'<button class="make-store" data-mz-store=' + data.name + ' data-mz-action="makeMyStore">' + Hypr.getLabel('makeMyStore') + '</button>'
					) +
					'</div>';
				return content;
			},
			searchOnCurrentLocation: function (e) {
				var self = this,
					queryString = "";
				e.preventDefault();
				$("#content-loading").show();
				if (navigator.geolocation) {
					navigator.geolocation.getCurrentPosition(function (location) {
						var currentLocation = {
							"lat": location.coords.latitude,
							"lng": location.coords.longitude
						};
						$.cookie("currentPosition", JSON.stringify(currentLocation), {
							path: '/'
						});
						if (self.model.get("search")) {
							search = "";
							self.model.unset("search");
						}
						if (isHomeFurnitureSite) {
							queryString = "properties.displayOnline eq true and properties.storeType in['Home Furniture']";
						} else {
							if (window.location.search.indexOf('storeService') > 0) {
								returnParam = (window.location.search.split('storeService=')[1] || '');
								var serviceFilterString = decodeURIComponent(returnParam) === "Home Installs" ? "properties.storeServices in['Home Installs']" : "properties.commercialMaintenance eq true ";
								queryString = "properties.displayOnline eq true and properties.storeType in['Home Hardware','Home Building Centre','Home Hardware Building Centre'] and " + serviceFilterString;
							} else {
								queryString = "properties.displayOnline eq true and properties.storeType in['Home Hardware','Home Building Centre','Home Hardware Building Centre']";
							}
						}
						api.request('GET', '/api/content/documentlists/' + locationList + '/views/default/documents?filter=' + queryString + '&pageSize=1100', {
							cache: false
						}).then(function (response) {
							var nearStoresList = findNearStores(response.items, currentLocation, Hypr.getThemeSetting("storeDistanceInkm"));
							self.reRenderView(nearStoresList);
						});

					}, function (error) {
						search = "";
						self.model.set("search", "empty");
						setTimeout(function () {
							$("#content-loading").hide();
							self.model.unset("stores", []);
							self.render();
							$("#useMyLocation").hide();
							$("#locationError").show();
						}, 500);
					}, {
						enableHighAccuracy: true,
						maximumAge: 0,
						timeout: 5000
					});
				} else {
					// browser does not support geolocation.
					setTimeout(function () {
						$("#content-loading").hide();
					}, 500);
					$("#useMyLocation").hide();
					$("#locationError").show();
				}
			},
			continueWithNewStore: function () {
				var me = this;
				var returnUrl = "/";
				if (window.location.search.indexOf('returnUrl') > 0) {
					returnUrl = (window.location.search.split('returnUrl=')[1] || '');
				}else{
					var locale = apiContext.headers['x-vol-locale'];
					locale = locale.split('-')[0];
					var currentLocale = '';
					if (Hypr.getThemeSetting('homeFurnitureSiteId') != contextSiteId){
						currentLocale = locale === 'fr' ? '/fr' : '/en';
					}
					returnUrl = currentLocale;
				}
				$.ajax({
					url: "/set-purchase-location",
					data: {
						"purchaseLocation": storeToChange
					},
					type: "GET",
					success: function (response) {
						api.get("location", {
							code: storeToChange
						}).then(function (response) {
							var selectedStore = response.data;
							var expiryDate = new Date();
							expiryDate.setYear(expiryDate.getFullYear() + 1);
							$.cookie("preferredStore", JSON.stringify(selectedStore), {
								path: '/',
								expires: expiryDate
							});
							$.cookie("sessionStore",JSON.stringify(selectedStore.code),{path:'/'});
							if (user.isAuthenticated && !user.isAnonymous) {
								me.updateCustomerAttribute(returnUrl);
							} else {
								if (returnUrl.includes("/store/")) {
									window.location.href = locale === 'fr' ? '/fr' : '/en';
								} else {
									window.location.href = returnUrl;
								}
							}
						});
					},
					error: function (error) {

					}
				});
			},
			applyFilters: function (e) {
				var me = this;
				e.stopImmediatePropagation();
				var screenWidth = window.matchMedia("(max-width: 767px)");
				var filtersToApply = $(".store-locator-filters").find("input:checked");
				$(".store-locator-filters").removeClass('store-locator-filters-open');
				if (screenWidth.matches) {
					$("body,html").removeClass("no-scroll");
				}
				if (filtersToApply.length > 0) {
					$("#content-loading").show();
					checkedFilters = filtersToApply;
					me.getFilterValues(checkedFilters);
				} else {
					if (checkedFilters.length > 0) {
						$("#content-loading").show();
						checkedFilters = filtersToApply;
						me.getFilterValues(checkedFilters);
					}
				}
			},
			getFilterValues: function (filters) {
				var me = this;
				var storeTypeFilters = [],
					storeServiceFilters = [],
					storesProductsFilters = [],
					queryString = "",
					distanceFilter = "",
					preferredStore = null;
				if ($.cookie("preferredStore")) {
					preferredStore = $.parseJSON($.cookie("preferredStore"));
				}
				_.each(filters, function (filter) {
					var type = $(filter).data("mz-type");

					switch (type) {
						case "distance":
							distanceFilter = $(filter).data("mz-value");
							break;
						case "storeType":
							storeTypeFilters.push("'" + $(filter).data("mz-value") + "'");
							break;
						case "storeService":
							storeServiceFilters.push("'" + $(filter).data("mz-value") + "'");
							break;
						case "status":
							statusFilter = filter.checked;
							break;
						case "productsFilters":
							storesProductsFilters.push("'" + $(filter).data("mz-value") + "'");
							break;
					}
				});

				if (!isHomeFurnitureSite) {
					if (storeTypeFilters.length > 0 && storeServiceFilters.length > 0) {
						queryString = 'properties.storeType in[' + storeTypeFilters.join() + '] and properties.storeServices in[' + storeServiceFilters.join() + ']';
					} else if (storeServiceFilters.length > 0) {
						queryString = 'properties.storeServices in[' + storeServiceFilters.join() + ']';
					} else if (storeTypeFilters.length > 0) {
						queryString = 'properties.storeType in[' + storeTypeFilters.join() + ']';
					}
					if (!queryString) {
						/* Check Store Service parameter in url*/
						if (window.location.search.indexOf('storeService') > 0) {
							returnParam = (window.location.search.split('storeService=')[1] || '');
							var serviceFilter = decodeURIComponent(returnParam) === "Home Installs" ? "properties.storeServices in['Home Installs']" : "properties.commercialMaintenance eq true ";
							queryString = "properties.storeType in['Home Hardware','Home Building Centre', 'Home Hardware Building Centre'] and properties.displayOnline eq true and " + serviceFilter;
						} else {
							queryString = "properties.storeType in['Home Hardware','Home Building Centre', 'Home Hardware Building Centre'] and properties.displayOnline eq true";
						}
					} else {
						/* Check Store Service parameter in url*/
						if (window.location.search.indexOf('storeService') > 0) {
							returnParam = (window.location.search.split('storeService=')[1] || '');
							var serviceFilterString = decodeURIComponent(returnParam) === "Home Installs" ? "properties.storeServices in['Home Installs']" : "properties.commercialMaintenance eq true ";
							queryString += " and properties.displayOnline eq true and " + serviceFilterString;
						} else {
							queryString += " and properties.displayOnline eq true";
						}
					}
				} else {
					if (storesProductsFilters.length > 0 && storeServiceFilters.length > 0) {
						queryString = 'properties.homeFurnitureServices in[' + storesProductsFilters.join() + '] and properties.storeServices in[' + storeServiceFilters.join() + ']';
					} else if (storeServiceFilters.length > 0) {
						queryString = 'properties.storeServices in[' + storeServiceFilters.join() + ']';
					} else if (storesProductsFilters.length > 0) {
						queryString = 'properties.homeFurnitureServices in[' + storesProductsFilters.join() + ']';
					}
					if (!queryString) {
						queryString += "properties.storeType in['Home Furniture'] and properties.displayOnline eq true";
					} else {
						queryString += "and properties.storeType in['Home Furniture'] and properties.displayOnline eq true";
					}
				}

				api.request('GET', '/api/content/documentlists/' + locationList + '/views/default/documents?filter=' + queryString + '&pageSize=1100', {
					cache: false
				}).then(function (response) {
					var nearStoresList;
					if (distanceFilter) {
						// filter response with distance
						nearStoresList = searchBasedLatLong ? findNearStores(response.items, searchBasedLatLong, distanceFilter) : findNearStores(response.items, preferredStore.geo, distanceFilter);
						me.reRenderView(nearStoresList);
					} else {
						nearStoresList = searchBasedLatLong ? findNearStores(response.items, searchBasedLatLong, Hypr.getThemeSetting("storeDistanceInkm")) : findNearStores(response.items, preferredStore.geo, Hypr.getThemeSetting("storeDistanceInkm"));
						me.reRenderView(nearStoresList);
					}
				});
			},
			resetFilters: function () {

				$(".store-locator-filters").removeClass('store-locator-filters-open');
				var screenWidth = window.matchMedia("(max-width: 767px)");
				if (screenWidth.matches) {
					$("body,html").removeClass("no-scroll");
				}
				_.each(checkedFilters, function (filter) {
					var id = $(".store-locator-filters").find("#" + filter.id);
					$(id).prop("checked", true);
				});

			},
			reRenderView: function (storesList) {
				var me = this;
				var nearStoresList = [],
					preferredStore, currentStore;
				if (storesList && storesList.length > 0) {
					me.resetMap();
					if ($.cookie("preferredStore")) {
						currentStore = $.parseJSON($.cookie("preferredStore"));
						preferredStore = _.findWhere(storesList, {
							name: currentStore.code
						});
						nearStoresList = _.without(storesList, _.findWhere(storesList, {
							name: currentStore.code
						}));

						if (preferredStore)
							nearStoresList.unshift(preferredStore);

						nearStoresList = nearStoresList.slice(0, 49);
					} else {
						nearStoresList = storesList.slice(0, 49);
					}

					_.each(nearStoresList, function (store) {
						setStoreStatus(store, days[today.getDay()]);
					});

					if (statusFilter) {
						statusFilter = false;
						nearStoresList = _.filter(nearStoresList, function (store) {
							if (store.storeStatus.status === "open") {
								return store;
							}
						});
					}
				} else {
					setTimeout(function () {
						window.location.reload();
					}, 2000);
				}
				var storeData = {
					"stores": nearStoresList
				};
				me.model = new Backbone.Model(storeData);
				setTimeout(function () {
					me.render();
					$("#content-loading").hide();
					if (checkedFilters && checkedFilters.length > 0) {
						me.resetFilters();
					}
				}, 500);
			},
			getFrenchTranslation: function (service) {
				var servicesList = Hypr.getThemeSetting("storeServices");
				var storeService = _.find(servicesList, function (item) {
					return item.enName === service;
				});
				if (storeService)
					return storeService.frName;
			},
			getFrenchTypeTranslation: function (type) {
				var frenchValue = "";
				switch (type) {
					case "Home Building Centre":
						frenchValue = "Centre de Rénovation";
						break;
					case "Home Hardware Building Centre":
						frenchValue = "Centre de Rénovation Home Hardware";
						break;
					case "Home Hardware":
						frenchValue = "Quincaillerie Home Hardware";
						break;
				}
				return frenchValue;
			},
			addEmptySearch: function () {
				var me = this;
				me.model.set("noStores", "empty");
			}
		});

		/*This function finds stores near preferred store in 50km range by default*/
		function findNearStores(storeList, location, range) {
			var nearStores = [];
			_.each(storeList, function (store) {
				var distance = getDistanceFromLatLongInKm(store.properties.latitude, store.properties.longitude, location.lat, location.lng);
				if (distance <= range) {
					var storeData = {
						distance: parseFloat(distance.toFixed(3)),
						modelData: store
					};
					nearStores.push(storeData);
				}
			});
			nearStores = _.sortBy(nearStores, "distance");
			var resultArray = [];
			_.each(nearStores, function (store) {
				resultArray.push(store.modelData);
			});
			return resultArray;
		}
		/*This function calculates and return the distance between preferred store and input store*/
		function getDistanceFromLatLongInKm(lat1, lng1, lat2, lng2) {
			var earthRadius = 6371; // Radius of the earth in km
			var dLat = deg2rad(lat2 - lat1); // deg2rad below
			var dLng = deg2rad(lng2 - lng1);
			var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.sin(dLng / 2) * Math.sin(dLng / 2);
			var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
			var distance = earthRadius * c * 0.62137119; // Distance in Mi
			return distance;
		}
		/*this funtion converts deg to rad*/
		function deg2rad(deg) {
			return deg * (Math.PI / 180);
		}

		/*this funtion set the store working hours status i.e open/closed*/
		function setStoreStatus(store, day) {
			store.storeStatus = getStoreStatus(store, day);
		}

		/*this function calculates and returns status object with working hours*/
		function getStoreStatus(store, day) {
			var workingHours = "",
				statusData;
			var start = new Date(),
				end = new Date();
			switch (day) {

				case "Sunday":
					workingHours = store.properties.sundayHours;
					break;
				case "Monday":
					workingHours = store.properties.mondayHours;
					break;
				case "Tuesday":
					workingHours = store.properties.tuesdayHours;
					break;
				case "Wednesday":
					workingHours = store.properties.wednesdayHours;
					break;
				case "Thursday":
					workingHours = store.properties.thursdayHours;
					break;
				case "Friday":
					workingHours = store.properties.fridayHours;
					break;
				case "Saturday":
					workingHours = store.properties.saturdayHours;
					break;
			}
			var currentTime = moment(),
				startTime, endTime;
			if (workingHours !== "Closed") {
				var workHours = workingHours.split("-");
				startTime = moment(workHours[0], "HH:mm a");
				endTime = moment(workHours[1], "HH:mm a");
			}
			if (currentTime.isBetween(startTime, endTime)) {
				statusData = {
					"status": "open",
					"hours": workingHours
				};
			} else {
				statusData = {
					"status": "closed",
					"hours": workingHours
				};
			}
			return statusData;
		}

		try {
			$("#content-loading").show();
			if ($.cookie("preferredStore")) {
				api.request('GET', '/api/content/documentlists/' + locationList + '/views/default/documents?filter=' + filtersString + '&pageSize=1100', {
					cache: false
				}).then(function (response) {
					var currentStore = $.parseJSON($.cookie("preferredStore"));
					var range = Hypr.getThemeSetting("storeDistanceInkm");
					var nearStoresList = [];
					if (response && response.items.length > 0) {
						nearStoresList = findNearStores(response.items, currentStore.geo, range);
						var preferredStore = _.findWhere(nearStoresList, {
							name: currentStore.code
						});
						if (preferredStore) {
							nearStoresList = _.without(nearStoresList, _.findWhere(nearStoresList, {
								name: currentStore.code
							}));
							nearStoresList.unshift(preferredStore);
						}

						nearStoresList = nearStoresList.slice(0, 49);
						_.each(nearStoresList, function (store) {
							setStoreStatus(store, days[today.getDay()]);
						});
					}

					var storeData = {
						"stores": nearStoresList
					};
					storeView = new StoreView({
						el: $("#store-locator"),
						model: new Backbone.Model(storeData)
					});
					storeView.render();
					$("#content-loading").hide();
				}, function (error) {
					console.error("API Error: ", error);
				});
			} else {
				var storeData = {
					"stores": []
				};
				storeView = new StoreView({
					el: $("#store-locator"),
					model: new Backbone.Model(storeData)
				});
				storeView.addEmptySearch();
				storeView.render();
				$("#content-loading").hide();
			}
		} catch (error) {
			console.log("Error...while calling api..." + error);
		}

	});