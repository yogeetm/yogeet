require(["modules/jquery-mozu", 
	"underscore", 
	"hyprlive", 
	"modules/backbone-mozu", 
	'modules/api',
	'hyprlivecontext'], 
	function ($, _, Hypr, Backbone, api, HyprLiveContext) {
	
	
	
	$(document).ready(function() {			
		var postingFormResume = '';
		var postingFormApplication = '';
		var locale = require.mozuData('apicontext').headers['x-vol-locale'];
		var clearFields = function(){
			$("#name, #emailId, #coverLetter").val('');
			postingFormResume = '';
			postingFormApplication = '';
			$('.file-input').find('span').text('');
		};	
		
		$.urlParam = function(name){
	        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	        if (results===null){
	           return null;
	        }
	        else{
	           return decodeURI(results[1]) || 0;
	        }
	    };
	    	    
	    var fromCareersType = $.urlParam('fromCareersType');
	    var isRetail = false;
		var isCorporate = false;
	    if(fromCareersType == "corporate") {
	    	if(locale === "en-US") {
	    		$(".postings-link").attr("href", "/en/careers?careersType=" + fromCareersType);
	    	} else {
	    		$(".postings-link").attr("href", "/fr/careers?careersType=" + fromCareersType);
	    	}
	    	 
	    	 isRetail = false;
	 		 isCorporate = true;
	 		 $('.download-link.retail').remove();
	    } else {
	    	if(locale === "en-US") {
	    		$(".postings-link").attr("href", "/en/careers?careersType=" + fromCareersType);
	    	} else {
	    		$(".postings-link").attr("href", "/fr/careers?careersType=" + fromCareersType);
	    	}
	    	$('#apply-address-wrapper').remove();
	    	isRetail = true;
	 		isCorporate = false;
	 		 $('.download-link.corporate').remove();
	    }
		
		$('input[type="file"]').change(function(){
		    var file = this.files[0];		    
		    if(file) {
		    	var ext = file.name.split('.').pop().toLowerCase();	
		    	
		    	if ($.inArray(ext, ['doc', 'docx', 'pdf']) == -1) {
		    		$(this).parent().find('.errors').remove();
			    	$(this).next().after( "<span class='errors'>" + Hypr.getLabel('uploadCorrectFile') + "</span> ");
		        } else {
		        	$(this).parent().find('.errors').remove();  
		        	if($(this)[0].id === 'posting-form-resume') {
				    	postingFormResume = file.name;
				    } else if($(this)[0].id === 'posting-form-application') {
				    	postingFormApplication = file.name;
				    }
		        	$(this).parent().find('span').text(file.name);
		        }
		    	
		    	
		    	
		    }
		});
		
		$(document).on('click', '.submit-button', function(){			
			var errors = false;
			var emailReg = Backbone.Validation.patterns.email;
			$(".errors").remove();
			
			if($("#name").val() === ""){
			    $("#name").after( "<span class='errors'>" + Hypr.getLabel('missingName') + "</span> ");
			    errors = true;
			}
			
			if($("#emailId").val() === ""){
			    $("#emailId").after("<span class='errors'>" + Hypr.getLabel('missingEmail') + "</span>");
			    errors = true;                            
			  }else if(!emailReg.test($("#emailId").val())){
			    $("#emailId").after( "<span class='errors'>" + Hypr.getLabel('invalidEmail') + "</span>");
			    errors = true;
			  }
			
			if(!postingFormResume){
			    $("#posting-form-resume").next().after( "<span class='errors'>" + Hypr.getLabel('missingResume') + "</span> ");
			    errors = true;
			}
			
			if(!$('#g-recaptcha-response').val()){
			    $(".g-recaptcha").after( "<span class='errors'>" + Hypr.getLabel('enterCaptcha') + "</span> ");
			    errors = true;
			}
			
			var storeEmail = $(this).data('storeemail');
			var corpEmail = $(this).data('corpemail');
			
			if(!errors) {
				$('.careers-form').closest('form').addClass('is-loading');
				var formData = new FormData(document.getElementById("careers-form"));
				formData.append("storeEmail", storeEmail);
				formData.append("corpEmail", corpEmail);
				formData.append("isRetail", isRetail);
				formData.append("isCorporate", isCorporate);
				formData.append("locale", locale);
				$.ajax({
		           url: Hypr.getThemeSetting('jobPostingUrl') + $('.pagetitle').text(), 
		           type:"POST",
		           data : formData,
		           enctype: 'multipart/form-data',
		           processData: false,
		           contentType: false
	           }).done(function(data) {
	        	   $('.sucess-email').show();
	        	   $('.careers-form').closest('form').removeClass('is-loading');
	               clearFields();
	           }).fail(function(jqXHR, textStatus) {
	        	   $('.error-email').show();
	        	   
	           });
			} else {
				$('html, body').scrollTop($('.errors').closest('.form-group').offset().top);
			}
			
		});
	});
});