require(["modules/jquery-mozu", 
    "underscore", "hyprlive", 
    "modules/backbone-mozu", 
    "modules/models-checkout", 
    "modules/views-messages", 
    "modules/cart-monitor", 
    'hyprlivecontext', 
    'modules/editable-view', 
    'modules/preserve-element-through-render',
    'modules/xpress-paypal',
    'modules/models-customer',
    'modules/api',
    'creditcardvalidator',
    'shim!bootstrap',
    'pages/preferred-store'], 
    function ($, _, Hypr, Backbone, CheckoutModels, messageViewFactory, CartMonitor, HyprLiveContext, EditableView, preserveElements,PayPal, CustomerModels, api, creditcardvalidator) {

    var apiContext = require.mozuData('apicontext');    
    var CheckoutStepView = EditableView.extend({
        edit: function () {
            this.model.edit();
        },
        next: function (e) {
            // wait for blur validation to complete
            var me = this;
            me.editing.savedCard = false;
            
            //fix to make province empty when due to browser's auto fill feature, the province get selected other than Canada's states with the country as CA
            if(this.$el.hasClass('mz-checkoutform-paymentinfo')) {
            	if(me.model.get('billingContact').get('address').get('countryCode') === 'CA') {
            		var currentProvince = me.model.get('billingContact').get('address').get('stateOrProvince');
            		var canadaProvinceKeys = [];
            		_.each(Hypr.getThemeSetting('canadaStates'), function(province) {
            			canadaProvinceKeys.push(province.key);
            		});
            		var isCanadaProvince = _.contains(canadaProvinceKeys,currentProvince);
            		if(!isCanadaProvince) {
            			_.each($('.mz-addressform-state').children(), function(ele){
            				if($(ele).hasClass('mz-validationmessage')){
            					me.model.get('billingContact').get('address').set('stateOrProvince', '');
            					$(ele).siblings('.select-icon-cont').find('select').val(Hypr.getLabel('selectProvince'));
            				}
            			});
            		}
            	}
            }
            if(!$('#aeroplanInfo').hasClass('hidden')){
            	var isValid = me.validateFields('input[data-mz-validate-aeroplan="validate"]');
            	if(isValid){
            		me.model.parent.get('orderAttributes').forEach(function (attribute) {
            			me.setOrderAttributeValue(attribute);
            		});
                	var updateAttrs = [];
            		me.model.parent.get('orderAttributes').forEach(function (attribute) { //push attributes to be updated in updateAttrs array
            			var attrVal = attribute.value;	
            			if(attrVal){
            				updateAttrs.push({
                                'fullyQualifiedName': attribute.attributeFQN,
                                'values': [ attrVal ]
                            });
            			}
            		});
            		 me.model.parent.apiUpdateAttributes(updateAttrs).then(function(response) {
            			 me.model.set('attributes', response);
            		 });
            		 
            		 //update aeroplan number in customer attribute
            		var customer = new CustomerModels.EditableCustomer(); 
            		if(window.order.get("customer").get('id')) {
	              		customer.set("id",window.order.get("customer").get('id'));
	              		customer.fetch().then(function(response) {
	              			if(response.get('attributes')) {
	              				var isAeroplanNumberAttribute = _.find(response.get('attributes').toJSON(), function (attribute) { 
	                		        return attribute.fullyQualifiedName === Hypr.getThemeSetting('isAeroplanMember');
	                		    });
	              				if(isAeroplanNumberAttribute) {
	              					response.updateAttribute(isAeroplanNumberAttribute.fullyQualifiedName,isAeroplanNumberAttribute.attributeDefinitionId,[true]);
	              				}
	             				var aeroplanNumberAttribute = _.find(response.get('attributes').toJSON(), function (attribute) { 
	                		        return attribute.fullyQualifiedName === Hypr.getThemeSetting('aeroplanMemberNumber');
	                		    });
	                 			if(aeroplanNumberAttribute) {
	                 				response.updateAttribute(aeroplanNumberAttribute.fullyQualifiedName,aeroplanNumberAttribute.attributeDefinitionId,[me.model.get('aeroplanNumber')]);
	                 			}
	             			}
	              		});
            		}
            		 
            	}
            	else return false;
            }
            _.defer(function () {
            	ga('ec:setAction', 'checkout_option', {
                	'step': 3,
                	'option': "Order Review"
                });
                ga('send', 'event', 'Checkout', 'Checkout Step Change', 'Order Review');
                me.model.next();
            });
        },
        choose: function () {
            var me = this;
            me.model.choose.apply(me.model, arguments);
        },
        constructor: function () {
            var me = this;
            EditableView.apply(this, arguments);
            me.resize();
            setTimeout(function () {
                me.$('.mz-panel-wrap').css({ 'overflow-y': 'hidden'});
            }, 250);
            me.listenTo(me.model,'stepstatuschange', me.render, me);
            me.$el.on('keypress', 'input', function (e) {
                if (e.which === 13) {
                    me.handleEnterKey(e);
                    return false;
                }
            });
        },
        initStepView: function() {
            this.model.initStep();
        },
        handleEnterKey: function (e) {
            this.model.next();
        },
        render: function () {
            this.$el.removeClass('is-new is-incomplete is-complete is-invalid is-undefined').addClass('is-' + this.model.stepStatus());
            EditableView.prototype.render.apply(this, arguments);
            this.resize();
        },
        setOrderAttributeValue: function(attribute) {
    		var me = this;
    		switch (attribute.attributeCode){
    			case Hypr.getThemeSetting('aeroplanMilesCode') :
    				if(me.model.get('aeroplanPromoCode')) {
    					var milesFactor = me.model.get('aeroplanMiles');
    					if(typeof(milesFactor) === 'string') { //if miles are already calculated with promotion and now user editing the step
    						var milesFactorToMultiply = milesFactor.split('X'); 
        					var aeroplanMiles = (Math.floor(me.model.parent.get('discountedTotal')/2)) * milesFactorToMultiply[0];
        					me.model.set('aeroplanMiles', aeroplanMiles);
    					} 
    				}
    				attribute.value = me.model.get('aeroplanMiles').toString();
    				break;
    			case Hypr.getThemeSetting('aeroplanNumberCode') :
    				var aeroplanNumber = $('#checkoutAeroplanNumberInput').val();
    				attribute.value = aeroplanNumber;
    				var aeroplanNumberPart = '******' + aeroplanNumber.slice(-3);
    				me.model.set('aeroplanNumberPart', aeroplanNumberPart);
    				break;
    			case Hypr.getThemeSetting('aeroplanPromoCode') :
    				if(me.model.get('aeroplanPromoCode')) {
    					attribute.value = me.model.get('aeroplanPromoCode');
    				}else {
	            		attribute.value = '';
	            	}
    				break;
    			case Hypr.getThemeSetting('aeroplanBonusMessage') :
    				if(me.model.get('aeroplanPromoCode')) {
    					attribute.value = Hypr.getLabel('withPromoAeroplanMsg', me.model.get('aeroplanMiles'));
    				}else{
    					attribute.value = Hypr.getLabel('defaultAeroplanMsg');
    				}
    				break;
    			case Hypr.getThemeSetting('loyalty_program_points1') :
    				attribute.value = (Math.floor(me.model.parent.get('discountedTotal')/2)).toString();
    				break;
    			case Hypr.getThemeSetting('loyalty_program_points2') :
    				if(me.model.get('aeroplanPromoCode')) {
    					var bonusMiles = me.model.get('aeroplanMiles') - Math.floor(me.model.parent.get('discountedTotal')/2);
    					attribute.value = bonusMiles.toString();
    				}
    				break;
    			case Hypr.getThemeSetting('loyalty_promotion_code1') :
    				attribute.value = Hypr.getLabel('loyalty_promotion_code1_value');
    				break;
    		}
        },
        validateFields: function(inputToValidate) {
        	var isValid = true;
        	$(inputToValidate).each(function() {
        		var isAeroplanNumberValid = true;
        		if(this.hasAttribute('data-mz-validate-aeroplan-number')) {
    				if (!Backbone.Validation.patterns.number.test($(this).val()) || $(this).val().length != 9){
    					isValid = false;
    					isAeroplanNumberValid = false;
    					$(this).css({
    	                    "border": "1px solid #cccccc",
    	                    "background": "#f2dede"
    	                });
    	                $(this).siblings('.mz-validationmessage').text($(this).attr('validation-msg')); 
    				}
    			}
        		
        		if ($.trim($(this).val()) === '' || $(this).val() === 'N/A' || $(this).val() === 'n/a') {
	                isValid = false;
	                $(this).css({
	                    "border": "1px solid #cccccc",
	                    "background": "#f2dede"
	                });
	                $(this).siblings('.mz-validationmessage').text($(this).attr('validation-msg')); 
    			}
    			else {
    				if(isAeroplanNumberValid) {
		                $(this).css({
		                    "border": "",
		                    "background": ""
		                });
	                	$(this).siblings('.mz-validationmessage').text('');
	                }
	            }
        	});
        	return isValid;
        },
        resize: _.debounce(function () {
            this.$('.mz-panel-wrap').animate({'height': this.$('.mz-inner-panel').outerHeight() });
        },200)
    });

    var OrderSummaryView = Backbone.MozuView.extend({
        templateName: 'modules/checkout/checkout-order-summary',

        initialize: function () {
            var me = this, EHFTotal = 0;
            
            _.each(me.model.get('items'), function(item) {
            	if(item.handlingAmount) {
            		EHFTotal += item.handlingAmount;
            	}
            });
            me.model.set('EHFTotal', EHFTotal);
            var contextSiteId = apiContext.headers["x-vol-site"];
            var frSiteId = Hypr.getThemeSetting("frSiteId");
            if(frSiteId === contextSiteId){
                me.model.set("isFrenchSite", true);   
            }
            this.listenTo(this.model.get('billingInfo'), 'orderPayment', this.onOrderCreditChanged, this);
        },

        editCart: function () {
            window.location =  (HyprLiveContext.locals.siteContext.siteSubdirectory||'') + "/cart";
        },
        
        onOrderCreditChanged: function (order, scope) {
            this.render();
        },

        // override loading button changing at inappropriate times
        handleLoadingChange: function () { }
    });
    
    var CartSummaryView = Backbone.MozuView.extend({
    	templateName: 'modules/checkout/checkout-cart-summary',
    	initialize: function(){
    		var me = this;
    		//calculate the quantity of total items to show on order confirmation
        	var items = me.model.get('items');
        	var firstItemQuantity = 0;
        	_.each(items, function(item, index) {
        		if(item.product.productCode != 'EHF101') {
        			firstItemQuantity += item.quantity;
        		}
        	});
        	me.model.set('totalItemsQuantity', firstItemQuantity);
    	}
	});
    
    var InStorePickUpInfoSummaryView = Backbone.MozuView.extend({
    	templateName: 'modules/checkout/instore-pickup-info',
    	autoUpdate: [
    	    'firstPersonFirstName',
    	    'firstPersonLastName',
    	    'firstPersonEmail',
    	    'secondPersonFirstName',
    	    'secondPersonLastName',
    	    'secondPersonEmail'
    	],
    	additionalEvents: {
    		"change [data-mz-second-person-info]" : 'showSecondPersonInfo',
            "click #changeMyStore": "changeMyStore"
    	},
    	initialize: function () {
    		//set inStorePickUpStep value in model based on which pickup information form will be populated on click of edit
    		if($.cookie('inStorePickUpStep')){
    			this.model.set('inStorePickUpStep', $.cookie('inStorePickUpStep'));
    		}else if(this.model.get('attributes') && this.model.get('attributes').length > 0) {
    			this.model.set('inStorePickUpStep', 'complete');
    		}else {
    			this.model.set('inStorePickUpStep', 'incomplete');
    		}
    		
			var secondPerson = _.find(this.model.get('attributes'), function (attribute) { 
            	return attribute.fullyQualifiedName == 'tenant~' + Hypr.getThemeSetting('secondPersonFirstNameCode');
            });
			//set secondPersonRequired value in model to show second pickup person values pre-filled
			if(secondPerson && secondPerson.values[0] != 'N/A'){
				this.model.set('secondPersonRequired', true); 
			}else {
				this.model.set('secondPersonRequired', false);
			}
    		
    		if(this.model.get('attributes') && this.model.get('attributes').length > 0 && (this.model.get('inStorePickUpStep') === 'complete')){
    			this.$el.addClass('is-complete');
    		}else {
    			this.$el.addClass('is-incomplete');
    		}
    		//set values in orderAttributes to show instore pickup form prefilled if user refreshesh the page in between
    		var me = this;
    		if(this.model.get('attributes') && this.model.get('attributes').length > 0) {
				me.model.get('attributes').forEach(function (attribute) {
					me.setOrderAttributeValueOnPageReload(attribute);
        		});
			}
    		if($.cookie("preferredStore")){
    			var currentStore = $.parseJSON($.cookie("preferredStore"));
    			me.model.set('preferredStore', currentStore);
    		}
    	},
    	setOrderAttributeValueOnPageReload: function(attribute) {
    		this.model.get('orderAttributes').forEach(function (currAttribute) { 
				if(currAttribute.attributeFQN === attribute.fullyQualifiedName) {
					currAttribute.value = attribute.values[0];
				}
            });
    	},
    	setOrderAttributeValue: function(attribute) {
    		var me = this;
    		switch (attribute.attributeCode){
    			case Hypr.getThemeSetting('firstPersonFirstNameCode') :
    				attribute.value = me.model.get('firstPersonFirstName') ? me.model.get('firstPersonFirstName') : $('#checkout-attribute-firstPersonFirstName').val();
    				break;
    			case Hypr.getThemeSetting('firstPersonLastNameCode') :
    				attribute.value = me.model.get('firstPersonLastName') ? me.model.get('firstPersonLastName') : $('#checkout-attribute-firstPersonLastName').val();
    				break;
    			case Hypr.getThemeSetting('firstPersonEmailCode') :
    				attribute.value = me.model.get('firstPersonEmail') ? me.model.get('firstPersonEmail') : $('#checkout-attribute-firstPersonEmail').val();
    				break;
    			case Hypr.getThemeSetting('secondPersonFirstNameCode') :
    				attribute.value = me.model.get('secondPersonFirstName');
    				break;
    			case Hypr.getThemeSetting('secondPersonLastNameCode') :
    				attribute.value = me.model.get('secondPersonLastName');
    				break;
    			case Hypr.getThemeSetting('secondPersonEmailCode') :
    				attribute.value = me.model.get('secondPersonEmail');
    				break;
    		}
    	},
    	validateFields: function(inputToValidate) { //validations for pickup form fields using JQuery
    		var isValid = true;
    		
    		$(inputToValidate).each(function() {
    			var isEmailValid = true;
    			if(this.hasAttribute('data-mz-validate-email-field')) {
    				if (!Backbone.Validation.patterns.email.test($(this).val())){
    					isValid = false;
    					isEmailValid = false;
    					$(this).css({
    	                    "border": "1px solid #cccccc",
    	                    "background": "#f2dede"
    	                });
    	                $(this).siblings('.mz-validationmessage').text(Hypr.getLabel('emailMissing')); 
    				}
    			}
    			if ($.trim($(this).val()) === '' || $(this).val() === 'N/A' || $(this).val() === 'n/a') {
	                isValid = false;
	                $(this).css({
	                    "border": "1px solid #cccccc",
	                    "background": "#f2dede"
	                });
	                $(this).siblings('.mz-validationmessage').text($(this).attr('validation-msg')); 
    			}
    			else {
	                $(this).css({
	                    "border": "",
	                    "background": ""
	                });
	                if(isEmailValid) {
	                	$(this).siblings('.mz-validationmessage').text('');
	                }
	            }
    		});
    		return isValid;
    	},
    	edit: function() {
    		this.model.set('inStorePickUpStep', 'incomplete'); 
    		$.cookie('inStorePickUpStep', 'incomplete', {path: '/' }); //set this value in cookie to access it on page reload
    		var secondPerson = _.find(this.model.get('attributes'), function (attribute) { 
            	return attribute.fullyQualifiedName == 'tenant~' + Hypr.getThemeSetting('secondPersonFirstNameCode');
            });
			if(secondPerson && secondPerson.values[0] != 'N/A'){
				this.model.set('secondPersonRequired', true); 
			}else {
				this.model.set('secondPersonRequired', false);
			}
    		this.$el.removeClass('is-complete').addClass('is-incomplete');
    		this.render();
    	},
    	next: function() {
    		var me = this;
    		var isValid = false;
    		me.model.set('firstPersonFirstName', $('#checkout-attribute-firstPersonFirstName').val());
    		me.model.set('firstPersonLastName', $('#checkout-attribute-firstPersonLastName').val());
    		me.model.set('firstPersonEmail', $('#checkout-attribute-firstPersonEmail').val());
    		if(!$('#second-person-info').hasClass('hidden')) {
    			me.model.set('secondPersonFirstName', $('#checkout-attribute-secondPersonFirstName').val());
        		me.model.set('secondPersonLastName', $('#checkout-attribute-secondPersonLastName').val());
        		me.model.set('secondPersonEmail', $('#checkout-attribute-secondPersonEmail').val());
    		}
    		me.model.get('orderAttributes').forEach(function (attribute) {
    			me.setOrderAttributeValue(attribute);
    		});
    		isValid = me.validateFields('input[data-mz-validate-field="validate"]');
    		
			if(!$('#second-person-info').hasClass('hidden') && isValid) {
				isValid = me.validateFields('input[data-mz-validate-second-person-field="validate"]'); 
    		} 
    		if(!isValid) {
    			return false;
    		}else {
        		var updateAttrs = [];
        		me.model.get('orderAttributes').forEach(function (attribute) { //push attributes to be updated in updateAttrs array
        			var attrVal = attribute.value;	
        			if(attrVal){
        				updateAttrs.push({
                            'fullyQualifiedName': attribute.attributeFQN,
                            'values': [ attrVal ]
                        });
        			}
        		});
        		 me.model.apiUpdateAttributes(updateAttrs).then(function(response) {
        			 me.model.set('attributes', response);
        			 me.model.set('inStorePickUpStep', 'complete'); 
        			 $.cookie('inStorePickUpStep', 'complete', {path: '/' }); //set this value in cookie to access it on page reload
        			 me.$el.removeClass('is-incomplete').addClass('is-complete');
        			 me.render();
            		 _.invoke(window.checkoutViews.steps, 'initStepView');
        		 });
    		}
    		var customer = new CustomerModels.EditableCustomer(); 
            if(window.order.get("customer").get('id')) {
            	customer.set("id",window.order.get("customer").get('id'));
            	var isNameUpdated = false;
         		customer.fetch().then(function(response) {
         			if(response.get('lastName') === 'N/A' || response.get('lastName') === 'n/a') {
         				response.set('lastName', me.model.get('firstPersonLastName'));
         				isNameUpdated = true;
         			}
         			if(response.get('firstName') === 'N/A' || response.get('firstName') === 'n/a') {
         				response.set('firstName', me.model.get('firstPersonFirstName'));
         				isNameUpdated = true;
         			}
         			if(isNameUpdated) {
         				customer.apiUpdate();
         			}
         		});
         	}
            ga('ec:setAction', 'checkout_option', {
            	'step': 2,
            	'option': "Payment Information"
            });
            ga('send', 'event', 'Checkout', 'Checkout Step Change', 'Payment Information');
    	},
    	showSecondPersonInfo: function (e){
    		var me = this;
    		$('#second-person-info').toggleClass('hidden');
    		if($('#second-person-info').hasClass('hidden')){ //if user unchecks second person info in edit, set attributes values as "N/A" and hide on UI
    			me.model.set('secondPersonRequired', false);
    			me.model.set('secondPersonFirstName','N/A');
    			me.model.set('secondPersonLastName', 'N/A');
    			me.model.set('secondPersonEmail', 'N/A');
    		}else {
    			me.model.set('secondPersonRequired', true);
    			me.model.set('secondPersonFirstName', '');
    			me.model.set('secondPersonLastName', '');
    			me.model.set('secondPersonEmail', '');
    			me.model.get('orderAttributes').forEach(function (attribute) {
        			me.setOrderAttributeValue(attribute);
        		});
    		}
    		me.render();
    	},
        changeMyStore: function(e){
            e.preventDefault();
            var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            locale = locale.split('-')[0];
            var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
            var currentLocale = '';
            if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite){
            	currentLocale = locale === 'fr' ? '/fr' : '/en';
            }
            window.location.href=currentLocale + "/store-locator?returnUrl=/cart";
        },
        callStore: function(e){
            var screenWidth = window.matchMedia("(max-width: 767px)");
            if(!screenWidth.matches){
                e.preventDefault();
            }
        }
    });
    
    var ShippingAddressView = CheckoutStepView.extend({
        templateName: 'modules/checkout/step-shipping-address',
        autoUpdate: [
             'firstName',
             'lastNameOrSurname',
             'address.address1',
             'address.address2',
             'address.address3',
             'address.cityOrTown',
             'address.countryCode',
             'address.stateOrProvince',
             'address.postalOrZipCode',
             'address.addressType',
             'phoneNumbers.home',
             'contactId',
             'email'
         ],
         renderOnChange: [
             'address.countryCode',
             'contactId'
         ],
        beginAddContact: function () {
            this.model.set('contactId', 'new');
        }
    });

    var ShippingInfoView = CheckoutStepView.extend({
        templateName: 'modules/checkout/step-shipping-method',
        renderOnChange: [
            'availableShippingMethods'
        ],
        additionalEvents: {
            "change [data-mz-shipping-method]": "updateShippingMethod"
        },
        updateShippingMethod: function (e) {
            this.model.updateShippingMethod(this.$('[data-mz-shipping-method]:checked').val());
        }
    });

    var poCustomFields = function() {
        
        var fieldDefs = [];

        var isEnabled = HyprLiveContext.locals.siteContext.checkoutSettings.purchaseOrder &&
            HyprLiveContext.locals.siteContext.checkoutSettings.purchaseOrder.isEnabled;

            if (isEnabled) {
                var siteSettingsCustomFields = HyprLiveContext.locals.siteContext.checkoutSettings.purchaseOrder.customFields;
                siteSettingsCustomFields.forEach(function(field) {
                    if (field.isEnabled) {
                        fieldDefs.push('purchaseOrder.pOCustomField-' + field.code);
                    }
                }, this);
            }

        return fieldDefs;
    };

    var visaCheckoutSettings = HyprLiveContext.locals.siteContext.checkoutSettings.visaCheckout;
    var pageContext = require.mozuData('pagecontext');
    var currentCardElementId = '';
    var currentSelectedCardId = '';
    var appliedCouponCodes = [];
    var currentContact;
    var BillingInfoView = CheckoutStepView.extend({
        templateName: 'modules/checkout/step-payment-info',
        autoUpdate: [
            'savedPaymentMethodId',
            'paymentType',
            'card.paymentOrCardType',
            'card.cardNumberPartOrMask',
            'card.nameOnCard',
            'card.expireMonth',
            'card.expireYear',
            'card.cvv',
            'card.isCardInfoSaved',
            'card.isDefaultPayMethod',
            'check.nameOnCheck',
            'check.routingNumber',
            'check.checkNumber',
            'isSameBillingShippingAddress',
            'billingContact.firstName',
            'billingContact.lastNameOrSurname',
            'billingContact.address.address1',
            'billingContact.address.address2',
            'billingContact.address.address3',
            'billingContact.address.cityOrTown',
            'billingContact.address.countryCode',
            'billingContact.address.stateOrProvince',
            'billingContact.address.postalOrZipCode',
            'billingContact.phoneNumbers.home',
            'billingContact.email',
            'creditAmountToApply',
            'digitalCreditCode',
            'purchaseOrder.purchaseOrderNumber',
            'purchaseOrder.paymentTerm',
            'couponCode',
            'aeroplanNumber',
            'aeroplanLastName'
        ].concat(poCustomFields()),
        renderOnChange: [
            'paymentType',
            'isSameBillingShippingAddress',
            'usingSavedCard',
            'savedPaymentMethodId'
        ],
        additionalEvents: {
            "change [data-mz-digital-credit-enable]": "enableDigitalCredit",
            "change [data-mz-digital-credit-amount]": "applyDigitalCredit",
            "change [data-mz-digital-add-remainder-to-customer]": "addRemainderToCustomer",
            "click [name='paymentType']": "resetPaymentData",
            "change [data-mz-purchase-order-payment-term]": "updatePurchaseOrderPaymentTerm",
            "click [data-mz-value='savedPaymentMethodId']" : "setCurrentCreditCardNumber",
            "change [data-mz-existing-billing-addr]" : 'setBillingAddress',
            "change [data-mz-new-billing-addr]" : 'newBillingAddress',
            "change [data-mz-new-billing-addr1]" : 'newBillingAddressInModal',
            "change [data-mz-value='card.isDefaultPayMethod']" : 'makeDefaultCard',
            "change [data-mz-value='card.expireYear']" : "validateExpireYear",
            "change [data-mz-value='card.expireMonth']" : "validateExpireMonth",
            "change [data-mz-value='billingContact.address.countryCode']" : "changeProvince",
            "keyup [data-mz-value='card.cardNumberPartOrMask']" : "detectCardType",
            "change [data-mz-aeroplan-miles]" : 'showAeroplanForm'
        },

        initialize: function () {
            // this.addPOCustomFieldAutoUpdate();
            this.listenTo(this.model, 'change:digitalCreditCode', this.onEnterDigitalCreditCode, this);
            this.listenTo(this.model, 'orderPayment', function (order, scope) {
                    this.render();
            }, this);
            this.listenTo(this.model, 'billingContactUpdate', function (order, scope) {
                    this.render();
            }, this);
            this.listenTo(this.model, 'change:savedPaymentMethodId', function (order, scope) {
            	if(currentSelectedCardId){
            		this.model.set('savedPaymentMethodId', currentSelectedCardId);
            		this.model.set('usingSavedCard', true); 
            	}
                $('[data-mz-saved-cvv]').val('').change();
                this.render();
            }, this);
            this.codeEntered = !!this.model.get('digitalCreditCode');
            
            if(this.model.parent.get('couponCodes').length > 0){
            	this.model.set('appliedCouponCodes', this.model.parent.get('couponCodes'));
            	this.model.get('appliedCouponCodes').forEach(function(code){
            		appliedCouponCodes.push(code);
            	});
            }
            this.listenTo(this.model, 'change:couponCode', this.onEnterCouponCode, this);
            this.codeEntered = !!this.model.parent.get('couponCode');
            var me = this;
            me.$el.on('keypress', 'input', function (e) {
                if (e.which === 13) {
                    if (me.codeEntered) {
                        me.handleEnterKey();
                    }
                    return false;
                }
            });
            
            var currentCardIndicator = $.cookie('currentCardIndicator');
            if(currentCardIndicator) {
            	var self = this;
            	self.model.set('currentSavedCard', currentCardIndicator); //set current saved card's id to use it to show saved_indicator
            	setTimeout(function(){
            		$.removeCookie('currentCardIndicator', { path: '/' });
            	},1000);
            }
            
            //To show first saved credit card selected on page reload
            if(me.model.savedPaymentMethods()) {
            	setTimeout(function(){
                	$('#savedCredits1').attr('checked',true); 
                	$('#saved-credit-card-info1').addClass('in');
                	currentCardElementId = 'saved-credit-card-info1';
                	me.model.set('savedPaymentMethodId', $('#saved-payments1').attr('value'));
                	me.model.set('usingSavedCard', true);
                }, 500);
            }
            
            if(!this.model.get('billingContact.address')){
            	this.model.set('billingContact.address.countryCode', 'CA'); //for anonymous user
            }
            
            var aeroplanMiles = Math.floor(this.model.parent.get('discountedTotal')/2); 
            this.model.set('aeroplanMiles', aeroplanMiles);
            var documentListForAeroplanPromo = Hypr.getThemeSetting('documentListForAeroplanPromo');
            api.get('documentView', {listName: documentListForAeroplanPromo}).then(function(response) {
            	if(response.data.items.length > 0) {
            		var aeroplanMilesFactor = response.data.items[0].properties.markupFactor;
            		me.model.set('aeroplanMiles', aeroplanMilesFactor);
            		var promoCode = response.data.items[0].properties.promoCode;
            		me.model.set('aeroplanPromoCode', promoCode);
            	}
            });
            
            var customer = new CustomerModels.EditableCustomer(); 
            if(window.order.get("customer").get('id')) {
            	customer.set("id",window.order.get("customer").get('id'));
         		customer.fetch().then(function(response) {
         			me.model.set('aeroplanLastName', response.get('lastName'));
         			if(response.get('attributes')) {
         				var aeroplanNumberAttribute = _.find(response.get('attributes').toJSON(), function (attribute) { 
            		        return attribute.fullyQualifiedName === Hypr.getThemeSetting('aeroplanMemberNumber');
            		    });
             			if(aeroplanNumberAttribute) {
             				me.model.set('aeroplanNumber', aeroplanNumberAttribute.values[0]);
             				me.model.set('aeroplanLastName', response.get('lastName'));
             			}
         			}
         		});
            }
     		
        },
        showAeroplanForm: function() {
        	var me = this;
        	$('#aeroplanInfo').toggleClass('hidden');
        	if($('#aeroplanInfo').hasClass('hidden')) {
        		me.model.set('aeroplanNumber', 'N/A');
        	}else {
        		me.model.set('aeroplanNumber', ' ');
        		if(me.model.get('aeroplanLastName')){
        			me.model.set('aeroplanLastName', me.model.get('aeroplanLastName'));
        		}else {
        			me.model.set('aeroplanLastName', ' ');
        		}
        	}
        },
        onEnterCouponCode: function (model, code) {
            if (code && !this.codeEntered) {
                this.codeEntered = true;
                this.$el.find('button').prop('disabled', false);
            }
            if (!code && this.codeEntered) {
                this.codeEntered = false;
                this.$el.find('button').prop('disabled', true);
            }
        },
        deleteAllCardClasses: function(e) {
        	var classNames =  $(e.currentTarget).siblings('.defaultCardImg').attr("class").split(" ");
    	    var newclasses =[];
    	    var classToRemove = '';
    	    for(var i=0; i<classNames.length; i++) {
    	    	classToRemove = classNames[i].search(/show+/);
    	        if(classToRemove) newclasses[newclasses.length] = classNames[i];
    	    }
    	    return newclasses;
        },
        
//    	mastercard 51-55,2221-2720
//    	visa 4
//    	amex 34,37
//    	discover 6011, 622126-622925, 644-649, 65
        
        detectCardType: function(e) {
        	var me = this;
        	if($.trim($(e.currentTarget).val()) === ''){
        		var newcl = me.deleteAllCardClasses(e);
        	    $(e.currentTarget).siblings('.defaultCardImg').removeClass().addClass(newcl.join(" ")).addClass('showDefaultCard'); 
        	}else {
        		$(e.currentTarget).validateCreditCard(function(result) {
	           		 if(result.card_type){
	           			var newcl = [];
	           			 switch (result.card_type.name) {
	           			 	case 'visa' :
	           			 		newcl = me.deleteAllCardClasses(e);
	           			 		$(e.currentTarget).siblings('.defaultCardImg').removeClass().addClass(newcl.join(" ")).addClass('showVisaImg');
	           			 		me.model.set('card.paymentOrCardType', 'VISA');
	           			 		break;
	           			 	case 'mastercard' :
		           				newcl = me.deleteAllCardClasses(e);
	           			 		$(e.currentTarget).siblings('.defaultCardImg').removeClass().addClass(newcl.join(" ")).addClass('showMastercardImg');
	           			 		me.model.set('card.paymentOrCardType', 'MC');
	           			 		break;
	           			 	case 'amex' :
		           				newcl = me.deleteAllCardClasses(e);
	           			 		$(e.currentTarget).siblings('.defaultCardImg').removeClass().addClass(newcl.join(" ")).addClass('showAmexImg');
	           			 		me.model.set('card.paymentOrCardType', 'AMEX');
	           			 		break;
	           			 	case 'discover' :
		           				newcl = me.deleteAllCardClasses(e);
	           			 		$(e.currentTarget).siblings('.defaultCardImg').removeClass().addClass(newcl.join(" ")).addClass('showDiscoverImg');
	           			 		me.model.set('card.paymentOrCardType', 'DISCOVER');
	           			 		break;
	           			 	default : 
		           			 	newcl = me.deleteAllCardClasses(e);
		                	    $(e.currentTarget).siblings('.defaultCardImg').removeClass().addClass(newcl.join(" ")).addClass('showDefaultCard');
		                	    break;
	           			 }
	           		 }
                });
        	}
        },
        addCoupon: function (e) {
            // add the default behavior for loadingchanges
            // but scoped to this button alone
            var self = this;
            this.$el.addClass('is-loading');
            this.model.parent.set('couponCode', this.model.get('couponCode'));
            this.model.parent.addCoupon().ensure(function() {
                self.$el.removeClass('is-loading');
                if(!self.model.get('invalidCouponCode')){
                	appliedCouponCodes.push(self.model.get('couponCode'));
                    self.model.set('appliedCouponCodes', appliedCouponCodes);
                }
                self.model.unset('couponCode');
                self.render();
            });
        },
        changeProvince: function(e) {
        	$(e.currentTarget).closest('.new-billing-address-form').find('div[class*="province-"]').addClass('hidden');
        	if($(e.currentTarget).val() === 'US') {
        		$(document).find('.province-us').removeClass('hidden');
        	}else if($(e.currentTarget).val() === 'CA') {
        		$(document).find('.province-ca').removeClass('hidden');
        	}else {
        		$(document).find('.province-other').removeClass('hidden').find('input').val('');
        	}
        	
        },
        removeCoupon: function(e) {
        	var couponCode = $(e.currentTarget).attr('data-mz-value');
        	var self = this;
        	this.model.parent.removeCoupon(couponCode).ensure(function() {
        		if(appliedCouponCodes){
        			appliedCouponCodes = _.without(appliedCouponCodes, couponCode);
            		self.model.set('appliedCouponCodes', appliedCouponCodes);
        		}else {
        			self.model.set('appliedCouponCodes', ''); 
        		}
        		self.render();
        	});
        },
        resetPaymentData: function (e) {
            if (e.target !== $('[data-mz-saved-credit-card]')[0]) {
                $("[name='savedPaymentMethods']").val('0');
            }
            var aeroplanNumber = '', aeroplanLastName = '', aeroplanMiles= '';
            aeroplanMiles = this.model.get('aeroplanMiles');
            if(this.model.get('aeroplanNumber') && this.model.get('aeroplanNumber') != 'N/A') {
            	aeroplanNumber = this.model.get('aeroplanNumber');
            	aeroplanLastName = this.model.get('aeroplanLastName');
            }
            this.model.clear();
            if(aeroplanNumber) { 
            	this.model.set('aeroplanNumber', aeroplanNumber);
            	this.model.set('aeroplanLastName', aeroplanLastName);
            	$('#aeroplanInfo').removeClass('hidden');
            	$('#aeroplanMember').prop('checked', true);
            }
            this.model.set('aeroplanMiles', aeroplanMiles);
            this.model.set('usingSavedCard', false); 
            this.model.set('paymentType', 'CreditCard');
            this.model.set('card.isCardInfoSaved', true);
        	$('#saveCreditCcard').attr('checked',true); 
            this.model.set('customerAddresses', window.order.get("customer").toJSON()); //set customerAddresses to show all the billing addresses in credit card form
            $('#' + currentCardElementId).removeClass('in');
            $('#' + currentCardElementId).parent().find('.payment-method-input').attr('checked', false);
            this.model.resetAddressDefaults(); 
            
            var activePayment = this.model.activePayments();
            if(activePayment.length > 0) {
            	this.model.getOrder().get('billingInfo').set('billingContact', activePayment[0].billingInfo.billingContact); 
            	this.model.get('billingContact').set('contactId', activePayment[0].billingInfo.billingContact.id);
            	this.model.set('billingContact', activePayment[0].billingInfo.billingContact);
            }
            if(HyprLiveContext.locals.siteContext.checkoutSettings.purchaseOrder.isEnabled) {
                this.model.resetPOInfo();
            }
        },
        validateExpireYear: function(e) {
        	var currentDate = new Date();
        	if($(e.currentTarget).val() < currentDate.getFullYear()){
	        	$('.temp-expired-symbol').removeClass('hidden');
	        	$(e.currentTarget).parent().parent('.expiration-details').addClass('temp-highlight-expired-card');
    		}else {
    			$('.temp-expired-symbol').addClass('hidden');
	        	$(e.currentTarget).parent().parent('.expiration-details').removeClass('temp-highlight-expired-card');
    		}
        	this.model.set('card.expireYear', $(e.currentTarget).val());
        },
        validateExpireMonth: function(e) {
        	var currentDate = new Date();
        	if(this.model.get('card.expireYear') >= currentDate.getFullYear()){
        		if(this.model.get('card.expireYear') === currentDate.getFullYear() && $(e.currentTarget).val() < (currentDate.getMonth() + 1)){
            		$('.temp-expired-symbol').removeClass('hidden');
    	        	$(e.currentTarget).parent().parent('.expiration-details').addClass('temp-highlight-expired-card');
            	}else {
        			$('.temp-expired-symbol').addClass('hidden');
    	        	$(e.currentTarget).parent().parent('.expiration-details').removeClass('temp-highlight-expired-card');
        		}
        	}
        	this.model.set('card.expireMonth', $(e.currentTarget).val());
        },
        setCurrentCreditCardNumber: function(e){
        	currentSelectedCardId = $(e.currentTarget).attr('value');
        	this.model.set('savedPaymentMethodId',currentSelectedCardId);
        	currentCardElementId = $(e.currentTarget).attr('data-mz-current-credit-card');
        },
        setBillingAddress: function(e){
        	var contactId = $(e.currentTarget).attr('value');
        	var customerAddresses = this.model.get('customerAddresses');
        	currentContact = _.find(customerAddresses.contacts, function(contact){
        		return contact.id == contactId;
        	});
        	if($(e.currentTarget).closest('.modal-dialog').find('.modal-body').length === 0) {
            	this.model.getOrder().get('billingInfo').set('billingContact', currentContact); 
            	this.model.get('billingContact').set('contactId', currentContact.id);
            	this.model.set('billingContact', currentContact);
        	}
        	this.model.set('isAddressFormOpen', false);
        	if($(e.currentTarget).parents('.mz-contactselector ').siblings('.new-billing-address-form').length === 0){ //for edit card modal
        		$(e.currentTarget).parents('.mz-contactselector ').siblings('.billing-new-address-container').children('.new-billing-address-form').children('[id^=newBillingAddressInModal]').addClass('hidden'); 
        	}else {
        		$(e.currentTarget).parents('.mz-contactselector ').siblings('.new-billing-address-form').children('#newBillingAddress').addClass('hidden');
        	}
        },
        resetAddress: function(){
        	var address = this.model.get('billingContact').get('address');
        	address.set('address1', '');
        	address.set('address2', '');
        	address.set('cityOrTown', '');
        	address.set('stateOrProvince', '');
        	address.set('postalOrZipCode', '');
        	this.model.get('billingContact').set('phoneNumbers.home', '');
        	this.model.get('billingContact').set('firstName', '');
        	this.model.get('billingContact').set('lastNameOrSurname', '');
        },
        newBillingAddress: function(e) {
        	this.model.get('billingContact').set('contactId', 'new'); 
        	this.resetAddress();
        	this.model.set('isAddressFormOpen', true);  //set this value so that on country change form will be shown open after render
        	this.render();
        	$('#addNewAddress').attr('checked', true);
        	$('#newBillingAddress').removeClass('hidden');
        },
        newBillingAddressInModal: function(e) {
        	this.model.get('billingContact').set('contactId', 'new'); 
        	$(e.currentTarget).parents('.new-billing-address-form').find('input').val('');
        	$(e.currentTarget).parents('.new-billing-address-form').find('#country').val('CA');
        	$(e.currentTarget).parents('.new-billing-address-form').find('div[class*="province-"]').addClass('hidden');
        	$(e.currentTarget).parents('.new-billing-address-form').find('.province-ca').removeClass('hidden').find('select').val('');
        	this.resetAddress();
        	this.model.set('billingContact.address.countryCode', 'CA');
        	$(e.currentTarget).parent().siblings('[id^=newBillingAddressInModal]').toggleClass('hidden');
        },
        beginEditCard: function(e) {
            this.model.set('customerAddresses', window.order.get("customer").toJSON());
            this.render();  
        },
        cancelEditCard: function(e) {
        	$('.step-payment-continue-btn-div').addClass('is-loading');
        	window.location.reload();
        },
        updateCreditCard: function(e) {
        	var currentCredit = $(e.currentTarget).parent().attr('value');
        	this.model.getOrder().get('billingInfo').get('card').set('id', currentCredit);
        	this.model.set('customerAddresses', window.order.get("customer").toJSON());
            this.render(); 
        },
        makeDefaultCard: function(e) {
        	if($(e.currentTarget).is(':checked')){
        		this.model.set('card.isCardInfoSaved', true); //if make default card is checked then save the same card information
        		$('#saveCreditCcard').prop('checked',true);
        		$('#saveCreditCcard').prop('disabled',true);
        		$('#saveCreditCcard').siblings('.checkbox-label-inside').addClass('disabled-input');
        	}
        	else if(!$(e.currentTarget).is(':checked')) {
        		$('#saveCreditCcard').prop('disabled',false);
        		$('#saveCreditCcard').siblings('.checkbox-label-inside').removeClass('disabled-input');
        	}
        },
        updatePurchaseOrderPaymentTerm: function(e) {
            this.model.setPurchaseOrderPaymentTerm(e.target.value);
        },
        showBillingAddresses: function(){ 
        	$('.billing-address-summary').addClass('hidden');
        	$('.billing-address-section').toggleClass('hidden');
        },
        render: function() {
            preserveElements(this, ['.v-button', '.p-button'], function() {
                CheckoutStepView.prototype.render.apply(this, arguments);
            });
            var billingContactEmail = _.find(this.model.parent.get('attributes'), function (attribute) {
            	return attribute.fullyQualifiedName == 'tenant~' + Hypr.getThemeSetting('firstPersonEmailCode');
            });
            if(billingContactEmail) {
            	this.model.get('billingContact').set('email',billingContactEmail.values[0]); 
            }
            var status = this.model.stepStatus();
            if (visaCheckoutSettings.isEnabled && !this.visaCheckoutInitialized && this.$('.v-button').length > 0) {
                window.onVisaCheckoutReady = _.bind(this.initVisaCheckout, this);
                require([pageContext.visaCheckoutJavaScriptSdkUrl]);
                this.visaCheckoutInitialized = true;
            }

            if (this.$(".p-button").length > 0)
                PayPal.loadScript();
            
            if(this.model.get('usingSavedCard')){
            	$('#' + currentCardElementId).addClass('in');
                $('#' + currentCardElementId).parent().find('.payment-method-input').attr('checked', true);
            }
        	if(this.model.parent.get('attributes') && this.model.parent.get('attributes').length > 0) {
        		this.$el.find('.mz-formstep-body').removeClass('hidden'); 
        	}else {
        		this.$el.find('.mz-formstep-body').addClass('hidden'); 
        	}
        	
        	if(this.model.get('isAddressFormOpen')){
        		$('#addNewAddress').attr('checked', true);
            	$('#newBillingAddress').removeClass('hidden');
        	}
        	if(this.model.get('aeroplanNumber') === 'N/A'){
        		$('#checkoutAeroplanNumberInput').val('');
        	}
        		
        },
        updateAcceptsMarketing: function(e) {
            this.model.getOrder().set('acceptsMarketing', $(e.currentTarget).prop('checked'));
        },
        updatePaymentType: function(e) {
            var newType = $(e.currentTarget).val();
            this.model.set('usingSavedCard', e.currentTarget.hasAttribute('data-mz-saved-credit-card'));
            this.model.set('paymentType', newType);
        },
        beginEditingCard: function() {
            var me = this;
            if (!this.model.isExternalCheckoutFlowComplete()) {
                this.editing.savedCard = true;
                this.render();
            } else {
                this.cancelExternalCheckout();
            }
        },
        finishEditingCard: function(e) {
        	var contactId = this.model.get('billingContact.contactId');
        	if(currentContact && contactId != 'new') {
        		this.model.getOrder().get('billingInfo').set('billingContact', currentContact); 
            	this.model.get('billingContact').set('contactId', currentContact.id);
            	this.model.set('billingContact', currentContact);
        	}
        	if(!this.model.validate()){
        		$(e.currentTarget).parent().addClass('is-loading');
        		this.model.parent.saveCustomerCard().then(function(card) {  //model.parent is an order object which have saveCustomerCard method
            		var saved_indicator = $(e.currentTarget).parents('.saved-payments').attr('value');
            		$.cookie('currentCardIndicator', saved_indicator, {path: '/' });
                	window.location.href = "/cart/checkout";
            	});
        	}else {
        		var validationMsgs = $(e.currentTarget).closest('.modal-dialog').find('.modal-body').find('.mz-validationmessage');
        		var validationAttr = validationMsgs.map(function(){
        			return $(this).attr("data-mz-validationmessage-for");
        		});
        		var invalidFieldsKeys = _.keys(this.model.validate());
        		var index = _.indexOf(validationAttr, invalidFieldsKeys[invalidFieldsKeys.length - 1]);
        		$(e.currentTarget).closest('.modal-dialog').find('.modal-body').ScrollTo($(validationMsgs[index]).siblings('input'));
        		$('.saved-credit-card-info .security-label-section .mz-validationmessage').addClass('hidden');
        		$('.saved-credit-card-info .security-label-section .cvv-input-box').removeClass('is-invalid');
        	}
        },
        beginEditingExternalPayment: function () {
            var me = this;
            if (this.model.isExternalCheckoutFlowComplete()) {
                this.doModelAction('cancelExternalCheckout').then(function () {
                    me.editing.savedCard = true;
                    me.render();
                });
            }
        },
        beginEditingBillingAddress: function() {
            this.editing.savedBillingAddress = true;
            this.render();
        },
        beginApplyCredit: function () {
            this.model.beginApplyCredit();
            this.render();
        },
        cancelApplyCredit: function () {
            this.model.closeApplyCredit();
            this.render();
        },
        cancelExternalCheckout: function () {
            var me = this;
            this.doModelAction('cancelExternalCheckout').then(function () {
                me.editing.savedCard = false;
                me.render();
            });
        },
        finishApplyCredit: function () {
            var self = this;
            this.model.finishApplyCredit().then(function() {
                self.render();
            });
        },
        removeCredit: function (e) {
            var self = this,
                id = $(e.currentTarget).data('mzCreditId');
            this.model.removeCredit(id).then(function () {
                self.render();
            });
        },
        getDigitalCredit: function (e) {
            var self = this;
            this.$el.addClass('is-loading');
            this.model.getDigitalCredit().ensure(function () {
                self.$el.removeClass('is-loading');
            });
        },
        stripNonNumericAndParseFloat: function (val) {
            if (!val) return 0;
            var result = parseFloat(val.replace(/[^\d\.]/g, ''));
            return isNaN(result) ? 0 : result;
        },
        applyDigitalCredit: function(e) {
            var val = $(e.currentTarget).prop('value'),
                creditCode = $(e.currentTarget).attr('data-mz-credit-code-target');  //target
            if (!creditCode) {
                //console.log('checkout.applyDigitalCredit could not find target.');
                return;
            }
            var amtToApply = this.stripNonNumericAndParseFloat(val);
            
            this.model.applyDigitalCredit(creditCode, amtToApply, true);
            this.render();
        },
        onEnterDigitalCreditCode: function(model, code) {
            if (code && !this.codeEntered) {
                this.codeEntered = true;
                this.$el.find('input#digital-credit-code').siblings('button').prop('disabled', false);
            }
            if (!code && this.codeEntered) {
                this.codeEntered = false;
                this.$el.find('input#digital-credit-code').siblings('button').prop('disabled', true);
            }
        },
        enableDigitalCredit: function(e) {
            var creditCode = $(e.currentTarget).attr('data-mz-credit-code-source'),
                isEnabled = $(e.currentTarget).prop('checked') === true,
                targetCreditAmtEl = this.$el.find("input[data-mz-credit-code-target='" + creditCode + "']"),
                me = this;

            if (isEnabled) {
                targetCreditAmtEl.prop('disabled', false);
                me.model.applyDigitalCredit(creditCode, null, true);
            } else {
                targetCreditAmtEl.prop('disabled', true);
                me.model.applyDigitalCredit(creditCode, 0, false);
                me.render();
            }
        },
        addRemainderToCustomer: function (e) {
            var creditCode = $(e.currentTarget).attr('data-mz-credit-code-to-tie-to-customer'),
                isEnabled = $(e.currentTarget).prop('checked') === true;
            this.model.addRemainingCreditToCustomerAccount(creditCode, isEnabled);
        },
        handleEnterKey: function (e) {
            var source = $(e.currentTarget).attr('data-mz-value');
            if (!source) return;
            switch (source) {
                case "creditAmountApplied":
                    return this.applyDigitalCredit(e);
                case "digitalCreditCode":
                    return this.getDigitalCredit(e);
            }
        },
        /* begin visa checkout */
        initVisaCheckout: function () {
            var me = this;
            var visaCheckoutSettings = HyprLiveContext.locals.siteContext.checkoutSettings.visaCheckout;
            var apiKey = visaCheckoutSettings.apiKey || '0H1JJQFW9MUVTXPU5EFD13fucnCWg42uLzRQMIPHHNEuQLyYk';
            var clientId = visaCheckoutSettings.clientId || 'mozu_test1';
            var orderModel = this.model.getOrder();


            if (!window.V) {
                //console.warn( 'visa checkout has not been initilized properly');
                return false;
            }
            // on success, attach the encoded payment data to the window
            // then call the sdk's api method for digital wallets, via models-checkout's helper
            window.V.on("payment.success", function(payment) {
                //console.log({ success: payment });
                me.editing.savedCard = false;
                me.model.parent.processDigitalWallet('VisaCheckout', payment);
            });

          

            window.V.init({
                apikey: apiKey,
                clientId: clientId,
                paymentRequest: {
                    currencyCode: orderModel.get('currencyCode'),
                    subtotal: "" + orderModel.get('subtotal')
                }
            });
        }
        /* end visa checkout */
    });

    var CouponView = Backbone.MozuView.extend({
        templateName: 'modules/checkout/coupon-code-field',
        handleLoadingChange: function (isLoading) {
            // override adding the isLoading class so the apply button 
            // doesn't go loading whenever other parts of the order change
        },
        initialize: function () {
            var me = this;
            this.listenTo(this.model, 'change:couponCode', this.onEnterCouponCode, this);
            this.codeEntered = !!this.model.get('couponCode');
            this.$el.on('keypress', 'input', function (e) {
                if (e.which === 13) {
                    if (me.codeEntered) {
                        me.handleEnterKey();
                    }
                    return false;
                }
            });
        },
        onEnterCouponCode: function (model, code) {
            if (code && !this.codeEntered) {
                this.codeEntered = true;
                this.$el.find('button').prop('disabled', false);
            }
            if (!code && this.codeEntered) {
                this.codeEntered = false;
                this.$el.find('button').prop('disabled', true);
            }
        },
        autoUpdate: [
            'couponCode'
        ],
        addCoupon: function (e) {
            // add the default behavior for loadingchanges
            // but scoped to this button alone
            var self = this;
            this.$el.addClass('is-loading');
            this.model.addCoupon().ensure(function() {
                self.$el.removeClass('is-loading');
                self.model.unset('couponCode');
                self.render();
            });
        },
        handleEnterKey: function () {
            this.addCoupon();
        }
    });

    var CommentsView = Backbone.MozuView.extend({
        templateName: 'modules/checkout/comments-field',
        autoUpdate: ['shopperNotes.comments']
    });

    var attributeFields = function(){
        var me = this;

        var fields = [];

        var storefrontOrderAttributes = require.mozuData('pagecontext').storefrontOrderAttributes;
        if(storefrontOrderAttributes && storefrontOrderAttributes.length > 0) {

            storefrontOrderAttributes.forEach(function(attributeDef){
                fields.push('orderAttribute-' + attributeDef.attributeFQN);
            }, this);

        }

        return fields;
    };

    var ReviewOrderView = Backbone.MozuView.extend({
        templateName: 'modules/checkout/step-review',
        autoUpdate: [
            'createAccount',
            'agreeToTerms',
            'emailAddress',
            'password',
            'confirmPassword',
            'shopperNotes.comments'
//        ].concat(attributeFields()),
          ],
        additionalEvents: {
          "click #checkoutCommentsLink": "focusTextArea"
        },
        renderOnChange: [
            'createAccount',
            'isReady'
        ],
        initialize: function () {
            var me = this;
            this.$el.on('keypress', 'input', function (e) {
                if (e.which === 13) {
                    me.handleEnterKey();
                    return false;
                }
            });
            this.model.on('passwordinvalid', function(message) {
                me.$('[data-mz-validationmessage-for="password"]').text(message);
            });
            this.model.on('userexists', function (user) {
                me.$('[data-mz-validationmessage-for="emailAddress"]').html(Hypr.getLabel("customerAlreadyExists", user, encodeURIComponent(window.location.pathname)));
            });
        },
        focusTextArea: function() {
        	if($('#checkoutOrderComments').hasClass('hidden')) {
        		$('#checkoutOrderComments').removeClass('hidden');
        		$('#checkoutCommentsLink').removeClass('collapsed');
        		$('#checkoutOrderComments').focus(); 
        	}else {
        		$('#checkoutOrderComments').addClass('hidden');
        		$('#checkoutCommentsLink').addClass('collapsed');
        	}
        },
        submit: function () {
            var self = this;
            this.model.set('agreeToTerms', true);
            $.removeCookie('inStorePickUpStep', { path: '/' });  //remove cookie value of inStorePickUpStep once order is placed
            this.model.get('shopperNotes').set('comments', $('#checkoutOrderComments').val());
            _.defer(function () {
                self.model.submit();
            });
        },
        handleEnterKey: function () {
            this.submit();
        }
    });

    var ParentView = function(conf) {
      var gutter = parseInt(Hypr.getThemeSetting('gutterWidth'), 10);
      if (isNaN(gutter)) gutter = 15;
      var mask;
      conf.model.on('beforerefresh', function() {
         killMask();
         conf.el.css('opacity',0.5);
         var pos = conf.el.position();
         mask = $('<div></div>', {
           'class': 'mz-checkout-mask'
         }).css({
           width: conf.el.outerWidth() + (gutter * 2),
           height: conf.el.outerHeight() + (gutter * 2),
           top: pos.top - gutter,
           left: pos.left - gutter
         }).insertAfter(conf.el);
      });
      function killMask() {
        conf.el.css('opacity',1);
        if (mask) mask.remove();
      }
      conf.model.on('refresh', killMask); 
      conf.model.on('error', killMask);
      return conf;
    };



    $(document).ready(function () {
        var $checkoutView = $('#checkout-form'),
            checkoutData = require.mozuData('checkout');
        var checkoutModel = window.order = new CheckoutModels.CheckoutPage(checkoutData),
            checkoutViews = {
                parentView: new ParentView({
                  el: $checkoutView,
                  model: checkoutModel
                }),
                steps: {
                    shippingAddress: new ShippingAddressView({
                        el: $('#step-shipping-address'),
                        model: checkoutModel.get('fulfillmentInfo').get('fulfillmentContact')
                    }),
                    shippingInfo: new ShippingInfoView({
                        el: $('#step-shipping-method'),
                        model: checkoutModel.get('fulfillmentInfo')
                    }),
                    paymentInfo: new BillingInfoView({
                        el: $('#step-payment-info'),
                        model: checkoutModel.get('billingInfo')
                    })
                },
                inStorePickUpInfoSummary: new InStorePickUpInfoSummaryView({ //separate view for instore pickup (first step of checkout with instore pickup)
                    el: $('#instore-pickup-info'),
                    model: checkoutModel
                }),
                cartSummary: new CartSummaryView({
                	el: $('#cart-summary'),
                	model: checkoutModel
                }),
                orderSummary: new OrderSummaryView({
                    el: $('#order-summary'),
                    model: checkoutModel
                }),
                couponCode: new CouponView({
                    el: $('#coupon-code-field'),
                    model: checkoutModel
                }),
                comments: Hypr.getThemeSetting('showCheckoutCommentsField') && new CommentsView({
                    el: $('#comments-field'),
                    model: checkoutModel
                }),
                
                reviewPanel: new ReviewOrderView({
                    el: $('#step-review'),
                    model: checkoutModel
                }),
                messageView: messageViewFactory({
                    el: $checkoutView.find('[data-mz-message-bar]'),
                    model: checkoutModel.messages
                })
            };

        window.checkoutViews = checkoutViews;

        checkoutModel.on('complete', function() {
            CartMonitor.setCount(0);
            window.location = (HyprLiveContext.locals.siteContext.siteSubdirectory||'') + "/checkout/" + checkoutModel.get('id') + "/confirmation";
        });

        var $reviewPanel = $('#step-review');
        checkoutModel.on('change:isReady',function (model, isReady) {
            if (isReady) {
                setTimeout(function () { window.scrollTo(0, $reviewPanel.offset().top); }, 750);
            }
        });
        checkoutViews.inStorePickUpInfoSummary.render();
        
        //render these views initially to get the values on the page load which have set in initialize method of these views  
        checkoutViews.cartSummary.render();
        checkoutViews.orderSummary.render();
        
        if(window.order) {
        	if(window.order.get('items')[0].fulfillmentLocationCode !== $.parseJSON($.cookie("preferredStore")).code){
            	$("#checkoutStoreChangeModal").modal({
            		show: 'true',
            		backdrop: 'static',
            	    keyboard: false
            	});
            } 
        }
        
        if(checkoutModel.get('attributes') && checkoutModel.get('attributes').length > 0){
        	 _.invoke(checkoutViews.steps, 'initStepView');
        }

        $checkoutView.noFlickerFadeIn();
        
        /**
         * Called when the user begins the checkout process.
         * @param {Array} cart An array representing the user's shopping cart.
         */
        var cartItems = window.order.get('items');
        for (var i=0; i < cartItems.length; i++){
    	  	// addItem should be called for every item in the shopping cart.
        	var item = cartItems[i];
        	var currAttribute = _.findWhere(item.product.properties, {'attributeFQN':Hypr.getThemeSetting('brandDesc')});
    		ga('ec:addProduct', {
    		  'id': item.product.productCode,                     																	
    	      'name': item.product.name,              																	  
    		  'category': item.product.categories[0].id,  
    		  'brand': currAttribute ? currAttribute.values[0].stringValue : '',    											    
    		  'price': parseFloat(item.unitPrice.extendedAmount).toFixed(2),              												 
    		  'quantity': parseInt(item.quantity, 10)                   															   
    		});
        }
    	// In the case of checkout actions, an additional actionFieldObject can
    	// specify a checkout step and option.
    	ga('ec:setAction','checkout', {
    	    'step': 1,           // A value of 1 indicates this action is first checkout step.
    	    'option' : 'Instore Pickup Information'
    	});
    	
    	ga('send', 'pageview'); 
        
    });
});
