define(['modules/jquery-mozu',
	"modules/backbone-mozu",
	'underscore',
	"hyprlivecontext",
	"modules/api",
	'hyprlive'], function($, Backbone, _, HyprLiveContext, api, Hypr) {
	var locale = require.mozuData('apicontext').headers['x-vol-locale'];
	var articleProductTypeId = Hypr.getThemeSetting('articleProductTypeId');
	var articleQueryString = 'productTypeId eq ' + articleProductTypeId;
	var ArticleCategoryView = Backbone.MozuView.extend({
        templateName: 'pages/article-category',
        previousArticle: function(e) {
        	e.preventDefault();
        	var me = this;
        	var startIndex = me.model.get('startIndex') - me.model.get('pageSize');
        	var currentPage = me.model.get('currentPage') - 1;
        	var hasPreviousPage;
        	if(currentPage == 1) {
        		hasPreviousPage = false;
        	} else {
        		hasPreviousPage = true;
        	}
        	var hasNextPage = true;  
        	var articleQueryStringTemp = articleQueryString + ' and CategoryId eq ' + window.categoryId;
        	me.articlesApiCall(articleQueryStringTemp, startIndex, hasPreviousPage, hasNextPage, currentPage);
     	
        },
        pageArticle: function(e) {
        	e.preventDefault();
        	var me = this;
        	var selectedPageNumber = $(e.currentTarget).data('mz-page-num');
        	var startIndex = (selectedPageNumber * me.model.get('pageSize')) - me.model.get('pageSize');
        	var currentPage = selectedPageNumber;
        	var hasPreviousPage;
        	if(currentPage == 1) {
        		hasPreviousPage = false;
        	} else {
        		hasPreviousPage = true;
        	}
        	
        	var hasNextPage;
        	if (me.model.get('totalCount')/me.model.get('pageSize') > currentPage) {
        		hasNextPage = true;
        	} else {
        		hasNextPage = false;
        	}
        	var articleQueryStringTemp = articleQueryString + ' and CategoryId eq ' + window.categoryId;
        	me.articlesApiCall(articleQueryStringTemp, startIndex, hasPreviousPage, hasNextPage, currentPage);
        },
        nextArticle: function(e) {
        	e.preventDefault();
        	var me = this;
        	var startIndex = me.model.get('startIndex') + me.model.get('pageSize');
        	var hasPreviousPage = true;
        	var currentPage = me.model.get('currentPage') + 1;
        	var hasNextPage;
        	if (me.model.get('totalCount')/me.model.get('pageSize') > currentPage) {
        		hasNextPage = true;
        	} else {
        		hasNextPage = false;
        	}
        	var articleQueryStringTemp = articleQueryString + ' and CategoryId eq ' + window.categoryId;
        	me.articlesApiCall(articleQueryStringTemp, startIndex, hasPreviousPage, hasNextPage, currentPage);
        },
        articlesApiCall: function(searchFilter, startIndex, hasPreviousPage, hasNextPage, currentPage) {
        	var me = this;
        	$('html, body').scrollTop(145);
        	api.get('search', {query:'', filter: searchFilter, facet: 'categoryId', facetTemplate: 'categoryId:' + window.categoryId, pageSize: 16, startIndex: startIndex, sortBy: 'createDate desc'} ).then(function(response) {
        		me.model = new Backbone.Model(response.data);
        		me.model.set('childrenCategories', window.childrenCategories);
        		
        		if (me.model.get('totalCount')/me.model.get('pageSize') > currentPage) {
            		hasNextPage = true;
            	} else {
            		hasNextPage = false;
            	}
        		me.model.set('hasNextPage', hasNextPage);
        		me.model.set('currentPage', currentPage);
        		me.model.set('hasPreviousPage', hasPreviousPage);
        		me.model.set('isArticle', true);
        		
				var middlePageNumbers = [];
    			
    			if(currentPage <= 3) {
    				middlePageNumbers = [];
    				for(var x=2; x<response.data.pageCount; x++) {
    	    			if(x <= 6) {
    	    				middlePageNumbers.push(x);
    	    			}
    	    		}
    			} else if(currentPage >= response.data.pageCount-2) {
    				middlePageNumbers = [];
    				for(var y=response.data.pageCount-5; y<=response.data.pageCount-1; y++) {
    	    			if(y >= 2) {
    	    				middlePageNumbers.push(y);
    	    				console.log(middlePageNumbers);
    	    			}
    	    		}
    			} else {
    				middlePageNumbers = [];
    				for(var z=2; z>=1; z--) {
        				if(currentPage != 1) {
        					middlePageNumbers.push(currentPage - z);
        				}
            		}
        			middlePageNumbers.push(currentPage);
        			for(var p=1; p<=2; p++) {
            			middlePageNumbers.push(currentPage + p);
            		}
    			}
    			me.model.set('middlePageNumbers', middlePageNumbers);
        		
    			me.render();
    			changePaginationLinks();
        	});
        }
        
	});
	
	var ArticleCategoryMenuView = Backbone.MozuView.extend({
        templateName: 'pages/article-category-menu',
        events: {
            "change #pagemenu": "gotoArticleCategory"
        },
        gotoArticleCategory: function(e){
        	console.log($("#pagemenu option:selected")[0].value);
        	window.location.href = $("#pagemenu option:selected")[0].value;
        	
        }
	});
	
	function changePaginationLinks(){
		var paginationLinks = $('[data-mz-pagenumbers]').find('a');
		paginationLinks.map(function(index){
			$(paginationLinks[index]).attr('href', '#');
		});
	}
	
	$(document).ready(function() {
		var categoryID = $('#article-category').data('categoryid');
		window.categoryId = categoryID;
		var searchFilter = articleQueryString + ' and CategoryId eq ' + categoryID;
    	api.get('search', {query:'', filter: searchFilter, facet: 'categoryId', facetTemplate: 'categoryId:' + categoryID, pageSize: 16, startIndex: 0, sortBy: 'createDate desc'} ).then(function(response) {
    		var articleCategoryModel = response.data;  
    		var childrenCategories = _.findWhere(articleCategoryModel.facets, {field: "CategoryId"});
    		if(childrenCategories){
    			childrenCategories = childrenCategories;
    		} else {
    			childrenCategories = _.findWhere(articleCategoryModel.facets, {field: "categoryId"});
    		}    		
    		
    		childrenCategories = _.findWhere(childrenCategories.values, {value: window.categoryId.toString()});
    		if(childrenCategories) {
    			articleCategoryModel.childrenCategories = childrenCategories.childrenFacetValues;
    			window.childrenCategories = childrenCategories.childrenFacetValues;
    		} else {
    			articleCategoryModel.childrenCategories = [];
    			window.childrenCategories = [];
    		}
    		
    		articleCategoryModel.isArticle = true;
    		articleCategoryModel.hasPreviousPage = false;
    		articleCategoryModel.currentPage = 1;
    		
    		var middlePageNumbers = [];
    		for(var x=2; x<articleCategoryModel.pageCount; x++) {
    			if(x <= 6) {
    				middlePageNumbers.push(x);
    			}
    		}
    		articleCategoryModel.middlePageNumbers = middlePageNumbers;
    		
    		if(articleCategoryModel.pageCount > 1) {
    			articleCategoryModel.hasNextPage = true;
    		} else {
    			articleCategoryModel.hasNextPage = false;
    		}
    		
    		articleCategoryModel.locale = locale;
    		
    		if(articleCategoryModel.items.length > 0) {
    			var articleCategoryView = new ArticleCategoryView({
			       el: $('#article-category'),
			       model: new Backbone.Model(articleCategoryModel)
				});
				articleCategoryView.render();
				
				var articleCategoryMenuView = new ArticleCategoryMenuView({
				       el: $('#article-category-menu'),
				       model: new Backbone.Model(articleCategoryModel)
					});
				articleCategoryMenuView.render();
					
				var breadcrumbLinks = $('.mz-breadcrumb-link');
				breadcrumbLinks.map(function(index){
					$(breadcrumbLinks[index]).attr('href', $(breadcrumbLinks[index]).attr('href') + '?categoryType=article');
				});
				
				var hhhPagelink = '<a href="/here-s-how-hub" class="mz-breadcrumb-link is-first hidden-xs">' +  Hypr.getLabel('hereHowHub') + '</a><span class="mz-breadcrumb-separator hidden-xs">|</span>';
				$('.mz-breadcrumbs .col-sm-12').prepend(hhhPagelink);
				changePaginationLinks();
    		}
    	});  
	});
});
