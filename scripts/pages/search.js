define(['modules/jquery-mozu',
	"modules/backbone-mozu",
	'underscore',
	"modules/views-collections",
	"hyprlivecontext",
	"modules/api",
	'hyprlive',
	'modules/product-quick-view'], function($, Backbone, _, CollectionViewFactory, HyprLiveContext, api, Hypr, quickViewBind) {
	var locale = require.mozuData('apicontext').headers['x-vol-locale'];
	var searchQuery = HyprLiveContext.locals.pageContext.search.query;
	var articleProductTypeId = Hypr.getThemeSetting('articleProductTypeId');
	var productTypeId = Hypr.getThemeSetting('productTypeId');
    var apiContext = require.mozuData('apicontext');
    var currentSite = apiContext.headers['x-vol-site'];
    var homeFurnitureSiteId = Hypr.getThemeSetting('homeFurnitureSiteId');
	var searchProductQueryString = 'productTypeId eq ' + productTypeId;
	var searchArticleQueryString = 'productTypeId eq ' + articleProductTypeId;
	var makeurl = "search?query=";
	var globalSearchProductView, globalExpertAdviceView;
	if(locale == "en-US"){
		makeurl = '/en/search?query=';
	} else{
		makeurl = '/fr/search?query=';
	}
	
	if(searchQuery){
		makeurl = makeurl + searchQuery;
	}
	var isPurchesLocation = false;
	if(apiContext.headers['x-vol-purchase-location']){
		isPurchesLocation  = true;
	} else {
		isPurchesLocation = false;
	}
	window.makeProductUrl = window.makeArticleUrl = '';
	var defaultSort = Hypr.getThemeSetting('defaultSort'),
	sortByOptions = [
		{
			value: defaultSort,
			text: Hypr.getLabel('default')
		},{
			value: "price asc",
			text: Hypr.getLabel('sortByPriceAsc')
		},{
			value: "price desc",
			text: Hypr.getLabel('sortByPriceDesc')
		},{
			value: "productName asc",
			text: Hypr.getLabel('sortByNameAsc')
		},{
			value: "productName desc",
			text: Hypr.getLabel('sortByNameDesc')
		},{
			value: "createDate desc",
			text: Hypr.getLabel('sortByDateAsc')
		},{
			value: "createDate asc",
			text: Hypr.getLabel('sortByDateDesc')
		}
	];
	var ExpertAdviceView = Backbone.MozuView.extend({
        templateName: 'modules/common/expert-advice-view',
        additionalEvents: {
	        'change select[data-mz-value="pageSize"]': 'pageSize'
	    },
		pageSize: function(e){
			var me = this;
			var selectedPageSize = $(e.currentTarget).val();
			window.articlePageSize = selectedPageSize;
			var searchArticleQueryStringTemp = me.makeArticleQueryString();
			me.articlesApiCall(searchArticleQueryStringTemp, 0, window.sortbyValueArticles, window.sortValueArticles, false, true, 1);
		},
        initialize: function() {
        	this.model.set('currentLocale', locale);
			this.model.set('currentSite', currentSite);
        },
        sortArticle: function(e) {        	
			e.preventDefault();
			var me = this;
			window.sortbyValueArticles = $(e.currentTarget).data('value');
			window.sortValueArticles = $(e.currentTarget).text();
        	me.getproductSortByValue();
    		
        },
        applyFacets: function(e){
        	var me = this;
			var selected = $(e.currentTarget).closest('.filters').find('input[name="sortBy"]:checked');
			window.sortbyValueArticles = $(selected).data('value');
			window.sortValueArticles = $(selected).data('sort-value');
			me.getproductSortByValue();
        },
        getproductSortByValue: function() {
        	var me = this;
        	var searchArticleQueryStringTemp = me.makeArticleQueryString();
    		me.articlesApiCall(searchArticleQueryStringTemp, 0, window.sortbyValueArticles, window.sortValueArticles, false, true, 1);
        },
        sortAndFilter: function(e){
        	e.preventDefault();
        	$(e.currentTarget).closest('.tab-pane').find('.filters').addClass('open');
        	moreLessConfig();
        },
        clearFacets:function(){
        	var me = this;
        	$(document).find('.filters').removeClass('open');
        	var searchArticleQueryStringTemp = me.makeArticleQueryString();
        	me.articlesApiCall(searchArticleQueryStringTemp, 0, '', Hypr.getLabel('default'), false, true, 1);
        },
        drillDownSort: function(e) {
        	e.preventDefault();
        	var me = this;
        	window.categoryArticlesFilterId = $(e.currentTarget).data('mz-hierarchy-id');
        	var searchArticleQueryStringTemp = me.makeArticleQueryString();
        	me.articlesApiCall(searchArticleQueryStringTemp, 0, window.sortbyValueArticles, window.sortValueArticles, false, true, 1);
        },
        allCategories: function() {
        	var me = this;
        	window.categoryArticlesFilterId = '';
        	me.articlesApiCall(searchArticleQueryString, 0, window.sortbyValueArticles, window.sortValueArticles, false, true, 1);
        },
        next: function(e) {
        	e.preventDefault();
        	var me = this;
        	var startIndex = me.model.get('startIndex') + me.model.get('pageSize');
        	var hasPreviousPage = true;
        	var currentPage = me.model.get('currentPage') + 1;
        	var hasNextPage;
        	if (me.model.get('totalCount')/me.model.get('pageSize') > currentPage) {
        		hasNextPage = true;
        	} else {
        		hasNextPage = false;
        	}
        	
        	var searchArticleQueryStringTemp = me.makeArticleQueryString();
        	me.articlesApiCall(searchArticleQueryStringTemp, startIndex, window.sortbyValueArticles, window.sortValueArticles, hasPreviousPage, hasNextPage, currentPage);

        },
        previous: function(e){
        	e.preventDefault();
        	var me = this;
        	var startIndex = me.model.get('startIndex') - me.model.get('pageSize');
        	var currentPage = me.model.get('currentPage') - 1;
        	var hasPreviousPage;
        	if(currentPage == 1) {
        		hasPreviousPage = false;
        	} else {
        		hasPreviousPage = true;
        	}
        	var hasNextPage = true;  
        	var searchArticleQueryStringTemp = me.makeArticleQueryString();
        	me.articlesApiCall(searchArticleQueryStringTemp, startIndex, window.sortbyValueArticles, window.sortValueArticles, hasPreviousPage, hasNextPage, currentPage);
     	
        },
        page: function(e) {
        	e.preventDefault();
        	var me = this;
        	var selectedPageNumber = $(e.currentTarget).data('mz-page-num');
        	var startIndex = (selectedPageNumber * me.model.get('pageSize')) - me.model.get('pageSize');
        	var currentPage = selectedPageNumber;
        	var hasPreviousPage;
        	if(currentPage == 1) {
        		hasPreviousPage = false;
        	} else {
        		hasPreviousPage = true;
        	}
        	
        	var hasNextPage;
        	if (me.model.get('totalCount')/me.model.get('pageSize') > currentPage) {
        		hasNextPage = true;
        	} else {
        		hasNextPage = false;
        	}
        	var searchArticleQueryStringTemp = me.makeArticleQueryString();
        	me.articlesApiCall(searchArticleQueryStringTemp, startIndex, window.sortbyValueArticles, window.sortValueArticles, hasPreviousPage, hasNextPage, currentPage);

        },
        makeArticleQueryString: function(){
        	if(window.categoryArticlesFilterId) {
        		return searchArticleQueryString + ' and CategoryId req ' + window.categoryArticlesFilterId + '';
        	} else {
        		return searchArticleQueryString;
        	}
        },
        articlesApiCall: function(searchArticlesQueryString, startIndex, sortbyValueArticles, sortValueArticles, hasPreviousPage, hasNextPage, currentPage){
        	var me = this;
        	$('html, body').scrollTop(145);
        	$('.content-loading').show();
        	api.get('search', {query:searchQuery, filter:searchArticlesQueryString, facet: 'categoryId', pageSize: window.articlePageSize, startIndex: startIndex, sortBy:sortbyValueArticles} ).then(function(response) {
    			me.model = new Backbone.Model(response.data);
    			
    			if (me.model.get('totalCount')/me.model.get('pageSize') > currentPage) {
            		hasNextPage = true;
            	} else {
            		hasNextPage = false;
            	}
    			
    			me.model.set('sortByValue', sortValueArticles);
    			me.model.set('narrowSearchList', window.globalSearchCategorylist);
    			me.model.set('hasPreviousPage', hasPreviousPage);
    			me.model.set('hasNextPage', hasNextPage);
    			me.model.set('currentPage', currentPage);
    			me.model.set('firstIndex', startIndex + 1);    			
    			me.model.set('lastIndex', startIndex + me.model.get('items').length);
    			
    			var middlePageNumbers = [];
    			
    			if(currentPage <= 3) {
    				middlePageNumbers = [];
    				for(var x=2; x<response.data.pageCount; x++) {
    	    			if(x <= 6) {
    	    				middlePageNumbers.push(x);
    	    			}
    	    		}
    			} else if(currentPage >= response.data.pageCount-2) {
    				middlePageNumbers = [];
    				for(var y=response.data.pageCount-5; y<=response.data.pageCount-1; y++) {
    	    			if(y >= 2) {
    	    				middlePageNumbers.push(y);
    	    			}
    	    		}
    			} else {
    				middlePageNumbers = [];
    				for(var z=2; z>=1; z--) {
        				if(currentPage != 1) {
        					middlePageNumbers.push(currentPage - z);
        				}
            		}
        			middlePageNumbers.push(currentPage);
        			for(var p=1; p<=2; p++) {
            			middlePageNumbers.push(currentPage + p);
            		}
    			}
    			me.model.set('middlePageNumbers', middlePageNumbers);
    			
    			//Making URL as per filters
    			var makeArticleUrl = '';
    			if(me.model.get('startIndex')){
    				makeArticleUrl = makeArticleUrl + '&ExpStartIndex=' + me.model.get('startIndex');
    			}
    			if(window.categoryArticlesFilterId){
    				makeArticleUrl = makeArticleUrl + '&ExpCategoryId=' + window.categoryArticlesFilterId;
    			}
    			if(sortbyValueArticles) {
    				makeArticleUrl = makeArticleUrl + '&ExpSortBy=' + sortbyValueArticles;
    			}    
    			window.makeArticleUrl = makeArticleUrl;
    		    window.history.pushState({},"", makeurl + window.makeProductUrl + window.makeArticleUrl);
    		    
            	me.render();
            	$('.content-loading').hide();
            	$(document).find('a[href="#expert-advice-tab""] > span').text(me.model.get('totalCount'));
            	
    		});
        },
        render: function(){
        	var me = this;
        	me.model.set('currentLocale', locale);
        	me.model.set('currentSite', currentSite);
            Backbone.MozuView.prototype.render.apply(this, arguments);
            moreLessConfig();
        }
	});
	
	var SearchProductView = Backbone.MozuView.extend({
		templateName: "modules/common/search-products-view",
		additionalEvents: {
	        'change select[data-mz-value="pageSize"]': 'pageSize'
	    },
		pageSize: function(e){
			var me = this;
			var selectedPageSize = $(e.currentTarget).val();
			window.productPageSize = selectedPageSize;
			var searchProductQueryStringTemp = me.makeProductQueryString();
    		me.productApiCall(searchProductQueryStringTemp, 0, window.sortbyValueProducts, window.sortValueProducts, false, true, 1, window.categoryID, window.facetValueFilter);
        },
		initialize: function() {
			this.model.set('currentLocale', locale);
			this.model.set('currentSite', currentSite);
		},
		sortProducts: function(e){
			e.preventDefault();
        	var me = this;
        	window.sortbyValueProducts =  $(e.currentTarget).data('value');
        	window.sortValueProducts =  $(e.currentTarget).text();
        	me.getproductSortByValue();
		},
		applyFacets: function(e){
        	var me = this;
			var selected = $(e.currentTarget).closest('.filters').find('input[name="sortBy"]:checked');
			window.sortbyValueProducts = $(selected).data('value');
			window.sortValueProducts =  $(selected).data('sort-value');
			me.getproductSortByValue();
        },
        getproductSortByValue: function() {
        	var me = this;
        	var categoryId = window.categoryID;
        	var searchProductQueryStringTemp = me.makeProductQueryString();
    		me.productApiCall(searchProductQueryStringTemp, 0, window.sortbyValueProducts, window.sortValueProducts, false, true, 1, window.categoryID, window.facetValueFilter);
        },
        sortAndFilter: function(e){
        	e.preventDefault();
        	$(e.currentTarget).closest('.tab-pane').find('.filters').addClass('open');
        	moreLessConfig();
        },
        clearFacets:function(){
        	var me = this;
        	$(document).find('.filters').removeClass('open');
        	var searchProductQueryStringTemp = me.makeProductQueryString();
        	me.productApiCall(searchProductQueryStringTemp, 0, '', Hypr.getLabel('default'), false, true, 1, window.categoryID, '');
        },
        drillDownSort: function(e) {
        	e.preventDefault();
        	var me = this;
        	var categoryID = $(e.currentTarget).data('mz-hierarchy-id');
        	window.categoryID = categoryID;
        	var searchProductQueryStringTemp = me.makeProductQueryString();        	
        	me.productApiCall(searchProductQueryStringTemp, 0, window.sortbyValueProducts, window.sortValueProducts, false, true, 1, window.categoryID, window.facetValueFilter);
        },
        filterbyFacet: function(e) {
        	var me = this;
        	var facetValueFilter = "";
        	var selected = $(document).find('input[data-mz-action="filterbyFacet"]:checked');
        	selected.each(function(index) {   
            	var facetName = $(selected[index]).data('mz-facet');
            	var facetValue = $(selected[index]).data('mz-facet-value');
        		if(facetName == 'Price') {
        			facetValueFilter = facetValueFilter + facetValue + ',';
            	}else {
            		facetValueFilter = facetValueFilter + facetName + ':' + facetValue + ',';
            	}            	
            });
        	window.facetValueFilter = facetValueFilter;
        	var searchProductQueryStringTemp = me.makeProductQueryString();        	
        	if($(window).width() > 767 || $(e.currentTarget).data('mz-facet') == HyprLiveContext.locals.themeSettings.soldItemAttr) {
        		me.productApiCall(searchProductQueryStringTemp, 0, window.sortbyValueProducts, window.sortValueProducts, false, true, 1, window.categoryID, window.facetValueFilter);
        	}
        },        
        allCategories: function() {
        	var me = this;
        	window.categoryID = Hypr.getThemeSetting('searchableProductsId');
        	window.facetValueFilter = '';
        	var searchProductQueryStringTemp = me.makeProductQueryString();
        	me.productApiCall(searchProductQueryStringTemp, 0, window.sortbyValueProducts, window.sortValueProducts, false, true, 1, window.categoryID, window.facetValueFilter);
        },
        next: function(e) {
        	e.preventDefault();
        	var me = this;
        	var startIndex = me.model.get('startIndex') + me.model.get('pageSize');
        	var hasPreviousPage = true;
        	var currentPage = me.model.get('currentPage') + 1;
        	var hasNextPage;
        	if (me.model.get('totalCount')/me.model.get('pageSize') > currentPage) {
        		hasNextPage = true;
        	} else {
        		hasNextPage = false;
        	}
        	var searchProductQueryStringTemp = me.makeProductQueryString();
        	me.productApiCall(searchProductQueryStringTemp, startIndex, window.sortbyValueProducts, window.sortValueProducts, hasPreviousPage, hasNextPage, currentPage, window.categoryID, window.facetValueFilter);
        },
        previous: function(e){
        	e.preventDefault();
        	var me = this;
        	var startIndex = me.model.get('startIndex') - me.model.get('pageSize');
        	var currentPage = me.model.get('currentPage') - 1;
        	var hasPreviousPage;
        	if(currentPage == 1) {
        		hasPreviousPage = false;
        	} else {
        		hasPreviousPage = true;
        	}
        	var hasNextPage = true;        
        	var searchProductQueryStringTemp = me.makeProductQueryString();
        	me.productApiCall(searchProductQueryStringTemp, startIndex, window.sortbyValueProducts, window.sortValueProducts, hasPreviousPage, hasNextPage, currentPage, window.categoryID, window.facetValueFilter);
       	},
        page: function(e) {
        	e.preventDefault();
        	var me = this;
        	var selectedPageNumber = $(e.currentTarget).data('mz-page-num');
        	var startIndex = (selectedPageNumber * me.model.get('pageSize')) - me.model.get('pageSize');
        	var currentPage = selectedPageNumber;
        	var hasPreviousPage;
        	if(currentPage == 1) {
        		hasPreviousPage = false;
        	} else {
        		hasPreviousPage = true;
        	}
        	
        	var hasNextPage;
        	if (me.model.get('totalCount')/me.model.get('pageSize') > currentPage) {
        		hasNextPage = true;
        	} else {
        		hasNextPage = false;
        	}
        	var searchProductQueryStringTemp = me.makeProductQueryString();
        	me.productApiCall(searchProductQueryStringTemp, startIndex, window.sortbyValueProducts, window.sortValueProducts, hasPreviousPage, hasNextPage, currentPage, window.categoryID, window.facetValueFilter);
        },
        makeProductQueryString: function(){
        	if(window.categoryID) {
        		return searchProductQueryString + ' and CategoryId req ' + window.categoryID + '';
        	} else {
        		return searchProductQueryString;
        	}
        },
        productApiCall: function(searchProductQueryString, startIndex, sortbyValueProducts, sortValueProducts, hasPreviousPage, hasNextPage, currentPage, categoryID, facetValueFilter){
        	var me = this;
        	$('html, body').scrollTop(145);
        	$('.content-loading').show();
        	searchProductQueryString = searchProductQueryString;
        	api.get('search', {query:searchQuery, filter:searchProductQueryString, facet: 'categoryId', facetTemplate: 'categoryId:' + categoryID + '', facetValueFilter: facetValueFilter, pageSize: window.productPageSize, startIndex: startIndex, sortBy:sortbyValueProducts} ).then(function(response) {
    			me.model = new Backbone.Model(response.data);

    			var removeValFrom = [];
        		_.map(response.data.facets, function(item, index){       			
        			if(item.values.length === 0) {
        				removeValFrom.push(index);
        			}
        		});
        		
        		response.data.facets = response.data.facets.filter(function(value, index) {
        		     return removeValFrom.indexOf(index) == -1;
        		});
        		
        		me.model.set('facets', response.data.facets);
    			if (me.model.get('totalCount')/me.model.get('pageSize') > currentPage) {
            		hasNextPage = true;
            	} else {
            		hasNextPage = false;
            	}
    			me.model.set('sortByValue', sortValueProducts);
    			me.model.set('narrowSearchList', window.globalSearchCategorylist);
    			me.model.set('hasPreviousPage', hasPreviousPage);
    			me.model.set('hasNextPage', hasNextPage);
    			me.model.set('currentPage', currentPage);
    			me.model.set('firstIndex', startIndex + 1); 
    			me.model.set('isPurchesLocation', isPurchesLocation); 
    			me.model.set('lastIndex', startIndex + me.model.get('items').length);
    			var middlePageNumbers = [];    			
    			if(currentPage <= 3) {
    				middlePageNumbers = [];
    				for(var x=2; x<response.data.pageCount; x++) {
    	    			if(x <= 6) {
    	    				middlePageNumbers.push(x);
    	    			}
    	    		}
    			} else if(currentPage >= response.data.pageCount-2) {
    				middlePageNumbers = [];
    				for(var y=response.data.pageCount-5; y<=response.data.pageCount-1; y++) {
    	    			if(y >= 2) {
    	    				middlePageNumbers.push(y);
    	    			}
    	    		}
    			} else {
    				middlePageNumbers = [];
    				for(var z=2; z>=1; z--) {
        				if(currentPage != 1) {
        					middlePageNumbers.push(currentPage - z);
        				}
            		}
        			middlePageNumbers.push(currentPage);
        			for(var p=1; p<=2; p++) {
            			middlePageNumbers.push(currentPage + p);
            		}
    			}
    			me.model.set('middlePageNumbers', middlePageNumbers);
    			
    			//Making URL as per filters
    			var facetValueFilterUrl = '';
    			_.map(me.model.get('facets'), function(item, index){
    				if(item.field != 'CategoryId' && item.field != 'system~price-list-entry-type') {
    					_.map(item.values, function(facet, i){
    						if(facet.isApplied){
    							facetValueFilterUrl = facetValueFilterUrl + facet.filterValue + ',';
    			            }
    					});
    				}
    			});
    			if(!facetValueFilterUrl){
    				facetValueFilterUrl='';
    			}    			
    			
    			var makeProductUrl = '';
    			if(me.model.get('startIndex')){
    				makeProductUrl = makeProductUrl + '&startIndex=' + me.model.get('startIndex');
    			}
    			if(window.categoryID){
    				makeProductUrl = makeProductUrl + '&categoryId=' + window.categoryID;
    			}
    			if(facetValueFilterUrl){
    				makeProductUrl = makeProductUrl + '&facetValueFilter=' + facetValueFilterUrl;
    			}
    			if(sortbyValueProducts) {
    				makeProductUrl = makeProductUrl + '&sortBy=' + sortbyValueProducts;
    			}    	
    			window.makeProductUrl = makeProductUrl;
    		    window.history.pushState({},"", makeurl + window.makeProductUrl + window.makeArticleUrl);
            	me.render();
            	$(document).find('a[href="#product-tab"] > span').text(me.model.get('totalCount'));
            	$('.content-loading').hide();
    		});
        },
        render: function(){
        	var me = this;
            var contextSiteId = apiContext.headers["x-vol-site"];
            var homeFurnitureSiteId = Hypr.getThemeSetting("homeFurnitureSiteId");
            var frSiteId = Hypr.getThemeSetting("frSiteId");
            if(homeFurnitureSiteId === contextSiteId){
                me.model.set("isHomeFurnitureSite", true);
            }
            if(frSiteId === contextSiteId){
                var productItems = me.model.get('items');
                _.each(productItems, function(product){
                     product.isFrenchSite = true;
                });
            }
            var currentPurchaseLocation = require.mozuData('apicontext').headers['x-vol-purchase-location'];
            me.model.set("currentPurchaseLocation", currentPurchaseLocation);
            me.model.set('currentLocale', locale);
			me.model.set('currentSite', currentSite);
            Backbone.MozuView.prototype.render.apply(this, arguments);
            
        	if (window.gridStatus == "list") {
            	$('.result-view-toggle').find('li').removeClass('is-current');
            	$('.result-view-toggle').find('li:first-child').addClass('is-current');
            	$(document).find('.mz-productlist').addClass('list');
            } else {
            	$('.result-view-toggle').find('li').removeClass('is-current');
            	$('.result-view-toggle').find('li:nth-child(2)').addClass('is-current');
            	$(document).find('.mz-productlist').removeClass('list');
            }
        	
        	moreLessConfig();
        }
        
    });
	
    var NoSearchView = Backbone.MozuView.extend({
    	templateName: "modules/common/no-search-view"
    });	
    
    $.urlParam = function(name){
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results===null){
           return null;
        }
        else{
           return decodeURI(results[1]) || 0;
        }
    };
    var facetHeightWithPX;
    function moreLessConfig(){
    	var facetHeight = Hypr.getThemeSetting('facetDefaultHeight');
    	facetHeightWithPX = Hypr.getThemeSetting('facetDefaultHeight')+'px';
    	/*Code for moretag on facets - Start*/
        $('.mz-facetingform').find('.mz-l-sidebaritem .facet-details > ul').each(function(){
        	if($(this).height() > facetHeight ) {
        		$(this).parent().find('.moretag').css('display', 'inline-block');
        		if($(this).closest('.mz-l-sidebaritem').hasClass('for-caregory')){
        			$(this).css({'height': '205px', 'overflow-y': 'hidden'});
        		}else {
        			$(this).css({'height': facetHeightWithPX, 'overflow-y': 'hidden'});
        		}
        	}
        });
        $('.mz-facetingform').find('.mz-l-sidebaritem #Price > ul').css({'height': 'auto', 'overflow-y': 'hidden'});
        /*Code for moretag on facets - End*/
    }
    
    function calculateMiddlePageNumbers(dataModel) {
    	var middlePageNumbers = [];    			
		if(dataModel.currentPage <= 3) {
			middlePageNumbers = [];
			for(var x=2; x<dataModel.pageCount; x++) {
    			if(x <= 6) {
    				middlePageNumbers.push(x);
    			}
    		}
		} else if(dataModel.currentPage >= dataModel.pageCount-2) {
			middlePageNumbers = [];
			for(var y=dataModel.pageCount-5; y<=dataModel.pageCount-1; y++) {
    			if(y >= 2) {
    				middlePageNumbers.push(y);
    			}
    		}
		} else {
			middlePageNumbers = [];
			for(var z=2; z>=1; z--) {
				if(dataModel.currentPage != 1) {
					middlePageNumbers.push(dataModel.currentPage - z);
				}
    		}
			middlePageNumbers.push(dataModel.currentPage);
			for(var p=1; p<=2; p++) {
    			middlePageNumbers.push(dataModel.currentPage + p);
    		}
		}
		dataModel.middlePageNumbers = middlePageNumbers;
    }
    
		var noResultModel = {};
		$('.content-loading').show();
		var getProducts = function(isDirectLink){		
			var facetValueFilter = decodeURIComponent($.urlParam('facetValueFilter'));
			if(facetValueFilter=='0' || facetValueFilter=='null') {
				facetValueFilter = '';
			}
			var categoryId = $.urlParam('categoryId');
			var startIndex;
			if ($.urlParam('startIndex')) {
				startIndex = $.urlParam('startIndex');
			} else {
				startIndex = 0;
			}
			var tempSearchProductQueryString;
			if (!categoryId) {
				categoryId = Hypr.getThemeSetting('searchableProductsId');
			}
			window.categoryID = categoryId;
			tempSearchProductQueryString = searchProductQueryString + ' and CategoryId req ' + categoryId + '';
			
			var sortbyValueProducts;
			if ($.urlParam('sortBy')) {
				sortbyValueProducts = $.urlParam('sortBy');
			} else {
				sortbyValueProducts = HyprLiveContext.locals.themeSettings.defaultSort;
			}
			
			if(sortbyValueProducts) {
				var sortValueProducts = _.findWhere(sortByOptions, {value: sortbyValueProducts});
				sortValueProducts = sortValueProducts.text;
			}
			
	    	var searchProductResult;    
	    	tempSearchProductQueryString = tempSearchProductQueryString;
	    	api.get('search', {query:searchQuery, filter:tempSearchProductQueryString, facet: 'categoryId', facetTemplate: 'categoryId:' + categoryId + '', facetValueFilter: facetValueFilter, pageSize: window.productPageSize, startIndex: startIndex, sortBy:sortbyValueProducts} ).then(function(response) {
	    		searchProductResult = response.data;       		
	    		searchProductResult.facets = response.data.facets; 
	    		searchProductResult.isPurchesLocation = isPurchesLocation;
	    		if(searchProductResult.totalCount > 0){
		    		var searchProductCount = searchProductResult.totalCount;
		    		var childrenFacetValues = _.findWhere(response.data.facets, {facetType: "Hierarchy"});
		    		var globalSearchCategorylist=[];
		    		_.map(childrenFacetValues.values, function(category){
		    			_.map(category.childrenFacetValues, function(childrenitem){
		    				_.map(childrenitem.childrenFacetValues, function(item){
			    				globalSearchCategorylist.push(item);
			    			});
		    			});
		    		});
		    		
		    		globalSearchCategorylist = _.sortBy(globalSearchCategorylist, function(item){ return -item.count; });
		    		
		    		globalSearchCategorylist = globalSearchCategorylist.slice(0, 6);
		    		
		    		searchProductResult.currentPage = (searchProductResult.startIndex/searchProductResult.pageSize) + 1;
		    		calculateMiddlePageNumbers(searchProductResult);   		
		    		
		    		if(sortValueProducts) {
		    			searchProductResult.sortByValue = sortValueProducts;
		    			window.sortValueProducts = sortValueProducts;
		    		} else {
		    			searchProductResult.sortByValue = Hypr.getLabel('default');
		    			window.sortValueProducts = Hypr.getLabel('default');
		    		}
		    		if(sortbyValueProducts) {
		    			window.sortbyValueProducts = sortbyValueProducts;
		    		} else {
		    			window.sortbyValueProducts = "";
		    		}		    		
		    		
		    		if(searchProductResult.currentPage == 1) {
		    			searchProductResult.hasPreviousPage = false;
		        	} else {
		        		searchProductResult.hasPreviousPage = true;
		        	}
		    		
		    		
		    		if (searchProductCount/searchProductResult.pageSize > searchProductResult.currentPage) {
		    			searchProductResult.hasNextPage = true;
	            	} else {
	            		searchProductResult.hasNextPage = false;
	            	}
		    		
		    		searchProductResult.firstIndex = searchProductResult.startIndex + 1;
		    		searchProductResult.lastIndex = searchProductResult.startIndex + searchProductResult.items.length;
		    		if(globalSearchCategorylist.length === 6) {
		    			var filterString = "categoryId eq ",
			    			lastIndex = 5;
			    		_.map(globalSearchCategorylist, function(item, i) {
			    			filterString = filterString + item.value;
			    			if(i != lastIndex){
			    				filterString = filterString + ' or categoryId eq ';
			    			}
			    		});
			    		
			    		api.get('categories', {filter: filterString} ).then(function(categories) {
			    			globalSearchCategorylist = categories.data.items;
							window.globalSearchCategorylist = globalSearchCategorylist;
							searchProductResult.narrowSearchList = window.globalSearchCategorylist;
							if(isDirectLink) {
								var searchProductView = new SearchProductView({
							       el: $('#product-tab'),
							       model:  new Backbone.Model(searchProductResult)
								});
								globalSearchProductView = searchProductView;
								searchProductView.render(); 
							} else {
								globalSearchProductView = new Backbone.Model(searchProductResult);
								globalSearchProductView.render(); 
							}
							   			
				    		$(document).find('a[href="#product-tab"] > span').text(searchProductCount);
				    		$('.content-loading').hide();
			    		});	    		
		    		} else {
		    			searchProductResult.narrowSearchList = window.globalSearchCategorylist = false;
		    			if(isDirectLink) {
			    			var searchProductView = new SearchProductView({
						       el: $('#product-tab'),
						       model:  new Backbone.Model(searchProductResult)
							});
			    			globalSearchProductView = searchProductView;
							searchProductView.render();  
		    			} else {
		    				globalSearchProductView.model = new Backbone.Model(searchProductResult);	
		    				globalSearchProductView.render(); 
						}
			    		$(document).find('a[href="#product-tab"] > span').text(searchProductCount);
			    		$('.content-loading').hide();
		    		} 
	    		} else {
	    			noResultModel.tab = 'Products';
	        			var noSearchView = new NoSearchView({
	 	    		       el: $('#product-tab'),
	 	    		       model: new Backbone.Model(noResultModel)
	 	    			});
	        			noSearchView.render();  
	        			$('.content-loading').hide();
	        			$(document).find('a[href="#product-tab"] > span').text('0');
	        			$(document).find('a[href="#expert-advice-tab"]').trigger('click');
	    		}
	    		
	    	});
    	
		};
		
		var getArticles = function(isDirectLink){
    		var ExpStartIndex;
    		if ($.urlParam('ExpStartIndex')) {
    			ExpStartIndex = $.urlParam('ExpStartIndex');
    		} else {
    			ExpStartIndex = 0;
    		}
    		
    		var expCategoryId = $.urlParam('ExpCategoryId');
    		var tempSearchArticleQueryString;
    		if (expCategoryId) {
    			tempSearchArticleQueryString = searchArticleQueryString + ' and CategoryId req ' + expCategoryId + '';
    		} else {
    			expCategoryId = '';
    			tempSearchArticleQueryString = searchArticleQueryString;
    		}
    		
    		var sortbyValueArticles;
    		if ($.urlParam('ExpSortBy')) {
    			sortbyValueArticles = $.urlParam('ExpSortBy');
    		} else {
    			sortbyValueArticles = HyprLiveContext.locals.themeSettings.defaultSort;
    		}    		
    		
    		if(sortbyValueArticles) {
    			var sortValueArticles = _.findWhere(sortByOptions, {value: sortbyValueArticles});
        		sortValueArticles = sortValueArticles.text;
    		}
    		
        	var expertArticles;        	
        	api.get('search', {query:searchQuery, filter:tempSearchArticleQueryString, facet: 'categoryId', pageSize: window.articlePageSize, startIndex: ExpStartIndex, sortBy: sortbyValueArticles} ).then(function(response) {
        		expertArticles = response.data;   
        		if(expertArticles.totalCount > 0){
	        		var expertAdviceCount = expertArticles.totalCount;
	        		
	        		
	        		expertArticles.currentPage = (expertArticles.startIndex/expertArticles.pageSize) + 1;
	        		calculateMiddlePageNumbers(expertArticles);	        		
	        		
	        		if(sortValueArticles) {
	        			expertArticles.sortByValue = sortValueArticles;
		    			window.sortValueArticles = sortValueArticles;
		    		} else {
		    			expertArticles.sortByValue = Hypr.getLabel('default');
		    			window.sortValueArticles = Hypr.getLabel('default');
		    		}
		    		if(sortbyValueArticles) {
		    			window.sortbyValueArticles = sortbyValueArticles;
		    		} else {
		    			window.sortbyValueArticles = "";
		    		}	        		
	        		
		    		if(expertArticles.currentPage == 1) {
		    			expertArticles.hasPreviousPage = false;
		        	} else {
		        		expertArticles.hasPreviousPage = true;
		        	}
		    		
		    		
		    		if (expertArticles.totalCount/expertArticles.pageSize > expertArticles.currentPage) {
		    			expertArticles.hasNextPage = true;
	            	} else {
	            		expertArticles.hasNextPage = false;
	            	}
	        		      		
	        		expertArticles.firstIndex = expertArticles.startIndex + 1;
	        		expertArticles.lastIndex = expertArticles.startIndex + expertArticles.items.length;
	    			
	    			if(isDirectLink) {
	    				var expertAdviceView = new ExpertAdviceView({
	 	    		       el: $('#expert-advice-tab'),
	 	    		       model: new Backbone.Model(expertArticles)
	 	    			});	 	    			
	    				globalExpertAdviceView = expertAdviceView;
						expertAdviceView.render();
					} else {
						globalExpertAdviceView.model = new Backbone.Model(expertArticles);
						globalExpertAdviceView.render(); 
					}
	    			
	    			$('.content-loading').hide();
	        		$(document).find('a[href="#expert-advice-tab"] > span').text(expertAdviceCount);
	        		if(searchQuery) {
	        			$(document).find('.tt-input').val(searchQuery);
	        			$(document).find('.mz-searchbox-field > input[type="search"]').val(searchQuery);
	        		}
	        		
        		} else {
        			noResultModel.tab = 'Articles';
        			var noSearchView = new NoSearchView({
 	    		       el: $('#expert-advice-tab'),
 	    		       model: new Backbone.Model(noResultModel)
 	    			});
        			noSearchView.render(); 
        			$('.content-loading').hide();
        			$(document).find('a[href="#expert-advice-tab"] > span').text('0');
        			if(searchQuery) {
	        			$(document).find('.tt-input').val(searchQuery);
	        			$(document).find('.mz-searchbox-field > input[type="search"]').val(searchQuery);
	        		}
        		}
        		
        	});
        };        
        
        if (homeFurnitureSiteId == currentSite) {
        	getProducts(true);
        } else {
			getProducts(true);
			getArticles(true);
        }
       
        
    	$(document).on('click', '.result-view-toggle li a', function (e){
        	e.preventDefault();
        	$(this).closest('.result-view-toggle').find('li').removeClass('is-current');
        	$(this).closest('li').addClass('is-current');
        	
        	if($(this).data('toggle-type') == 'list') {
        		window.gridStatus = "list";
        		$(document).find('.mz-productlist').addClass('list');
        	} else {
        		window.gridStatus = "";
        		$(document).find('.mz-productlist').removeClass('list');
        	}        	
        	$(document).find('#expert-advice-tab .mz-productlist').removeClass('list');
       });
    	
    	$(document).find('.filter-button').on('click', function() {
        	$(document).find('.filters').removeClass('open');
        });
    	
    	$('.result-type-selector a').on('click', function(){
    		setTimeout(function(){
    			moreLessConfig();
    		}, 100);
    		
    	});
    	
    	$(document).on('click', '.moretag', function(event){
        	event.preventDefault();
        	var viewLess = Hypr.getLabel('viewLess');
        	var viewMore = Hypr.getLabel('viewMore');
        	if($(this).find('span').text() === viewMore) {
        		$(this).parent().find('.mz-facetingform-facet').css('height', 'auto');
            	$(this).find('span').text(viewLess);
        	} else {
        		if($(this).closest('.mz-l-sidebaritem').hasClass('for-caregory')){
        			$(this).parent().find('.mz-facetingform-facet').css('height', '205px');
        		}else {
        			$(this).parent().find('.mz-facetingform-facet').css('height', facetHeightWithPX);
        		}
        		
            	$(this).find('span').text(viewMore);
        	}
        	
        });
    	
    	window.onpopstate = function(event) {
    		$('.content-loading').show();
    		if (homeFurnitureSiteId == currentSite) {
            	getProducts(false);
            } else {
    			getProducts(false);
    			getArticles(false);
            }
		};
});