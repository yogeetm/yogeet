define(['modules/jquery-mozu',
	"hyprlivecontext"], function($, HyprLiveContext) {
	$(document).ready(function(){
		setTimeout(function(){
			var searchQuery = HyprLiveContext.locals.pageContext.query.query;
			searchQuery = searchQuery.split('+').join(' ');
			$(document).find('#search_submit').trigger('click');
			$(document).find('.tt-input').val(searchQuery);
		},1000);
		
	});
	
});