define(['modules/api',
        'modules/backbone-mozu',
        'underscore',
        'modules/jquery-mozu',
        'modules/models-cart',
        'modules/cart-monitor',
        'hyprlivecontext',
        'hyprlive',
        'modules/preserve-element-through-render',
        'modules/modal-dialog',
        'modules/xpress-paypal',
        "modules/models-product",
        "modules/views-messages"
      ], function (api, Backbone, _, $, CartModels, CartMonitor, HyprLiveContext, Hypr, preserveElement, modalDialog, paypal, ProductModels, messageViewFactory) {

	var previousCartMessages = [];
	var appliedCouponCodes = [];
  var configPromises = [];
  var entityListItems;
  var user = require.mozuData('user');
  var apiContext = require.mozuData('apicontext');

  var MessageModel = Backbone.MozuModel.extend({});
  var MessageCollection = new Backbone.Collection();
  
  
  var messageView = messageViewFactory({
  	el: $('[data-mz-message-bar]'),
  	model: MessageCollection
  });
  
  var messageModel = new MessageModel();
  
    var CartView = Backbone.MozuView.extend({
        templateName: "modules/cart/cart-table",
        additionalEvents: {
          "click .changePreferredStore": "changePreferredStore"
        },
        initialize: function () {

            this.pickerDialog = this.initializeStorePickerDialog();
            var me = this;
            previousCartMessages = me.model.get('cartMessage');
            var cartData = this.model.apiModel.data.items;
            var preferredStore = $.parseJSON($.cookie("preferredStore"));
            var itemsToRemove=[], cartTotalCount=0;
            
            /* find products which are having different store and remove those from cart by api call*/
            cartData.forEach(function(item){
              if(item.fulfillmentLocationCode !== preferredStore.code){
                itemsToRemove.push(item);//line item
              }
            });
            var queryString = "";
            itemsToRemove.forEach(function(item,index){
              if(index != itemsToRemove.length-1){
                queryString += "productCode eq "+item.product.productCode+" or ";
              }else{
                queryString += "productCode eq "+item.product.productCode;
              }
            });
            
            //Fetch entitylist for the products from cart
            var entityListForEHF = Hypr.getThemeSetting('entityListForEHF');
            if(queryString){
            	var ehfQueryString = queryString + " and province eq " + preferredStore.address.stateOrProvince;
            	api.get('entityList', { listName: entityListForEHF, filter: ehfQueryString }).then(function(response) {
    	    		if(response.data.items.length > 0) { //if EHF is applied for current product
    	    			entityListItems = response.data;
    	    		}
                });
            }
            
            // perform steps to remove products from cart and then re add products having price > 0 to cart
            if(itemsToRemove.length > 0){
              $("#content-loading").show();
              $(".cart-page-container").css('visibility', 'hidden');
              var removeSteps = this.getRemoveSteps(itemsToRemove);
              me.model.set("showTotal",false);
                api.steps(removeSteps).then(function(response){
                  var isEcomStore = _.findWhere(preferredStore.attributes, {"fullyQualifiedName":Hypr.getThemeSetting("isEcomStore")});
                  if(isEcomStore && isEcomStore.values[0] === 'Y'){
                    api.get("products",{filter: queryString}).then(function(productResponse){
                       me.performAddSteps(itemsToRemove,productResponse.data.items);
                    }, function(errorResponse) {
                      //handle product fetch error
                    });
                  }else{
                    $("#content-loading").hide();
                    $(".cart-page-container").css('visibility', 'visible');
                  }
                }, function(errorResponse) {
                  //handle remove steps error
                });
            }else{
              me.model.set("showTotal",true);
            }
            //setup coupon code text box enter.
            this.listenTo(this.model, 'change:couponCode', this.onEnterCouponCode, this);
            this.codeEntered = !!this.model.get('couponCode');
            this.$el.on('keypress', 'input', function (e) {
                if (e.which === 13) {
                    if (me.codeEntered) {
                        me.handleEnterKey();
                    }
                    return false;
                }
            });

            this.listenTo(this.model.get('items'), 'quantityupdatefailed', this.onQuantityUpdateFailed, this);

            var visaCheckoutSettings = HyprLiveContext.locals.siteContext.checkoutSettings.visaCheckout;
            var pageContext = require.mozuData('pagecontext');
            if (visaCheckoutSettings.isEnabled) {
                window.onVisaCheckoutReady = initVisaCheckout;
                require([pageContext.visaCheckoutJavaScriptSdkUrl], initVisaCheckout);
            }
            //cache for storing info retrieved through API calls
            this.fulfillmentInfoCache = [];
            this.model.get('items').forEach(function(item){
              var dataObject = {
                cartItemId: item.id,
                locations: []
              };
              me.fulfillmentInfoCache.push(dataObject);

            });
            this.appliedCouponCodes();
            
            //update cart count excluding EHF product 
            _.each(me.model.get('items').toJSON(), function(product){
        		if(product.product.productCode != 'EHF101') {
        			cartTotalCount += product.quantity;
        		}
        	});
            me.model.set('count', cartTotalCount);
            me.model.set('cartCount', cartTotalCount);
            var documentListForAeroplanPromo = Hypr.getThemeSetting('documentListForAeroplanPromo');
		    api.get('documentView', {listName: documentListForAeroplanPromo}).then(function(response) {
		    	if(response.data.items.length > 0) {
		    		var aeroplanMilesFactor = response.data.items[0].properties.markupFactor;
		    		me.model.set('aeroplanMiles', aeroplanMilesFactor);
		    		me.model.set('isPromoApplied', true);
		    		me.render();
		    	}
			});
        },
        setAeroplanMiles: function(aeroplanMiles) {
        	var me = this;
        	
        },
        resolvePromises: function(promises){
            var deferred = $.Deferred();
            var fulfilled = 0, length = promises.length;
            var results = [];

            if (length === 0) {
                deferred.resolve(results);
            } else {
                promises.forEach(function(promise, i){
                    $.when(promise()).then(function(value) {
                        results = value;
                        fulfilled++;
                        if(fulfilled === length){
                            deferred.resolve(results);
                        }
                    });
                });
            }
            return deferred.promise();
        },
        performAddSteps: function(oldItems,newItems){
          var self = this,
              preferredStore = $.parseJSON($.cookie("preferredStore")),
              finalProducts;
          var tempOldItems = $.extend(true, {}, oldItems);
          self.getFinalProducts(oldItems, newItems);
          $.when(self.resolvePromises(configPromises)).then(function(results) {
              finalProducts = results;
              var tasks = finalProducts.map(function(finalProduct) {
            	  var oldItem = _.find(oldItems, function(item, index){
            		  if (item.product.productCode === finalProduct.attributes.productCode) {
            			  var itemToReturn = item;
            			  return itemToReturn;
            		  }
            	  });
            	  return function(){
	                  return finalProduct.apiAddToCartForPickup({
	                      fulfillmentLocationCode: preferredStore.code,
	                      fulfillmentMethod: ProductModels.Product.Constants.FulfillmentMethods.PICKUP,
	                      fulfillmentLocationName: preferredStore.name,
	                      quantity: oldItem.quantity
	                  });
                  };
              });
             
              api.steps(tasks).then(function(response){
                  self.model.apiGet().then(function(){
                    $("#content-loading").hide();
                    $(".cart-page-container").css('visibility', 'visible');
                    messageModel.set({message: previousCartMessages.message});
                    messageModel.set({messageType: previousCartMessages.messageType});
                    messageModel.set({productsRemoved: previousCartMessages.productsRemoved});
                	MessageCollection.add(messageModel);
                	self.trigger('error', previousCartMessages.message);
                	messageView.render();
                  });
                  self.model.set("showTotal",true);
              }, function(errorResponse) {
                  //handle error response
              });
          });
        },
        getRemoveSteps:function(items){
          var self = this;
          var tasks = items.map(function(item){
            return function(){
              return self.model.removeItem(item.id);
            };  
          });
          return tasks;
        },
        getFinalProducts: function(cartData, productsToAdd){
          var finalProducts=[],
              self = this,
              price;
          _.each(cartData,function(cartItem) {
            _.each(productsToAdd, function(product){
              if(cartItem.product.productCode == product.productCode){
                configPromises.push(function() {
                  return $.Deferred(function(dfd) {
                    var productModel = new ProductModels.Product(product);
                    if (cartItem.product.variationProductCode) {
                      _.each(cartItem.product.options, function(currentOption) {
                            self.configureProduct(currentOption, productModel);
                        });
                        productModel.on("productConfigured", function(){
                            price = productModel.get("price");
                            if(price.attributes.price > 0){
                              finalProducts.push(productModel);
                              self.addRelatedEHFProduct(productModel, dfd, finalProducts);
                            }else {
                            	dfd.resolve(finalProducts);
                            }
                        });
                    }else{
                        price = productModel.get("price");
                      	if(price.attributes.price > 0){
                          finalProducts.push(productModel);
                          self.addRelatedEHFProduct(productModel, dfd, finalProducts);
                      	}else{
                    	  dfd.resolve(finalProducts);
                      	}
                    }
                  }).promise();
                });
              }
             });
          });
        },
        configureProduct: function($optionEl, currentModel){
          var newValue = $optionEl.value,
                oldValue,
                id = $optionEl.attributeFQN,
                isPicked = true,
                option = currentModel.get('options').findWhere({
                    'attributeFQN': id
                });
            if (option) {
                if (option.get('attributeDetail').inputType === "YesNo") {
                    option.set("value", isPicked);
                } else if (isPicked) {
                    oldValue = option.get('values');
                    if (oldValue !== newValue && !(oldValue === undefined && newValue === '')) {
                        option.set('value', newValue);
                    }
                }
            }
        },
        addRelatedEHFProduct: function(productModel, dfd, finalProducts) {
        	var productCode, preferredStore = $.parseJSON($.cookie("preferredStore"));
        	var relatedEHF;
        	if(productModel.get('variationProductCode')) {
        		productCode = productModel.get('variationProductCode');
        	}else {
        		productCode = productModel.get('productCode');
        	}
        	if(entityListItems && entityListItems.items.length > 0) {
        		relatedEHF = _.findWhere(entityListItems.items, {'productCode': productCode, 'province': preferredStore.address.stateOrProvince});
        	}
        	var ehfAttributeFQN = Hypr.getThemeSetting('ehfFees');
            var ehfOption = productModel.get('options').findWhere({
                'attributeFQN': ehfAttributeFQN
            });
            if(ehfOption && ehfOption.get('shopperEnteredValue')) {
            	ehfOption.set('shopperEnteredValue', ''); //remove previous EHF
            	ehfOption.set('value', '');
            }
            if(relatedEHF && ehfOption) {
	      		ehfOption.set('shopperEnteredValue', relatedEHF.feeAmt);
	      		ehfOption.set('value', relatedEHF.feeAmt);
	      		dfd.resolve(finalProducts);
            }else {
        	  dfd.resolve(finalProducts);
            }
        },
        render: function() {
            var me = this, EHFTotal = 0;
            _.each(me.model.get('items').toJSON(), function(item) {
            	if(item.handlingAmount) {
            		EHFTotal += item.handlingAmount;
            	}
            });
            me.model.set('EHFTotal', EHFTotal);
            var contextSiteId = apiContext.headers["x-vol-site"];
            var frSiteId = Hypr.getThemeSetting("frSiteId");
            if(frSiteId === contextSiteId){
                me.model.set("isFrenchSite", true);   
            }
            if(!me.model.get('isPromoApplied')) {
            	var aeroplanMiles;
                aeroplanMiles = Math.floor(me.model.get('total')/2);
                me.model.set('aeroplanMiles', aeroplanMiles);
            }
            if(me.model.get('isEmpty') && previousCartMessages) {
            	messageModel.set({message: previousCartMessages.message});
                messageModel.set({messageType: previousCartMessages.messageType});
                messageModel.set({productsRemoved: previousCartMessages.productsRemoved});
            	MessageCollection.add(messageModel);
            	me.trigger('error', previousCartMessages.message);
            }
            preserveElement(this, ['.v-button', '.p-button'], function() {
                Backbone.MozuView.prototype.render.call(this);
            });
        },
        updateQuantity: function(e){
        	var me = this;
        	var field = $(e.currentTarget).attr('data-mz-value'); //this field is used to increment/decrement the quantity
          
        	var quantity = $(e.currentTarget).parent().siblings('.quantity-input-field').val();
	        if(field === 'qty-decrement'){
	          $(e.currentTarget).parent().siblings('.quantity-input-field').val(parseInt(quantity, 10) - 1);
	        }
	          
	        if(field === 'qty-increment'){
	          $(e.currentTarget).parent().siblings('.quantity-input-field').val(parseInt(quantity, 10) + 1);
	        } 
	        var $qField = "";
	        if(field != 'quantity'){
	        	$qField = $(e.currentTarget).parent().siblings('.quantity-input-field');
	        }else {
	        	$qField = $(e.currentTarget);
	        }
	        
        	if(isNaN($qField.val()) || $qField.val() <= 0 ) { //if quantity is not a number or less than equal to 0 then update the quantity to 1
        		e.preventDefault();
        		$qField.val(1);
        		$('#qty-decrement-field').addClass('is-disabled');
        		var currentItemId = $qField.data('mz-cart-item'),
        		currentItem = me.model.get("items").get(currentItemId),
        		self = this;
        		currentItem.set('quantity', newQuantity);
        		currentItem.apiUpdateQuantity(1).then(function(item) {
        			self.updateCount();
        			self.model.apiGet();
        			self.model.set("showTotal",true);
        		});
        	}else {
                var newQuantity = parseInt($qField.val(), 10),
                    id = $qField.data('mz-cart-item'),
                    item = this.model.get("items").get(id);

                if (item && !isNaN(newQuantity)) {
                    item.set('quantity', newQuantity);
                    var preferredStore = $.parseJSON($.cookie("preferredStore"));
                    item.apiUpdateQuantity(item.get("quantity")).then(function(item) {
                    	me.updateCount();
                    });
                }
        	}
        	
        },
        updateCount: function() {
        	var self = this;
        	api.get('cart',{}).then(function(response){
            	var cartTotalCount = 0;
            	_.each(response.data.items, function(product){
            		if(product.product.productCode != 'EHF101') {
            			cartTotalCount = cartTotalCount + product.quantity;
            		}
            	});
            	self.model.set('count',cartTotalCount);
            	self.model.set('cartCount', cartTotalCount);
            	self.render();
            	CartMonitor.setCount(cartTotalCount);
            });
        },

        addToWishlist: function (e) {
          var self = this;
          if (user.isAnonymous) {
        	  var locale = require.mozuData('apicontext').headers['x-vol-locale'];
              locale = locale.split('-')[0];
              var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
              var currentLocale = '';
              if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite){
              	currentLocale = locale === 'fr' ? '/fr' : '/en';
              }
      		  window.location.href=currentLocale + "/user/login?returnurl=/cart";
      	} else {     		
      	
          var id = $(e.currentTarget).data('mz-wishlist-item');
          var product = self.model.get('items').get(id).get('product');
          
          var currentItem = this.model.get("items").get(id);
          
          product.whenReady(function() {
            if (!product.validate()) {
              product.apiAddToWishlist({
                        customerAccountId: require.mozuData('user').accountId,
                        quantity: self.model.get('items').get(id).get("quantity")
                    }).then(function(item) { 
                      self.model.removeItem(id).then(function(item){ //After product is successfully added to wishlist, 
                          var addedToWishlistProductDetails = {'productName': product.get('name'), 'productUrl': product.get('url')}; //save its details into new variable to show added to wishlist msg on cart page.
                            self.model.set('addedToWishlistProductDetails', addedToWishlistProductDetails);
                            self.updateCount();
                            /*Update Wishlist Count*/
                            api.get('wishlist',{}).then(function(response){         
                              var items = response.data.items[0].items;
                                var totalQuantity = 0;
                                _.each(items, function(item, index) {
                                  totalQuantity += item.quantity;
                                });
                                $('#wishlist-count').html('('+totalQuantity+')');
                            });
                            self.render();
                        });
                    });
	            }
	          });
      		}
        },
        onQuantityUpdateFailed: function(model, oldQuantity) {
            var field = this.$('[data-mz-cart-item=' + model.get('id') + ']');
            if (field) {
                field.val(oldQuantity);
            }
            else {
                this.render();
            }
        },
        removeItem: function(e) {
          var self = this;
            if(require.mozuData('pagecontext').isEditMode) {
                // 65954
                // Prevents removal of test product while in editmode
                // on the cart template
                return false;
            }
            var $removeButton = $(e.currentTarget),
                id = $removeButton.data('mz-cart-item');
            
            var currentItem = this.model.get("items").get(id);
            
        	var product = self.model.get('items').get(id).get('product'); //Get product to be removed
        	self.model.removeItem(id).then(function(item) { //After product is successfully removed from model, 
            	self.updateCount();
            	var removedProductDetails = {'productName': product.get('name'), 'productUrl': product.get('url')}; //save its details into new variable to show deleted msg on cart page.
                self.model.set('removedProductDetails', removedProductDetails);
                self.render();
            });
            return false;
        },
        empty: function() {
            this.model.apiDel().then(function() {
                window.location.reload();
            });
        },
        initializeStorePickerDialog: function(){

          var me = this;

          var options = {
            elementId: "mz-location-selector",
            body: "", //to be populated by makeLocationPickerBody
            hasXButton: true,
            width: "400px",
            scroll: true,
            bodyHeight: "600px",
            backdrop: "static"

          };

          //Assures that each store select button has the right behavior
          $('#mz-location-selector').on('click', '.mz-store-select-button', function(){
            me.assignPickupLocation($(this).attr('mz-store-select-data'));
          });

          //Assures that the radio buttons reflect the accurate fulfillment method
          //if the dialog is closed before a store is picked.

          $('.modal-header').on('click', '.close', function(){
            var cartModelItems = window.cartView.cartView.model.get("items");
            var cartItemId = $(this).parent().parent().find('.modal-body').attr('mz-cart-item');
            var cartItem = me.model.get("items").get(cartItemId);
            me.render();
          });

          return modalDialog.init(options);

        },
        changeStore: function(e){
          //click handler for change store link.launches store picker
          var cartItemId = $(e.currentTarget).data('mz-cart-item');
          var cartItem = this.model.get("items").get(cartItemId);
          var productCode = cartItem.apiModel.data.product.variationProductCode || cartItem.apiModel.data.product.productCode;
          this.pickStore(productCode, cartItemId);
        },
        pickStore: function(productCode, cartItemId){
          /*
          Parent function for switching from ship to pickup from within cart
          or choosing a new pickup location from within cart. Runs a set of api
          calls using the cartItemId and that item's product code to get
          necessary inventory information and display a dialog containing that
          information.
          */
          var me = this;
          var listOfLocations = [];

          //before we get inventory data, we'll see if it's cached

          var filtered = this.fulfillmentInfoCache.filter(function(item){
            return item.cartItemId == cartItemId;
          });
          var cachedItemInvData;

          if (filtered.length!==0){
            cachedItemInvData = filtered[0];
          } else {
            //NGCOM-344
            //If the filtered array is empty, it means the item we're checkoutSettings
            // was added to the cart some time after page load, probably during a BOGO
            //sale re-rendering.
            //Let's go ahead and add it to the cache, then stick it in our
            //cachedItemInvData variable.
            var newCacheData = {
              cartItemId: cartItemId,
              locations: []
            };
            me.fulfillmentInfoCache.push(newCacheData);
            cachedItemInvData = newCacheData;
          }

          var index = this.fulfillmentInfoCache.indexOf(cachedItemInvData);

          if(cachedItemInvData.locations.length===0){
            //The cache doesn't contain any data about the fulfillment
            //locations for this item. We'll do api calls to get that data
            //and update the cache.

            me.getInventoryData(cartItemId, productCode).then(function(inv){
              if (inv.totalCount===0){
                //Something went wrong with getting inventory data.
                var $bodyElement = $('#mz-location-selector').find('.modal-body');
                me.pickerDialog.setBody(Hypr.getLabel("noNearbyLocationsProd"));
                $bodyElement.attr('mz-cart-item', cartItemId);
                me.pickerDialog.show();

              } else {
                //TO-DO: Make 1 call with GetLocations
                var invItemsLength = inv.items.length;
              inv.items.forEach(function(invItem, i){
                  me.handleInventoryData(invItem).then(function(handled){
                    listOfLocations.push(handled);
                    me.fulfillmentInfoCache[index].locations.push({
                      name: handled.data.name,
                      code: handled.data.code,
                      locationData: handled,
                      inventoryData: invItem
                    });
                    me.model.get('storeLocationsCache').addLocation(handled.data);

                    if (i==invItemsLength-1){
                      //We're in the midst of asynchrony, but we want this dialog
                      //to go ahead and open right away if we're at the end of the
                      //for loop.
                      var $bodyElement = $('#mz-location-selector').find('.modal-body');
                      me.pickerDialog.setBody(me.makeLocationPickerBody(listOfLocations, inv.items, cartItemId));
                      $bodyElement.attr('mz-cart-item', cartItemId);
                      me.pickerDialog.show();
                    }
                  },
                function(error){
                  //NGCOM-337
                  //If the item had inventory information for a location that
                  //doesn't exist anymore or was disabled, we end up here.
                  //The only reason we would need to take any action here is if
                  //the errored location happened to be at the end of the list,
                  //and the above if statement gets skipped -
                  //We need to make sure the dialog gets opened anyways.
                  if (i==invItemsLength-1){
                    var $bodyElement = $('#mz-location-selector').find('.modal-body');
                    me.pickerDialog.setBody(me.makeLocationPickerBody(listOfLocations, inv.items, cartItemId));
                    $bodyElement.attr('mz-cart-item', cartItemId);
                    me.pickerDialog.show();
                  }

                });
                });
              }
              });


          } else {
            //This is information we've retrieved once since page load!
            //So we're skipping the API calls.
            var inventoryItems = [];
            this.fulfillmentInfoCache[index].locations.forEach(function(location){
              listOfLocations.push(location.locationData);
              inventoryItems.push(location.inventoryData);
            });
            var $bodyElement = $('#mz-location-selector').find('.modal-body');
            me.pickerDialog.setBody(me.makeLocationPickerBody(listOfLocations, inventoryItems, cartItemId));
            me.pickerDialog.show();
          }

        },
        getInventoryData: function(id, productCode){
          //Gets basic inventory data based on product code.
          return window.cartView.cartView.model.get('items').get(id).get('product').apiGetInventory({
            productCode: productCode
          });
        },
        handleInventoryData: function(invItem){
          //Uses limited inventory location from product to get inventory names.
            return api.get('location', invItem.locationCode);
        },
        changeFulfillmentMethod: function(e){
          //Called when a radio button is clicked.

          var me = this;
          var $radioButton = $(e.currentTarget),
              cartItemId = $radioButton.data('mz-cart-item'),
              value = $radioButton.val(),
              cartItem = this.model.get("items").get(cartItemId);

              if (cartItem.get('fulfillmentMethod')==value){
                //The user clicked the radio button for the fulfillment type that
                //was already selected so we can just quit.
                return 0;
              }

              if (value=="Ship"){
                var oldFulfillmentMethod = cartItem.get('fulfillmentMethod');
                var oldPickupLocation = cartItem.get('fulfillmentLocationName');
                var oldLocationCode = cartItem.get('fulfillmentLocationCode');

                cartItem.set('fulfillmentMethod', value);
                cartItem.set('fulfillmentLocationName', '');
                cartItem.set('fulfillmentLocationCode', '');

                cartItem.apiUpdate().then(function(success){}, function(error){
                  cartItem.set('fulfillmentMethod', oldFulfillmentMethod);
                  cartItem.set('fulfillmentLocationName', oldPickupLocation);
                  cartItem.set('fulfillmentLocationCode', oldLocationCode);

                });


              } else if (value=="Pickup"){
                  //first we get the correct product code for this item.
                  //If the product is a variation, we want to pass that when searching for inventory.
                  var productCode = cartItem.apiModel.data.product.variationProductCode || cartItem.apiModel.data.product.productCode;
                  //pickStore function makes api calls, then builds/launches modal dialog
                  this.pickStore(productCode, cartItemId);
              }

        },
        makeLocationPickerBody: function(locationList, locationInventoryInfo, cartItemId){
          /*
          Uses a list of locations to build HTML to stick into the
          location picker. cartItemId is added as an attribute to each select
          button so that it can be used to assign the new pickup location to the
          right cart item.

          locationList should be a list of fulfillment locations with complete
          location data (what we need is the name). locationInventoryInfo will
          contain stock levels for the current product(cartItemId) by location code.

          */

          var me = this;

          var body = "";
          locationList.forEach(function(location){
            //We find the inventory data that matches the location we're focusing on.
            var matchedInventory = locationInventoryInfo.filter(function(locationInventory){
              return locationInventory.locationCode == location.data.code;
            });
            //matchedInventory should be a list of one item.

            var stockLevel = matchedInventory[0].stockAvailable;
            var allowsBackorder = location.data.allowFulfillmentWithNoStock;

            //Piece together UI for a single location listing
            var locationSelectDiv = $('<div>', { "class": "location-select-option", "style": "display:flex", "data-mz-cart-item":cartItemId });
            var leftSideDiv = $('<div>', {"style": "flex:1"});
            var rightSideDiv = $('<div>', {"style": "flex:1"});
            leftSideDiv.append('<h4 style="margin: 6.25px 0 6.25px">'+location.data.name+'</h4>');
            //If there is enough stock or the store allows backorder,
            //we'll let the user click the select button.
            //Even if these two conditions are met, the user could still be
            //halted upon trying to proceed to checkout if
            //the product isn't configured to allow for backorder.

          var address = location.data.address;

          leftSideDiv.append($('<div>'+address.address1+'</div>'));
          if(address.address2){leftSideDiv.append($('<div>'+address.address2+'</div>'));}
          if(address.address3){leftSideDiv.append($('<div>'+address.address3+'</div>'));}
          if(address.address4){leftSideDiv.append($('<div>'+address.address4+'</div>'));}
          leftSideDiv.append($('<div>'+address.cityOrTown+', '+address.stateOrProvince+' '+address.postalOrZipCode+'</div>'));
            var $selectButton;
            if (stockLevel>0 || allowsBackorder){
                leftSideDiv.append("<p class='mz-locationselect-available'>"+Hypr.getLabel("availableNow")+"</p>");
                var buttonData = {
                  locationCode: location.data.code,
                  locationName: location.data.name,
                  cartItemId: cartItemId
                };

                $selectButton = $("<button>", {"type": "button", "class": "mz-button mz-store-select-button", "style": "margin:25% 0 0 25%", "aria-hidden": "true", "mz-store-select-data": JSON.stringify(buttonData) });
                $selectButton.text(Hypr.getLabel("selectStore"));
                rightSideDiv.append($selectButton);


              } else {
                leftSideDiv.append("<p class='mz-locationselect-unavailable'>"+Hypr.getLabel("outOfStock")+"</p>");
                $selectButton = $("<button>", {"type": "button", "class": "mz-button is-disabled mz-store-select-button", "aria-hidden": "true", "disabled":"disabled", "style": "margin:25% 0 0 25%"});
                $selectButton.text(Hypr.getLabel("selectStore"));
                rightSideDiv.append($selectButton);
              }

              locationSelectDiv.append(leftSideDiv);
              locationSelectDiv.append(rightSideDiv);
              body+=locationSelectDiv.prop('outerHTML');

          });

          return body;

        },
        assignPickupLocation: function(jsonStoreSelectData){
          //called by Select Store button from store picker dialog.
          //Makes the actual change to the item using data held by the button
          //in the store picker.
          var me = this;
          this.pickerDialog.hide();

          var storeSelectData = JSON.parse(jsonStoreSelectData);
          var cartItem = this.model.get("items").get(storeSelectData.cartItemId);
          //in case there is an error with the api call, we want to get all of the
          //current data for the cartItem before we change it so that we can
          //change it back if we need to.
          var oldFulfillmentMethod = cartItem.get('fulfillmentMethod');
          var oldPickupLocation = cartItem.get('fulfillmentLocationName');
          var oldLocationCode = cartItem.get('fulfillmentLocationCode');

          cartItem.set('fulfillmentMethod', 'Pickup');
          cartItem.set('fulfillmentLocationName', storeSelectData.locationName);
          cartItem.set('fulfillmentLocationCode', storeSelectData.locationCode);
          cartItem.apiUpdate().then(function(success){}, function(error){
            cartItem.set('fulfillmentMethod', oldFulfillmentMethod);
            cartItem.set('fulfillmentLocationName', oldPickupLocation);
            cartItem.set('fulfillmentLocationCode', oldLocationCode);
            me.render();
          });


        },
        proceedToCheckout: function () {
            //commenting  for ssl for now...
            //this.model.toOrder();
            // return false;
            this.model.isLoading(true);
            // the rest is done through a regular HTTP POST
        },
        addCoupon: function () {
            var self = this;
            this.model.addCoupon().ensure(function () {
                self.model.unset('couponCode');
                self.appliedCouponCodes();
                self.render();
            });
        }, 
        appliedCouponCodes: function() {
          var cartItems = this.model.get('items').models; 
          //To make unique applied coupon codes array
          _.each(cartItems, function(item){
            var productDiscount = item.get('productDiscount');
            if(productDiscount && !productDiscount.excluded && productDiscount.couponCode) {
              appliedCouponCodes.push(productDiscount.couponCode);
            } 
          });
          appliedCouponCodes = _.uniq(appliedCouponCodes);
          this.model.set('appliedCouponCodes', appliedCouponCodes);
        },
        removeCoupon: function(e) {
          var couponCode = $(e.currentTarget).attr('data-mz-value');
          var self = this;
          this.model.removeCoupon(couponCode).ensure(function() {
            appliedCouponCodes = _.without(appliedCouponCodes, couponCode);
            self.model.set('appliedCouponCodes', appliedCouponCodes);
            self.render();
          });
        },
        onEnterCouponCode: function (model, code) {
            if (code && !this.codeEntered) {
                this.codeEntered = true;
                this.$el.find('#cart-coupon-code').prop('disabled', false);
            }
            if (!code && this.codeEntered) {
                this.codeEntered = false;
                this.$el.find('#cart-coupon-code').prop('disabled', true);
            }
        },
        autoUpdate: [
            'couponCode'
        ],
        handleEnterKey: function () {
            this.addCoupon();
        },
        guestCheckoutLogin: function(){
          var locale = require.mozuData('apicontext').headers['x-vol-locale'];
          locale = locale.split('-')[0];
          var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
          var currentLocale = '';
          if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite){
          	currentLocale = locale === 'fr' ? '/fr' : '/en';
          }
          window.location.href = currentLocale + "/user/login?returnurl=/cart/checkout";             
          return false;
        },
        setPreferredStore: function(){
          var self = this;
          self.model.set({"preferredStore":$.parseJSON($.cookie("preferredStore"))});
        },
        changePreferredStore: function(e){
            e.preventDefault();
            var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            locale = locale.split('-')[0];
            var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
            var currentLocale = '';
            if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite){
            	currentLocale = locale === 'fr' ? '/fr' : '/en';
            }
            window.location.href=currentLocale + "/store-locator?returnUrl="+window.location.pathname;
        },
        callStore: function(e){
            var screenWidth = window.matchMedia("(max-width: 767px)");
            if(!screenWidth.matches){
                e.preventDefault();
            }
        }
    });

  function renderVisaCheckout(model) {

    var visaCheckoutSettings = HyprLiveContext.locals.siteContext.checkoutSettings.visaCheckout;
    var apiKey = visaCheckoutSettings.apiKey;
    var clientId = visaCheckoutSettings.clientId;

    //In case for some reason a model is not passed
    if(!model) {
      model = CartModels.Cart.fromCurrent();
    }

    function initVisa(){
      var delay = 200;
      if(window.V) {
          window.V.init({
            apikey: apiKey,
            clientId: clientId,
            paymentRequest: {
                currencyCode: model ? model.get('currencyCode') : 'USD',
                subtotal: "" + model.get('subtotal')
            }});
          return;
        }
        _.delay(initVisa, delay);
    }

    initVisa();

  }
    /* begin visa checkout */
    function initVisaCheckout () {
      if (!window.V) {
          //console.warn( 'visa checkout has not been initilized properly');
          return false;
      }

      // on success, attach the encoded payment data to the window
      // then turn the cart into an order and advance to checkout
      window.V.on("payment.success", function(payment) {
          // payment here is an object, not a string. we'll stringify it later
          var $form = $('#cartform');

          _.each({

              digitalWalletData: JSON.stringify(payment),
              digitalWalletType: "VisaCheckout"

          }, function(value, key) {

              $form.append($('<input />', {
                  type: 'hidden',
                  name: key,
                  value: value
              }));

          });

          $form.submit();

      });
    }
    /* end visa checkout */

   
    $(document).ready(function() {
        var cartModel = CartModels.Cart.fromCurrent(),
            cartViews = {

                cartView: new CartView({
                    el: $('#cart'),
                    model: cartModel,
                    messagesEl: $('[data-mz-message-bar]')
                })

            };

        cartModel.on('ordercreated', function (order) {
            cartModel.isLoading(true);
            window.location = (HyprLiveContext.locals.siteContext.siteSubdirectory||'') + '/checkout/' + order.prop('id');
        });

        cartModel.on('sync', function() {
            CartMonitor.setCount(cartModel.get('count'));
        });

        window.cartView = cartViews;

        CartMonitor.setCount(cartModel.get('count'));
        cartViews.cartView.setPreferredStore();
        _.invoke(cartViews, 'render');

        renderVisaCheckout(cartModel);
        paypal.loadScript();
    });
});
