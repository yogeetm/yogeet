require(["modules/jquery-mozu", 
	"underscore", 
	"hyprlive", 
	"modules/backbone-mozu", 
	'modules/api',
	'hyprlivecontext'], 
	function ($, _, Hypr, Backbone, api, HyprLiveContext) {
	
	$(document).ready(function() {	
		var clearFields = function(){
			$("#name, #emailAddress, #subject, #number1, #number2, #number3, #message").val('');
			$("#reason").val('0');
		};
		
		var emailReg = Backbone.Validation.patterns.email;
		$(".errors").remove();
		
		$('#contact-button').on('click',function(){
			var errors = false;
			if (!$("#name").val() || $("#name").val() === ""){
				$("#name").addClass('is-invalid');
				$('[data-mz-validationmessage-for="name"]').text(Hypr.getLabel('nameMissing'));
				errors = true;
			}else{
				$("#name").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="name"]').text('');
			}
			
			if($("#emailAddress").val() === ""){
				$("#emailAddress").addClass('is-invalid');
				$('[data-mz-validationmessage-for="emailAddress"]').text(Hypr.getLabel('emailMissing'));
				errors = true;                            
			}else if(!emailReg.test($("#emailAddress").val())){
				$("#emailAddress").addClass('is-invalid');
				$('[data-mz-validationmessage-for="emailAddress"]').text(Hypr.getLabel('emailMissing'));
				errors = true;
			}else{
				 $("#emailAddress").removeClass('is-invalid');
				 $('[data-mz-validationmessage-for="emailAddress"]').text('');
			}
			
			if(!$("#subject").val() || $("#subject").val() === ""){
				$("#subject").addClass('is-invalid');
				$('[data-mz-validationmessage-for="subject"]').text(Hypr.getLabel('subjectMissing'));
				errors = true;
			}else{
				$("#subject").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="subject"]').text('');
			}
			
			var reason = $('#reason option:selected').val();
			if(!reason || reason === ""){
				$("#reason").addClass('is-invalid');
				$('[data-mz-validationmessage-for="reason"]').text(Hypr.getLabel('subjectMissing'));
				errors = true;
			}else{
				$("#reason").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="reason"]').text('');
			}
			
			if(!$("#message").val() || $("#message").val() === ""){
				$("#message").addClass('is-invalid');
				$('[data-mz-validationmessage-for="message"]').text(Hypr.getLabel('messageMissing'));
				errors = true;
			}else{
				$("#message").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="message"]').text('');
			}
			if(!$('#g-recaptcha-response').val()){
				$('[data-mz-validationmessage-for="captcha"]').text(Hypr.getLabel('missingCaptcha'));
				errors = true;
			}else{
				$('[data-mz-validationmessage-for="captcha"]').text('');
			}
			 digitLength();
			 
			 if(!errors) {
				 $('.contact-us-form').addClass('is-loading');
				 var data = {
					 name:$("#name").val(),
					 email:$("#emailAddress").val(),
					 subject: $("#subject").val(),
					 phoneNumber: $("#phoneNumber").val(),
					 reason: $('#reason option:selected').val(),
					 customerMessage: $("#message").val()
				 };
				 $.ajax({
					method: 'POST',
            		contentType: 'application/json; charset=utf-8',
            		url: Hypr.getThemeSetting('formPostingUrl') + 'customerService',
            		data: JSON.stringify(data),     
            		success:function(response){
            			$('.sucess-email').show();
     	        	   	$('.contact-us-form').removeClass('is-loading');
     	        	   	clearFields();
        			},
        			error: function (error) {
        				$('.error-email').show();
        			}
				 });
			 }
			
		});
		
		  $(".digit-length").keypress(function(){
			  digitLength();
		  });
		  
		  
		  var digitLength = function(){ 
			//starts phonenumber 3 and 4 fields value
		        var inputQuantity = [];
		        $(function() {
		          $(".digit-length").each(function(i) {
		            inputQuantity[i]=this.defaultValue;
		             $(this).data("idx",i);
		          });
		          $(".digit-length").on("keyup", function (e) {
		            var $field = $(this),
		                val=this.value,
		                $thisIndex=parseInt($field.data("idx"),10); 
		            if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid") ) {
		                this.value = inputQuantity[$thisIndex];
		                return;
		            } 
		            if (val.length > Number($field.attr("maxlength"))) {
		              val=val.slice(0, 5);
		              $field.val(val);
		            }
		            inputQuantity[$thisIndex]=val;
		            if($(this).val().length==$(this).attr("maxlength")){
		                $(this).next().focus();
		            }
		            var num1 = $('#number1').val();
		            var num2 = $('#number2').val();
		            var num3 = $('#number3').val();
		            var number = num1 + num2 + num3;
		            $('#phoneNumber').val(number);
		            
		          });      
		        });
			
		    };
		  
	});
});