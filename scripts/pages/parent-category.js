require([
    'modules/jquery-mozu',
    'hyprlive',
    'underscore',
    'modules/backbone-mozu',
    'modules/api',
    'modules/models-product'
],
function($, Hypr, _, Backbone, api, ProductModels) {
	
	var catID = $('[data-mz-category]').data('mzCategory');
	var catName = $('[data-mz-category-name]').data('mzCategoryName');
	var categoryCount = Hypr.getThemeSetting('categoryCount');
	
	var CategoryListModel = Backbone.MozuModel.extend({
		mozuType: 'categories'
	});
	
	var CategoryListView = Backbone.MozuView.extend({
		templateName: 'modules/common/category-listing',

		render : function() {
			Backbone.MozuView.prototype.render.apply(this);			
			this.appendFilterValue();
		}
	});	
	
	var renderCategoryData = function(facetValues, catName, selectedFitment) {
		var category = _.findWhere(facetValues, { value: catID.toString() });
		var firstCategoryListModel, secondCategoryListModel, firstFilter, secondFilter = null;
		if(category && category.childrenFacetValues.length > 0) {			
			var childCategories = category.childrenFacetValues;
			if( childCategories.length > 65 ) {
				var firstSubset = childCategories.slice(0,65);
		        var secondSubset = childCategories.splice(65, 130);
		        
		        firstFilter = 'categoryId eq ' + firstSubset.map(function(childFacet) {
					return childFacet.value;
				}).join(' or categoryId eq ');
				
				secondFilter = 'categoryId eq ' + secondSubset.map(function(childFacet) {
					return childFacet.value;
				}).join(' or categoryId eq ');
				
				firstCategoryListModel = new CategoryListModel();
				secondCategoryListModel = new CategoryListModel();

				firstCategoryListModel.set('filter', firstFilter);
				firstCategoryListModel.set('pageSize', categoryCount);
				
				secondCategoryListModel.set('filter', secondFilter);
				secondCategoryListModel.set('pageSize', categoryCount);
			} else {
				firstFilter = 'categoryId eq ' + childCategories.map(function(childFacet) {
					return childFacet.value;
				}).join(' or categoryId eq ');
				
				firstCategoryListModel = new CategoryListModel();

				firstCategoryListModel.set('filter', firstFilter);
				firstCategoryListModel.set('pageSize', categoryCount);
			}
			
			
			firstCategoryListModel.fetch().then(function(response) {
				var firstSortedCategoryObject = null;
				
				if(secondCategoryListModel) {
					firstSortedCategoryObject = response.get('items');
					secondCategoryListModel.fetch().then(function(secondResponse) {
						var secondSortedCategoryObject = secondResponse.get('items');
						var categoryCollection = new Backbone.Collection();
						
						categoryCollection.add(firstSortedCategoryObject);
						categoryCollection.add(secondSortedCategoryObject);
						
						categoryCollection = _.sortBy(categoryCollection.models, function(category){
							if(category && category.get('content')) {
								return category.get('content').name;
							}
						});
						
						var categoryListView = new CategoryListView({
							el: $('#category-container'),
							model: new Backbone.Collection(categoryCollection)
						});
						categoryListView.render();
					});
				} else {
					firstSortedCategoryObject = _.sortBy(response.get('items'), function(item){return item.content.name;});
					var categoryListView = new CategoryListView({
						el: $('#category-container'),
						model: new Backbone.Collection(firstSortedCategoryObject)
					});
					categoryListView.render();
				} 
			});
		}
		else {
			var categoryModel = new Backbone.Model();
			categoryModel.set('modelName', catName);
			
			var categoryListView = new CategoryListView({
				el: $('#category-container'),
				model: categoryModel
			});
			categoryListView.render();
		}
	};
});