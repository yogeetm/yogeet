define(['modules/api',
        'modules/backbone-mozu',
        'underscore',
        'modules/jquery-mozu',
        'modules/confirmation-models-orders',
        'hyprlivecontext',
        'hyprlive',
        'modules/preserve-element-through-render'],
        function (api, Backbone, _, $, OrderModels, HyprLiveContext, Hypr, preserveElement) {
          /*
          Our Order Confirmation page doesn't involve too much logic, but our
          order model doesn't include enough details about pickup locations.
          We are running an api call with the fulfillmentLocationCodes of the
          items in the order. The model on the confirmation page will then
          include an array of locationDetails.
          */
          var apiContext = require.mozuData('apicontext');
          var ConfirmationView = Backbone.MozuView.extend({
            templateName: 'modules/confirmation/confirmation-detail',
            additionalEvents: {
        		"click #confirmation-order-link" : 'redirectMyaccount'
    	    },
    	    redirectMyaccount:function(){
    	        window.location = "/myaccount?click=true";
    	    },
            initialize: function() {
            	//calculate the quantity of total items to show on order confirmation
            	var items = this.model.get('items').toJSON();
            	var firstItemQuantity = 0;
            	_.each(items, function(item, index) {
            		if(item.product.productCode != 'EHF101') {
            			firstItemQuantity += item.quantity;
            		}
            	});
            	this.model.set('totalItemsQuantity', firstItemQuantity);
            	var EHFTotal = 0;
            	_.each(items, function(item) {
                	if(item.handlingAmount) {
                		EHFTotal += item.handlingAmount;
                	}
                });
                this.model.set('EHFTotal', EHFTotal);
                var contextSiteId = apiContext.headers["x-vol-site"];
                var frSiteId = Hypr.getThemeSetting("frSiteId");
                if(frSiteId === contextSiteId){
                    this.model.set("isFrenchSite", true);   
                }
            },
            render: function() {
              Backbone.MozuView.prototype.render.apply(this);
            },
            callStore: function(e){
                var screenWidth = window.matchMedia("(max-width: 767px)");
                if(!screenWidth.matches){
                    e.preventDefault();
                }
            } 
          });

          var ConfirmationModel = OrderModels.Order.extend({
          getLocationData: function(){
            var codes = [];
            var items = this.get('items');

            items.forEach(function(item){
              if (codes.indexOf(item.get('fulfillmentLocationCode'))==-1)
              codes.push(item.get('fulfillmentLocationCode'));
            });

            var queryString = "";
            codes.forEach(function(code, index){
              if (index != codes.length-1){
                queryString += "code eq "+code+" or ";
              } else {
                queryString += "code eq "+code;
              }
            });
            return api.get('locations', {filter: queryString});
          }
        });

          $(document).ready(function(){
            var confModel = ConfirmationModel.fromCurrent();
            confModel.getLocationData().then(function(response){
              confModel.set('locationDetails', response.data.items[0]);

              var confirmationView = new ConfirmationView({
                  el: $('#confirmation-container'),
                  model: confModel
              });
              confirmationView.render();
              });

            });
        });
