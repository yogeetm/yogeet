define(['modules/jquery-mozu',
    'modules/backbone-mozu',
    'underscore'
],
function($,backbone,_){

    var dtaobj = backbone.MozuModel.extend({
        defaults: {
    
            Listeners: 0
        }
    });

var demoWorker = backbone.MozuView.extend({

    tagName: "span",
    className: "trial",

    initialize: function () {

        this.model.on("change", this.pushNotification, this);

    },

    pushNotification: function () {


        this.$el.css({"color": "red"});
        this.$el.html("Listeners: " + this.model.get("Listeners") + "   ");
    },

render:function(){

    var template = _.template($('.name').html());
    var html = template(this.model.toJSON());
    this.$el.html(html);
    return this;

}

});

dtaobj = new dtaobj({name:" IT worked WELL!!!"});
demoWorker = new demoWorker({el:".name",model:dtaobj});
demoWorker.render();
demoWorker.pushNotification();


}


);