Ext.widget({
	xtype: 'mz-form-widget',
	itemId: 'footerForm',
    items: [
			{
				xtype: 'container',
				id: 'mainConatiner',
				items:[
						{
							xtype: 'panel',
							layout: 'hbox',
							items: [
							        {
    						        	xtype: 'mz-input-text',
    					    			fieldLabel: 'Default Video Link',
    					    			name: 'DefaultVideoLink',
    					    			itemId: 'DefaultVideoLinkId',
    					    			flex: 2,
    					    			style: {
    					    				marginLeft: '15px'
    					    			},
    					    			allowBlank: false
    						        },
    						        {
    						        	xtype: 'mz-input-text',
    					    			fieldLabel: "Default Video's Poster Link",
    					    			name: 'DefaultVideoPosterLinks',
    					    			itemId: 'DefaultPosterFieldId',
    					    			flex: 2,
    					    			style: {
    					    				marginLeft: '10px'
    					    			},
    					    			allowBlank: false
    						        }
    						        
						        ]
						},
						{
							xtype: 'panel',
							layout: 'hbox',
							items: [

									{
										xtype: 'mz-input-textarea',
										fieldLabel: 'Default Video Description',
										name: 'DefaultVideoDescriptions',
										itemId: 'DefaultVideoDescriptionId',
										style: {
											marginLeft: '10px'
										},
										flex: 2
									},
							        {
										xtype: "mz-input-checkbox",
										name: "autoplay",
										id:"autoplay",
										fieldLabel: "Autoplay",
										style: {
											marginLeft: '10px',
											marginTop: '25px'
										},
										flex: 2,
									},
									{
										xtype: "mz-input-checkbox",
										name: "repeat",
										id:"repeat",
										fieldLabel: "Repeat video",
										style: {
											marginLeft: '10px',
											marginTop: '25px' 
										},
										flex: 2,
									}
							]
						},
				       {
		            	xtype: 'form',
		            	name: 'headerForm',
		            	id: 'headerForm',
		            	items: [
		                        {
		                            xtype: 'panel',
		                            border: false,
		                            layout: 'hbox',
		                            name: 'headerPanel',
		                            items: [
										{
											xtype: 'mz-input-text',
											fieldLabel: 'Popular Videos Section Heading',
											name: 'Header'
										},
		                                {
		                                	xtype: 'mz-input-number',
		                        			fieldLabel: 'Number of videos',
		                        			id: 'noOfVideos',
		                        			name: 'noOfVideos',
		                        			value: 1,
		                        			style: {
										        marginLeft: '10px'
										    }
		                                },
		                                {
		                                	xtype: 'button',
		                    			    text: 'Add',
		                    			    name: 'AddBtn',
		                    			    style: {
		                    			        marginTop: '30px',
		                    			        marginLeft: '10px'
		                    			    },
		                    			    listeners: {
		                    			    	click: function(e){
		                    			    		var noOfFields = Ext.getCmp('noOfVideos').getValue();
		                    			    		for(var i = 0; i < noOfFields; i++){
		                    			    			e.up('form').add(
		                    			    					{
			                    			    					xtype: 'container',
			                    			    					layout: 'hbox',
		                    			    						items: [
																			{
																				xtype: 'mz-input-text',
																				fieldLabel: 'Video Title',
																				name: 'VideoTitles',
																				itemId: 'VideoTitlesId',
																				flex: 2,
																				allowBlank: false
																			},
		                    			    						        {
		                    			    						        	xtype: 'mz-input-text',
		                    			    					    			fieldLabel: 'Video Link',
		                    			    					    			name: 'VideoLinks',
		                    			    					    			itemId: 'VideoFieldId',
		                    			    					    			flex: 2,
		                    			    					    			style: {
		                    			    					    				marginLeft: '10px'
		                    			    					    			},
		                    			    					    			allowBlank: false
		                    			    						        },
		                    			    						        {
		                    			    						        	xtype: 'mz-input-text',
		                    			    					    			fieldLabel: "Video's Poster Link",
		                    			    					    			name: 'PosterLinks',
		                    			    					    			itemId: 'PosterFieldId',
		                    			    					    			flex: 2,
		                    			    					    			style: {
		                    			    					    				marginLeft: '10px'
		                    			    					    			},
		                    			    					    			allowBlank: false
		                    			    						        },
		                    			    						        {
		                    			    						        	xtype: 'mz-input-textarea',
		                    			    					    			fieldLabel: 'Video Description',
		                    			    					    			name: 'VideoDescriptions',
		                    			    					    			itemId: 'VideoDescriptionId',
		                    			    					    			style: {
		                    			    					    				marginLeft: '10px'
		                    			    					    			},
		                    			    					    			flex: 2
		                    			    						        },
		                    			    						        {
		                    			    						        	xtype: 'button',
		                    			                        			    text: 'Delete',
		                    			                        			    name: 'DeleteBtn',
		                    			                        			    style: {
		                    			                        			        marginTop: '30px',
		                    			                        			        marginLeft: '10px'
		                    			                        			    },
		                    			                        			    listeners: {
		                    			                        			    	click: function(e){
		                    			                        			    		e.up('container').destroy();
		                    			                        			    	}
		                    			                        			    }
		                    			    						        }
		            			    						        ]
		            			    							}
		            			    					);
		                    			    		}
		                    			    	}
		                    			    }
		                                }
		                            ]
		                        }
		                    ]
				       }
				   ]
				}
			], //footerForm items end
			getData: function() {
		    	var videoData = this.getValues(), videoDesc = [];
		    	if(videoData.VideoDescriptions && !Ext.isArray(videoData.VideoDescriptions)){
		    		videoDesc = videoData.VideoDescriptions;
		    	}
		    	if(videoData.VideoLinks) {
		    		if(Ext.isArray(videoData.VideoLinks)){
			    		var out = videoData.VideoLinks.map(function(item, index){
			        		return { title: videoData.VideoTitles[index], videoLink: item, posterLink: videoData.PosterLinks[index], videoDescription: videoData.VideoDescriptions[index] ? videoData.VideoDescriptions[index] : '' };
			        	});
			    		var finalArray = [];
			    		while(out.length > 0) {
			    			var subArray = out.splice(0,3);
			    			finalArray.push(subArray);
			    		}
			    		videoData.videoValues = finalArray;
			    	}else{
			    		videoData.videoValues = [];
			    		var tempData = [];
			    		tempData.push({ title: videoData.VideoTitles, videoLink: videoData.VideoLinks, posterLink : videoData.PosterLinks, videoDescription: videoData.VideoDescriptions[0] });
			    		videoData.videoValues.push(tempData);
			    	}
			    	
			    	delete videoData.VideoLinks;
			    	delete videoData.VideoDescriptions;
			    	delete videoData.autoPlay;
			    	delete videoData.PosterLinks;
			    	delete videoData.VideoTitles;
		    	}
		    	
		    	return Ext.applyIf(videoData, this.data);
		    },
		    setData: function (data) {
		        this.getForm().setValues(data);

		        if (!Ext.Object.isEmpty(data) && data.videoValues) {
		            for (var i = 0; i < data.videoValues.length; i++) {
		            	for (var j = 0; j < data.videoValues[i].length; j++) {
		                Ext.getCmp('headerForm').add({
		                    xtype: 'container',
		                    layout: 'hbox',
		                    items: [
								{
									xtype: 'mz-input-text',
									fieldLabel: 'Video Title',
									name: 'VideoTitles',
									itemId: 'VideoTitlesId',
									flex: 2,
									value: data.videoValues[i][j].title,
									allowBlank: false
								},
		                        {
		                        	xtype: 'mz-input-text',
					    			fieldLabel: 'Video Link',
					    			name: 'VideoLinks',
					    			itemId: 'VideoFieldId',
					    			flex: 2,
		                            value: data.videoValues[i][j].videoLink,
		                            style: {
		                                marginLeft: '20px'
		                            },
		                            allowBlank: false
						        },
						        {
						        	xtype: 'mz-input-text',
					    			fieldLabel: "Video's Poster Link",
					    			name: 'PosterLinks',
					    			itemId: 'PosterFieldId',
					    			value: data.videoValues[i][j].posterLink,
					    			flex: 2,
					    			allowBlank: false
						        },
		                        {
						        	xtype: 'mz-input-textarea',
					    			fieldLabel: 'Video Description',
					    			name: 'VideoDescriptions',
					    			itemId: 'VideoDescriptionId',
		                            value: data.videoValues[i][j].videoDescription,
		                            style: {
		                                marginLeft: '20px'
		                            },
		                            flex: 2
						        },
		                        {
		                            xtype: 'button',
		                            text: 'Delete',
		                            name: 'DeleteBtn',
		                            style: {
		                                marginTop: '30px',
		                                marginLeft: '10px'
		                            },
		                            flex: 1,
		                            listeners: {
		                                click: function (e) {
		                                    e.up('container').destroy();
		                                }
		                            }
						        }
					        ]
		                });
		            	}
		            }
		        }
		        this.data = data;
		    }
});