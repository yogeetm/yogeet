Ext.widget({
	xtype: 'mz-form-widget',
	itemId: 'footerForm',
    items: [
            {
				xtype: 'container',
				id: 'mainConatiner',
				items:[
				    {
				    	xtype: 'panel',
						layout: 'hbox',
						items: [
								{
									xtype: 'mz-input-text',
									fieldLabel: 'Header',
									name: 'Header'
								},
								{
									xtype: 'mz-input-text',
									fieldLabel: 'Custom Class Name',
									name: 'customClass',
									itemId: 'customClassId',
									style: {
								        marginLeft: '10px'
								    }
								},
								{
									xtype: 'mz-input-dropdown',
					    			fieldLabel: 'Select Alignment',
					    			name: 'Alignment',
					    			itemId: 'AlignmentId',
					    			flex: 2,
					    			store: [
					    			        'Vertical',
					    			        'Horizontal'
					    			        ],
					    			style: {
					    				marginLeft: '10px'
					    			}
								}
					        ]
				    }
		       ]
	        },
            {
            	xtype: 'form',
            	name: 'headerForm',
            	id: 'headerForm',
            	items: [
                        {
                            xtype: 'panel',
                            border: false,
                            layout: 'hbox',
                            name: 'headerPanel',
                            items: [
                                {
                                	xtype: 'mz-input-number',
                        			fieldLabel: 'Number of fields',
                        			id: 'noOfFields',
                        			name: 'NoOfFields',
                        			value: 1
                        			
                                },
                                {
                                	xtype: 'button',
                    			    text: 'Add',
                    			    name: 'AddBtn',
                    			    style: {
                    			        marginTop: '30px',
                    			        marginLeft: '10px'
                    			    },
                    			    listeners: {
                    			    	click: function(e){
                    			    		var noOfFields = Ext.getCmp('noOfFields').getValue();
                    			    		for(var i = 0; i < noOfFields; i++){
                    			    			e.up('form').add(
                    			    					{
	                    			    					xtype: 'container',
	                    			    					layout: 'hbox',
                    			    						items: [
                    			    						        {
                    			    						        	xtype: 'mz-input-text',
                    			    					    			fieldLabel: 'Sub Header',
                    			    					    			name: 'SubHeader',
                    			    					    			itemId: 'subheaderFields',
                    			    					    			flex: 2,
                    			    					    			allowBlank: false
                    			    						        },
                    			    						        {
                    			    						        	xtype: 'mz-input-text',
                    			    					    			fieldLabel: 'Sub Header Link',
                    			    					    			name: 'SubHeaderLinks',
                    			    					    			itemId: 'SubHeaderLinksId',
                    			    					    			style: {
                    			    					    				marginLeft: '10px'
                    			    					    			},
                    			    					    			flex: 2
                    			    						        },
                    			    						        {
                    			    						        	xtype: 'mz-input-dropdown',
                    			    					    			fieldLabel: 'Select Target',
                    			    					    			name: 'TargetLinks',
                    			    					    			itemId: 'TargetLinksId',
                    			    					    			flex: 2,
                    			    					    			store: [
                    			    					    			        '_self',
                    			    					    			        '_blank'
                    			    					    			        ],
                    			    					    			style: {
                    			    					    				marginLeft: '10px'
                    			    					    			}
                    			    						        },
                    			    						        {
                    			    						        	xtype: 'button',
                    			                        			    text: 'Delete',
                    			                        			    name: 'DeleteBtn',
                    			                        			    flex: 1,
                    			                        			    style: {
                    			                        			        marginTop: '30px',
                    			                        			        marginLeft: '10px'
                    			                        			    },
                    			                        			    listeners: {
                    			                        			    	click: function(e){
                    			                        			    		e.up('container').destroy();
                    			                        			    	}
                    			                        			    }
                    			    						        }
            			    						        ]
            			    							}
            			    					);
                    			    		}
                    			    	}
                    			    }
                                },
                                {
                                	xtype: 'mz-input-number',
                        			fieldLabel: "Active link's number",
                        			id: 'activeLinkNumber',
                        			name: 'activeLinkNumber',
                        			style: {
                        				marginLeft: '10px'
                        			}
                                }
                            ]
                        }
                    ]
            }
    ],
    getData: function() {
    	var footerData = this.getValues();
    	if(Ext.isArray(footerData.SubHeader)){
    		var out = footerData.SubHeader.map(function(item, index){
        		return { title: item, link: footerData.SubHeaderLinks[index], target: footerData.TargetLinks[index]};
        	});
    		footerData.navigationLinks = out;
    	}else{
    		footerData.navigationLinks = [];
    		footerData.navigationLinks.push({title: footerData.SubHeader, link: footerData.SubHeaderLinks, target: footerData.TargetLinks})
    	}
    	
    	delete footerData.SubHeader;
    	delete footerData.SubHeaderLinks;
    	delete footerData.TargetLinks;
    	
    	return Ext.applyIf(footerData, this.data);
    },
    setData: function(data) {
    	this.getForm().setValues(data);
    	if(!Ext.Object.isEmpty(data)){
    		for(var i = 0; i < data.navigationLinks.length; i++){
    			Ext.getCmp('headerForm').add(
    					{
	    					xtype: 'container',
	    					layout: 'hbox',
    						items: [
    						        {
    						        	xtype: 'mz-input-text',
    					    			fieldLabel: 'Sub Header',
    					    			name: 'SubHeader',
    					    			itemId: 'subheaderFields',
    					    			value: data.navigationLinks[i].title,
    					    			flex: 2,
    					    			allowBlank: false
    						        },
    						        {
    						        	xtype: 'mz-input-text',
    					    			fieldLabel: 'Sub Header Link',
    					    			name: 'SubHeaderLinks',
    					    			itemId: 'SubHeaderLinksId',
    					    			value: data.navigationLinks[i].link,
    					    			style: {
    					    				marginLeft: '10px'
    					    			},
    					    			flex: 2
    						        },
    						        {
    						        	xtype: 'mz-input-dropdown',
    					    			fieldLabel: 'Select Target',
    					    			name: 'TargetLinks',
    					    			itemId: 'TargetLinksId',
    					    			value: data.navigationLinks[i].target,
    					    			flex: 2,
    					    			store: [
    					    			        '_self',
    					    			        '_blank'
    					    			        ],
    					    			style: {
    					    				marginLeft: '10px'
    					    			},
    					    			
    						        },
    						        {
    						        	xtype: 'button',
                        			    text: 'Delete',
                        			    name: 'DeleteBtn',
                        			    flex: 1,
                        			    style: {
                        			        marginTop: '30px',
                        			        marginLeft: '10px'
                        			    },
                        			    listeners: {
                        			    	click: function(e){
                        			    		e.up('container').destroy();
                        			    	}
                        			    }
    						        }
					        ]
						}
					);
    		}
    		
    	}
    	this.data = data;
    }
});

