Ext.widget({
	xtype: 'mz-form-widget',
	itemId: 'linkSublinkform',
    items: [
            {
				xtype: 'container',
				id: 'linkSublinkContainer',
				items:[
				    {
				    	xtype: 'panel',
						layout: 'hbox',
						items: [
								{
									xtype: 'mz-input-text',
									fieldLabel: 'Header',
									name: 'Header'
								},
					        ]
				    }
		       ]
	        },
            {
            	xtype: 'form',
            	name: 'headerForm',
            	items: [
                        {
                            xtype: 'panel',
                            border: false,
                            layout: 'hbox',
                            name: 'headerPanel',
                            items: [
                                {
                                	xtype: 'mz-input-number',
                        			fieldLabel: 'Number of fields',
                        			id: 'noOfFields',
                        			name: 'NoOfFields',
                        			value: 1
                        			
                                },
                                {
                                	xtype: 'button',
                    			    text: 'Add',
                    			    name: 'AddBtn',
                    			    style: {
                    			        marginTop: '30px',
                    			        marginLeft: '10px'
                    			    },
                    			    listeners: {
                    			    	click: function(e){
                    			    		var noOfFields = Ext.getCmp('noOfFields').getValue();
                    			    		for(var i = 0; i < noOfFields; i++){
                    			    			e.up('form').add(
                    			    					{
	                    			    					xtype: 'container',
	                    			    					layout: 'hbox',
                    			    						items: [
                    			    						        {
                    			    						        	xtype: 'mz-input-text',
                    			    					    			fieldLabel: 'Sublink link',
                    			    					    			name: 'sublinkLink',
                    			    					    			itemId: 'sublinkLink',
                    			    					    			flex: 2,
                    			    					    			allowBlank: false
                    			    						        },
                    			    						        {
                    			    						        	xtype: 'mz-input-text',
                    			    					    			fieldLabel: 'Sublink URL',
                    			    					    			name: 'sublinkUrl',
                    			    					    			itemId: 'sublinkUrl',
                    			    					    			style: {
                    			                        			        marginLeft: '10px'
                    			                        			    },
                    			    					    			flex: 2,
                    			    					    			allowBlank: false
                    			    						        },
                    			    						        {
                    			    						        	xtype: 'mz-input-text',
                    			    					    			fieldLabel: 'Sublink subTitle',
                    			    					    			name: 'sublinkSubTitle',
                    			    					    			itemId: 'sublinkSubTitle',
                    			    					    			style: {
                    			                        			        marginLeft: '10px'
                    			                        			    },
                    			    					    			flex: 2,
                    			    					    			allowBlank: false
                    			    						        },
                    			    						        {
                    			    						        	xtype: 'mz-input-textarea',
                    			    					    			fieldLabel: 'Sublink description',
                    			    					    			name: 'sublinkDescription',
                    			    					    			itemId: 'sublinkDescription',
                    			    					    			style: {
                    			    					    				marginLeft: '10px',
                    			    					    				marginBottom: '30px'
                    			    					    			},
                    			    					    			flex: 2
                    			    						        },
                    			    						        {
                    			    						        	xtype: 'button',
                    			                        			    text: 'Delete',
                    			                        			    name: 'DeleteBtn',
                    			                        			    flex: 1,
                    			                        			    style: {
                    			                        			        marginTop: '30px',
                    			                        			        marginLeft: '10px'
                    			                        			    },
                    			                        			    listeners: {
                    			                        			    	click: function(e){
                    			                        			    		e.up('container').destroy();
                    			                        			    	}
                    			                        			    }
                    			    						        }
            			    						        ]
            			    							}
            			    					);
                    			    		}
                    			    	}
                    			    }
                                }
                            ]
                        }
                    ]
            }
    ],
    getData: function() {
    	var linkSubLinksData = this.getValues();
    	if(Ext.isArray(linkSubLinksData.sublinkLink)){
    		var out = linkSubLinksData.sublinkLink.map(function(item, index){
        		return { title: item, url: linkSubLinksData.sublinkUrl[index], subtitle: linkSubLinksData.sublinkSubTitle[index], description: linkSubLinksData.sublinkDescription[index]};
        	});
    		linkSubLinksData.linksAndSubLinks = out;
    	}else{
    		linkSubLinksData.linksAndSubLinks = [];
    		linkSubLinksData.linksAndSubLinks.push({title: linkSubLinksData.sublinkLink, url: linkSubLinksData.sublinkUrl, subtitle: linkSubLinksData.sublinkSubTitle,  description: linkSubLinksData.sublinkDescription})
    	}
    	
    	delete linkSubLinksData.sublinkLink;
    	delete linkSubLinksData.sublinkUrl;
    	delete linkSubLinksData.sublinkSubTitle;
    	delete linkSubLinksData.sublinkDescription;
    	
    	return Ext.applyIf(linkSubLinksData, this.data);
    },
    setData: function(data) {
    	this.getForm().setValues(data);
    	if(!Ext.Object.isEmpty(data)){
    		for(var i = 0; i < data.linksAndSubLinks.length; i++){
    			Ext.getCmp('linkSublinkContainer').add(
    					{
	    					xtype: 'container',
	    					layout: 'hbox',
    						items: [
    						        {
    						        	xtype: 'mz-input-text',
    					    			fieldLabel: 'Sublink link',
    					    			name: 'sublinkLink',
    					    			itemId: 'sublinkLink',
    					    			value: data.linksAndSubLinks[i].title,
    					    			flex: 2,
    					    			allowBlank: false
    						        },
    						        {
    						        	xtype: 'mz-input-text',
    					    			fieldLabel: 'Sublink URL',
    					    			name: 'sublinkUrl',
    					    			itemId: 'sublinkUrl',
    					    			value: data.linksAndSubLinks[i].url,
    					    			flex: 2,
    					    			allowBlank: false
    						        },
    						        {
    						        	xtype: 'mz-input-text',
    					    			fieldLabel: 'Sublink subtitle',
    					    			name: 'sublinkSubTitle',
    					    			itemId: 'sublinkSubTitle',
    					    			value: data.linksAndSubLinks[i].subtitle,
    					    			style: {
    					    				marginLeft: '10px',
    					    				marginBottom: '30px'
    					    			},
    					    			flex: 2
    						        },
    						        {
    						        	xtype: 'mz-input-textarea',
    					    			fieldLabel: 'Sublink description',
    					    			name: 'sublinkDescription',
    					    			itemId: 'sublinkDescription',
    					    			value: data.linksAndSubLinks[i].description,
    					    			style: {
    					    				marginLeft: '10px',
    					    				marginBottom: '30px'
    					    			},
    					    			flex: 2
    						        },
    						        {
    						        	xtype: 'button',
                        			    text: 'Delete',
                        			    name: 'DeleteBtn',
                        			    flex: 1,
                        			    style: {
                        			        marginTop: '30px',
                        			        marginLeft: '10px'
                        			    },
                        			    listeners: {
                        			    	click: function(e){
                        			    		e.up('container').destroy();
                        			    	}
                        			    }
    						        }
					        ]
						}
					);
    		}
    		
    	}
    	this.data = data;
    }
});

