Ext.widget({
    xtype: 'mz-form-widget',
    itemId: 'featuredBrands',
    items: [
        {
            xtype: 'container',
            layout: 'hbox',
            items: [
                {
                    xtype: 'mz-input-text',
                    name: 'brandName',
                    fieldLabel: 'Brand Name',
                    flex: 2
                }, {
                    xtype: 'mz-input-color',
                    name: 'brandBgColor',
                    fieldLabel: 'Tile Background',
                    flex: 2,
                    style: {
                        marginLeft: '10px'
                    }
                },{
                    xtype: 'mz-input-color',
                    name: 'brandTextColor',
                    fieldLabel: 'Font Color',
                    flex: 2,
                    style: {
                        marginLeft: '10px'
                    }
                }
                
            ]
        }, {
            xtype: 'container',
            layout: 'hbox',
            items: [
                {
                    xtype: 'mz-input-text',
                    name: 'brandLinkText',
                    fieldLabel: 'Brand Link Text',
                    flex: 2
                }, {
                    xtype: 'mz-input-text',
                    name: 'brandLink',
                    fieldLabel: 'Brand Link',
                    flex: 2,
                    style: {
                        marginLeft: '10px'
                    }
                },{
                    xtype: 'mz-input-color',
                    name: 'brandLinkColor',
                    fieldLabel: 'Brand Link Color',
                    flex: 2,
                    style: {
                        marginLeft: '10px'
                    }
                }
            ]
        }, {
            xtype: 'container',
            layout: 'hbox',
            items: [
                {
                    xtype: 'mz-input-image',
                    name: 'brandImage',
                    fieldLabel: 'Featured Brand Image',
                    flex: 2
                }, {
                    xtype: 'mz-input-textarea',
                    name: 'brandDesc',
                    fieldLabel: 'Brand Description',
                    flex: 2,
                    style: {
                        marginLeft: '10px'
                    }
                }
            ]
        }
    ]

});
