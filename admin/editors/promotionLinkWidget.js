Ext.widget({
    xtype: 'mz-form-widget',
    itemId: 'promotionalLinkWidget',
    items: [
    	{
            xtype: 'container',
            layout: 'vbox',
            items: [
            	{
            		xtype: 'panel',
                    layout: 'vbox',
                    items: [
                    	{
                            xtype: 'mz-input-image',
                            name: 'promotionalLinkImage',
                            fieldLabel: 'Promotional Image:'
                        },{
                            xtype: 'mz-input-text',
                            name: 'promotionalLinkDesc',
                            fieldLabel: 'Promotional Description:'
                        },{
                            xtype: 'mz-input-text',
                            name: 'promotionalLinkText',
                            fieldLabel: 'Link Text:'
                        },{
                            xtype: 'mz-input-text',
                            name: 'promotionalLink',
                            fieldLabel: 'Link:'
                        }
                    ]
            	}
            ]
    	}
	]
});