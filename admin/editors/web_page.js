﻿Ext.widget({
    xtype: 'mz-form-webpage',
    items: [
       {
		xtype: 'mz-input-text',
		fieldLabel: 'Page Heading',
		name: 'pageHeading',
		itemId: 'pageHeadingId'
	   }
    ],
    setData: function(data){
    	this.getForm().setValues(data);
    }
});