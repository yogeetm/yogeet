Ext.widget({
    xtype: 'mz-form-widget',
    itemId: 'salesBannerWithImage',
    items: [
    	{
            xtype: 'container',
            layout: 'vbox',
            items: [
            	{
            		xtype: 'panel',
                    layout: 'hbox',
                    items: [
                    	{
                    		xtype: 'mz-input-image',
                            name: 'salesBannerImage',
                            fieldLabel: 'Banner Image',
                            flex: 2
                        },{
                        	xtype: 'mz-input-image',
                            name: 'salesBannerLogo',
                            fieldLabel: 'Banner Logo',
                            flex: 2,
                            style: {
                            	 marginLeft: '10px'
                            }
                        }
                    ]
            	},
            	{
            		xtype: 'panel',
                    layout: 'hbox',
                    items: [
                    	{
                            xtype: 'mz-input-textarea',
                            name: 'salesBannerDesc',
                            fieldLabel: 'Description',
                            flex: 2
                        },
                        {
                            xtype: 'mz-input-color',
                            name: 'salesBannerDescColor',
                            fieldLabel: 'Description Font Color',
                            flex:2,
                            style: {
                           	 marginLeft: '10px'
                           }
                        }
                    ]
            	}
            ]
    	}
	]
});