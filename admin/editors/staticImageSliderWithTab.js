Ext.widget({
	xtype: 'mz-form-widget',
	itemId: 'StaticimgSliderWithTabForm',
	items: [
	        {
			xtype: 'container',
			id: 'static-imageContainer',
			name: 'static-imageContainer',
			items: [
			        {
			    	 xtype: 'panel',
			    	 layout: 'hbox',
			    	 items: [ 
			    	       {
							xtype: 'mz-input-text',
							fieldLabel: 'Custom Class Name',
							name: 'customClass',
							itemId: 'customClassId'
			    	       },
			    	       {
		                    	xtype: 'mz-input-number',
		            			fieldLabel: 'Number of fields',
		            			id: 'noOfFields',
		            			name: 'NoOfFields',
		            			value: 1,
		            			style: {
							        marginLeft: '30px'
							    }
		                    },
		                    {
		                    	xtype: 'button',
		        			    text: 'Add',
		        			    name: 'AddBtn',
		        			    style: {
		        			        marginTop: '30px',
		        			        marginLeft: '10px'
		        			    }, 
		        			    listeners: {
		        			    	click: function(e){
		        			    		var noOfFields = Ext.getCmp('noOfFields').getValue();
		        			    		
		        			    		for(var i=0; i<noOfFields; i++){
		        			    			e.up('form').add(
		        			    				{
	            			    					xtype: 'container',
	            			    					style: "background:#ccb;margin:10px; padding:10px",
	        			    						items: [
        			    						        {
        			    						        	xtype: 'panel',
        			    						        	layout: 'hbox',
        			    						        	items: [
    			    						        	        {
    													        	xtype: 'mz-input-image',
    													            fieldLabel: 'Image',
    													            name: 'Images' 
    													        },
    													        {
    													        	xtype: 'panel',
    	        			    						        	layout: 'vbox',
    	        			    						        	items: [
    	        			    						        	    {
    	    													        	xtype: 'panel',
    	    	        			    						        	layout: 'hbox',
    	    	        			    						        	items: [
    																			{
    																			    name: "arrowText",
    																			    fieldLabel: "Arrow Text",
    																			    xtype: "mz-input-text",
    																			    flex: 1,
    																			    style: {
    																						marginLeft: '10px'
    																					}
    																			 },
    																			 {
     																			    name: "linktoopen",
     																			    fieldLabel: "Link To Open",
     																			    xtype: "mz-input-text",
     																			    flex: 1,
     																			    style: {
     																						marginLeft: '10px'
     																					}
     																			 }
    		        			    						        	     ]
    	    													        }
																	]
    													        }
			    						        	        ]
        			    						        },   
        			    						        {
        			    						        	xtype: 'panel',
        			    						        	layout: 'hbox',
        			    						        	items: [
                 			    						        {
                 			    						        	xtype: 'button',
                 			                        			    text: 'Delete',
                 			                        			    name: 'DeleteBtn',
                 			                        			    flex: 1,
                 			                        			    style: {
                 			                        			        marginTop: '30px',
                 			                        			        marginLeft: '10px'
                 			                        			    },
                 			                        			    listeners: {
                 			                        			    	click: function(e){
                 			                        			    		e.up('panel').up('container').destroy();
                 			                        			    	}
                 			                        			    }
                 			    						        } 
			    						        	        ]
        			    						        }
        			    						        
        			    						     ]
		        			    				}
		        			    			);
		        			    		}
		        			    	} 
		        			    }
		                    }
						]
			        }
		        ]
	        } 
        ],
        getData: function(){
        	var sliderData = this.getValues();
        	Ext.each(sliderData.Images, function(item, index){
        		if(sliderData.arrowText) {
        			if(Ext.isArray(sliderData.arrowText)){
        				item.arrowText = sliderData.arrowText[index];
        			}else{
        				item.arrowText = sliderData.arrowText;
        			}
        		}
        		if(sliderData.linktoopen) {
        			if(Ext.isArray(sliderData.linktoopen)){
        				item.linktoopen = sliderData.linktoopen[index];
        			}else{
        				item.linktoopen = sliderData.linktoopen;
        			}
        		}
        		
        	});
        	delete sliderData.arrowText;
        	delete sliderData.linktoopen;
        	return Ext.applyIf(sliderData, this.data);
        },
        setData: function(data){
        	this.getForm().setValues(data);
        	if(!Ext.Object.isEmpty(data)){
        		for(var i = 0; i < data.Images.length; i++){
        			Ext.getCmp('static-imageContainer').add(
        					{
		    					xtype: 'container',
		    					style: "background:#ccb;margin:10px; padding:10px",
	    						items: [
    						        {
    						        	xtype: 'panel',
    						        	layout: 'hbox',
    						        	items: [
						        	        {
									        	xtype: 'mz-input-image',
									            fieldLabel: 'Image',
									            name: 'Images',
									            value: data.Images[i]
									        },
									        {
									        	xtype: 'panel',
		    						        	layout: 'vbox',
		    						        	items: [
		    						        	    {
											        	xtype: 'panel',
    			    						        	layout: 'hbox',
    			    						        	items: [
															{
															    name: "arrowText",
															    fieldLabel: "arrowText Text",
															    xtype: "mz-input-text",
															    value: data.Images[i].arrowText,
															    flex: 1,
															    style: {
																		marginLeft: '10px'
																	}
															 },
															 {
															    name: "linktoopen",
															    fieldLabel: "Link To Open",
															    xtype: "mz-input-text",
															    value: data.Images[i].linktoopen,
															    flex: 1,
															    style: {
																		marginLeft: '10px'
																	}
															 }
			    						        	     ]
											        }
												]
									        }
					        	        ]
    						        },   
    						        {
    						        	xtype: 'panel',
    						        	layout: 'hbox',
    						        	items: [
		    						         {
		    						        	xtype: 'button',
		                        			    text: 'Delete',
		                        			    name: 'DeleteBtn',
		                        			    flex: 1,
		                        			    style: {
		                        			        marginTop: '30px',
		                        			        marginLeft: '10px'
		                        			    },
		                        			    listeners: {
		                        			    	click: function(e){
		                        			    		e.up('panel').up('container').destroy();
		                        			    	}
		                        			    }
		    						        }  
					        	        ]
    						        }
    						     ]
		    				}
        			);
        		}   
        	}
        	this.data = data; 
        }
});