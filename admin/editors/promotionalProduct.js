Ext.widget({
	  xtype: 'mz-form-widget',
	  itemId: 'promotionalProduct',
	  items:[
     	{
     		xtype: "container",
     		layout: "hbox",
     		items :[
				{
					xtype: "mz-input-color",
					name: "backgroundColor",
					value:"#eb0029",
					id:"backgroundColor",
					fieldLabel: "Section Background",
					flex: 2
				},
     		    {	
					xtype: "mz-input-color",
					value:"#ffffff",
					name: "textColor",
	        		id:"textColor",
	        		fieldLabel: "Section Text Color",
	        		flex: 2,
					style:{
						marginLeft:"10px"
					}
     		    }
	        ]
     	 },
     	 {
     		 xtype:"container",
     		 layout:"hbox",
     		 items:[
				{
					xtype: "mz-input-text",
					name: "buttonText",
					id:"buttonText",
					fieldLabel: "Button Text",
					flex: 2
				},
     		    {
                    xtype: "mz-input-dropdown",
                    name: "buttonTextColor",
                    fieldLabel: "Button Background",
                    flex:2,
                    style:{
						marginLeft:"10px"
					},
                    store: [
                        "Yellow",
                        "Red"
                     ]
                 }
     		 ]
     	 },
     	 {
     		 xtype:"container",
     		 layout:"hbox",
     		 items:[	
				{
					xtype: "mz-input-product",
					name: "productCode",
					id:"productCode",
					fieldLabel: "Promotional Product",
					flex: 2
				},
				{
					xtype: "mz-input-text",
					name: "promotionalTitle",
					id:"promotionalTitle",
					fieldLabel: "Promotional Title",
					flex: 2,
					style:{
						marginTop:"12px"
					}
				}
     		]
     	 },
     	 {
     		 xtype:"container",
     		 layout:"hbox",
     		 items:[
				{
					xtype: "mz-input-textarea",
					name: "productDescription",
					id:"productDescription",
					fieldLabel: "Promotional Text (Max Character Length: 160)",
					maxLength: 160,
					flex: 2
				}
     		]
     	 }
     ]
});