Ext.widget({
	xtype: 'mz-form-widget',
	itemId: 'footerForm',
    items: [
			{
				xtype: 'container',
				id: 'mainConatiner',
				items:[
				       {
		            	xtype: 'form',
		            	name: 'headerForm',
		            	id: 'headerForm',
		            	items: [
		                        {
		                            xtype: 'panel',
		                            border: false,
		                            layout: 'hbox',
		                            name: 'headerPanel',
		                            items: [
		                                {
		                                	xtype: 'mz-input-number',
		                        			fieldLabel: 'Number of sections',
		                        			id: 'noOfSections',
		                        			name: 'noOfSections',
		                        			value: 1,
		                        			style: {
										        marginLeft: '10px'
										    }
		                                },
		                                {
		                                	xtype: 'button',
		                    			    text: 'Add',
		                    			    name: 'AddBtn',
		                    			    style: {
		                    			        marginTop: '30px',
		                    			        marginLeft: '10px'
		                    			    },
		                    			    listeners: {
		                    			    	click: function(e){
		                    			    		var noOfFields = Ext.getCmp('noOfSections').getValue();
		                    			    		for(var i = 0; i < noOfFields; i++){
		                    			    			e.up('form').add(
		        		        			    				{
		        	            			    					xtype: 'container',
		        	            			    					style: "background:#ccb;margin:10px; padding:10px",
		        	        			    						items: [
		                			    						        {
		                			    						        	xtype: 'panel',
		                			    						        	layout: 'hbox',
		                			    						        	items: [
		            			    						        	        {
		            													        	xtype: 'mz-input-image',
		            													            fieldLabel: 'Image',
		            													            name: 'Images' 
		            													        },
		            													        {
		            													        	xtype: 'panel',
		            	        			    						        	layout: 'vbox',
		            	        			    						        	items: [
		            	        			    						        	    {
		            	        			    						        	    	xtype: 'panel',
		            	    	        			    						        	layout: 'hbox',
		            	    	        			    						        	items: [
		        																				{
		        																					xtype: "mz-input-text",
		        																				    name: "SectionTitles",
		        																				    fieldLabel: "Section Title",
		        																				    flex: 1,
		        																				    style: {
		        																							marginLeft: '10px'
		        																						}
		        																				  },
		        																				  {
		        																					xtype: "mz-input-text",
		        																				    name: "SectionLinks",
		        																				    fieldLabel: "Section Link",
		        																				    flex: 1,
		        																				    style: {
		        																							marginLeft: '10px'
		        																						}
		        																				  }    
		            	    	        			    						        	]
		            	        			    						        	    },
		            	        			    						        	    {
		            	    													        	xtype: 'panel',
		            	    	        			    						        	layout: 'hbox',
		            	    	        			    						        	items: [
		            																			{
		            																				xtype: "mz-input-text",
		            																			    name: "SectionDesc",
		            																			    fieldLabel: "Section Description",
		            																			    flex: 1,
		            																			    style: {
		            																						marginLeft: '10px'
		            																					}
		            																			 },
		            																			 {
		             																			    name: "findOutMoreLinks",
		             																			    fieldLabel: "Find Out More Links",
		             																			    xtype: "mz-input-text",
		             																			    flex: 1,
		             																			    style: {
		             																						marginLeft: '10px'
		             																					}
		             																			 }
		            		        			    						        	     ]
		            	    													        },
		            	    													        {
				                         			    						        	xtype: 'button',
				                         			                        			    text: 'Delete',
				                         			                        			    name: 'DeleteBtn',
				                         			                        			    flex: 1,
				                         			                        			    style: {
				                         			                        			        marginTop: '30px',
				                         			                        			        marginLeft: '10px'
				                         			                        			    },
				                         			                        			    listeners: {
				                         			                        			    	click: function(e){
				                         			                        			    		e.up('panel').up('container').destroy();
				                         			                        			    	}
				                         			                        			    }
				                         			    						        }
		        																	]
		            													        }
		        			    						        	        ]
		                			    						        }
		                			    						     ]
		        		        			    				}
		        		        			    			);
		                    			    		}
		                    			    	}
		                    			    }
		                                }
		                            ] 
		                        }
		                    ]
				       }
				   ]
				}
			], //footerForm items end
			getData: function(){
				var imageSectionData = this.getValues();
				Ext.each(imageSectionData.Images, function(item, index){
					
					if(imageSectionData.SectionTitles) {
						if(Ext.isArray(imageSectionData.SectionTitles)){
							item.SectionTitle = imageSectionData.SectionTitles[index];
						}else{
							item.SectionTitle = imageSectionData.SectionTitles;
						}
					}
					if(imageSectionData.SectionLinks) {
						if(Ext.isArray(imageSectionData.SectionLinks)){
							item.SectionLink = imageSectionData.SectionLinks[index];
						}else{
							item.SectionLink = imageSectionData.SectionLinks;
						}
					}
					if(imageSectionData.SectionDesc) {
						if(Ext.isArray(imageSectionData.SectionDesc)){
							item.SectionDesc = imageSectionData.SectionDesc[index];
						}else{
							item.SectionDesc = imageSectionData.SectionDesc;
						}
					}
				});
				
				var finalArray = [];
	    		while(imageSectionData.Images.length > 0) {
	    			var subArray = imageSectionData.Images.splice(0,12);
	    			finalArray.push(subArray);
	    		}
	    		imageSectionData.Images = finalArray;
		    	delete imageSectionData.SectionTitles;
				delete imageSectionData.SectionLinks;
				delete imageSectionData.SectionDesc; 
				delete imageSectionData.findOutMoreLinks;
				return Ext.applyIf(imageSectionData, this.data);
							
			},
			setData: function(data){
				this.getForm().setValues(data);
				if(!Ext.Object.isEmpty(data)){
					for(var i = 0; i < data.Images.length; i++){
						Ext.getCmp('headerForm').add(
								{
			    					xtype: 'container',
			    					style: "background:#ccb;margin:10px; padding:10px",
		    						items: [
	    						        {
	    						        	xtype: 'panel',
	    						        	layout: 'hbox',
	    						        	items: [
    						        	        {
										        	xtype: 'mz-input-image',
										            fieldLabel: 'Image',
										            name: 'Images',
										            value: data.Images[i]
										        },
										        {
										        	xtype: 'panel',
			    						        	layout: 'vbox',
			    						        	items: [
			    						        	    {
			    						        	    	xtype: 'panel',
        			    						        	layout: 'hbox',
        			    						        	items: [
																{
																	xtype: "mz-input-text",
																    name: "SectionTitles",
																    fieldLabel: "Section Title",
																    flex: 1,
																    value: data.Images[i].SectionTitle,
																    style: {
																			marginLeft: '10px'
																		}
																  },
																  {
																	xtype: "mz-input-text",
																    name: "SectionLinks",
																    fieldLabel: "Section Link",
																    value: data.Images[i].SectionLink,
																    flex: 1,
																    style: {
																			marginLeft: '10px'
																		}
																  }    
        			    						        	]
			    						        	    },
			    						        	    {
												        	xtype: 'panel',
        			    						        	layout: 'hbox',
        			    						        	items: [
																{
																	xtype: "mz-input-text",
																    name: "SectionDesc",
																    fieldLabel: "Section Description",
																    value: data.Images[i].SectionDesc,
																    flex: 1,
																    style: {
																			marginLeft: '10px'
																		}
																 },
																 {
																	    name: "findOutMoreLinks",
																	    fieldLabel: "Find Out More Links",
																	    value: data.Images[i].findOutMoreLinks,
																	    xtype: "mz-input-text",
																	    flex: 1,
																	    style: {
																				marginLeft: '10px'
																			}
																	 }
    			    						        	     ]
												        },
												        {
         			    						        	xtype: 'button',
         			                        			    text: 'Delete',
         			                        			    name: 'DeleteBtn',
         			                        			    flex: 1,
         			                        			    style: {
         			                        			        marginTop: '30px',
         			                        			        marginLeft: '10px'
         			                        			    },
         			                        			    listeners: {
         			                        			    	click: function(e){
         			                        			    		e.up('panel').up('container').destroy();
         			                        			    	}
         			                        			    }
         			    						        }
													]
										        }
						        	        ]
	    						        }
	    						     ]
			    				}
						);
					}   
				}
				this.data = data; 
			}
});