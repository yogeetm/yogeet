<div class="mz-l-container">
	<div class="view-order-detail-container">
		<div class="container">
			<h2 class="view-order-title">
				{% if model.status == 'Errored'%}
					{{ labels.errorOrder }}
				{% else %}
					{{ labels.thanksForYourOrder }}
				{% endif %}
			</h2>
			<div class="order-detail-section order-number">
				{{ labels.referEmailConfirmation|safe }} 
				{% if not user.isAnonymous && model.status != 'Errored' %} 
					<a href="javascript:;" class="confirmation-link view-order-link" id="confirmation-order-link">{{ labels.viewOrderDetails }} </a> 
				{% endif %}
			</div>
			<div class="row ">
				<div class="col-lg-6 col-md-6 col-sm-6 order-detail-section pickup-ready-note">
					{% if model.status == 'Errored'%}
						<div class="errored-message-container">
							{{ labels.errorOrderMessage|string_format(themeSettings.helpNumber)|safe }}
						</div>
					{% else %}
						{{ labels.emailWithPickupReady|string_format(model.email)|safe }}
					{% endif %}
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 order-detail-section question-with-link">
					{{ labels.questionWithLinks|safe }}
				</div>
			</div>
		</div>
	</div>
	<div class="confirmation-container">
		<div class="container">
			<!-- order detail-header -->
			<div class="order-detail-header">
				<div class="row">
					<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
	                	<h3 class="order-detail-title">{{ labels.orderDetails }}</h3>
	                  	<div class="order-detail-section order-date-number">
	                  		{% if model.submittedDate %}<span>{{ labels.orderOnDate}} {{ model.submittedDate|date('Y-m-d')}}</span> {% endif %}
						</div>
	                </div>
	                <div class="col-lg-3 col-md-3 col-sm-4 col-lg-12">
	                  <a href="#" onclick=" window.print();" class="btn-common print-button">{{ labels.printOrderDetails }}</a>
	                </div>
	            </div>
			</div>
			<!-- Store pick up details -->
			<div class="store-detail-container">
				<div class="row">
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 store-pickup-location">
						<h4 class="confirmation-subtitle">{{ labels.yourPickupStore }}</h4>
						{% include "modules/location/preferred-store" with model=model.locationDetails %}
		                <div class="store-pickup-note">
		                	{{ labels.confirmationStorePickUpNote|safe }}
		                </div>
					</div>		
					<!-- Store Hours -->
					<div class="col-lg-3 col-md-3 col-sm-4 col-lg-offset-1 col-md-offset-1 store-opening-hours">
		                <h4 class="confirmation-subtitle">{{ labels.storeHours }}</h4>
		                <ul class="hour-list">
		                	{% if model.locationDetails.regularHours.monday.label %}
			                	<li>
			                		<span class="week-day">{{labels.monday}}</span> 
			                		<span class="week-time">{{model.locationDetails.regularHours.monday.label}}</span>
			                	</li>
		                	 {% endif %} 
		                	 {% if model.locationDetails.regularHours.tuesday.label %} 
			                	<li>
			                		<span class="week-day">{{labels.tuesday}}</span> 
			                		<span class="week-time">{{model.locationDetails.regularHours.tuesday.label}}</span>
			                	</li>
		                	{% endif %}
		                	{% if model.locationDetails.regularHours.wednesday.label %}
			                	<li>
			                		<span class="week-day">{{labels.wednesday}}</span> 
			                		<span class="week-time">{{model.locationDetails.regularHours.wednesday.label}}</span>
			                	</li>
		                	{% endif %}
		                	{% if model.locationDetails.regularHours.thursday.label %}
			                	<li>
			                		<span class="week-day">{{labels.thursday}}</span> 
			                		<span class="week-time">{{model.locationDetails.regularHours.thursday.label}}</span>
			                	</li>
		                	{% endif %}
		                	{% if model.locationDetails.regularHours.friday.label %} 
			                	<li>
			                		<span class="week-day">{{labels.friday}}</span> 
			                		<span class="week-time">{{model.locationDetails.regularHours.friday.label}}</span>
			                	</li>
		                	{% endif %}  
		                	{% if model.locationDetails.regularHours.saturday.label %}  
			                	<li>
			                		<span class="week-day">{{labels.saturday}}</span> 
			                		<span class="week-time">{{model.locationDetails.regularHours.saturday.label}}</span>
			                	</li>
		                	{% endif %} 
		                	{% if model.locationDetails.regularHours.sunday.label %}  
			                	<li>
			                		<span class="week-day">{{labels.sunday}}</span> 
			                		<span class="week-time">{{model.locationDetails.regularHours.sunday.label}}</span>
			                	</li>
		                	{% endif %} 
		                </ul>
		            </div>		
					<!-- Pickup By Section -->
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pickup-by-section">
		                <h4 class="confirmation-subtitle">{{ labels.pickUpBy }}</h4>
		                <p>
		                	<strong class="pickup-person">
		                	{% with labels.firstPersonAttrFQN|string_format(themeSettings.firstPersonFirstNameCode) as firstPerson %}
								{% with model.attributes|findwhere("fullyQualifiedName", firstPerson) as attribute %} 
							          <span>{{ attribute.values|first }} </span>
								{% endwith %}
							{% endwith %}
		                	{% with labels.firstPersonAttrFQN|string_format(themeSettings.firstPersonLastNameCode) as firstPerson %}
								{% with model.attributes|findwhere("fullyQualifiedName", firstPerson) as attribute %}   
							          <span>{{ attribute.values|first }}</span>
								{% endwith %}
							{% endwith %}
		                	</strong>
		                	<br>
		                	{% with labels.firstPersonAttrFQN|string_format(themeSettings.firstPersonEmailCode) as firstPerson %}
								{% with model.attributes|findwhere("fullyQualifiedName", firstPerson) as attribute %}   
							          <span>{{ attribute.values|first }}</span>
								{% endwith %}
							{% endwith %}
		                </p>
		                <p>
		                	<strong class="second-pickup-person">
							{% with labels.firstPersonAttrFQN|string_format(themeSettings.secondPersonFirstNameCode) as secondPerson %}
								{% with model.attributes|findwhere("fullyQualifiedName", secondPerson) as attribute %}   
							          {% if attribute and attribute.values|first != 'N/A' %}<span>{{ attribute.values|first }}</span>{% endif %}
								{% endwith %}
							{% endwith %}
							{% with labels.firstPersonAttrFQN|string_format(themeSettings.secondPersonLastNameCode) as secondPerson %}
								{% with model.attributes|findwhere("fullyQualifiedName", secondPerson) as attribute %}   
							          {% if attribute and attribute.values|first != 'N/A' %}<span>{{ attribute.values|first }}</span>{% endif %}
								{% endwith %}
							{% endwith %}
							</strong>
							<br>
							{% with labels.firstPersonAttrFQN|string_format(themeSettings.secondPersonEmailCode) as secondPerson %}
								{% with model.attributes|findwhere("fullyQualifiedName", secondPerson) as attribute %}   
							          {% if attribute and attribute.values|first != 'N/A' %}<span>{{ attribute.values|first }}</span>{% endif %}
								{% endwith %}
							{% endwith %}
							<br>
		                	{% with labels.firstPersonAttrFQN|string_format(themeSettings.secondPersonFirstNameCode) as secondPerson %}
		                		{% with model.attributes|findwhere("fullyQualifiedName", secondPerson) as attribute %} 
			                		{% if attribute and attribute.values|first != 'N/A' %}<small>{{ labels.photoIdNote }}</small>{% endif %}    
			                	{% endwith %}
			                {% endwith %}
		                </p>
              		</div>
				</div>
				
				<!-- Second Row -->
				<div class="payment-billing-section">
					<div class="row">
					  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 payment-method-section">
		                <h4 class="confirmation-subtitle">{{ labels.paymentMethod }}</h4>
		                <p class="">
		                	<div class="mz-l-stack-section mz-checkout-current-payment payment-summary">
            					<div class="payment-summary-card-number">
				                	<span class="defaultCardImg card-img-in-summary {% if not model.billingInfo.card.paymentOrCardType %} showDefaultCard{% endif %}{% if model.billingInfo.card.paymentOrCardType == "VISA" %} showVisaImg{% endif %}
									{% if model.billingInfo.card.paymentOrCardType == "MC" %} showMastercardImg{% endif %}{% if model.billingInfo.card.paymentOrCardType == "AMEX" %} showAmexImg{% endif %}{% if model.billingInfo.card.paymentOrCardType == "DISCOVER" %} showDiscoverImg{% endif %}"></span>
				                	<span class="card-number-part">{{ model.billingInfo.card.cardNumberPartOrMask|default("**** **** **** ****") }} </span>
		                		</div>
		                	</div>
		                </p>
		                {% with labels.firstPersonAttrFQN|string_format(themeSettings.aeroplanMilesCode) as aeroplanMilesCode %}
		                		{% with model.attributes|findwhere("fullyQualifiedName", aeroplanMilesCode) as aeroplanMiles %}
		                			{% if aeroplanMiles and aeroplanMiles.values|first != 'N/A' %}
		               	 				<p class="">
						                	<img src="{% make_url "cdn" "/resources/images/payment/aeroplan-small.png" %}" alt="Aeroplan Logo" class="">{{ labels.aeroplan }}<br>
						                	{% with aeroplanMiles.values|first as milesEarned %}
						                		<small>{{ labels.aeroplanMilesHaveEarned|string_format(milesEarned) }}</small>
						                	{% endwith %}
		                				</p>
		                			{% endif %}
                				{% endwith %}
                		{% endwith %}
		              </div>
		              
		              <!-- Billing contact-->
						<div class="col-lg-3 col-md-3 col-sm-4 col-lg-offset-1 col-md-offset-1 billing-addess-section">
							<h4 class="confirmation-subtitle">{{ labels.billing }}</h4>
				            <div class="mz-addresssummary">{% include "modules/common/address-summary" with model=model.billingInfo.billingContact %}</div>
				        </div>
		              
		              <!-- Order Summary -->
		              	
		              	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 order-summary-section">
		              		{% include "modules/common/checkout-order-summary" %} 
		              	</div>
					</div>
				</div>
				
				<!-- Order notes Section -->
				<div class="order-note-section {% if not model.shopperNotes.comments %} hidden {% endif %}">
					<div class="order-note-detail"> 
						<h4 class="confirmation-subtitle">{{ labels.notesAboutOrder }}</h4>
						<p class="no-margin">{{ model.shopperNotes.comments }}</p>
					</div>
				</div>
				
				<!-- order Summary detail container -->
				<div class="items-in-order-container">
					<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 order-item-container">
						{% include "modules/common/confirmation-order-summary-product" %} 
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>