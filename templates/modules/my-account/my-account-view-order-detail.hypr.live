<div class="myaccount-view-order-detail">
	<div class="view-all-order-container">
		<a class="view-all-orders">
			<i class="material-icons">keyboard_arrow_left</i> 
			<span>{{ labels.viewAllOrders }}</span>
		</a>
	</div>
	<div class="row detail-order-section">
		<div class="col-lg-8 col-md-8">
			
			<h2 class="view-order-detail-number">{{ labels.orderNumber }}: {{ model.omsOrder.orderID }}</h2>
			<p class="ordered-on"> 
				{% if model.submittedDate %}
					{{ labels.orderOnDate }} {{ model.submittedDate|date("Y-m-d") }}
	            {% else %}
	                {{ labels.orderOnDate }} {{ model.auditInfo.createDate|date("Y-m-d") }}
	            {% endif %}
			</p>
			<p class="myaccount-order-status">
				{% with model.omsOrder as order %}
					{% with order.shipments|first as shipments %}
			    		{% if shipments.shipmentStatus == "ASSIGNED" or shipments.shipmentStateName == "shipper_sent_product" or shipments.shipmentStateName == "shipper_product_at_store" or shipments.shipmentStateName == "fulfiller_requests_product" or shipments.shipmentStateName == "fulfiller_readying_package" or shipments.shipmentStateName == "fulfiller_has_order_in_stock" or shipments.shipmentStateName == "fulfiller_window" %}
			        		<i class="fa fa-hourglass-half" aria-hidden="true"></i>
			        		<span>{{ labels.orderBeingProcessed }}</span>
			    		{% endif %}
			    		{% if shipments.shipmentStatus == "Cancelled" or shipments.shipmentStatus == "CANCELLED" %}
			        		<i class="material-icons">close</i>
			        		<span>{{ labels.orderCancelled }}</span>
			        	{% endif %}
			        	{% if shipments.shipmentStateName == "ready_for_pickup" %}
			        		<i class="material-icons">local_shipping</i>
			        		<span>{{ labels.orderItemRedayPickupStore }}</span>
			        	{% endif %}
			        	{% if shipments.shipmentStateName == "fulfilled" or shipments.shipmentStateName == "return_initaited" or shipments.shipmentStateName == "return_accepted_for_shipper" or shipments.shipmentStateName == "return_accepted_for_dealer" or shipments.shipmentStateName == "return_accepted_by_fulfiller_for_shipper" %}
			        		<i class="material-icons green-link">shopping_basket</i>
			        		<span>{{ labels.orderCompletePickup }}</span>
			        	{% endif %}
			        	
		        	{% endwith %}
	        	{% endwith %}
	    	</p>
		</div>
		<div class="col-lg-4 col-md-4 print-section">
			<a onclick=" window.print();" href="" class="printorder-detail">
				<i class="material-icons">print</i> 
				<span>{{ labels.printOrderDetails }}</span>
			</a>
		</div>
	</div>
	<!-- Steps Container Start-->
	<div class="order-status-steps-container hidden-sm hidden-xs">
		{% with model.omsOrder as order %}
			{% if order %}
				{% with order.shipments|first as shipments %}
					
						{% if shipments.shipmentStatus != "Canceled" or shipments.shipmentStatus != "CANCELED" %}
						<ul class="order-status-steps">
						    <li {% if shipments.shipmentStatus == "ASSIGNED" or shipments.shipmentStateName == "shipper_sent_product" or shipments.shipmentStateName == "shipper_product_at_store" or shipments.shipmentStateName == "fulfiller_requests_product" or shipments.shipmentStateName == "fulfiller_readying_package" or shipments.shipmentStateName == "fulfiller_has_order_in_stock" or shipments.shipmentStateName == "fulfiller_window" %} class="is-active" {% endif %}>
						      <i class="fa fa-hourglass-half" aria-hidden="true"></i>
						      <span>{{ labels.orderBeingProcessed }}</span>
						    </li>
						    <li {% if shipments.shipmentStateName == "ready_for_pickup" %} class="is-active" {% endif %}>
						      <i class="material-icons">local_shipping</i>
						      <span>{{ labels.orderItemRedayPickupStore }}</span>
						    </li>
						    <li {% if shipments.shipmentStateName == "fulfilled" or shipments.shipmentStateName == "return_initaited" or shipments.shipmentStateName == "return_accepted_for_shipper" or shipments.shipmentStateName == "return_accepted_for_dealer" or shipments.shipmentStateName == "return_accepted_by_fulfiller_for_shipper" %} class="is-active" {% endif %} >
						      <i class="material-icons green-link">shopping_basket</i>
						      <span>{{ labels.orderCompletePickup }}</span>
						    </li>
					    </ul>
					    {% endif %}			   
			    {% endwith %}
	     	{% endif %}
	    {% endwith %}
	</div>
	<!-- Steps Container End-->
	
	<div class="col-sm-12 col-xs-12 visible-sm visible-xs order-summary-section">
  		{% include "modules/common/order-summary" %} 
  	</div>
  	
	<div class="row">
		<div class="store-detail-container">
		
			<!-- Store Pickup By Section -->
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 store-pickup-location">
				<h4 class="view-order-subtitle">{{ labels.yourPickupStore }}</h4>
                <div class="prefer-store-block block-bottom-margin">
{#					{% include "modules/location/preferred-store" with model=model.currentStore %}#}
					<div class="store-address">
						 <div class="address-card">
						 	 {% with model.omsOrder.shipments|first as location %}
						 	 <a href="#" class="store-name">{{ location.shipperName }}</a>
					            {% endwith %}
					     </div>
					</div>
				</div>	
	            <div class="store-pickup-note">
	            	{{ labels.confirmationStorePickUpNote|safe }}
	            </div>
			</div>	
			
		
			<!-- Pickup By Section -->
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pickup-by-section">
	            <h4 class="view-order-subtitle">{{ labels.pickUpBy }}</h4>				
				<div class="pickup-person-container">
					<div class="pickup-person">
					   <p>
		                	<strong class="pickup-person">
		                		<span>{{ model.omsOrder.customData.firstPersonFirstName }} </span>
								<span>{{ model.omsOrder.customData.firstPersonLastName }}</span>								
		                	</strong>
		                	<br>
		                	<span>{{ model.omsOrder.customData.firstPersonEmail }} </span>
		                </p>
		                <p>
		                	<strong class="pickup-person">
		                		<span>{{ model.omsOrder.customData.secondPersonFirstName }} </span>
								<span>{{ model.omsOrder.customData.secondPersonLastName }}</span>								
		                	</strong>
		                	<br>
		                	<span>{{ model.omsOrder.customData.secondPersonEmail }} </span>
							<br>
		                	{% with labels.firstPersonAttrFQN|string_format(themeSettings.secondPersonFirstNameCode) as secondPerson %}
		                		{% with order.AttributeDefinitions|findwhere("fullyQualifiedName", secondPerson) as attribute %} 
			                		{% if attribute and attribute.values|first != 'N/A' %}<small>{{ labels.photoIdNote }}</small>{% endif %}    
			                	{% endwith %}
			                {% endwith %}
		                </p>
				    </div>
	            </div>
            
	  		</div>
	  		
  		</div>
	</div>
	<!-- End row  -->
	<div class="row">
		<!-- Second Row -->
		<div class="myaccount-payment-billing-section">
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 payment-billing-section">
				  <div class="payment-method-section">
		            <p class="">
{#		            	<div class="mz-l-stack-section mz-checkout-current-payment payment-summary">#}
{#        					<div class="payment-summary-card-number">#}
{#			                	<span class="defaultCardImg card-img-in-summary {% if not model.billingInfo.card.paymentOrCardType %} showDefaultCard{% endif %}{% if model.billingInfo.card.paymentOrCardType == "VISA" %} showVisaImg{% endif %}#}
{#								{% if model.billingInfo.card.paymentOrCardType == "MC" %} showMastercardImg{% endif %}{% if model.billingInfo.card.paymentOrCardType == "AMEX" %} showAmexImg{% endif %}{% if model.billingInfo.card.paymentOrCardType == "DISCOVER" %} showDiscoverImg{% endif %}"></span>#}
{#			                	<span class="card-number-part">{{ model.billingInfo.card.cardNumberPartOrMask|default("**** **** **** ****") }} </span>#}
{#	                		</div>#}
{#	                	</div>#}
		            </p>
	   	 				<p class="">
		                	<img src="{% make_url "cdn" "/resources/images/payment/aeroplan-small.png" %}" alt="Aeroplan Logo" class="">{{ labels.aeroplan }}<br>
		                	<small>{{ labels.aeroplanMilesHaveEarned|string_format(model.omsOrder.customData.loyalty_program_points1) }}</small>
	    				</p>
		          </div>
		          
		           <!-- Billing contact-->
				<div class="myaccount-billing-addess-section">
					<h4 class="view-order-subtitle">{{ labels.billing }}</h4>
{#		            <div class="mz-addresssummary">{% include "modules/common/address-summary" with model=model.billingInfo.billingContact %}</div>#}
					<div class="mz-addresssummary"><span>{{ model.omsOrder.shippingAddressInformation.firstName}}{{ model.omsOrder.shippingAddressInformation.lastName}}</span>
					<span>{{ model.omsOrder.shippingAddressInformation.addressLine1}}</span>
					<span>{{ model.omsOrder.shippingAddressInformation.city}}, {{ model.omsOrder.shippingAddressInformation.state}}, {{ model.omsOrder.shippingAddressInformation.postalCode}}, {{ model.omsOrder.shippingAddressInformation.countryCode}}</span> 
					<span>{{ model.omsOrder.shippingAddressInformation.phone}}</span>
					
					</div>
		        </div>
		    </div>
          <!-- Order Summary -->
          	<div class="col-lg-6 col-md-6 hidden-xs hidden-sm order-summary-section order-summary-desktop">
          		{% include "modules/common/order-summary" %}  
          	</div>
		</div>
	</div>
	<!-- End row  -->
	
	<!-- Order notes Section -->
	<div class="order-note-section {% if not model.shopperNotes.comments %} hidden {% endif %}">
		<div class="order-note-detail"> 
			<h4 class="view-order-subtitle">{{ labels.notesAboutOrder }}</h4>
			<p class="no-margin">{{ model.shopperNotes.comments }}</p>
		</div>
	</div>
	
	<!-- order Summary detail container -->
	<div class="items-in-order-container">
		<div class="order-item-container">
			{% include "modules/common/order-summary-product" %} 
		</div>
	</div>
	
	
</div>