{% if model.isEmpty  %}
<div class="cart-header">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<h1 class="mz-pagetitle cart-header-title">{{ labels.cart }}</h1>
			</div>
		</div>
		<div class="row empty-cart">
			<h1 class="mz-pagetitle cart-header-title">{{ labels.emptyCartMsg }}</h1>
			<div class="empty-cart-msg"><a href="{{siteContext.siteSubdirectory}}/" class="link">{{ labels.startShoppingMsg }}</a><span> {{ labels.andAddItems }}</span></div>
		</div>
	</div>
</div>
{% else %}
<div class="cart-header">
	<div class="container">
		<div class="row cart-page-text">
		<div class="col-xs-12 visible-xs">
		  <h1 class="mz-pagetitle cart-header-title">{{ labels.cart }}</h1>
		</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 cart-page-order-text">
				<h1 class="mz-pagetitle cart-header-title hidden-xs">{{ labels.cart }}</h1>
				<div class="cart-page-hidden-text">
				<p class="ship-to-store-txt">{{ labels.shipToStroreFree }}</p>
				<p class="shipping-info">
					{{ labels.itemsSentMsg }}
					<strong>{{ labels.threeToTenDays }}</strong>
					 {{ labels.yourOrder }}.
				</p>
				<ul class="cart-header-links">
					<li class="support-link-list">
						<a class="support-link" href="/help-and-support#mz-drop-zone-product-availability-content">{{ labels.shippingLabel }}</a>
					</li>
					<li class="support-link-list">
						<a class="support-link" href="/help-and-support">{{ labels.faqsLabel }}</a>
					</li>
					<li class="support-link-list">
						<a class="support-link" href="/help-and-support#return-and-exchange">{{ labels.returPolicyLabel }}</a>
					</li>
				</ul>
				</div>
			</div>
			<div class="col-lg-5 col-md-5 col-md-offset-1 col-sm-6 col-xs-12">
				<div class="cart-checkout">
					<div class="cart-total">
						<span  {% if not model.showTotal %}class='hidden'{% endif %}>
							{{ labels.cartTotal }} ({{model.cartCount}} {% if model.cartCount > 1 %}{{ labels.itemsLabel }}{% else %}{{ labels.itemLabel }}{% endif %}):
							<br>
							{% if model.isFrenchSite %}
								<span class="cart-total-amount">{{ model.total|currency|replace(",", " ")|replace(".", ",")|replace("$", "")}}* {{ labels.frCurrency }}</span>
							{% else %}
								<span class="cart-total-amount">{{ model.total|currency}}*</span>	
							{% endif %}
						</span>
					</div>
					<div class="cart-checkout-action {% if not model.showTotal %}is-loading{% endif %}">
						{% if not user.isAuthenticated or user.isAnonymous %}
						    <button id="guest-checkout" class="cart-checkout-btn"  {% if model.isEmpty %} disabled="disabled" {% endif %} data-mz-action="guestCheckoutLogin">{{ labels.proceedToCheckout }}</button>
						{%else%}
							<button id="cart-checkout" class="cart-checkout-btn"  {% if model.isEmpty %} disabled="disabled" {% endif %} data-mz-action="proceedToCheckout">{{ labels.proceedToCheckout }}</button>
						{% endif %}
						<a href="/" class="cart-continue-shopping-link">{{ labels.continueShopping }}</a>
					</div>
				</div>
				<p class="pickup-store">{{ labels.yourPickupStore }}:</p>
				<div class="prefer-store-block block-bottom-margin">
					{% include "modules/location/preferred-store" with model=model.preferredStore %}
				</div>
			</div>
		</div>
	</div>
</div> {% comment %} End cart-header {% endcomment %}
<div class="container cart-table-container">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-sm-12">
			{% include "modules/cart/cart-table-item" with item as item %}
		</div>
	</div>
</div>
<div class="container order-summary-container">
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="mz-carttable-checkout-couponcode" id="coupon-code-field">
				{% include "modules/common/coupon-code-field" %}
			</div>
			{% if model.aeroplanMiles %}
				<div class="hidden-xs aeroplan-section-cart">
					<div class="aeroplan-miles-info">
						<div class="aeroplan-miles-info-lead">{{ labels.youCouldEarnAeroplanMiles|string_format(model.aeroplanMiles)|safe }}</div>
						{{ labels.whenYouPurchaseItemTxt }}
					</div>
				</div>
			{% endif %}
		</div>
		<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 col-md-offset-1 order-summary-section">
			{% include "modules/common/checkout-order-summary" %}
			<div class="row cart-checkout-action">
				<div class="col-md-6 col-md-offset-6 {% if not model.showTotal %}is-loading{% endif %}">
				{% if not user.isAuthenticated or user.isAnonymous %}
				    <button id="guest-checkout" class="cart-checkout-btn"  {% if model.isEmpty %} disabled="disabled" {% endif %} data-mz-action="guestCheckoutLogin">{{ labels.proceedToCheckout }}</button>
				{%else%}
					<button id="cart-checkout" class="cart-checkout-btn"  {% if model.isEmpty %} disabled="disabled" {% endif %} data-mz-action="proceedToCheckout">{{ labels.proceedToCheckout }}</button>
				{% endif %}
				</div>
			</div>
			<p class="tax-info"><small>{{ labels.taxinfo }}</small></p> 
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 visible-xs">
			{% if model.aeroplanMiles %}
				<div class="aeroplan-section-cart">
					<div class="aeroplan-miles-info">
						<div class="aeroplan-miles-info-lead">{{ labels.youCouldEarnAeroplanMiles|string_format(model.aeroplanMiles)|safe }}</div>
						{{ labels.whenYouPurchaseItemTxt }}
					</div>
				</div>
			{% endif %}
		</div>
	</div>
</div>
{% endif %}